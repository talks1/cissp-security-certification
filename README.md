
# Certified Information Systems Security Professional

This certification is provided by [International Information Systems Security Certification Consortium ](https://www.isc2.org/Certifications/CISSP) which is a non-profit.

It is a vendor neutral organization.

It organizes breaks down security information into 8 domains:
- Security and Risk Management
- Asset Security
- Security Architecture and Engineering
- Communication and Network Security
- Identity and Access Management
- Security Assessment and Testing
- Security Operations
- Software Development Security

## References
[Guide](./reference/UltimateGuideCISSP-Web.pdf)

[Exam Outline](./references/CISSPExamOutline-2018-v718.pdf)
[Exam Outline](./references/CISSP-Exam-Outline-English-April-2021.pdf)

## Eligibility

To qualify for the CISSP, candidates must pass the exam and have at least five years of cumulative, paid work experience in two or more of the eight domains of the (ISC)² CISSP Common Body of Knowledge (CBK ® ).

## The exam
Exam is 3 hours with 150 questions and must get 700 out of 1000 points.

Register for the exam [here](https://protect-au.mimecast.com/s/b1haCANppVhNrYq4t2wyKY?domain=home.pearsonvue.com)

## The CISSP certification

This is a very broad certification.
The concepts of the exam will age well as it is dealing with concepts rather than vendor specific details.

## Common Body of Knowlege
The exam target 8 domains of expertise

## Ethics
The material incorporates canons of ethics.

##  Examinatinon Nature
The nature of the exam seems more about classifying and using terms in a particular way.
The difficulty is largely in understanding and applying terms in a specific way.
I suppose is is part of the way to 'protect the profession'

## material for review
```

ISC2 Flash cards
- [Domain 1: Security and Risk Management](https://www.isc2.org/training/self-study-resources/flashcards/cissp/security-and-risk-management)
-	[Domain 2: Asset Security](https://www.isc2.org/training/self-study-resources/flashcards/cissp/asset-security)
-	[Domain 3: Security Architecture and Engineering](https://www.isc2.org/training/self-study-resources/flashcards/cissp/security-architecture-and-engineering)
-	[Domain 4: Communication and Network Security](https://www.isc2.org/training/self-study-resources/flashcards/cissp/communication-and-network-security)
-	[Domain 5: Identity and Access Management](https://www.isc2.org/training/self-study-resources/flashcards/cissp/identity-and-access-management)
-	[Domain 6: Security Assessment and Testing](https://www.isc2.org/training/self-study-resources/flashcards/cissp/security-assessment-and-testing)
-	[Domain 7: Security Operations](https://www.isc2.org/training/self-study-resources/flashcards/cissp/security-operations)
-	[Domain 8: Software Development Security](https://www.isc2.org/training/self-study-resources/flashcards/cissp/software-development-security)

Cybrary.It : Kelly Handerhan
Course 
Kaplan Test

Sagar Bansal CISSP Master

Colin Weaver - IT Dojo - Questions of the day for CISSP
https://www.youtube.com/watch?v=jZSAZ1neFZk&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=1

ISC2 Practice Exam

Linked In Training https://www.linkedin.com/learning/paths/prepare-for-the-certified-information-systems-security-professional-cissp-exam?u=25288675

NIST https://www.nist.gov/itl/applied-cybersecurity/privacy-engineering/collaboration-space/focus-areas/risk-assessment/tools


TestBank: http://app.efficientlearning.com
```

## References

The Official (ISC)² CISSP CBK Reference, 5th Edition by John Warsinske, Mark Graff, Kevin Henry, Christopher Hoover, Ben Malisow, Sean Murphy, C. Paul Oakes, George Pajari, Jeff T. Parker, David Seidl, Mike Vasquez, Publisher: Sybex (2019)
(ISC)² Code of Ethics, (2020)
Information Security Management Handbook, Sixth Edition by Harold F. Tipton and Micki Krause. Publisher: CRC Press. (2007)
Building an Information Security Awareness Program: Defending Against Social Engineering and Technical Threats, First Edition by Bill Gardner and Valerie Thomas. Publisher: Syngress. (2014)
Information Security Handbook: Develop a Threat Model and Incident Response Strategy to Build a Strong Information Security Framework by Darren Death. Publisher: Packt Publishing. (2017)
Security Program and Policies: Principles and Practices, Second Edition by Sari Greene. Publisher: Pearson IT Certification. (2014)
Fundamentals of Information Systems Security, Third Edition by David Kim and Michael G. Solomon. Publisher:  Jones & Bartlett Learning. (2016) 
Threat Modeling: Designing for Security, First Edition by Adam Shostack. Publisher: Wiley (2014)            
NIST SP 800-88, Rev 1, Guidelines for Media Sanitization by Larry Feldman and Gregory A. Witte.  February 2015
NIST SP 800-122, Guide to Protecting the Confidentiality of Personally Identifiable Information (PII) by Erika McCallister, Tim Grance and Karen Scarfone. April 2010     
Information Security and IT Risk Management, First Edition by Manish Agrawal, Alex Campoe and Eric Pierce. Publisher: Wiley. (2014)    
Defensive Security Handbook, First Edition by Amanda Berlin and Lee Brotherston. Publisher: O'Reilly Media. (2017)      
Cloud Security and Privacy: An Enterprise Perspective on Risks and Compliance (Theory in Practice), First Edition by Tim Mather, Subra Kumaraswamy, and Shahed Latif. Publisher: O'Reilly Media, Inc. (2009)
NIST SP 800-37 Rev 2, Risk Management Framework for Information Systems and Organizations: A System Life Cycle Approach for Security and Privacy. Joint Task Force Transformation Initiative.  December 2018
Wireless and Mobile Device Security by Jim Doherty. Publisher: Jones & Bartlett Learning. (2015)           
Data Center Handbook, First Edition by Hwaiyu Geng. Publisher: John Wiley & Sons Inc. (2014)
NIST SP 800-124, Rev 1, Guidelines for Managing the Security of Mobile Devices in the Enterprise by Murugiah Souppaya and Karen Scarfone. June 2013    
The Complete Guide to Physical Security by Paul R. Baker and‎ Daniel J. Benny. Publisher: CRC Press. (2012)        
Applied Cryptography: Protocols, Algorithms and Source Code in C, 20th Anniversary Edition by Bruce Schneier. Publisher: John Wiley & Sons. (2015)           
NIST 800-53 Rev 4, Security and Privacy Controls for Federal Information Systems and Organizations. Joint Task Force Transformation Initiative. April 2013
Security in Computing, Fifth Edition by Charles Pfleeger, Shari Pfleeger, and Jonathan Margulies. Publisher: Pearson Education, Inc. (2015)
NIST SP 800-41, Rev 1, Guidelines on Firewalls and Firewall Policy by Karen Scarfone and Paul Hoffman. September 2009
NIST SP 800-153, Guidelines for Securing Wireless Local Area Networks (WLANs) by Murugiah Souppaya and Karen Scarfone. February 2012       
Computer and Information Security Handbook, Third Edition by John R. Vacca. Publisher: Kaufmann Publishers. (2017)
End-to-End Network Security: Defense-in-Depth by Omar Santos. Publisher: Cisco Press. (2007)  
Applied Network Security by Michael McLafferty, Warun Levesque and Arthur Salmon. Publisher: Packt Publishing. (2017)          
Firewall Fundamentals by Ido Dubrawsky and Wes Noonan. Publisher: Cisco Press. (2006)          
Network Defense and Countermeasures: Principles and Practices, Third Edition by Chuck Easttom. Publisher: Pearson IT Certification. (2018)      
Identity and Access Management: Business Performance Through Connected Intelligence, First Edition by Ertem Osmanoglu. Publisher: Syngress. (2013)
Access Control, Authentication, and Public Key Infrastructure, Second Edition by Erin Banks, Tricia Ballad, Bill Ballad and Mike Chapple. Publisher: Jones & Bartlett Learning. (2013)          
Federated Identity Primer, First Edition by Derrick Rountree. Publisher: Syngress. (2012)
Identity Management: A Primer, First Edition by Graham Williamson, David Yip, Ilan Sharoni and Kent Spaulding. Publisher: MC Press. (2009)
Cloud Computing by Kris Jamsa. Publisher: Jones & Bartlett Learning. (2012)      
ISO/IEC 27002:2013, Information technology — Security techniques — Code of practice for information security controls. ISO-IEC. (2013)
NIST SP 800-115, Technical Guide to Information Security Testing and Assessment by Karen Scarfone, Murugiah Souppaya, Amanda Cody and Angela Orebaugh. September 2008      
Security Controls Evaluation, Testing, and Assessment Handbook, First Edition by Leighton Johnson. Publisher: Syngress. (2015)
NIST SP 800-137, Information Security Continuous Monitoring (ISCM) for Federal Information Systems and Organizations by Kelley Dempsey, Nirali Shah, Chawla Arnold, Johnson Ronald Johnston, Alicia Clay Jones, Angela Orebaugh, Matthew Scholl, and Kevin Stine. September 2011   
NIST 800-53A Rev 4, Assessing Security and Privacy Controls in Federal Information Systems and Organizations: Building Effective Assessment Plans. Joint Task Force Transformation Initiative. December 2014
The Basics of IT Audit: Purposes, Processes, and Practical Information by Stephen D. Gantz. Publisher: Syngress. (2013)   
Business Continuity and Disaster Recovery Planning for IT Professionals, Second Edition by Susan Snedaker. Publisher: Syngress. (2013)
NIST SP 800-34 Rev 1, Contingency Planning Guide for Federal Information Systems by Marianne Swanson, Pauline Bowen, Amy Wohl Phillips, Dean Gallup, and David Lynes.  November 2010         
Disaster Recovery, Crisis Response, and Business Continuity: A Management Desk Reference, First Edition by Jamie Watters and Janet Watters. Publisher: Apress. (2013)
Disaster Recovery Planning: Preparing for the Unthinkable, Third Edition by Jon William Toigo. Publisher: Prentice Hall. (2002)    
The Disaster Recovery Handbook: A Step-by-Step Plan to Ensure Business Continuity and Protect Vital Operations, Facilities, and Assets, Third Edition by Michael Wallace and Lawrence Webber. Publisher: AMACOM. (2017)
NIST SP 800-61, Rev 2, Computer Security Incident Handling Guide by Paul Cichonski, Tom Millar, Tim Grance and Karen Scarfone. August 2012
Secure and Resilient Software Development, First Edition by Mark Merkow and Lakshmikanth Raghavan. Publisher: Auerbach Publications. (2010)
Building Secure Software: How to Avoid Security Problems the Right Way, First Edition by John Viega and Gary R. McGraw. Publisher: Addison-Wesley Professional. (2006)
Application Security in the ISO 27001 Environment by Anbalahan Siddharth, Pakala Sangit, Shetty Sachin, Ummer Firosh, Mangla Anoop and Vasudevan Vinod. Publisher: IT Governance Publishing. (2008)  
Software Security: Building Security In, First Edition by Gary McGraw. Publisher: Addison-Wesley Professional. (2006)
Secure Coding: Principles and Practices by Mark G. Graff and Kenneth R. van Wyk. Publisher: O'Reilly Media, Inc. (2003) 
Information Security: Principles and Practices, Second Edition by Mark Merkow and Jim Breithaupt. Publisher: Pearson IT Certification. (2014)