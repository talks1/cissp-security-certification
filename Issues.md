
# Dont allow user to be able to preform an action that they dont require to do their job

## Security Best Practice 

Contractor should not have access to do something on a system that they dont require?

## Issue
This sounds good but how do we know what they require.

If we do some activity with great scale then this is reasonable to determine the process and determine how to do that activity and lock it down.
But if there is not enough scale there is not enough budget to figure out what is required we risk preventing someone from acheiving some outcome required.


## Conventional Wisdom

Because the risk is great is they can do more than is required we lock them out as the default. Its less risky to shut things down and enable unknown activities.

## Alternate Wisdom

Give users sandbox (ideally managed by them) where their actions are no risk.

Dont provide an environment to do some work to someone else. Let them provide their environment.

You provide communication channel to give them the data and receive data.


# Is there value in a policy which is abstract and not specific...i.e. a corporate policy which isnt clear.

## Security Best Practice 

Security standards are the specifics of a security strategy and link to a security policy.  The policy shouldnt change but the standards will change.

## Conventional Wisdom

Yes a corporate policy is required

## Alternate Wisdom

A policy which is not specific adds confusion and cost overhead as it requires consensus to interpret.

A corporate policy should be known to be appropriate by assessment.  Survey employees regarding application of the policy to determine if it is specific enough.

# If there is no agreement around practices or processes then there can be no due diligence

There is a risk that the team is expected to follow some process but if they are not involved in this decision they cant be expected to do so.

## Risk avoidance

## Principal of least privilege
- Its expensive
- Its slow
- It requires forsight greater than may be easily available
- It requires excessive role planning
- It causes brittle support services because support depends on information not easily accessible

When principle of least privilege is proposed that risk analysis doesnt stack up and the risk should be accepted rather than prevented


## Using an automated risk management tool

Does our company have one?

What do this offer


## CISSP is an organization first standard

The standard promotes activities to that support upper management but there is no economic justification for how the activities which support upper management provide for competitiveness and profitability.

Roles defined 5 levels between upper management and those doing the work.


## CISSP doesnt address the activity of forming or changing a solution to reduce risk

Is optioneering from a security perspective a control?

Yes, we can build a fort around some asset but maybe it makes sense to distribute the asset so they are less of a target.  In google land servers are not assets because they can be replaced frequently.

# Identify Risks

Identifying risks is probably a creative , collaborative , ongoing process.  
But we dont have good tools for this.

Is there a tool to support an activitity
- gather information from a group
- provides feedback to the group
- randomized validated surveys of this information

# Can security controls themselves be a Denial of Service

Everying control which prevents access is a potential denial of service

# Risk requires an understanding of the business and goals

Do we all understand the business to be in a good position to identify risks?

# Data classification considers impacts to organization if there is loss of confidentiality, integrity or available

But the controls themselves impose a threat on the ability to make use of the information to analyze and make predictions that impacts the organization. However this impacts doesnt seem to be reflected in the material.

# Data Classification programs suggested is a relatively static activity of identity information inventory, classify,  label and handle

But digital information is more fluid in this because
- information can easily be joined and aggregated ...potentially making the information more sensitivity than its constituent parts (violation of Tranquility Principle)
    - example of operational status of one vehicle in a battle group may not be all that important but it you know the status of most units you can infer a view of the readiness of that unit
- Dont we need 
    - an algebra for how data combination impacts classification
        - maybe logic is a fuzzy logic or neural net



# CISSP takes a view that security considerations are at business macro level but most decisions are at a smaller scale
 For example the material talks about Walmart web services being offline will mean X millions of dollars being lost and their there is need for large scale controls.

 However its likely that walmart web presence is in terms of a number of systems which probably and separate failure rates and are independent (as small teams need to have created them). Consequently the scale for large scale controls may not really be there.

 Does our organization of the scale to justify some of the controls we have in place?
 Alternatively should we retain diversity to avoid large scale points of failure?

 # CISSP ethics include acting honorable and responsible but what if winning work puts us in a difficult position with regards to this.

 We can win work without knowing expected scope and be required to estimate the work.
 How much responsibility do we take for an estimates which can provide a misleading expectation to the uniformed.

 # Security testing is like looking for a needle in a haystack and as such as an expensive operation

 If we really want to facilitate security which probably have to align to some paradigm which there is sufficient scale to be able to perform the testing cost effectively.   It would seem appropriate to have a layered architecture where the outermost public facing layer is both constant, multi-tenanted so that there is scale to validate security of that layer.


 # CISSP is recommending processes without recoginizing the difficulties in executing processes
 For example the Risk Management Framework - Prepare, Categorize, Select,...
 These sound good but they are both ambigious steps and political charged activities.

 # CISSP takes the point of view that security should occur throughout the Asset Life Cycle

 This is an expensive point of view.
 IN a lot of ways security is an optimization that may be excessive.  Take the idea that data should not be exposed in messages if not required.

 # Principles of Access Controls include separation of duties
This principle encourages peer review which has benefits yes but also has drawbacks in that these is a barrier to change/action.
We have to be very careful when we apply separation of duties that the 

# Motive , Opportunity and Means
There should be a session on Motives hear because its counterintuitive to some to rationalize why people want to attack systems.

# Availability Control

Should there be an availability control that separate non-secret information out from an application where is doesnt need to be protected and therefore is more available.
Every time we apply a security control we restrict availability.

# Information Security Policy

What is the appropriate context for a security policy.  e.g. should Defense projects be separate out from non-defense project


# CIS Benchmarks

Benchmarks for security practices

As these represent a cost there needs to be a corresponding benefit projection in order to demonstrate due care in even applying these benchmarks?

If we can get the cost of these benchmarks down to low number then this probably is not required but until that point this would need to be considered.


# Principle of Least Privilege

This principle states 'Grant user permissions for only what they need to do to do their jobs' .  

This sounds good but implies a heavy constraint in that 'users need to have well defined roles'
Defining what a role should or shouldnt do is rather limiting as business need to adapt and change.

# Isnt the principle of least privilege in conflict with the need for availability and redundancy as a way to achieve this

# How does the principle of least privelege deal with the uncertainty of tasks required to perform a certain activity


# When is the right time to secure a system
Conventional wisdom says the security must be considered from the start.
However security is likely to get in the way when developing a system.
Does it make sense to develop a system to the point of useability and then apply security principles to move it to maturity level 2 or 3.


