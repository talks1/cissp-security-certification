let Processor = require('real-value-unified-md2json')
let path = require('path')
let fs = require('fs')
let glob = require('glob-fs')({gitignore: true})

async function main(){
    //console.log(process.argv[2])

    var files = glob.readdirSync('./docs/**/Question*.md');
    let ps = files.map(async file=>{
        
        const {fileProcessor} = Processor()
        let contentName = path.basename(file,'.md')
        console.log(contentName)
        let data = await fileProcessor(file)
        let exportStatement = `export default ${JSON.stringify(data,null,' ')}`
        fs.writeFileSync(`./src/generated/${contentName}.mjs`,exportStatement)
    })
    await Promise.all(ps)
    // 
    //     
}
main()