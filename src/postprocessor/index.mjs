import  Domain1 from '../generated/Questions.Domain1.mjs'
import  Domain2 from '../generated/Questions.Domain2.mjs'
import  Domain3 from '../generated/Questions.Domain3.mjs'
import  Domain4 from '../generated/Questions.Domain4.mjs'
import  Domain5 from '../generated/Questions.Domain5.mjs'
import  Domain6 from '../generated/Questions.Domain6.mjs'
import  Domain7 from '../generated/Questions.Domain7.mjs'
import  Domain8 from '../generated/Questions.Domain8.mjs'
import  Ethics from '../generated/Questions.Ethics.mjs'
import  TestBank from '../generated/Questions.TestBank.mjs'
import assert from 'assert'

import path from 'path'
import fs from 'fs'

const domains = {
    'domain1': Domain1,
    'domain2': Domain2,
    'domain3': Domain3,
    'domain4': Domain4,
    'domain5': Domain5,
    'domain6': Domain6,
    'domain7': Domain7, 
    'domain8': Domain8,
    'ethics': Ethics,
    'testbank': TestBank
}

const processor = ()=>{

    Object.keys(domains).forEach((domainKey)=> {
        const domain = domains[domainKey]

        let compiledQuestions = domain.map(question=>{
            if(!question.items) return null            
            let id = question.items.find(i=>i.name==='Id'|| i.name==='ID')
    
            assert(id, `A question doesnt have an ID ${JSON.stringify(question)}`)

            let answer = question.items.find(i=>i.name==='Answer')
            let correctAnswers = []
            if(Array.isArray(answer.value)){
                correctAnswers = answer.value.map(v=>v.value)
            } else  if(typeof answer.value==='string'){
                correctAnswers = answer.value.split(',')
            } else {
                console.log(answer)
            }
    
            let explanation = question.items.find(i=>i.name==='Explanation').value
    

            assert(id.value.length>6, `A question id value appears to be missing a UUID ${id.value}`)
    

            return {
                type: 'QUESTION',
                id: id.value,
                question: question.name,
                answer: correctAnswers.join(','),
                explanation
            }
        })
    
        let compiledAnswer = domain.flatMap(question=>{        
            if(!question.items) return null
            const answerChoices = []
            let id = question.items.find(i=>i.name==='Id'|| i.name==='ID')
    
            let choices = ['A','B','C','D','E','F','G','H','I','J','K','L']
            choices.map((choice)=>{
                let c = question.items.find(i=>i.name===choice)
                if(c) answerChoices.push({
                    type: 'ANSWER',
                    id: `${id.value}-${choice}`,
                    QuestionId: id.value,
                    choice,
                    answer: c.value})
            })
            return answerChoices
        }) 
    
        let compiled = [
                ...compiledQuestions,
                ...compiledAnswer
            ]
        compiled = compiled.filter(n=>n)
    
        let exportStatement = `export const Log = ${JSON.stringify(compiled,null,' ')}`
        let location = path.join(process.cwd(),`../../2020-12-31-PeformanceTracking/application/src/persistentlog/cissp/${domainKey}.ts`)
        fs.writeFileSync(location,exportStatement)
    })
}

processor()