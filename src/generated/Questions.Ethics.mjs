export default [
 {
  "name": "Which of the of the following are not one of the ICS2 code of ethics",
  "attrs": [],
  "items": [
   {
    "name": "Id",
    "attrs": [],
    "value": "ETH.14c04a21-b094-4309-af29-248671ec2756"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Protect Society"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Use methods, tools, responsible"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Act honorable, honestly,justly, responsible and legally"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Provide diligent and competent services to principals"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Avoid competing professional and personal interests"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Advance and protect the profession"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B,E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following statements best aligns with ISC2 code of ethics (Choose Four)",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "EHT.45f534d7-1276-456a-ac81-fe0f4033bb6b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Work to protect society, public trust and infraststructure"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Avoid using the internet as a test network, consider the potential outcomes of your actions"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "be honest, act responsibly and within the confines of the law"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Deliver your product ontime , as defined and within the allowed budget"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Be decisive, confident and articulate when dealing with principles"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Safeguard your system using a complement of administrative technical and physical controls"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "be competent in what you do and be diligent in the maintenance of that competenence"
   },
   {
    "name": "H",
    "attrs": [],
    "value": "Seek, through your actions, to improve the profession"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A,C,G,H"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=vp7DA6TqDNw"
   }
  ],
  "value": ""
 }
]