export default [
 {
  "name": "What tool can an organization use to reduce or avoid risk when working with external vendors, consultants, or contractors?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "034e318f-259a-4342-a5cc-f00081ae227d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "IPSEC"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "VPN"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "SLA"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "BCP"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A service-level agreement (SLA) is a contract with an external entity and is an important part of risk reduction and risk avoidance. By clearly defining the expectations and penalties for external parties, everyone involved knows what is expected of them and what the consequences are in the event of a failure to meet those expectations."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What form of testing stresses demands on the disaster recovery team to derive an appropriate response from a disaster scenario?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "034e318f-259a-4342-a5cc-f00081ae227d024c2bbe-104a-40bc-8200-588107d4d294"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Full Interruption Test"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Parallel Test"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Simulation Test"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Structure Walk Through"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Simulation tests are similar to the structured walk-throughs. In simulation tests, disaster recovery team members are presented with a scenario and asked to develop an appropriate response."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "An organization is hosting a website that accesses resources from multiple organizations. They want users to be able to access all of the resources but only log on once. What should they use?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.015"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Federal database for CIA"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Kerberos"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Diameter"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "SSO"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Single sign-on (SSO) allows a user to log on once and access multiple resources. This is not called a federal database but might be related to federation if multiple authentication systems must be integrated to establish the SSO. Kerberos is an effective SSO system within a single organization but not between organizations. Diameter is a newer alternative to RADIUS used for centralized authentication often for remote connections; thus, it is not appropriate for this scenario."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What form of proximity device is able to generate its own electricity from a magnetic field?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.107"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Passive Device"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Self Powered Device"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Field Powered Device"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Transponder"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A field-powered device has electronics that activate when the device enters the electromagnetic field that the reader generates. Such devices actually generate electricity from an EM field to power themselves."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What aspect of security governance is based on the idea that senior management is responsible for the success or failure of a security endeavor?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.038"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Sarbanes-Oxley Act 2002"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "COBIT"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Accreditation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Top-down approach"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "he top-down approach is the aspect of security governance that is based on the idea that senior management is responsible for the success or failure of a security endeavor."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "In what scenario would you perform bulk transfers of backup data to a secure off-site location?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.007"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Incremental backup"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Differential backup"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Full backup"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Electronic vaulting"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "Electronic vaulting describes the transfer of backup data to a remote backup site in a bulk-transfer fashion."
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not a benefit of VLANs?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.046"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Traffic isolation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Data/traffic encryption"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Traffic management"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Reduced vulnerability to sniffers"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "VLANs do not impose encryption on data or traffic. Encrypted traffic can occur within a VLAN, but encryption is not imposed by the VLAN."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following types of IDS is effective only against known attack methods?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.063"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Host based"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Network based"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Knowledge based"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Behavior based"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A knowledge-based IDS is effective only against known attack methods, which is its primary drawback."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Administrators often consider the relationship between assets, risk, vulnerability, and threat under what model?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.038"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "CIA Triad"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "CIA Triplex"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Operations Security Tetrahedron"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Operations security triple"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The operations security triple is a conceptual security model that encompasses three concepts (assets, risk, and vulnerability) and their interdependent relationship within a structured, formalized organization."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What form of intellectual property is used to protect words, slogans, and logos?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.084"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Patent"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Copyright"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Trademark"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Trade secret"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Trademarks are used to protect the words, slogans, and logos that represent a company and its products or services."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What form of cloud service provides the customer with the ability to run their own custom code but does not require that they manage the execution environment or operating system?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.150"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "SaaS"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "PaaS"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "IaaS"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "SECaaS"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Platform as a Service is the concept of providing a computing platform and software solution stack to a virtual or cloud-based service. Essentially, it involves paying for a service that provides all the aspects of a platform (that is, OS and complete solution package). A PaaS solution grants the customer the ability to run custom code of their choosing without needing to manage the environment."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following statements is true?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.055"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "An open system does not allow anyone to view its programming code."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "A closed system does not define whether or not its programming code can be viewed."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "An open source program can only be distributed for free."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "A closed source program cannot be reverse engineered or decompiled."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A closed system is designed to work well with a narrow range of other systems, generally all from the same manufacturer. The standards for closed systems are often proprietary and not normally disclosed. However, a closed system (as a concept) does not define whether or not its programming code can be viewed. An open system (as a concept) also does not define whether or not its programming code can be viewed. An open source program can be distributed for free or for a fee. A closed source program can be reverse engineered or decompiled."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What technique may be used if an individual wants to prove knowledge of a fact to another individual without revealing the fact itself?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.084"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Split-knowledge proof"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Work function"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Digital signature"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Zero-knowledge proof"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Zero-knowledge proofs confirm that an individual possesses certain factual knowledge without revealing the knowledge."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is not a goal of cryptographic systems?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "b475934.CISSPSG8E.be6.104"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Nonrepudiation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Confidentiality"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Availability"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Integrity"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The four goals of cryptographic systems are confidentiality, integrity, authentication, and nonrepudiation."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not considered a type of auditing activity?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.026"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Recording of event data"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Data reduction"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Log analysis"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Deployment of countermeasures"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Deployment of countermeasures is not considered a type of auditing activity but is instead an attempt to prevent security problems. Auditing includes recording event data in logs, using data reduction methods to extract meaningful data, and analyzing logs."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is an example of a code?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.096"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Rivest"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "AES"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "10 system"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Skipjack"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The 10 system is a code used in radio communications for brevity and clarity."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "You are concerned about the risk that fire poses to your $25 million data center facility. Based on expert opinion, you determine that there is a 1 percent chance that fire will occur each year. Experts advise you that a fire would destroy half of your building. What is the single loss expectancy of your facility to fire?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "b475934.CISSPSG8E.be5.059"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "$25,000,000"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "$12,500,000"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "$250,000"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "$125,000"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The single loss expectancy is the amount of damage that would occur in one fire. The scenario states that a fire would destroy half the building, resulting in $12,500,000 of damage"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Your manager is concerned that the business impact assessment recently completed by the BCP team doesn’t adequately take into account the loss of goodwill among customers that might result from a particular type of disaster. Where should items like this be addressed?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.061"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Continuity strategy"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Quantitative analysis"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Likelihood assessment"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Qualitative analysis"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The qualitative analysis portion of the business impact assessment (BIA) allows you to introduce intangible concerns, such as loss of customer goodwill, into the BIA planning process."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What strategy basically consists of multiple layers of antivirus, malware, and spyware protection distributed throughout a given network environment?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.039"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "CIA Triad"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Concentric circle"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Operations security triple"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Separation of duties"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A concentric circle security model comprises several mutually independent security applications, processes, or services that operate toward a single common goal."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Most of the higher-order security models, such as Bell-LaPadula and Biba, are based on which of the following?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.114"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Take-Grant model"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "State machine model"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Brewer and Nash model"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Clark Wilson model"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which is not a part of an electronic access control lock?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.047"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "An electromagnet"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "A credential reader"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "A door sensor"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "A biometric scanner"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "An electronic access control (EAC) lock comprises three elements: an electromagnet to keep the door closed, a credential reader to authenticate subjects and to disable the electromagnet, and a door-closed sensor to reenable the electromagnet."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not a typical security concern with VoIP?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.030"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "VLAN hopping"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Caller ID falsification"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Vishing"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "SPIT"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "VLAN hopping is a switch security issue, not a VoIP security issue. Caller ID falsification, vishing, and SPIT (spam over Internet telephony) are VoIP security concerns."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is a security perimeter? (Choose all that apply.)",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.c08.10"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "The boundary of the physically secure area surrounding your system"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "The imaginary boundary that separates the TCB from the rest of the system"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "The network where your firewall resides"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Any connections to your computer system"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A,B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "When an intruder gains unauthorized access to a facility by asking an employee to hold open a door because their arms are full of packages, this is known as what type of attack?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.029"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Tailgating"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Masquerading"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Impersonation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Piggybacking"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Piggybacking is when an authorized person knowingly lets you through.\nTailgating is when an authorized persons unknowingly lets you through."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is a procedure designed to test and perhaps bypass a system’s security controls?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.at.27"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Logging usage data"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "War dialing"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Penetration testing"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Deploying secured desktop workstations"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Penetration testing is the attempt to bypass security controls to test overall system security."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following storage devices is most likely to require encryption technology in order to maintain data security in a networked environment?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.c09.11"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Hard disk"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Backup tape"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Removable drives"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "RAM"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Removable drives are easily taken out of their authorized physical location, and it is often not possible to apply operating system access controls to them. Therefore, encryption is often the only security measure short of physical security that can be afforded to them. Backup tapes are most often well controlled through physical security measures. Hard disks and RAM chips are often secured through operating system access controls."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not a form of spoofed traffic filtering?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.049"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Block inbound packets whose source address is an internal address"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Block outbound packets whose source address is an external address"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Block outbound packets whose source address is an unassigned internal address"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Block inbound packets whose source address is on a block/black list"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Using a block list or black list is a valid form of security filtering; it is just not a form of spoofing filtering."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following alternate processing arrangements is rarely implemented?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.077"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Hot site"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Warm site"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Cold site"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "MAA site"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Mutual assistance agreements are rarely implemented because they are difficult to enforce in the event of a disaster requiring site activation."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is a security risk of an embedded system that is not commonly found in a standard PC?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.c09.05"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Software flaws"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Access to the internet"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Control of a mechanism in the physical world"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Power loss"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Because an embedded system is in control of a mechanism in the physical world, a security breach could cause harm to people and property. This typically is not true of a standard PC. Power loss, internet access, and software flaws are security risks of both embedded systems and standard PCs."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following business impact assessment (BIA) tasks should be performed last?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.063"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Risk identification"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Resource prioritization"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Impact assessment"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Likelihood assessment"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Resource prioritization is the final step of the business impact assessment (BIA) process."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following assumptions is not necessary before you trust the public key contained on a digital certificate?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.124"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "The digital certificate of the CA is authentic."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "You trust the sender of the certificate."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "The certificate is not listed on a CRL."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "The certificate actually contains the public key."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "You do not need to trust the sender of a digital certificate as long as the certificate meets the other requirements listed and you trust the certification authority."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the first priority in any business continuity plan?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.040"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Data restoration"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Personal safety"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Containing damage"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Activating an alternate site"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The primary concern in any business continuity or disaster recovery effort is ensuring personal safety for everyone involved."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "On what port do DHCP clients request a configuration?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.005"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "25"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "110"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "68"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "443"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Dynamic Host Configuration Protocol (DHCP) uses port 68 for client request broadcast and port 67 for server point-to-point response."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not one of the benefits of maintaining an object’s integrity?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.117"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Unauthorized subjects should be prevented from making modifications."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Authorized subjects should be prevented from making unauthorized modifications."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Objects should be available for access at all times without interruption to authorized individuals."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Objects should be internally and externally consistent so that their data is a correct and true reflection of the real world and any relationship with any child, peer, or parent object is valid, consistent, and verifiable."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Option C is not an example of protecting integrity; it is the principle of availability."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is not a major asset category normally covered by the BCP (business continuity plan)?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.135"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "People"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Documentation"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Infrastructure"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Buildings/facilities"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The BCP normally covers three major asset categories: people, infrastructure, and buildings/facilities."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which software development life cycle model allows for multiple iterations of the development process, resulting in multiple prototypes, each produced according to a complete design and testing process?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.010"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Software Capability Maturity model"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Waterfall model"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Development cycle"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Spiral model"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The spiral model allows developers to repeat iterations of another life cycle model (such as the waterfall model) to produce a number of fully tested prototypes."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the composition theories refers to the management of information flow from one system to another?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.134"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Feedback"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Hookup"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Cascading"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Waterfall"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Cascading is when input for one system comes from the output of another system. Feedback is when one system provides input to another system, which reciprocates by reversing those roles (so that system A first provides input for system B and then system B provides input to system A). Hookup is when one system sends input to another system but also sends input to external entities. Waterfall is a form of project management not related to information flow."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is a data object passed from the Transport layer to the Network layer called when it reaches the Network layer?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.017"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Protocol data unit"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Segment"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Datagram"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Frame"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A data object is called a datagram or a packet in the Network layer. It is called a PDU in layers 5 through 7. It is called a segment in the Transport layer and a frame in the Data Link layer."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not true regarding firewalls?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.c11.13"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "They are able to log traffic information."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "They are able to block viruses."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "They are able to issue alarms based on suspected attacks."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "They are unable to prevent internal attacks."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Most firewalls offer extensive logging, auditing, and monitoring capabilities as well as alarms and even basic IDS functions. Firewalls are unable to block viruses or malicious code transmitted through otherwise authorized communication channels, prevent unauthorized but accidental or intended disclosure of information by users, prevent attacks by malicious users already behind the firewall, or protect data after it passed out of or into the private network."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following cryptographic attacks can be used when you have access to an encrypted message but no other information?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.032"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Known plain-text attack"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Frequency analysis attack"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Chosen cipher-text attack"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Meet-in-the-middle attack"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Frequency analysis may be used on encrypted messages. The other techniques listed require additional information, such as the plaintext or the ability to choose the ciphertext."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Federation is a means to accomplish ???.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.147"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Accountability logging"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "ACL verification"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Single sign-on"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Trusted OS hardening"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Federation or federated identity is a means of linking a subject’s accounts from several sites, services, or entities in a single account. Thus, it is a means to accomplish single sign-on. Accountability logging is used to relate digital activities to humans. ACL verification is a means to verify that correct permissions are assigned to subjects. Trusted OS hardening is the removal of unneeded components and securing the remaining elements."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is an access object?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.c08.05"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "A resource a user or process wants to access"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "A user or process that wants to access a resource"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "A list of valid access rules"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "The sequence of valid access types"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "An object is a resource a user or process wants to access. Option A describes an access object."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of trusted recovery process always requires the intervention of an administrator?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.130"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Automated"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Manual"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Function"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Controlled"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A manual type of trusted recovery process (described in the Common Criteria) always requires the intervention of an administrator. Automated recovery can perform some type of trusted recovery for at least one type of failure. Function recovery provides automated recovery for specific functions and will roll back changes to a secure state if recovery fails. Controlled is not a specific type of trusted recovery process."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is a threat modeling methodology that focuses on a risk-based approach instead of depending upon an aggregated threat model and that provides a method of performing a security audit in a reliable and repeatable procedure?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.003"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "VAST"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Trike"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "STRIDE"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "DREAD"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Trike is a threat modeling methodology that focuses on a risk-based approach instead of depending upon the aggregated threat model used in STRIDE and DREAD; it provides a method of performing a security audit in a reliable and repeatable procedure. Visual, Agile, and Simple Threat (VAST) is a threat modeling concept based on Agile project management and programming principles."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following does not need to be true in order to maintain the most efficient and secure server room?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.c10.05"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "It must be human compatible."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "It must include the use of nonwater fire suppressants."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "The humidity must be kept between 40 and 60 percent."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "The temperature must be kept between 60 and 75 degrees Fahrenheit."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A computer room does not need to be human compatible to be efficient and secure. Having a human-incompatible server room provides a greater level of protection against attacks."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not a valid access control model?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.c14.14"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Discretionary Access Control model"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Nondiscretionary Access Control model"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Mandatory Access Control model"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Compliance-Based Access Control model"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Compliance-Based Access Control model is not a valid type of access control model. The other answers list valid access control models."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Among the following scenarios, which course of action is insufficient to increase your posture against brute-force and dictionary-driven attacks?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.051"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Restrictive control over physical access"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Policy-driven strong password enforcement"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Two-factor authentication deployment"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Requiring authentication timeouts"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Requiring authentication timeouts bears no direct result on password attack protection. Strong password enforcement, restricted physical access, and two-factor authentication help improve security posture against automated attacks."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "In a relational database, what type of key is used to uniquely identify a record in a table and can have multiple instances per table?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.101"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Candidate key"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Primary key"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Unique key"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Foreign key"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A candidate key is a subset of attributes that can be used to uniquely identify any record in a table. No two records in the same table will ever contain the same values for all attributes composing a candidate key. Each table may have one or more candidate keys, which are chosen from column headings."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What sort of criminal act is committed when an attacker is able to intercept more than just network traffic?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.056"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Sniffing"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Eavesdropping"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Snooping"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "War dialing"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Eavesdropping is the ability to intercept network and telephony communications along with physical and electronic documents and even personal conversations."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What database security feature uses a locking mechanism to prevent simultaneous edits of cells?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.049"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Semantic integrity mechanism"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Concurrency"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Polyinstantiation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Database partitioning"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Concurrency uses a “lock” feature to allow an authorized user to make changes and then “unlock” the data elements only after the changes are complete. This is done so another user is unable able to access the database to view and/or make changes to the same elements at the same time."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is system accreditation?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.c08.02"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Formal acceptance of a stated system configuration"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "A functional evaluation of the manufacturer’s goals for each hardware and software component to meet integration standards"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Acceptance of test results that prove the computer system enforces the security policy"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "The process to specify secure communication between machines"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "When an attacker selects a target, they must perform reconnaissance to learn as much as possible about the systems and their configuration before launching attacks. What is the gathering of information from any publicly available resource, such as websites, social networks, discussion forums, file services, and public databases?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.148"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Banner grabbing"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Port scanning"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Open source intelligence"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Enumeration"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Open source intelligence is the gathering of information from any publicly available resource. This includes websites, social networks, discussion forums, file services, public databases, and other online sources. This also includes non-Internet sources, such as libraries and periodicals."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What frequency does an 802.11n-compliant device employ that allows it to maintain support for legacy 802.11g devices?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.009"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "5 GHz"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "900 MHz"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "7 GHz"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "2.4 GHz"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "802.11n can use the 2.4 GHz and 5 GHz frequencies. The 2.4 GHz frequency is also used by 802.11g and 802.11b. The 5 GHz frequency is used by 802.11a, 802.11n, and 802.11ac"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "In a discussion of high-speed telco links or network carrier services, what does fault tolerance mean?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.012"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Error checking"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Redundancy"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Flow control"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Bandwidth on demand"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "In a discussion of high-speed telco links or network carrier services, fault tolerance means to have redundant connections."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following should be generally worker/user accessible?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.062"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Mission-critical data center"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Main workspaces"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Server vault"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Wiring closet"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Spoofing is primarily used to perform what activity?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.022"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Send large amounts of data to a victim"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Cause a buffer overflow"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Hide the identity of an attacker through misdirection"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Steal user account names and passwords"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Spoofing grants the attacker the ability to hide their identity through misdirection and is used in many different types of attacks. Spoofing doesn’t send large amounts of data to a victim. It can be used as part of a buffer overflow attack to hide the identity of the attacker but doesn’t cause buffer overflows directly. If user account names and passwords are stolen, the attacker can use them to impersonate the user."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which access control system discourages the violation of security processes, policies, and procedures?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.052"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Detective access control"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Preventive access control"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Corrective access control"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Deterrent access control"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Abnormal or unauthorized activities detectable to an IDS include which of the following? (Choose all that apply.)",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.137"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "External connection attempts"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Execution of malicious code"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Access to controlled objects"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "None of the above"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A,B,C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "IDSs watch for violations of confidentiality, integrity, and availability. Attacks recognized by IDSs can come from external connections (such as the Internet or partner networks), viruses, malicious code, trusted internal subjects attempting to perform unauthorized activities, and unauthorized access attempts from trusted locations."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following patterns of activity is indicative of a scanning attack?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.024"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Large number of blocked connection attempts on port 22"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Large number of successful connection attempts on port 80"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Large number of successful connection attempts on port 443"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Large number of disk failure events"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A high number of blocked connection attempts may indicate that an attacker is scanning systems that do not offer a particular service on a particular port. Port 22 is the TCP port usually used by the Secure Shell (SSH) protocol, a common target of scanning attacks."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of attack occurs when malicious users position themselves between a client and server and then interrupt the session and take it over?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "b475934.CISSPSG8E.be4.052"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Man-in-the-middle"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Spoofing"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Hijack"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Cracking"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "In a hijack attack, which is an offshoot of a man-in-the-middle attack, a malicious user is positioned between a client and server and then interrupts the session and takes it over. A man-in-the middle attack doesn’t interrupt the session and take it over. Spoofing hides the identity of the attacker. Cracking commonly refers to discovering a password but can also mean any type of attack."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "You are implementing AES encryption for files that your organization plans to store in a cloud storage service and wish to have the strongest encryption possible. What key length should you choose?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.120"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "192 bits"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "256 bits"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "512 bits"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "1024 bits"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The strongest keys supported by the Advanced Encryption Standard are 256 bits. The valid AES key lengths are 128, 192, and 256 bits."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the limit on the number of candidate keys that can exist in a single database table?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.081"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Zero"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "One"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Two"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "No limit"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "There is no limit to the number of candidate keys that a table can have. However, each table can have only one primary key."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What law protects the privacy rights of students?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.094"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "HIPAA"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "SOX"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "GLBA"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "FERPA"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Family Educational Rights and Privacy Act (FERPA) protects the rights of students and the parents of minor students."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What rule of evidence states that a written agreement is assumed to contain all terms of the agreement?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.123"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Real evidence"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Best evidence"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Parol evidence"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Chain of evidence"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The parol evidence rule states that when an agreement between parties is put into written form, the written document is assumed to contain all the terms of the agreement, and no verbal agreements may modify the written agreement."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Your database administrators recommend performing bulk transfer backups to a remote site on a daily basis. What type of strategy are they recommending?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.078"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Transaction logging"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Electronic vaulting"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Remote journaling"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Remote mirroring"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Electronic vaulting uses bulk transfers to copy database contents to a remote site on a periodic basis.\nRemote journaling is similar bit only involves transport incremental changes"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is access?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.001"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Functions of an object"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Information flow from objects to subjects"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Unrestricted admittance of subjects on a system"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Administration of ACLs"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Access is the transfer of information from an object to a subject. An object is a passive resource that does not have functions. Access is not unrestricted. Access control includes more than administration of access control lists (ACLs)."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "When it can be legally established that a human performed a specific action that was discovered via auditing, accountability has been established. What additional benefit is derived from this investigation and verification process?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.035"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Nonrepudiation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Privacy"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Abstraction"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Redundancy"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "When audit trails legally prove accountability, then you also reap the benefit of nonrepudiation."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What means of risk response transfers the burden of risk to another entity?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.123"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Mitigation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Assignment"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Tolerance"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Rejection"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Risk assignment or transferring risk is the placement of the cost of loss a risk represents onto another entity or organization. Purchasing insurance and outsourcing are common forms of assigning or transferring risk."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "The absence of which of the following can result in the perception that due care is not being maintained?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.103"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Periodic security audits"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Deployment of all available controls"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Performance reviews"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Audit reports for shareholders"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Failing to perform periodic security audits can result in the perception that due care is not being maintained. Such audits alert personnel that senior management is practicing due diligence in maintaining system security. An organization should not indiscriminately deploy all available controls but should choose the most effective ones based on risks. Performance reviews are useful managerial practices but not directly related to due care. Audit reports should not be shared with the public."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "A security management plan that discusses the needs of an organization to maintain security, the desire to improve control of authorized access, and the goal of implementing token-based security is what type of plan?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.076"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Functional"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Operational"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Strategic"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Tactical"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A strategic plan is a long-term plan that is fairly stable. It defines the organization’s goals, mission, and objectives. It is useful for about five years if it is maintained and updated annually. The strategic plan also serves as the planning horizon. Long-term goals and visions of the future are discussed in a strategic plan."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which type of connection created by a packet-switching networking system reuses the same basic parameters or virtual pathway each time it connects?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.137"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Bandwidth on-demand connection"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Switched virtual circuit"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Permanent virtual circuit"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "CSU/DSU connection"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A PVC reestablishes a link using the same basic parameters or virtual pathway each time it connects. SVCs use unique settings each time. Bandwidth on-demand links can be either PVCs or SVCs. CSU/DSU is not a packet-switching technology."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "APTs are most closely related to what type of attack category?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.132"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Military attacks"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Thrill attacks"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Grudge attacks"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Insider attacks"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Advanced persistent threats (APTs) are often associated with government and military actors."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "You are concerned about the risk that a hurricane poses to your corporate headquarters in south Florida. The building itself is valued at $15 million. After consulting with the National Weather Service, you determine that there is a 10 percent likelihood that a hurricane will strike over the course of a year. You hired a team of architects and engineers who determined that the average hurricane would destroy approximately 50 percent of the building. What is the annualized rate of occurrence (ARO)?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.037"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "5 percent"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "10 percent"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "50 percent"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "100 percent"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The ARO is, quite simply, the probability that a given disaster will strike over the course of a year. According to the experts quoted in the scenario, this chance is 10 percent."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following includes a record of system activity and can be used to detect security violations, software flaws, and performance problems?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.008"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Change logs"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Security logs"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "System logs"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Audit trail"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Audit trails provide a comprehensive record of system activity and can help detect a wide variety of security violations, software flaws, and performance problems. An audit trail includes a variety of logs, including change logs, security logs, and system logs, but any one of these individual logs can’t detect all the issues mentioned in the question."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the final stage in the life cycle of backup media, occurring after or as a means of sanitization?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.047"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Degaussing"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Destruction"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Declassification"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Defenestration"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Destruction is the final stage in the life cycle of backup media. Destruction should occur after proper sanitization or as a means of sanitization."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "There are generally three forms of governance within an enterprise organization, all of which have common goals, such as to ensure continued growth and expansion over time and to maintain resiliency to threats and the market. Which of the following is not one of these common forms of governance?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.058"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "IT"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Facility"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Corporate"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Security"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The three common forms of governance are IT, corporate, and security. Facility is not usually considered a form of governance, or it is already contained within one of the other three."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Biometric authentication devices fall under what top-level authentication factor type?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.122"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Type 1"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Type 2"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Type 3"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Type 4"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Biometric authentication devices represent a Type 3 (something you are) authentication factor."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which environment requires exact, specific clearance for an object’s security domain?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.072"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Hierarchical environment"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Hybrid environment"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Compartmentalized environment"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Organizational environment"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Compartmentalized environments require specific security clearances over compartments or domains instead of objects."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "When attempting to impose accountability on users, what key issue must be addressed?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.144"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Reliable log storage system"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Proper warning banner notification"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Legal defense/support of authentication"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Use of discretionary access control"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "To effectively hold users accountable, your security must be legally defensible. Primarily, you must be able to prove in a court that your authentication process cannot be easily compromised. Thus, your audit trails of actions can then be tied to a human."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "An employee retained access to sensitive data from previous job assignments. Investigators later caught him selling some of this sensitive data to competitors. What could have prevented the employee from stealing and selling the secret data?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.104"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Asset valuation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Threat modeling"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Vulnerability analysis"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "User entitlement audit"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A user entitlement audit can detect when employees have excessive privileges. Asset valuation identifies the value of assets. Threat modeling identifies threats to valuable assets. Vulnerability analysis detects vulnerabilities or weaknesses that can be exploited by threats."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "The Roscommon Rangers baseball team is concerned about the risk that a storm might result in the cancellation of a baseball game. There is a 30 percent chance that the storm will occur, and if it does, the team must refund all single-game tickets because the game cannot be rescheduled. Season ticket holders will not receive a refund and account for 20 percent of ticket sales. The ticket sales for the game are $1,500,000. What is the single loss expectancy in this scenario?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.143"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "$300,000"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "$1,050,000"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "$1,200,000"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "$1,500,000"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The single loss expectancy is calculated as the product of the exposure factor (80 percent) and the asset value ($1,500,000). In this example, the single loss expectancy is $1,200,000."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "The Bell-LaPadula access control model was designed to protect what aspect of security?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.028"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Confidentiality"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Integrity"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Accessibility"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Authentication"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Bell-LaPadula model is focused on maintaining the confidentiality of objects. Bell-LaPadula does not address the aspects of object integrity or availability."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Where is a good location for a turnstile?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.136"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Main entrance to a secure area"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Primary entrance for the public to enter a retail space"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "On secondary or side exits"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "On internal office intersections"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Turnstiles are most appropriate on secondary or side exits where a security guard is not available or is unable to maintain constant surveillance. The other options listed are not as ideal for the use of a turnstile."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following elements is not necessary in the BCP documentation?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.059"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Risk acceptance details"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Emergency response guidelines"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Risk assessment"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Mobile site plan"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Details of mobile sites are part of a disaster recovery plan, rather than a business continuity plan, since they are not deployed until after a disaster strikes."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Subjects can be held accountable for their actions toward other subjects and objects while they are authenticated to a system. What process facilitates this accountability?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.073"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Access controls"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Monitoring"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Account policies"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Performance review"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Monitoring the activities of subjects and objects, as well as of core system functions that maintain the operating environment and the security mechanisms, helps establish accountability on the system."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Abnormal or unauthorized activities detectable to an IDS include which of the following? (Choose all that apply.)",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.059"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "External connection attempts"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Execution of malicious code"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Access to controlled objects"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "None of the above"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A,B,C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "IDSs watch for violations of confidentiality, integrity, and availability. Attacks recognized by IDSs can come from external connections (such as the Internet or partner networks), viruses, malicious code, trusted internal subjects attempting to perform unauthorized activities, and unauthorized access attempts from trusted locations."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Using a network packet sniffer, you intercept a communication. After examining the IP packet’s header, you notice that the flag byte has the binary value of 00000100. What does this indicate?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.028"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "A flooding attack is occurring."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "A new session is being initiated."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "A reset has been transmitted."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "A custom-crafted IP packet is being broadcast."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The flag value of 00000100 indicates a RST, or reset flag, has been transmitted. This is not necessarily an indication of malicious activity."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "An organization has a patch management program but wants to implement another method to ensure that systems are kept up-to-date. What could they use?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.067"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Change management program"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Configuration management program"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Port scanners"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Vulnerability scanners"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Vulnerability scanners can check systems to ensure they are up-to-date with current patches (along with other checks) and are an effective tool to verify the patch management program. Change and configuration management programs ensure systems are configured as expected and unauthorized changes are not allowed. A port scanner is often part of a vulnerability scanner, but it will check only for open ports, not patches."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of evidence must be authenticated by a witness who can uniquely identify it or through a documented chain of custody?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.088"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Documentary evidence"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Testimonial evidence"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Real evidence"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Hearsay evidence"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Real evidence must be either uniquely identified by a witness or authenticated through a documented chain of custody."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of malicious code uses a filename similar to that of a legitimate system file?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.113"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "MBR"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Companion"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Stealth"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Multipartite"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Companion viruses use filenames that mimic the filenames of legitimate system files."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of decision-making involves the emotional impact of events on a firm’s workforce and client base?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.131"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Quantitative decision-making"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Continuity decision-making"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Qualitative decision-making"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Impact decision-making"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Qualitative decision-making takes non-numerical factors, such as emotional impact, into consideration."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "NAT allows the use of private IP addresses for internal use while maintaining the ability to communicate with the Internet. Where are the private IP addresses defined?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.087"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "RFC 1492"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "RFC 1661"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "RFC 1918"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "RFC 3947"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "NAT offers numerous benefits, including that you can use the private IP addresses defined in RFC 1918 in a private network and still be able to communicate with the Internet."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What form of access control is concerned with the data stored by a field rather than any other issue?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.049"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Content-dependent"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Context-dependent"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Semantic integrity mechanisms"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Perturbation"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Content-dependent access control is focused on the internal data of each field."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is a divestiture?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.053"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Asset or employee reduction"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "A distribution of profits to shareholders"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "A release of documentation to the public"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "A transmission of data to law enforcement during an investigation"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A divestiture is an asset or employee reduction."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Of the following, which provides some protection against tampering?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.041"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Security token"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Capabilities list"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Security label"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Access matrix"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A security label is usually a permanent part of the object to which it is attached, thus providing some protection against tampering. The other options do not offer such protection."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not a feature of packet switching?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.135"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Bursty traffic focused"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Fixed known delays"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Sensitive to data loss"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Supports any type of traffic"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Packet switching has variable delays; circuit switching has fixed known delays."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the length of a patent in the United States?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.023"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "7 years"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "14 years"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "20 years"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "35 years"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "U.S. patents are generally issued for a period of 20 years."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "In which phase of the business impact assessment do you compute loss expectancies?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.058"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Risk assessment"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Likelihood assessment"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Impact assessment"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Resource prioritization"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Loss expectancies are a measure of impact and are calculated during the impact assessment phase."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following security models is most often used for general commercial applications?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.099"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Brewer and Nash model"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Biba model"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Bell-LaPadula model"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Clark-Wilson model"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Of the four models mentioned, Biba and Clark-Wilson are most commonly used for commercial applications because both focus on data integrity. Of these two, Clark-Wilson offers more control and does a better job of maintaining integrity, so it’s used most often for commercial applications. Bell-LaPadula is used most often for military applications. Brewer and Nash applies only to datasets (usually within database management systems) where conflict-of-interest classes prevent subjects from accessing more than one dataset that might lead to a conflict-of-interest situation."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "In the Biba model, what rule prevents a user from reading from lower levels of classification?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.083"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Star axiom"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Simple property"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "No read up"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "No write down"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Biba simple property rule is “no read down.” The Biba star axiom is \"no write up\". \"No read up\" is the simple rule for Bell LaPadula. \"No write down\" is the star rule for Bell LaPadula."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the most likely problem to using VPNs when a separate firewall is present?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.132"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "You can’t filter on encrypted traffic."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "VPNs cannot cross firewalls."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Firewalls block all outbound VPN connections."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Firewalls greatly reduce the throughput of VPNs."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Firewalls are unable to filter on encrypted traffic within a VPN, which is a drawback. VPNs can cross firewalls. Firewalls do not have to always block outbound VPN connections. Firewalls usually only minimally affect the throughput of a VPN and then only when filtering is possible."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What are the only procedures that are allowed to modify a constrained data item (CDI) in the Clark-Wilson model?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.033"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Transformation procedures (TPs)"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Integrity verification procedure (IVP)"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Independent modification algorithm (IMA)"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Dependent modification algorithm (DMA)"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Transformation procedures (TPs) are the only procedures that are allowed to modify a CDI. The limited access to CDIs through TPs forms the backbone of the Clark-Wilson integrity model."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the second phase of the IDEAL software development model?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.085"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Developing"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Diagnosing"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Determining"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Designing"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The second phase of the IDEAL software development model is the Diagnosing stage."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not a critical member of a BCP team?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.091"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Legal representatives"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Information security representative"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Technical and functional experts"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Chief executive officer"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "While it is important to have executive-level support, it is not necessary (and quite unlikely!) to have the CEO on the team."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the term used to refer to the formal assignment of responsibility to an individual or group?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.056"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Governance"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Ownership"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Take grant"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Separation of duties"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Ownership is the formal assignment of responsibility to an individual or group."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "A process can function or operate as ????.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.054"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Subject only"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Object only"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Subject or object"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Neither a subject nor an object"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A process can function or operate as a subject or object. In fact, many elements within an IT environment, including users, can be subjects or objects in different access control situations."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following algorithms/protocols provides inherent support for nonrepudiation?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.087"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "HMAC"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "DSA"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "MD5"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "SHA-1"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Digital Signature Algorithm (as specified in FIPS 186-2) is the only one of the algorithms listed here that supports true digital signatures, providing integrity verification and nonrepudiation. HMAC allows for the authentication of message digests but supports only integrity verification. MD5 and SHA-1 are message digest creation algorithms and can be used in the generation of digital signatures but provide no guarantees of integrity or nonrepudiation in and of themselves."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following groupings restricts access to roles?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.024"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Grouping subjects"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Grouping privileges"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Grouping programs"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Grouping objects"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Role-based access control restricts access to roles by grouping subjects (such as users). Groups are assigned privileges but privileges aren’t grouped in roles. Programs aren’t grouped in roles. Objects such as files are often grouped within folders, but objects are not assigned as roles."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What category of malicious software includes rogue antivirus software?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.026"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Logic bombs"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Worms"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Trojan horses"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Spyware"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Rogue antivirus software is an example of a Trojan horse. Users are tricked into installing it, and once installed, it steals sensitive information and/or prompts the user for payment."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Change management should ensure that which of the following is possible?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.088"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Duplicate security is imposed on other systems."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Unauthorized changes to the system are prevented."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Vulnerable resources are locked down when threatened"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Changes can be rolled back to a previous state."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Change management is responsible for making it possible to roll back any change to a previous secured state."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What form of password attack utilizes a preassembled lexicon of terms and their permutations?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.133"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Rainbow tables"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Dictionary word list"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Brute force"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Educated guess"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Dictionary word lists are precompiled lists of common passwords and their permutations and serve as the foundation for a dictionary attack on accounts."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which conceptual security model offers the best preventive protection against viral infection and outbreak?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.044"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "ISO/OSI reference model"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Concentric circle"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Operations security triple"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "CIA Triad"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A concentric circle security model represents the best practice known as defense in depth, a layered approach to protecting IT infrastructure."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the vulnerability or feature of a PBX system that allows for an external entity to piggyback onto the PBX system and make long-distance calls without being charged for the tolls?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.105"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Black box"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "DTMF"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Vishing"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Remote dialing"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Remote dialing (aka hoteling) is the vulnerability or feature of a PBX system that allows for an external entity to piggyback onto the PBX system and make long-distance calls without being charged for the tolls."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What Clark-Wilson model feature helps protect against insider attacks by restricting the amount of authority any user possesses?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.140"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Simple integrity property"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Time of use"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Need to know"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Separation of duties"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Clark-Wilson model enforces separation of duties to further protect the integrity of data. This model employs limited interfaces or programs to control and maintain object integrity."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What regulation formalizes the prudent man rule that requires senior executives to take personal responsibility for their actions?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.045"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "CFAA"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Federal Sentencing Guidelines"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "GLBA"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Sarbanes–Oxley"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Federal Sentencing Guidelines released in 1991 formalized the prudent man rule, which requires senior executives to take personal responsibility for ensuring the due care that ordinary, prudent individuals would exercise in the same situation."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What occurs when the relationship between the plain text and the key is complicated enough that an attacker can’t merely continue altering the plain text and analyzing the resulting cipher text to determine the key? (Choose all that apply.)",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.106"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Confusion"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Transposition"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Polymorphism"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Diffusion"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A,D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Confusion and diffusion are two principles underlying most cryptosystems.\n Transposition is a way to acheive diffussion."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "On manual review systems, failure recognition is whose primary responsibility?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.061"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Tester or test-taker"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Client or representative"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Outsider or administrator"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Observer or auditor"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The observer or auditor of a manual review system is directly responsible for recognizing the failure of that system."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What protocol manages the security associations used by IPsec?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.090"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "ISAKMP"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "SKIP"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "IPComp"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "SSL"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Internet Security Association and Key Management Protocol (ISAKMP) provides background security support services for IPsec, including managing security associations."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "The ???? model focuses on preventing interference in support of integrity. This model is based on the idea of defining a set of system states, initial states, and state transitions. Through the use of and limitations to only these predetermined secure states, integrity is maintained, and interference is prohibited.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.126"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Biba"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Take grant"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Goguen−Meseguer"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Sutherland"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Sutherland model focuses on preventing interference in support of integrity. This model is based on the idea of defining a set of system states, initial states, and state transitions. Through the use of and limitations to only these predetermined secure states, integrity is maintained, and interference is prohibited.\n Biba relates to integrity and is non-discretionary lattice based model which applies simple star rule. No write down."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following models uses labels to define classifications levels of objects?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.120"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Mandatory access control model"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Discretionary access control model"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Role-based access control model"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Rule-based access control model"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A mandatory access control model uses labels to classify objects such as data. Subjects with matching labels can access these objects. None of the other models uses labels."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which VPN protocol can be implemented as the payload encryption mechanism when L2TP is used to craft the VPN connection for an IP communication?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.017"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "L2F"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "PPTP"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "IPsec"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "SSH"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "IPsec is a VPN protocol that can be implemented as the payload encryption mechanism when L2TP is used to craft the VPN connection for an IP communication."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What cryptographic goal does the challenge-response protocol support?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.080"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Confidentiality"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Integrity"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Authentication"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Nonrepudiation"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The challenge-response protocol is an authentication protocol that uses cryptographic techniques to allow parties to assure each other of their identity."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following definitions best explains the purpose of an intrusion detection system?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.011"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "A product that inspects incoming and outgoing traffic across a network boundary to deny transit to unwanted, unauthorized, or suspect packets"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "A device that provides secure termination or aggregation for IP phones, VoIP handsets, and softphones"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "A device that, using complex content categorization criteria and content inspection, prevents potentially dangerous content from entering a network"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "A product that automates the inspection of audit logs and real-time event information to detect intrusion attempts and possibly also system failures"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "An intrusion detection system (IDS) is a product that automates the inspection of audit logs and real-time event information to detect intrusion attempts. Option A defines a firewall, option B defines an IP telephony security gateway, and option C defines a content filtering system."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which IPsec protocol provides assurances of nonrepudiation?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.095"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "AH"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "ISAKMP"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "DH"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "ESP"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Authentication Header (AH) provides assurances of message integrity and nonrepudiation."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What form of infrastructure mode wireless networking deployment supports large physical environments through the use of a single SSID but numerous access points?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.048"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Stand-alone"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Wired extension"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Enterprise extended"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Bridge"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Enterprise extended infrastructure mode exists when a wireless network is designed to support a large physical environment through the use of a single SSID but numerous access points."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is the principle that objects are not disclosed to unauthorized subjects?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.067"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Integrity"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Nonrepudiation"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Sensitivity"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Confidentiality"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Confidentiality is the principle that objects are not disclosed to unauthorized subjects."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What layer of the ring protection scheme includes programs running in supervisory mode?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.089"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Level 0"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Level 1"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Level 3"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Level 4"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Supervisory mode programs are run by the security kernel, at Level 0 of the ring protection scheme."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Coordinated attack efforts against an infrastructure or system that limit or restrict its capacity to process legitimate traffic are what form of network-driven attack?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.023"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Denial of service"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Distributed denial of service"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Distributed reflective denial of service"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Differential denial of service"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "hat evidence standard do most civil investigations follow?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.058"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Beyond a reasonable doubt"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Beyond the shadow of a doubt"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Preponderance of the evidence"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Clear and convincing evidence"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Criminal cases use 'beyond reasonable doubt'\nwhile civil use 'more likely than not'"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Matthew recently completed writing a new song and posted it on his website. He wants to ensure that he preserves his copyright in the work. As a U.S. citizen, which of the following is the minimum that he must do to preserve his copyright in the song?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.080"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Register the song with the U.S. Copyright Office."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Mark the song with the © symbol."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Mail himself a copy of the song in a sealed envelope."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Nothing."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Matthew is not required to do anything. Copyright protection is automatic as soon as he creates the work."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which source of interference is generated by electrical appliances, light sources, electrical cables and circuits, and so on?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.002"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Cross-talk noise"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Radio frequency interference"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Traverse mode noise"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Common mode noise"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "RFI"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following would be considered a system compromise?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.029"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Forged spam email appearing to come from your organization"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Unauthorized use of an account by the legitimate user’s relative"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Probing a network searching for vulnerable services"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Infection of a system by a virus"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Sharing an account with a relative allows unauthorized access to a system, meeting the definition of a compromise. Forged spam email does not necessarily require access to your organization’s computing resources. Probing a network for vulnerable services is a scanning attack. Infection of a system by a virus is a malicious code attack."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What term describes the processor mode used to run the system tools used by administrators seeking to make configuration changes to a machine?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.032"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "User mode"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Supervisory mode"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Kernel mode"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Privileged mode"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "All user applications, regardless of the security permissions assigned to the user, execute in user mode. Supervisory mode, kernel mode, and privileged mode are all terms that describe the mode used by the processor to execute instructions that originate from the operating system."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is a means for IPv6 and IPv4 to be able to coexist on the same network? (Choose all that apply.)",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.146"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Dual stack"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Tunneling"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "IPsec v6"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "NAT-PT"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A,B,D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The means by which IPv6 and IPv4 can coexist on the same network is to use one or more of three primary options. Dual stack is to have most systems operate both IPv4 and IPv6 and use the appropriate protocol for each conversation. Tunneling allows most systems to operate a single stack of either IPv4 or IPv6 and use an encapsulation tunnel to access systems of the other protocol. Network Address Translation-Protocol Translation (NAT-PT) (RFC-2766) can be used to convert between IPv4 and IPv6 network segments similar to how NAT converts between internal and external addresses. There is no distinct IPsec version 6 because IPsec is native to IPv6, and IPsec does not assist or support a network operating both IPv4 and IPv6 on its own."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the best definition of a security model?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.c08.12"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "A security model states policies an organization must follow."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "A security model provides a framework to implement a security policy."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "A security model is a technical evaluation of each part of a computer system to assess its concordance with security standards."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "A security model is the process of formal acceptance of a certified configuration."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which type of intrusion detection system (IDS) can be considered an expert system?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.075"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Host-based"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Network-based"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Knowledge-based"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Behavior-based"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A behavior-based IDS can be labeled an expert system or a pseudo-artificial intelligence system because it can learn and make assumptions about events. In other words, the IDS can act like a human expert by evaluating current events against known events. A knowledge-based IDS uses a database of known attack methods to detect attacks. Both host-based and network-based systems can be either knowledge-based, behavior-based, or a combination of both."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is the most important and distinctive concept in relation to layered security?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.c01.12"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Multiple"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Series"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Parallel"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Filter"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Layering is the deployment of multiple security mechanisms in a series. When security restrictions are performed in a series, they are performed one after the other in a linear fashion. Therefore, a single failure of a security control does not render the entire solution ineffective."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What phase of the Electronic Discovery Reference Model puts evidence in a format that may be shared with others?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.060"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Production"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Processing"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Review"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Presentation"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Production places the information in a format that may be shared with others."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "The Jones Institute has six employees and uses a symmetric key encryption system to ensure confidentiality of communications. If each employee needs to communicate privately with every other employee, how many keys are necessary?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.078"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "1"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "6"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "15"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "30"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is risk?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.038"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Any potential occurrence that can cause an undesirable or unwanted outcome"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "The actual occurrence of an event that results in loss"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "The likelihood that any specific threat will exploit a specific vulnerability to cause harm to an asset"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "An instance of being exposed to asset loss due to a threat"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Risk is the likelihood that any specific threat will exploit a specific vulnerability to cause harm to an asset."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   },
   {
    "name": "PoorQuestionAnswer",
    "attrs": [],
    "value": "The answer given is that risk is a likelihood.  This seems to ignore that impact side of risks"
   }
  ],
  "value": ""
 },
 {
  "name": "In which phase of the SW-CMM does an organization use quantitative measures to gain a detailed understanding of the development process?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.c20.12"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Initial"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Repeatable"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Defined"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Managed"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "In the Managed phase, level 4 of the SW-CMM, the organization uses quantitative measures to gain a detailed understanding of the development process."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following describes a community cloud?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.c09.06"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "A cloud environment maintained, used, and paid for by a group of users or organizations for their shared benefit, such as collaboration and data exchange"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "A cloud service within a corporate network and isolated from the internet"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "A cloud service that is accessible to the general public typically over an internet connection"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "A cloud service that is partially hosted within an organization for private use and that uses external services to offer resources to outsiders"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A community cloud is a cloud environment maintained, used, and paid for by a group of users or organizations for their shared benefit, such as collaboration and data exchange. A private cloud is a cloud service within a corporate network and isolated from the internet. A public could is a cloud service that is accessible to the general public typically over an internet connection. A hybrid cloud is a cloud service that is partially hosted within an organization for private use and that uses external services to offer recourses to outsiders."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following does not reflect the protected elements under an access control methodology?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.061"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Integrity"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Scalability"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Accountability"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Confidentiality"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Access control provides security through confidentiality, integrity, and accountability."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not a valid reason why log files should be protected?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.145"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Log files can be used to reconstruct events leading up to an incident."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Attackers may try to erase their activity during or after an attack."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Unprotected log files cannot be used as evidence."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Log files include information on why an attack occurred."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Log files include information on what happened, when and where it happened, who was involved, and sometimes how, but they do not include why an attack occurred; they do not include the motivation of an attacker. Log files should be protected because they can be used to reconstruct events, attackers may try to modify them, and unprotected logs cannot be used as evidence."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What protocol is slowly replacing certificate revocation lists as a real-time method of verifying the status of a digital certificate?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.122"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "OLAP"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "LDAP"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "OSCP"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "BGP"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Online Certificate Status Protocol (OSCP) provides real-time query/response services to digital certificate users. This overcomes the latency inherent in the traditional certificate revocation list download and cross-check process."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What law protects the right of citizens to privacy by placing restrictions on the authority granted to government agencies to search private residences and facilities?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.c04.06"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Privacy Act"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Fourth Amendment"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Second Amendment"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Gramm-Leach-Bliley Act"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Fourth Amendment to the U.S. Constitution sets the “probable cause” standard that law enforcement officers must follow when conducting searches and/or seizures of private property. It also states that those officers must obtain a warrant before gaining involuntary access to such property."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What are the well-known ports?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.087"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "0 to 1,023"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "80, 135, 110, 25"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "0 to 65, 536"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "32,000 to 65,536"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Ports 0 to 1,023 are the well-known ports."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following security assessment and testing program components may be performed by security professionals in the IT organization?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.021"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Internal audit"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "External audit"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Criminal investigation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Assessment"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "IT staff may perform security assessments to evaluate the security of their systems and applications. Audits must be performed by internal or external auditors who are independent of the IT organization. Criminal investigations must be performed by certified law enforcement personnel."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What technique is commonly used by polymorphic viruses to escape detection?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.095"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Companion naming"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Encryption"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "AV tampering"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Exploitation of administrative privileges"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "While viruses may use all of the techniques described in this question to escape detection, the use of encryption is characteristic of polymorphic viruses."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What Biba property protects a subject from accessing an object at a lower integrity level?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.035"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "No read up"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "No read down"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "No write up"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "No write down"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Simple Integrity Property states that a subject cannot read an object of a lower integrity level (no read down)."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "The ",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.100"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Clark-Wilson"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Goguen-Meseguer"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Graham-Denning"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Bell-LaPadula"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Goguen-Meseguer model is based on predetermining the set or domain of objects that a subject can access. The set or domain is a list of those objects that a subject can access. This model is based on automation theory and domain separation. This means subjects are able to perform only predetermined actions against predetermined objects."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Why do operating systems need security mechanisms?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.033"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Humans are perfect."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Software is not trusted."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Technology is always improving."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Hardware is faulty."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "In which IPsec mode is the content of an encapsulated packet encrypted but not the header?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.062"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Transport"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Tunnel"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Vector"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Transparent"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "In transport mode, the IP packet data is encrypted, but the header of the packet is not."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following combinations of terms defines the operations security triple?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.013"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Confidentiality, integrity, and availability (the CIA Triad)"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Authentication, authorization, and accounting (AAA)"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "The relationship between assets, vulnerabilities, and threats"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Due care, due diligence, and operations controls"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The primary purpose for operations security is to safeguard information assets that reside in a system day to day, to identify and safeguard any vulnerabilities that might be present in that system, and to prevent any exploitation of threats. Administrators often call the relationship between assets, vulnerabilities, and threats an operations security triple. CIA relates to a model used to develop security policy, and AAA defines a framework for managing access to computer resources, enforcing policy, auditing usage, and providing usage data for charge backs or billing. Due care, due diligence, and operations controls are part of operations security but not in the same sense as in the relationship between assets, vulnerabilities, and threats."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of virus modifies operating system routines to trick antivirus software into thinking everything is functioning normally?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.096"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Multipart viruses"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Stealth viruses"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Encrypted viruses"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Polymorphic viruses"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Stealth viruses alter operating system file access routines so that when an antivirus package scans the system, it is provided with the information it would see on a clean system rather than with infected versions of data."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Your organization has recently implemented an SDN to separate the control plane from the data plane. Which of the following models will the SDN most likely use?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.125"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "ABAC"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "DAC"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "MAC"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "RBAC"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A software-defined network (SDN) typically uses an attribute-based access control (ABAC) model. SDNs don’t normally use the discretionary access control (DAC), mandatory access control, or role-based access control (RBAC) models."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is most directly associated with providing or supporting perfect forward secrecy?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.150"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "PBKDF2"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "ECDHE"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "HMAC"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "OCSP"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Elliptic Curve Diffie-Hellman Ephemeral, or Elliptic Curve Ephemeral Diffie-Hellman (ECDHE), implements perfect forward secrecy through the use of elliptic curve cryptography (ECC). PBKDF2 is an example of a key-stretching technology not directly supporting perfect forward secrecy. HMAC is a hashing function. OCSP is used to check for certificate revocation."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which wireless security protocol makes use of AES cryptography?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.096"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "WPS"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "WEP"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "WPA"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "WPA2"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "WPA2 replaces RC4 and TKIP (used by the original WPA) with AES and CCMP cryptography."
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which network topology offers multiple routes to each node to protect from multiple segment failures?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.098"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Ring"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Star"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Bus"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Mesh"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Mesh topologies provide redundant connections to systems, allowing multiple segment failures without seriously affecting connectivity."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What technology can be used to minimize the impact of a server failure immediately before the next backup was scheduled?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.081"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Clustering"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Differential backups"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Remote journaling"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Tape rotation"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Clustering servers adds a degree of fault tolerance, protecting against the impact of a single server failure."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "A host organization that houses on-site security staff has what form of security system?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.088"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Auxiliary system"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Centralized system"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Localized system"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Proprietary system"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "This describes a proprietary system, which is almost the same concept as a central station, but with the host organization has its own onsite security staff waiting to respond to security breaches. This is not an auxiliary system as there is no mention of contacting official emergency services, like police or fire/rescue. This is not a centralized system, as that is not a type of security system. A central station is a security system with offsite monitoring by a professional security services company. The term centralized is often used in contrast to decentralized in describing how something is managed, such as access control or authentication. This is not a localized system as there is no mention of an audible alarm."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is the type of antivirus response function that removes the malicious code but leaves damage unrepaired?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.068"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Cleaning"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Removal"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Stealth"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Polymorphism"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Removal removes the malicious code but does not repair the damage caused by it. Cleaning not only removes the code, but it also repairs any damage the code has caused."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of attack allows the transmission of sensitive data between classification levels through the direct or indirect manipulation of a shared storage media?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.099"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Trojan horse"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Direct access to media"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Permission creep"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Covert channel"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Covert channel attacks involve the illicit transfer of data by manipulating storage or timing channels."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Identification is the first step toward what ultimate goal?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.121"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Accountability"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Authorization"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Auditing"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Nonrepudiation"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Accountability is the ultimate goal of a process started by identification."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What common vulnerability has no direct countermeasure and little safeguards or validators?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.067"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Theft"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Fraud"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Omission"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Collusion"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Both omissions and errors are difficult aspects to protect against, particularly as they deal with human and circumstantial origins."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Mark recently completed work on a piece of computer software that he thinks will be especially valuable. He wants to protect the source code against others rewriting it in a different form. What is the best form of intellectual property protection he could seek in this case?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.082"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Trademark"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Trade secret"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Copyright"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Patent"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Mark’s best option is to treat the software source code as a trade secret. Copyright protection requires that he disclose the source in a public filing. Patent protection does not cover the actual program."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which process in the evaluation of a security system represents the formal acceptance of a certified configuration?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.079"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Certification"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Evaluation"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Accreditation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Formal analysis"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Accreditation is the formal declaration by the Designated Approving Authority (DAA) that an IT system is approved to operate in a particular security mode using a prescribed set of safeguards at an acceptable level of risk."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "In a(n) ",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.043"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Trusted"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Authorized"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Available"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Baseline"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "In a trusted system, all protection mechanisms work together to process sensitive data for many types of users while maintaining a stable and secure computing environment."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "The ????? model is based on the idea of defining a set of system states, initial states, and state transitions. Through the use of and limitations to only these predetermined secure states, integrity is maintained, and interference is prohibited.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.070"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Biba"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Sutherland"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Clark-Wilson"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Gramm-Leach-Bliley"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Sutherland model is based on the idea of defining a set of system states, initial states, and state transitions. Through the use of and limitations to only these predetermined secure states, integrity is maintained, and interference is prohibited."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following backup types does not alter the archive bit on backed-up files?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.101"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Full backup"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Remote journaling"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Incremental backup"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Differential backup"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Differential backups store all files that have been modified since the time of the most recent full backup. They do not alter the archive bit."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which form of physical security control focuses on facility construction and selection, site management, personnel controls, awareness training, and emergency response and procedures?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.150"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Technical"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Physical"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Administrative"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Logical"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Administrative physical security controls include facility construction and selection, site management, personnel controls, awareness training, and emergency response and procedures."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the primary function of a gateway as a network device?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.128"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Routing traffic"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Protocol translator"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Attenuation protection"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Creating virtual LANs"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The gateway is a network device (or service) that works at the Application layer. However, an Application layer gateway is a very specific type of component. It serves as a protocol translation tool. For example, an IP-to-IPX gateway takes inbound communications from TCP/IP and translates them over to IPX/SPX for outbound transmission."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "If a specific step-by-step guide does not exist that prescribes how to accomplish a necessary task, which of the following is used to create such a document?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.126"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Policy"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Standard"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Procedure"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Guideline"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A guideline offers recommendations on how standards and baselines are implemented and serves as an operational guide for both security professionals and users. Guidelines are flexible so they can be customized for each unique system or condition and can be used in the creation of new procedures (i.e., step-by-step guides)."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following items is not a critical piece of information in the chain of evidence?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.108"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "General description of the evidence"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Name of the person collecting the evidence"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Relationship of the evidence to the crime"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Time and date the evidence was collected"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The chain of evidence does not require that the evidence collector know or document the relationship of the evidence to the crime."
   },
   {
    "name": "Review",
    "attrs": [],
    "value": "Yes"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "A tunnel mode VPN is used to connect which types of systems?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.011"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Hosts and servers"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Clients and terminals"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Hosts and networks"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Servers and domain controllers"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "The following eight primary protection rules or actions define the boundaries of what security model?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.120"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Graham-Denning"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Bell-LaPadula"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Take-Grant"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Sutherland"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Graham-Denning model is focused on the secure creation and deletion of both subjects and objects. Ultimately, Graham-Denning is a collection of eight primary protection rules or actions (listed in the question) that define the boundaries of certain secure actions."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": "Securely create an object.\nSecurely create a subject.\nSecurely delete an object.\nSecurely delete a subject.\nSecurely provide the read access right.\nSecurely provide the grant access right.\nSecurely provide the delete access right.\nSecurely provide the transfer access right."
 },
 {
  "name": "Which component of the CIA Triad has the most avenues or vectors of attack and compromise?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.028"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Confidentiality"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Integrity"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Availability"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Accountability"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Availability has the most avenues or vectors of attack and compromise. Availability can be affected by damaging the resource, compromising the resource host, interfering with communications, or attacking the client."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "On a much smaller scale, ",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.140"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Recovery access control"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Corrective access control"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Detective access control"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Compensation access control"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Recovery access control is deployed to repair or restore resources, functions, and capabilities after a violation of security policies."
   },
   {
    "name": "Review",
    "attrs": [],
    "value": "Yes"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Why should an enterprise network implement endpoint security?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.040"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "To provide sufficient security on each individual host."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Centralized security mechanisms are too expensive."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Network security safeguards do not provide any protection for hosts."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Hardware security options are ineffective against software exploits."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Endpoint security is implemented in order to provide sufficient security on each individual host, rather than relying on network-based security only."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "During threat modeling, several options exist for ranking or rating the severity and priority of threats. Which of the following not a threat modeling ranking system?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.036"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "DREAD"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Probability * Damage Potential"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Qualitative analysis"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "High/medium/low"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Qualitative analysis is part of risk management/risk assessment, but it is not specifically a means of ranking or rating the severity and priority of threats under threat modelling. The three common means of ranking or rating the severity and priority of threats are DREAD, Probability * Damage Potential, and High/medium/low."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What government agency provides daily updates on wildfires in the United States?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.099"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "FEMA"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "NIFC"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "USGS"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "USFWS"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The National Interagency Fire Center provides daily updates on wildfires occurring in the United States.\nFEMA provides information on floods"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following encryption packages provides full disk encryption and is built into Microsoft Windows?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.031"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "TrueCrypt"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "PGP"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "EFS"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "BitLocker"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What network devices operate within the Physical layer?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.092"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Bridges and switches"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Firewalls"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Hubs and repeaters"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Routers"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What process state can be dependent on peripherals?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.130"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Ready"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Waiting"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Running"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Supervisory"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of disaster recovery test is the simplest to perform?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.114"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Structured walk-through"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Read-through"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Simulation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Parallel"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "In the read-through test, you distribute copies of the disaster recovery plan to key personnel for review but do not actually meet or perform live testing."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Among the following concepts, which element is not essential for an audit report?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.052"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Audit purpose"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Audit scope"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Audit results"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Audit overview"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is the least acceptable form of biometric device?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.003"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Iris scan"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Retina scan"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Fingerprint"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Facial geometry"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is a documented set of best IT security practices crafted by Information Systems Audit and Control Association (ISACA) and IT Governance Institute (ITGI)?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.033"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "ISO 17799"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "COBIT"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "OSSTMM"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Common Criteria (IS 15408)"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Control Objectives for Information and Related Technology (COBIT) is a documented set of best IT security practices crafted by Information Systems Audit and Control Association (ISACA) and IT Governance Institute (ITGI)."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What testing exercise would you perform that involves personnel relocation and remote site activation?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.068"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Parallel test"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Full-interruption test"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Structured walk-through"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Simulation test"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Parallel tests represent the next level in testing and involve actually relocating personnel to the alternate recovery site and implementing site activation procedures."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "The act of ",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.141"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Measuring and evaluating security metrics"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Monitoring performance"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Vulnerability analysis"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Crafting a baseline"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The act of measuring and evaluating security metrics is the practice of assessing the completeness and effectiveness of the security program. This should also include measuring it against common security guidelines and tracking the success of its controls."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "How is metadata generated?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.121"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "By creating a data warehouse"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "By performing data mining"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "By hosting a data mart"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "By authoring a data dictionary"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What form of attack is always possible when using a non-802.1x implementation of a wireless network?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.129"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Password guessing"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Encryption cracking"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "IV interception"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Packet replay attacks"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Password guessing is always a potential attack if a wireless network is not otherwise using some other form of authentication, typically accessed via 802.1x."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the best type of water-based fire suppression system for a computer facility?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.c10.19"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Wet pipe system"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Dry pipe system"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Preaction system"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Deluge system"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A preaction system is a combination dry pipe/wet pipe system. The system exists as a dry pipe until the initial stages of a fire (smoke, heat, and so on) are detected, and then the pipes are filled with water. The water is released only after the sprinkler head activation triggers are melted by sufficient heat."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Your organization was recently attacked by an APT. A root-cause analysis indicated that the attack was successful because an employee responded inappropriately to a malicious spam email. What phase of incident response does a root-cause analysis occur?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.084"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Detection"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Response"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Mitigation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Remediation"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A root-cause analysis typically occurs during the remediation stage. Incidents are identified and verified in the detection stage. During incident response, personnel assess the damage and collect evidence. Personnel isolate compromised systems during the mitigation stage."
   },
   {
    "name": "Review",
    "attrs": [],
    "value": "Yes"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "The ????? of a process consist of limits set on the memory addresses and resources it can access. This also states or defines the area within which a process is confined.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.105"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Isolation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Bounds"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Confinement"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Authentication"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The bounds of a process consist of limits set on the memory addresses and resources it can access. The bounds state or define the area within which a process is confined"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Methodical examination and review of environmental security and regulatory compliance, and a form of directive control, is known as what?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.035"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Penetration testing"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Compliance checking"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Auditing"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Ethical hacking"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Auditing is the periodic examination and review of a network to ensure that it meets security and regulatory compliance."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "When NAC is used to manage an enterprise network, what is most likely to happen to a notebook system once reconnected to the intranet after it has been out of the office for six weeks while in use by an executive on an international business trip?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.025"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Reimaged"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Updated at next refresh cycle"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Quarantine"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "User must reset their password"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "NAC often operates in a pre-admission philosophy in which a system must meet all current security requirements (such as patch application and antivirus updates) before it is allowed to communicate with the network. This often means systems that are not in compliance are quarantined or otherwise involved in a captive portal strategy in order to force compliance before network access is restored."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the most common reaction to the loss of physical and infrastructure support?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.117"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Deploying OS updates"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Vulnerability scanning"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Waiting for the event to expire"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Tightening of access controls"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "In most cases, you must simply wait until the emergency or condition expires and things return to normal. If physical and infrastructure support is lost, such as after a catastrophe, regular activity (including deploying updates, performing scans, or tightening controls) is not possible."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What form of interference is generated by a power differential between hot and ground wires?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.014"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Common mode noise"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Traverse mode noise"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Cross-talk noise"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Radio frequency interference"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Common mode noise is generated by the difference in power between the hot and ground wires of a power source or operating electrical equipment."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Who is responsible for the day-to-day maintenance of objects?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.124"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "The user"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "The owner"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "The custodian"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "The administrator"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A custodian is someone who has been assigned to or delegated the day-to-day responsibility of proper storage and protection of objects. A user is any subject who accesses objects on a system to perform some action or accomplish a work task. An owner is the person who has final corporate responsibility for the protection and storage of data. When discussing access to objects, three subject labels are used: user, owner, and custodian. Therefore, administrator is not an appropriate choice."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "The Goguen-Meseguer model is an ",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.111"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Integrity"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Confidentiality"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Non-interference"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Availability"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Goguen-Meseguer model is an integrity model based on predetermining the set or domain—a list of objects that a subject can access."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What programming environment offered by Microsoft includes the Common Language Interface?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.060"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "COM"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "DCOM"
   },
   {
    "name": "C",
    "attrs": [],
    "value": ".NET Framework"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "ActiveX"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The .NET Framework includes the Common Language Interface to support multiple programming languages."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "???? is a form of programming attack that is used to either falsify information being sent to a visitor or cause their system to give up information without authorization.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.108"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "SQL injection"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Buffer overflow"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "DDoS"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "XML exploitation"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "XML exploitation is a form of programming attack that is used to either falsify information being sent to a visitor or cause their system to give up information without authorization."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Third-party governance cannot be mandated for whom?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.147"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Internal entities"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "External consultants and suppliers"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Subsidiaries"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Commercial competitors"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Commercial competitors or any other entity that is not directly connected or related to the primary organization cannot have that organization’s third-party governance mandated or forced on them."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of information is not normally included in the risk acceptance/mitigation portion of BCP (business continuity plan) documentation?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.128"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Reasons for accepting risks"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Potential future events that might warrant reconsideration of the decision"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Identification of insurance policies that apply to a given risk"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Risk mitigation provisions and processes"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Insurance policies are an example of risk assignment/transference and would not be described in the risk acceptance/mitigation section of the documentation."
   },
   {
    "name": "Review",
    "attrs": [],
    "value": "Yes"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not a goal of the change control process of configuration management?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.093"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Implement changes in a monitored and orderly manner."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Changes are cost effective."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "A formalized testing process is included to verify that a change produces expected results."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "All changes can be reversed."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "While most business decisions need to include cost analysis, the change control process of configuration management is not directly concerned with cost effectiveness."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What security services are provided by Kerberos for authentication traffic?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.014"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Availability and nonrepudiation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Confidentiality and nonrepudiation"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Confidentiality and integrity"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Availability and authorization"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Kerberos provides confidentiality and integrity protection security services for authentication traffic using symmetric cryptography to encrypt tickets sent over the network to prove identification and provide authentication. The security services provide by Kerberos are not directly related to availability or nonrepudiation."
   },
   {
    "name": "Review",
    "attrs": [],
    "value": "Yes"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "A team that knows substantial information about its target, including on-site hardware/software inventory and configuration details, is best described as what?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.055"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Zero knowledge"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Infinite knowledge"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Absolute knowledge"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Partial knowledge"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Partial-knowledge teams possess a detailed account of organizational assets, including hardware and software inventory, prior to a penetration test."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the foundation of user and personnel security?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.047"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Background checks"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Job descriptions"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Auditing and monitoring"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Discretionary access control"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Job descriptions are essential to user and personnel security. Only when it’s based on a job description does a background check have true meaning. Without a job description, auditing and monitoring cannot determine when a user performs tasks outside of their assigned work. Without a job description, administrators do not know what level of access to assign via DAC."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of intellectual property protection is best suited for computer software?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.121"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Copyright"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Trademark"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Patent"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Trade secret"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Trade secrets are one of the best legal protections for computer software."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Of the following choices, what best describes the purpose of a honeypot or a honeynet?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.012"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "To keep attackers away from real systems or networks they might otherwise attack"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "To provide a lure for attackers by advertising the presence of a weak or insecure system or network"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "To provide an isolated network for testing software"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "To lure attackers into a bogus system or network environment and present sufficient material of apparent worth or interest to keep the attacker around long enough to track them down"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "RSA encryption relies on the use of two ????.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.104"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "One-way functions"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Prime numbers"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Hash functions"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Elliptic curves"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The strength of RSA encryption relies on the difficulty of factoring the two large prime numbers used to generate the encryption key."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following passwords uses a challenge-response mechanism to create a one-time password?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.027"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Synchronous one-time passwords"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Asynchronous one-time passwords"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Strong static passwords"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Passphrases"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "An asynchronous token generates and displays one-time passwords using a challenge-response process to generate the password. A synchronous token is synchronized with an authentication server and generates synchronous one-time passwords. Static passwords are not one-time passwords but instead stay the same for a period of time. A passphrase is a static password created from an easy-to-remember phrase."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What security model is based on dynamic changes of user privileges and access based on user activity?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.104"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Sutherland"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Brewer–Nash"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Biba"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Graham–Denning"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What form of security planning is designed to focus on timeframes of approximately one year and may include scheduling of tasks, assignment of responsibilities, hiring plans, maintenance plans, and even acquisition plans?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.055"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Strategic"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Operational"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Administrative"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Tactical"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Tactical planning is designed to focus on timeframes of approximately one year and may include scheduling of tasks, assignment of responsibilities, hiring plans, maintenance plans, and even acquisition plans"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What method is not integral to assuring effective and reliable security staffing?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.061"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Screening"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Bonding"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Training"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Conditioning"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Screening, bonding, and training are all vital procedures for ensuring effective and reliable security staffing because they verify the integrity and validate the suitability of said staffers."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What part of the Common Criteria specifies the claims of security from the vendor that are built into a target of evaluation?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.119"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Protection profiles"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Evaluation assurance level"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Certificate authority"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Security target"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Security targets (STs) specify the claims of security from the vendor that are built into a TOE."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Who is responsible for authoring the principle that can be summed up as “the enemy knows the system”?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.108"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Rivest"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Schneier"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Kerckchoffs"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Shamir"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Kerckchoffs’s principle states that a cryptographic system should remain secure even when all details of the system, except the key, are public knowledge."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "????? is any hardware, software, or administrative policy or procedure that defines and enforces access and restriction rights on an organizational level.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.060"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Logical control"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Technical control"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Access control"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Administrative control"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Access control is any hardware, software, or organizational administrative policy or procedure that grants or restricts access, monitors and records attempts to access, identifies users attempting to access, and determines whether access is authorized."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "A ???? contains levels with various compartments that are isolated from the rest of the security domain.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.129"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Hybrid environment"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Compartmentalized environment"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Hierarchical environment"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Security environment"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of system is authorized to process data at different classification levels only when all users have authorized access to those classification levels?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.083"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Compartmented mode system"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "System-high mode system"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Multilevel mode system"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Dedicated mode system"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Systems running in system-high mode are authorized to process data at different classification levels only if all system users have access to the highest level of classification processed."
   },
   {
    "name": "Review",
    "attrs": [],
    "value": "Yes"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What regulation formalizes the prudent man rule, requiring that senior executives of an organization take personal responsibility for ensuring due care?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.088"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "National Information Infrastructure Protection Act"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Federal Information Security Management Act"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Information Security Reform Act"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Federal Sentencing Guidelines"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Federal Sentencing Guidelines formalized the prudent man rule and applied it to information security."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following actions should never occur during the detection phase of incident response?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.011"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Powering down a compromised system"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Disconnecting a compromised system from the network"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Isolating a compromised system from other systems on the network"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Preserving the system in a running state"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "You should never power down a compromised system during the early stages of incident response because this may destroy valuable evidence stored in volatile memory. The other answers include steps that will isolate a system without destroying evidence in typical scenarios."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What law requires that communications carriers cooperate with federal agencies conducting a wiretap?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.109"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "CFAA"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "CALEA"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "EPPIA"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "ECPA"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Communications Assistance to Law Enforcement Act (CALEA) requires all communications carriers to make wiretaps possible for law enforcement with an appropriate court order."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is not a basic requirement for the admissibility of evidence?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.022"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Timely"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Relevant"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Material"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Competent"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "To be admissible, evidence must be relevant, material, and competent."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "A biometric system is matching subjects to a database using a one-to-many search. What is this providing?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.029"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Authentication"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Authorization"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Accountability"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Identification"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Biometric systems using a one-to-many search provide identification by searching a database for a match. Biometric systems using a one-to-one search provide authentication. Biometric systems do not provide authorization or accountability."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following files might be modified or created by a companion virus?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.043"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "COMMAND.EXE"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "CONFIG.SYS"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "AUTOEXEC.BAT"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "WIN32.DLL"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Companion viruses are self-contained executable files with filenames similar to those of existing system/program files but with a modified extension. The virus file is executed when an unsuspecting user types the filename without the extension at the command prompt."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following access control techniques uses labels to identify subjects and objects?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.107"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Discretionary access control"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Nondiscretionary access control"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Role-based access control"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Mandatory access control"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Mandatory access control uses labels to identify subjects and objects and grants access based on matching labels. Discretionary access control requires all objects to have an owner. Nondiscretionary access control provides centralized access controlled by an administrator. Role-based access control provides access based on membership within a role."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Of the following choices, what is the most appropriate action to take during the mitigation phase of incident response?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.113"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Isolate and contain"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Gather evidence"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Report"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Restore service"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "During the mitigation phase of an incident, you would take steps to isolate and contain the incident. You would gather evidence during the response phase. Personnel notify appropriate people during the reporting phase. Servers are restored during the recovery phase."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "The purchasing of insurance is a form of ????.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.066"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Risk mitigation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Risk assignment"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Risk acceptance"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Risk rejection"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Insurance is a form of risk assignment or transference."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What technology may database developers use to restrict users’ access to a limited subset of database attributes or records?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.041"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Polyinstantiation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Cell suppression"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Aggregation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Views"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Database views use SQL statements to limit the amount of information that users can view from a table."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "All of the following are implications of multilayer protocols except which one?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.020"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "VLAN hopping"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Multiple encapsulation"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Filter evasion using tunneling"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Static IP addressing"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Static IP addressing is not an implication of multilayer protocols; it is the identification feature of the IP protocol.\n VLAN hopping is because it relies upon encapsulating a packet with one VLAN with another package for another VLAN."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "QOn what principle does a SYN flood attack operate?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.138"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Sending overly large SYN packets"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Exploiting a platform flaw in Windows"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Using an amplification network to flood a victim with packets"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Exploiting the TCP/IP three-way handshake"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "SYN flood attacks are targeted at the standard three-way handshake process used by TCP/IP to initiate communication sessions."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "The term personal area network is most closely associated with what wireless technology?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.008"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "802.15"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "802.11"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "802.16"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "802.3"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "802.15 (aka Bluetooth) creates personal area networks (PANs)."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which backup facility is large enough to support current operational capacity and load, including the supportive infrastructure?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.127"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Mobile site"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Cloud provider"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Hot site"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Cold site"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the primary purpose of most malware today?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.073"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Infecting word processor documents"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Creating botnets"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Destroying data"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Sending spam"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Most malware is designed to add systems to botnets, where they are later used for other nefarious purposes, such as sending spam or participating in distributed denial-of-service attacks."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Practicing the activities that maintain continued application of security protocol, policy, and procedure is also called what?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.053"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Due notice"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Due diligence"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Due care"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Due date"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Due diligence is the action taken to apply, enforce, and perform responsibilities or rules as governed by organizational policy."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Among the following choices, what kind of IDS is considered an expert system?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.079"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Behavior-based"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Network-based"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Knowledge-based"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Host-based"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A behavior-based intrusion detection system (IDS) can be labeled an expert system or a pseudo-artificial intelligence system because it can learn and make assumptions about events. In other words, the IDS can act like a human expert by evaluating current events against known events."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "During the deencapsulation procedure, the ",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.109"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Transport"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Data Link"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Presentation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Ethernet"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "During the deencapsulation procedure, the Data Link layer strips its information and sends the message up to the Network layer.\n I think this is trying to say the the deencapsulation process starts at the data link layer.\n Its not a good question."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following network devices is used to connect networks that are using different network protocols?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.088"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Bridge"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Router"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Switch"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Gateway"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following forms of authentication provides the strongest security?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.106"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Password and a PIN"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "One-time password"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Passphrase and a smart card"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Fingerprint"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "C is 2 factor while the others are not.\nAmong these options, passphrase and a smart card provide the strongest authentication security because they deliver two-factor authentication. A password and a PIN are both in the same factor. Despite the security offered by one-time passwords, a two-factor authenticator is stronger."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the maximum key length supported by the Advanced Encryption Standard’s Rijndael encryption algorithm?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.076"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "128 bits"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "192 bits"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "256 bits"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "512 bits"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The AES/Rijndael algorithm is capable of operating with 128-, 192-, or 256-bit keys. The algorithm uses a block size equal to the length of the key."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "When possible, operations controls should be ????.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.066"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Simple"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Administrative"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Preventive"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Transparent"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "When possible, operations controls should be invisible, or transparent, to users. This keeps users from feeling hampered by security and reduces their knowledge of the overall security scheme, thus further restricting the likelihood that users will violate system security deliberately."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "In what security mode must each user have access approval and valid need to know for all information processed by the system?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.128"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Dedicated mode"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "System high mode"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Compartmented mode"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Multilevel mode"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The scenario presented in the question describes the three characteristics of dedicated mode."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which security mode provides the most granular control over resources and users?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.131"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Dedicated"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "System high"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Compartmented"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Multilevel"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "System high mode provides the most granular control over resources and users because it enforces clearances, requires need to know, and allows the processing of only single sensitivity levels. All the other levels either do not have unique need to know between users (dedicated), allow multiple levels of data processing (compartmented), or allow a wide number of users with varying clearance (multilevel)."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is the weakest method of authentication?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.027"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Synchronous one-time passwords"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Asynchronous one-time passwords"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Strong static passwords"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Retina scans"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is not a core principle of the Agile Manifesto?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.007"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Simplicity is essential."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Build projects around all team members equally."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Working software is the primary measure of progress."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "The best designs emerge from self-organizing teams."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Agile Manifesto says that you should build projects around motivated individuals and give them the support they need."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Of the following choices, what is a primary benefit when images are used to deploy new systems?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.148"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Provides baseline for configuration management"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Improves patch management response times"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Reduces vulnerabilities from unpatched systems"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Provides documentation for changes"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "When images are used to deploy systems, the systems start with a common baseline, which is important for configuration management. Images don’t necessarily improve the evaluation, approval, deployment, and audits of patches to systems within the network. While images can include current patches to reduce their vulnerabilities, this is because the image provides a baseline. Change management provides documentation for changes."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not a benefit of tunneling?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.099"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Tunneling provides a connection method across untrusted systems."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Traffic within a tunnel is isolated from inspection devices."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Tunneling allows nonroutable traffic to be routed across other networks."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Each encapsulated protocol includes its own error detection, error handling, acknowledgment, and session management features."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Tunneling is generally an inefficient means of communicating because all protocols include their own error detection, error handling, acknowledgment, and session management features, and using more than one protocol at a time just compounds the overhead required to communicate a single message."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following comes first?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.003"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Accreditation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Assurance"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Trust"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Certification"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Trust comes first. Trust is built into a system by crafting the components of security. Then assurance (in other words, reliability) is evaluated using certification and/or accreditation processes."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What form of interference is generated by a difference in power between hot and neutral wires of a power source?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.059"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Radio frequency interference"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Cross-talk noise"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Traverse mode noise"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Common mode noise"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Traverse mode noise is generated by the difference in power between the hot and neutral wires of a power source or operating electrical equipment."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "The act of performing a(n) ???  in order to drive the security policy is the clearest and most direct example of management of the security function.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.137"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Ethical hacking exercise"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Full interruption test"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Risk assessment"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Public review of policy"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The act of performing a risk assessment in order to drive the security policy is the clearest and most direct example of management of the security function."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which type of control provides extended options to existing controls and aids or supports administrative security policy?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.072"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Recovery access control"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Corrective access control"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Restorative access control"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Compensation access control"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Compensation access control is deployed to provide various options to existing controls to help enforce and support a security policy."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Generally, a privacy policy is designed to protect what?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.060"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "A user’s privacy"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "The public’s freedom"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Intellectual property"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "A company’s right to audit"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The purpose of a privacy policy is to inform users where they do and do not have privacy for the primary benefit of the protection of the company’s right to audit and monitor user activity."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What amendment to the US Constitution protects individuals against wiretapping and invasions of privacy?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.091"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "First"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Fourth"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Fifth"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Tenth"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Fourth Amendment, as interpreted by the courts, includes protections against wiretapping and other invasions of privacy."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which backup facility is large enough to support current operational capacity and load but lacks the supportive infrastructure?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.092"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Mobile site"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Service bureau"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Hot site"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Cold site"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A cold site is any facility that provides only the physical space for recovery operations while the organization using the space provides its own hardware and software systems."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Alan conducted a vulnerability scan of a system and discovered that it is susceptible to a SQL injection attack. Which one of the following ports would an attacker most likely use to carry out this attack?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.057"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "443"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "565"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "1433"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "1521"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "While SQL injection attacks do target databases, they do so by using web servers as intermediaries. Therefore, SQL injection attacks take place over web ports, such as 80 and 443, and not database ports, such as 1433 and 1521."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "When conducting an internal investigation, what is the most common source of evidence?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.026"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Historical data"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Search warrant"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Subpoena"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Voluntary surrender"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Internal investigations usually operate under the authority of senior managers, who grant access (i.e., voluntary surrender) to all information and resources necessary to conduct the investigation."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "During what phase of incident response do you collect evidence such as firewall logs?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.073"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Detection"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Response"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Compliance"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Remediation"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Evidence collection takes place during the response phase of the incident. Incidents are identified and verified during the detection phase. Compliance with laws might occur during the reporting phase, depending on the incident. Personnel typically perform a root-cause analysis during the remediation phase."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What aspect of an organizational security policy, procedure, or process affects all other aspects?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.006"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Awareness training"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Logical security"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Disaster recovery"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Physical security"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Physical security is the most prominent aspect of an organizational security policy because it directly and indirectly influences all other forms. Without physical security, no other processes and procedures are reliable."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following types of attack is most difficult to defend against?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.089"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Scanning"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Malicious code"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Grudge"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Distributed denial of service"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "It is difficult to defend against distributed denial-of-service attacks because of their sophistication and complexity."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "When you are configuring a wireless extension to an intranet, once you’ve configured WPA-2 with 802.1x authentication, what additional security step could you implement in order to offer additional reliable security?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.098"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Require a VPN"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Disable SSID broadcast."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Issue static IP addresses."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Use MAC filtering."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following methods is not a valid method of destroying data on a hard drive?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.127"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Purging the drive with a software program"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Copying data over the existing data"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Removing the platters and shredding them"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Removing the platters and disintegrating them"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Copying data over existing data is not reliable because data may remain on the drive as data remanence. Some software programs can overwrite the data with patterns of 1s and 0s to destroy the data. Shredding or disintegrating the platters will destroy the data."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the most important aspect of a biometric device?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.026"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Accuracy"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Acceptability"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Enrollment time"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Invasiveness"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The most important aspect of a biometric factor is its accuracy. If a biometric factor is not accurate, it may allow unauthorized users into a system. Acceptability by users, the amount of time it takes to enroll, and the invasiveness of the biometric device are additional considerations but not as important as its accuracy."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is confidentiality dependent on?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.115"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Integrity"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Availability"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Nonrepudiation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Auditing"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Without object integrity, confidentiality cannot be maintained. In fact, integrity and confidentiality depend on one another."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What phase of the Electronic Discovery Reference Model performs a rough cut of irrelevant information?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.059"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Collection"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Processing"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Review"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Analysis"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Processing screens the collected information to perform a “rough cut” of irrelevant information, reducing the amount of information requiring detailed screening."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "A(n) ??? system is one in which all protection mechanisms work together to process sensitive data for many types of users while maintaining a stable and secure computing environment.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.057"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Assured"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Updated"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Protected"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Trusted"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A trusted system is one in which all protection mechanisms work together to process sensitive data for many types of users while maintaining a stable and secure computing environment."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "How might you map out the organizational needs for transfer to or establishment in a new facility?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.003"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Inventory assessment"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Threat assessment"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Risk analysis"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Critical path analysis"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Critical path analysis can be defined as the logical sequencing of a series of events such that planners and integrators possess considerable information for the decision-making process."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "The Clark-Wilson access model is also called a(n) ",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.036"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Encrypted"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Triple"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Restricted"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Unrestricted"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What character, if eliminated from all web form input, would prevent the execution of many cross-site scripting attacks?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.098"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "$"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "&"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "˂"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "<"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "This is required for the script tag"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of attack uses malicious email and targets a group of employees within a single company?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.116"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Phishing"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Spear phishing"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Whaling"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Vishing"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Spear phishing targets a specific group of people such as a group of employees within a single company. Phishing goes to anyone without any specific target. Whaling is a form of phishing that targets high-level executives. Vishing is a form of phishing that commonly uses Voice over IP (VoIP)."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "A ",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.116"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Hybrid environment"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Compartmentalized environment"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Hierarchical environment"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Security environment"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Hybrid environments combine both hierarchical and compartmentalized environments so that security levels have subcompartments."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the frequency of an IT infrastructure security audit or security review based on?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.011"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Asset value"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Administrator discretion"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Risk"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Level of realized threats"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The frequency of an IT infrastructure security audit or security review is based on risk. You must establish the existence of sufficient risk to warrant the expense of, and interruption caused by, a security audit on a more or less frequent basis. Asset value and threats are part of risk but are not the whole picture, and assessments are not performed based only on either of these. A high-value asset with a low level of threats doesn’t present a high risk. Similarly, a low-value asset with a high level of threats doesn’t present a high risk. The decision to perform an audit isn’t usually relegated to an administrator."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "In what type of attack does the intruder initiate connections to both a client and a server?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.097"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Chosen plain-text attack"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Meet-in-the-middle attack"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Man-in-the-middle attack"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Replay attack"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "In the man-in-the-middle attack, a malicious individual sits between two communicating parties and intercepts all communications (including the setup of the cryptographic session).\n Meet-in-the middle is about having the some plain text and some output ciphertext and iterating through all k1 and k2 keys to find the combination that produces what is known."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following provides the best protection against mishandling media that contains sensitive information?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.047"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Marking"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Purging"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Sanitizing"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Retaining"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Marking (or labeling) media is the best choice of the available answers to protect against mishandling media. When properly marked, personnel are more likely to handle media properly. Purging and sanitizing methods remove sensitive information but do not protect against mishandling. Data retention refers to how long an organization keeps the data, not how it handles the data."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of virus modifies its own code as it travels from system to system in an attempt to evade signature detection?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.004"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Polymorphic"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Encrypted"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Multipartite"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Stealth"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Polymorphic viruses actually modify their own code as they travel from system to system. The virus’s propagation and destruction techniques remain the same, but the signature of the virus is somewhat different each time it infects a new system."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not true?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.039"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Policies, standards, baselines, guidelines, and procedures can be combined in a single document."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Not all users need to know the security standards, baselines, guidelines, and procedures for all security classification levels."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "When changes occur, it is easier to update and redistribute only the affected material rather than update a monolithic policy and redistribute it."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Higher up the formalization structure (that is, security policies), there are fewer documents because they are general broad discussions of overview and goals. Further down the formalization structure (that is, guidelines and procedures), there are many documents because they contain details specific to a limited number of systems, networks, divisions, areas, and so on."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Avoid combining policies, standards, baselines, guidelines, and procedures in a single document. Each of these structures must exist as a separate entity because each performs a different specialized function."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which is the most common form of perimeter security device or mechanism for any given business?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.086"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Security guards"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Fences"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Badges"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Lighting"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Lighting is by far the most pervasive and basic element of security because it illuminates areas and makes signs of hidden danger visible to all."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What security principle states that a thorough understanding of a system’s operational details is not necessary for most routine activities?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.119"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Process isolation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Abstraction"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Monitoring"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Hardware segmentation"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Abstraction states that a detailed understanding of lower system levels is not a necessary requirement for working at higher levels."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "The University of Outer Mongolia runs a web application that processes student tuition payments via credit card and is subject to PCI DSS. The university does not wish to perform web vulnerability scans on a regular basis because they consider them too time-consuming. What technology may they put in place that eliminates the PCI DSS requirement for recurring web vulnerability scans?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.079"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Web application firewall"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Intrusion prevention system"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Network vulnerability scanner"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "None. There is no exception to the recurring web vulnerability scan requirement."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "PCI DSS allows organizations to choose between performing annual web vulnerability assessment tests or installing a web application firewall."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What form of mobile device management provides users with a list of approved devices from which to select the device to implement?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.005"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "BYOD"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "CYOD"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "COPE"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "OCSP"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The concept of CYOD (choose your own device) provides users with a list of approved devices from which to select the device to implement. BYOD (bring your own device) is a policy that allows employees to bring their own personal mobile devices into work and use those devices to connect to (or through) the company network to business resources and/or the Internet. The concept of COPE (company-owned, personally enabled) is for the organization to purchase devices and provide them to employees. OCSP (Online Certificate Status Protocol) is used to check the revocation status of certificates."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the countermeasure cost/benefit equation?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.046"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "SLE * ARO"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "EF AV ARO"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "(ALE1 – ALE2) – CM cost"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Total risk + controls gap"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "To make the determination of whether the safeguard is financially equitable, use the following countermeasure cost/benefit equation: (ALE before countermeasure – ALE after implementing the countermeasure) – annual cost of countermeasure = value of the countermeasure to the company."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the purpose of a security impact analysis in the context of change management?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.078"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "To approve changes"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "To reject changes"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "To identify changes"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "To review changes"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A security impact analysis reviews change requests and evaluates them for potential negative impacts. All changes aren’t necessarily approved or rejected. The analysis doesn’t attempt to identify changes."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following describes putting similar elements into groups, classes, or roles that are assigned security controls, restrictions, or permissions as a collective?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.065"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Data classification"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Abstraction"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Superzapping"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Using covert channels"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Abstraction describes putting similar elements into groups, classes, or roles that are assigned security controls, restrictions, or permissions as a collective."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not a valid issue to consider when evaluating a safeguard?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.074"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Cost/benefit analysis"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Compliance with existing baselines"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Legal liability and prudent due care"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Compatibility with IT infrastructure"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "New safeguards establish new baselines; thus, compliance with existing baselines is not a valid consideration point."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not a valid security measure to protect against brute-force and dictionary attacks?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be2.114"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Enforce strong passwords through a security policy."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Maintain strict control over physical access."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Require all users to log in remotely."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Use two-factor authentication."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Requiring users to log in remotely does not protect against password attacks such as brute-force or dictionary attacks. Strong password policies, physical access control, and two-factor authentication all improve the protection against brute-force and dictionary password attacks."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which backup format stores only those files that have been set with the archive bit and have been modified since the last complete backup?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.004"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Differential backup"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Partial backup"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Incremental backup"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Full backup"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Differential backups store all files that have been modified since the time of the most recent full backup; they affect only those files that have the archive bit turned on, enabled, or set to 1."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "An organization wants to implement a cloud-based service using a combination of two separate clouds. Which deployment model should they choose?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be3.018"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Community"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Hybrid"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Private"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Public"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not an element defined under the Clark-Wilson model?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.031"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Constrained data item"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Transformation procedures"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Redundant commit statement"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Integrity verification procedure"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A constrained data item (CDI) is any data item whose integrity is protected by the security model. An unconstrained data item (UDI) is any data item that is not controlled by the security model. Any data that is to be input and hasn’t been validated, or any output, would be considered an unconstrained data item. An integrity verification procedure (IVP) is a procedure that scans data items and confirms their integrity. Transformation procedures (TPs) are the only procedures that are allowed to modify a CDI. The limited access to CDIs through TPs forms the backbone of the Clark-Wilson integrity model."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "A person who illicitly gains the trust or credentials from a trusted party has committed what criminal act?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.052"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Espionage"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Sabotage"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Reverse engineering"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Social engineering"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Social engineering is an attempt to deceive an insider into performing questionable actions on behalf of some unauthorized outsider."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What procedure returns business facilities and environments to a working state?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.079"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Reparation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Restoration"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Respiration"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Recovery"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Disaster restoration involves restoring a business facility and environment to a workable state."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is an access control list (ACL) based on?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.022"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "An object"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "A subject"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "A role"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "An account"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "An ACL is based on an object and includes a list of subjects that are granted access. A capability table is focused on a subject and includes a list of objects the subject can access. Roles and accounts are examples of subjects and may be included in an ACL, but they aren’t the focus."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Any process, mechanism, or tool that guides an organizational security implementation is what type of control?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.045"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Administrative control"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Corrective control"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Directive control"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Detective control"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "Directive controls guide an organizational security implementation and as such are control statements."
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is not a part of documenting the business continuity plan?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.064"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Documentation of policies for continuity"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Historical record"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Job creation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Identification of flaws"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "BCP documentation can be an arduous task, but it should not require the creation of a new position."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "The Twofish algorithm uses an encryption technique not found in other algorithms that XORs the plain text with a separate subkey before the first round of encryption. What is this called?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.063"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Preencrypting"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Prewhitening"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Precleaning"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Prepending"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of processing makes use of a multithreading technique at the operating system level?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.022"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Symmetric multiprocessing"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Multitasking"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Multiprogramming"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Massively parallel processing"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What attack involves an interruptive malicious user positioned between a client and server attempting to take over?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.082"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Man-in-the-middle"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Spoofing"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Hijacking"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Cracking"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Tom built a database table consisting of the names, telephone numbers, and customer IDs for his business. The table contains information on 30 customers. What is the cardinality of this table?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be4.036"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Two"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Three"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Thirty"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Undefined"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The cardinality of a table refers to the number of rows in the table, whereas the degree of a table is the number of columns."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following requirements does not come from the Children’s Online Privacy Protection Act?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.105"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Children must be kept anonymous when participating in online forums."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Parents must be provided with the opportunity to review information collected from their children."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Parents must give verifiable consent to the collection of information about children."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Websites must have a privacy notice."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What is a hardware-imposed network segmentation that requires a routing function to support intersegment communications otherwise known as?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be5.045"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Subnet"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "DMZ"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "VLAN"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Extranet"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A VLAN (virtual LAN) is a hardware-imposed network segmentation created by switches that requires a routing function to support communication between different segments."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not considered an example of data hiding?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.c01.13"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Preventing an authorized reader of an object from deleting that object"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Keeping a database from being accessed by unauthorized visitors"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Restricting a subject at a lower classification level from accessing data at a higher classification level"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Preventing an application from accessing hardware directly"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Preventing an authorized reader of an object from deleting that object is just an example of access control, not data hiding. If you can read an object, it is not hidden from you."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "What process or event is typically hosted by an organization and is targeted to groups of employees with similar job functions?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.c02.17"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Education"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Awareness"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Training"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Termination"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Training is teaching employees to perform their work tasks and to comply with the security policy. Training is typically hosted by an organization and is targeted to groups of employees with similar job functions."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "QWhich of the following represents accidental or intentional exploitations of vulnerabilities?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.c02.10"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Threat events"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Risks"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Threat agents"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Breaches"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Threat events are accidental or intentional exploitations of vulnerabilities.\nBreaches are intentional bypassing of controls"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "You’ve performed a basic quantitative risk analysis on a specific threat/vulnerability/risk relation. You select a possible countermeasure. When performing the calculations again, which of the following factors will change?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.c02.20"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Exposure factor"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Single loss expectancy"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Asset value"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Annualized rate of occurrence"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A countermeasure directly affects the annualized rate of occurrence, primarily because the counter-measure is designed to prevent the occurrence of the risk, thus reducing its frequency per year.\n What is changing here is the introduction of the countermeasure."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following attacks is the best example of a financial attack?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be6.137"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Denial of service"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Website defacement"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Port scanning"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Phone phreaking"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "Phone phreaking attacks are designed to obtain service while avoiding financial costs."
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 },
 {
  "name": "Once a system is compromised, ",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "tb475934.CISSPSG8E.be1.065"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Compensation access control"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Recovery access control"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Restorative access control"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Corrective access control"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Corrective access control is deployed to restore systems to normal after an unwanted or unauthorized activity has occurred."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#"
   }
  ],
  "value": ""
 }
]