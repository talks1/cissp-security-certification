export default [
 {
  "name": "Which of the following issues is NOT addressed by Kerberos?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.d0d13802-c128-46a9-a13f-d13f341d8f46"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Availability"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Confidentiality"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Integrity"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Authentication"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Kereberos doesnt address how to make the system more robust."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": "How difficult is it for the right people to get access. Dont forget availability."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following should NOT considered as key objectives of a smart card system for employees accessing facility and systems?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.24b9cefe-ed9f-4129-944a-ed9bee2a64ac"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Mechanism should provide strong resistance to fraud, tampering or other exploitation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Mechanism should allow rapid authentication"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Credentials are issued by only authorization officials"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Credential recipients must be adequately verified prior to credentials being issues"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Mechanisms must support SHA-256 and AES-192 or greater."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "AES-192 might not be involved in the smart card solution at all. All the other make sense for a system."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Organization is replacing username/password with a smart card based system.Which of the following is least likely to be a requirement for issuance of a smart card?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.ec78fd46-4a5d-462e-8ee4-a21d87d8e14e"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Requring person to appear in person for the registration process."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Requring user to provide knowledge of current password"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Recording copy of the users fingerprint during the process"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Taking a photo of the user to be included on the smart card"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Requiring user to provide passport and another form of government id"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "WIth smart card issues we want to associate the current with a specific person and the current password is the thing least directly proving the identity of a person."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "The Health Insurance Portability and Accountability Act (HIPAA), requires that medical providers keep which information private?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.5158e50b-d1ae-4294-938a-e5f60f7eae33"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Medical"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Personal"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Personal and medical"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Insurance"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Defines PHI (Personal Health Information)"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is correct",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.26eb28e6-0f62-4916-b878-2dbfee03f9d4"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Authorization precedes Identification precedes Authentication"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Identification precedes precedes Authorization preceds Authentication"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Identification precedes precedes Authentication preceds Authorization"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Authentication precedes Authorization precedes Identification"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following access control mechanisms allows information owners to control access to resources by evaluating the subject, object and the environment?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM561697b1-1cb4-49cd-bc67-a45bbbe30cb8."
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Rule based access control"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Attribute based access control"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Role based access control"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Discretionary Access Control"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=wfDkTHDoPbI"
   }
  ],
  "value": ""
 },
 {
  "name": "An organization uses PIV cards for desktop computer logins and physical access and wants to extend PIV-based authentication to the increasing number of company-issued smartphones and tablets. Which of the following will provide the best authentication security and the most seamless user experience?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.d436fc7e-31bc-4217-9e0f-f276e9063f40"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "MicroSD authentication tokens"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "TOTP authentication using key fob or mobile app"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Derived PIV credentials stored securely on the device"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "USB PIV card reader connected to the device"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Derived PIV is like a virtual card on the device. A software solution.\n Other options are more problematic as they are hardware based."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=fncd6XVXkdY"
   }
  ],
  "value": ""
 },
 {
  "name": "A system in your enterprise does not support individual user passwords but multiple admins require access at least once a month. Which of the solutions is the best to acheive Accountability?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.3283dee6-1985-45fe-834c-6605e5420732"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Isolate the legacy system to its own VLAN"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Change the password weekly and share with authorized users"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Utilize an enterprise password manager with password sharing features"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Remove the system from the network until a replacement can be identified"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Assign one admin to perform all tasks on the system"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Password manager can record the admin requesting password. They can also change the password after each admin has done their work."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": "Implication is that for Aurecon IT we can say that LastPass should be a recommended approach for shared system access."
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=jDPPxA6TthQ"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following are not characteristics of the challenge authentication protocol (CHAP)?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.770f3370-d056-46f3-8353-5ddd04312277"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Challenges are encrypted using a symmetric algorithm"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Authentication is negotiated via a 3-way handshake"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Authenticator will randomly require re-authentication"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "CHAP support mutual authentication by client and server"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The challenge is hashed using the shared secret rather than encrypted.\n The 3 way hanshake is challenge-response-accept"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=P7j3lJDMFgg"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following are not principles of access control",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.c99147e4-2de5-4612-9186-a9490e03b317"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Principle of least privilege"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Locards Principle"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Need to know"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Separation of duties"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Tranquility principle"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Principle of accountability"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C,E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Locards principle related to attackers always leaving some evidence\nTranquility principle relates to access control rules that are invariant\n Principle of accountability is not a principle of access control"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=BUcoABZzeQ4"
   }
  ],
  "value": ""
 },
 {
  "name": "Select the authentication mechanisms which are characteristic based",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.4bd05ed4-c3f3-4186-b45e-f1d1d2a8a343"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Password"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Rentinal Scan"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Soft token"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "vascular pattern scanner"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B,D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Characteristic based are some you are...biometric"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "I made this up"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not a mitigation technique that can be used to protect an application and its data.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.82e6bd9f-2557-44c5-9ba9-b89c831e3a13"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Fuzzing"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "ACLs"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Digital Signatures"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Disk Quotas"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Encryption"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Secure Logging"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Fuzzing is a technique to spam an application with lots of random inputs to check if there are some vulnerabilities. THis is not a migitation technique.\nInterestingly I wouldnt consider 'secure logging' a migitation technique but this was indicated."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=6IIWcS8iye0"
   }
  ],
  "value": ""
 },
 {
  "name": "Which resource-intensive access control mechanism implemented in databases, can allow/deny access to an object based on the data the object contains?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.f49428d3-894d-4c25-aaa8-3665d739df19"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Discretionary Access Control"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Mandatory Access Control"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Role Based Acess Control"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "View Based Access Control"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Content Dependent Access Control"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Context Based Access Control"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=WJWvcYv--OY"
   }
  ],
  "value": ""
 },
 {
  "name": "What is an advantage of content-dependent access control in databases?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.96369afd-0a44-4936-8ae8-2782b13ccf92"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Processing overhead."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "It ensures concurrency."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "It disallows data locking."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Granular control."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.briefmenow.org/isc2/what-is-an-advantage-of-content-dependent-access-control-in-databases/"
   }
  ],
  "value": ""
 },
 {
  "name": "If security was not part of the development of a database, how is it usually handled?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.80b59f91-e8a6-47ea-bf7f-aae9b60c6680"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Through cell suppression"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "By a trusted back end"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "By a trusted front end"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "By views"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Have to ensure security in the front end to compensate for the back end"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.briefmenow.org/isc2/if-security-was-not-part-of-the-development-of-a-database-how-is-it-usually-handled/"
   }
  ],
  "value": ""
 },
 {
  "name": "The Active Directory domain adminstrator has created a security group called 'R&D- Project Z' added members of the group 'Project Z' team to the group. He then configures a Group Policy Object (GPO) that allows only that group  to acess 'Project Z' servers. What type of access control is this an example of?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.4267a455-0292-4a85-be62-44f74fb8bbfa"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Discretionary"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Context-Dependent"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Non-Discretionary"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "View Based"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Non-Discretionary is another name for Role Based Access Control"
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=WJWvcYv--OY"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is an example of multi-factor authentication",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.40b54081-909d-4fd1-93a3-674b1c43707e"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "A split knowledge system"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "A username with an iris scanner"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "A smartcard and a PIN"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "A password and a PIN"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "A passphrase and a CAPTCHA challenge"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "A passphrase and a pre-shared key"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=KLlz1SY7qGo&t=43s"
   }
  ],
  "value": ""
 },
 {
  "name": "X.509 is the standard for which of the following?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.8aa1c3e6-f08a-4695-b5ca-232a5c0fd9de"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Kerberos"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "PKI Certificates"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Directory Services"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Diffie-Helman"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Risk Management Framework"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=KQ3qIRmJdbw"
   }
  ],
  "value": ""
 },
 {
  "name": "One important criteria with biometrics is how acceptable they will be to your work force. Of the following authentication types which is most likely to be met with strong resistance from the average user.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.87807170-05c0-4866-b498-59b06b16382b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Iris Scan"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Hand Geometry"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Palm Scan"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Fingerprint Scan"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Retina Scan"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Voice Analysis"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "Signature Dyanmics"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=5_YqDgPJhW0&list=RDCMUCwUkAunxT1BNbmKVOSEoqYA&index=4"
   }
  ],
  "value": ""
 },
 {
  "name": "During what phase of the access control process does a user prove his or her identity?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.b4e9ee30-4a94-4286-9c4e-2c6d4b2dbdab"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Authorization"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Identification"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Authentication"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Remediation"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4626246?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "How many separate roles are involved in a properly implemented registration and identity proofing process?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.74d782ed-a6a3-4fcd-bf2d-8f2366fa4729"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "1"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "2"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "3"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "4"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Request, Approval, Identity Proof and Issuance"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4626245?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is an example of multifactor authentication?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.0f947655-5374-4afd-84b4-c74eeced675f"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "ID Card and PIN"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "retinal scan and fingerprint"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "password and security questions"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "ID Card and Key"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4625518?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Jane uses an authentication token that requires her to push a button each time she wishes to login to a system. What type of token is she using?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.7c8da458-e0de-4714-9dd4-1d6aceee12b1"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "HOTP"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "SSL"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "HMAC"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "TOTP"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "HOTP - HMAC based One Time Password uses shared secret and sequence number\nTOTP - Uses shared secret and time of day"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4625518?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which authentication protocol requires the use of external encryption to protect passwords?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.1b8a32cb-5f08-4057-bef7-7955d3f9756b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "SAML"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "PAP"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "CHAP"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Kerberos"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "PAP has no encryption and therefore requires encryption in addition to its use"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4625518?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which choice is not an example of federated authentication?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.1af7f6d7-bd35-4788-bc44-d633b05ed5e0"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Google Accounts"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Twitter Accounts"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Facebook Connect"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "RADIUS"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4625518?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Ricky would like to use an authentication protocol that fully encrypts the authentication session, uses the reliable TCP protocol and will work on his Cisco devices. What protocol should he choose?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.47b9a1e4-774a-42f3-ba59-a04cf0893848"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "TACACS"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "RADIUS"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "XTACACS"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "TACACS+"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4625518?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "In the Kerberos protocol, what system performs authentication of the end user?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.5b1b533c-12af-4a16-8927-8268092a3767"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "TGT"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "SS"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "TGS"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "AS"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4625518?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "In SAML, what organization performs authentication of the end user?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.9342a92e-143e-40e7-a459-44f73f3e94b2"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Service Provider"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Principal"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Identity Provider"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Authentication Source"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4625518?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following are required for accountability?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.53804faf-84c0-4d62-993b-299c33fae693"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Identification"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Authorization"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Authentication"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Confidentiality"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Secure Logging"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Auditing"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "Scalability"
   },
   {
    "name": "H",
    "attrs": [],
    "value": "Availability"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A,C,E,F"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "PT"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following are NOT a mechanism to increase account security",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.a1969183-1ede-4460-a003-858632776137"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Enforce minimum password length"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Enforce password expiry after set periods"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Enforce minimum password complexity"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Prevent password reuse on reset"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Require re-authentication on set time periods"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Lock out users if too many incorrect attempts occur"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Require re-authentication on set time periods prevents session mis management but is not related to account security"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/password-policies?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not an important account management practice for security professionals?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.68ce9b3a-4633-4371-a972-4b160917b052"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Privilege Creep"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Separation of Duties"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Least Privilege"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Mandatory Vacations"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Privilege Creep is a symptom of inappropriately managing privileges while the others are practives for good account management."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4624770?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What Windows mechanism allows the easy application of security settings to groups of users?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.682a4be3-9472-4a58-956d-9706a100ba27"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "SCEP"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "GPO"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "MMC"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "ADUC"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4624770?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is not a normal account activity attribute to monitor?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.b9dc3219-1d5a-427f-a97d-cf8afb2ea42b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Login Time"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Incorrect Login Attempts"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Password"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Login Location"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4624770?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Tobias recently permanently moved from a job in accounting to a job in human resources but never had his accounting privileges revoked. What situation occurred in this case?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.d0dbe92f-84ab-402c-96b1-899329d87b86"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Job Rotation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Least Privilege"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Privilege Creep"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Separation of Duties"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4625517?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What command can administrators use to determine whether the SELinux kernel module is enabled?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.bb58de21-d4eb-4cd5-9abc-530187f2a069"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "secheck"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "selmodule"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "getenforce"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "fsck"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4625517?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "In a discretionary access control system, individual users have the ability to alter access permissions.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.893d3c77-2980-426e-9386-0a619f3974e9"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "True"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "False"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Mandatory Access Control - users cannot change permissions\nWith Discretionary Access Controls - they can"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "What file permission does NOT allow a user to launch an application?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.9b6a4de5-fb7c-473b-bd8b-377f1bc28d6d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "read and execute"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "read"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "modify"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "full control"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4625517?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Dan is engaging in a password cracking attack where he uses precomputed hash values. What type of attack is Dan waging?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.bb53e966-5c73-42ed-87af-23b8fe981099"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Rainbow Tables"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Hybrid"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Brute Force"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Dictionary"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4624771?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of website does the attacker use when waging a watering hole attack?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.65aa0101-b702-47cc-8c80-c52db17d4cce"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "software distribution site"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "known malicious site"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "site trusted by the end user"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "hacker forum"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4624771?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "A social engineer calls an administrative assistant in your organization and obtains her password by threatening her that her boss' account will be deleted if she does not provide the password to assist with troubleshooting. What type of attack is the social engineer using?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.cff2b913-4c3f-40cf-b1f1-4c085e90b40e"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "scarcity"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "liking"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "social proof"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "intimidation"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4624771?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of phishing attack focuses specifically on senior executives of a targeted organization?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.df358c4c-0ae4-42fc-95c2-5c6d4c119f7e"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "vishing"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "spear phishing"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "whaling"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "pharming"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4624771?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following would not be an asset that an organization would want to protect with access controls?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.6a5039d2-ce92-4753-8c9a-72af3de33b46"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Information"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Systems"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Devices"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Facilities"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "None of the above"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 13 Review Question 1\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 618). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is true related to a subject? A subject is always a user account. The subject is always the entity that provides or hosts the information or data. The subject is always the entity that receives information about or data from an object. A single entity can never change roles between subject and object.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.8ede983b-5750-4d24-a373-d3999224f448"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "A subject is always a user account"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "The subject is always the entity that hosts information or data"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "The subject is always the entity that receives information about or data from an object"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "A single entity can never change roles between subject and object"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "C. The subject is active and is always the entity that receives information about, or data from, the object. A subject can be a user, a program, a process, a file, a computer, a database, and so on. The object is always the entity that provides or hosts information or data. The roles of subject and"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 13 Review Question 2\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 618). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following types of access control uses fences, security policies, security awareness training, and antivirus software to stop an unwanted or unauthorized activity from occurring?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.46d03007-1312-48cf-ab82-8b6025d25020"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Preventative"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Detective"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Corrective"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Authoritative"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 13 Review Question 3\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 618). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What type of access controls are hardware or software mechanisms used to manage access to resources and systems, and provide protection for those resources and systems?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.e37e705c-a3c5-4195-af85-431d0a92b4bf"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Administrative"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Logical/Technical"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Physical"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Preventative"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 13 Review Question 4\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 618). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following best expresses the primary goal when controlling access to assets?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.092c642f-540d-4974-aa7e-b4a9d15f41cb"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Preserve Confidentiality, integrity, and availability of systems and data"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Ensure on valid objects can authenticate in the system"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Prevent unauthorized access to subjects"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Ensure that all subjects are authenticated"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A primary goal when controlling access to assets is to protect against losses, including any loss of confidentiality, loss of availability, or loss of integrity. Subjects authenticate on a system, but objects do not authenticate. Subjects access objects, but objects do not access subjects. Identification and authentication is important as a first step in access control, but much more is needed to protect assets."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 13 Review Question 5\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 618). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "A user logs in with a login ID and a password. What is the purpose of the login ID?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.bf7b94d4-1ceb-4500-8a1a-f35d690fc0e8"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Authentication"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Authorization"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Accountability"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Identification"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 13 Review Question 6\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 618). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Accountability requires all of the following items except one. Which item is not required for accountability?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.0ac0ec8a-9e02-4870-be07-2468b83b3a0e"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Identitication"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Authentication"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Auditing"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Authorization"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 13 Review Question 7\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 618). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following best identifies the benefit of a passphrase?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.0fafaf7f-f9aa-4d67-a04d-ba77fc634bf7"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "It is short"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "It is easy to remember"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "It includes a single set of characters"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "It is easy to crack"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 13 Review Question 8\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 618). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is an example of a Type 2 authentication factor?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.d0ef1137-a405-4f70-8ba6-17e2df588e18"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Something you have"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Something you are"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Something you do"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Something you know"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A Type 2 authentication factor is based on something you have, such as a smartcard or token device. Type 3 authentication is based on something you are and sometimes something you do, which uses physical and behavioral biometric methods. Type 1 authentication is based on something you know, such as passwords or PINs."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 13 Review Question 10\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 618). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Your organization issues devices to employees. These devices generate onetime passwords every 60 seconds. A server hosted within the organization knows what this password is at any given time. What type of device is this?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.a48f6c2b-0544-4b67-b3f7-879cbedec1ca"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Synchronous Token"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Asynchronous Token"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Smartcard"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Common Access Card"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A synchronous token generates and displays onetime passwords, which are synchronized with an authentication server. An asynchronous token uses a challenge-response process to generate the onetime password. Smartcards do not generate onetime passwords, and common access cards are a version of a smartcard that includes a picture of the user."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 13 Review Question 11\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 618). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What does the CER for a biometric device indicate?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.dda87e4d-a132-4596-bd50-884365717aaa"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "The sensitivity is to high"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "The sensitivity is to low"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "It indicates the point where false rejection rate equals false acceptance rate"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "When high enought, the biometric device is highly accurate"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The point at which the biometric false rejection rate and the false acceptance rate are equal is the crossover error rate (CER). It does not indicate that sensitivity is too high or too low. A lower CER indicates a higher-quality biometric device, and a higher CER indicates a less accurate device."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 13 Review Question 13\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 620). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What is the primary purpose of Kerberos?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.fe58e997-4925-4850-8273-cbb52add565b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Confidentiality"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Integrity"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Authentication"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Accountability"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 13 Review Question 15\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 620). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is the best choice to support a federated identity management (FIM) system?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.2496fbc4-8b48-4c31-9df6-cff19fd42710"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Kerberos"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "HTML"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "XML"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "SAML"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 13 Review Question 16\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 620). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What is the function of the network access server within a RADIUS architecture?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.7af826d2-2d5c-490b-a2a8-40a04dbdb64e"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Authentication Server"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Client"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "AAA server"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Firewall"
   },
   {
    "name": "E",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The network access server is the client within a RADIUS architecture. The RADIUS server is the authentication server and it provides authentication, authorization, and accounting (AAA) services. The network access server might have a host firewall enabled, but that isn’t the primary function."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 13 Review Question 17\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 620). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following AAA protocols is based on RADIUS and supports Mobile IP and VoIP?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.7afb7b76-27b1-4425-8bb4-592e67ca03de"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Distributed access control"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Diameter"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "TACACS+"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "TACACS"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Diameter is based on Remote Authentication Dial-in User Service (RADIUS), and it supports Mobile IP and Voice over IP (VoIP). Distributed access control systems such as a federated identity management system are not a specific protocol, and they don’t necessarily provide authentication, authorization, and accounting. TACACS and TACACS+ are authentication, authorization, and accounting (AAA) protocols, but they are alternatives to RADIUS, not based on RADIUS."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 13 Review Question 18\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 620). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following best describes an implicit deny principle?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.d4bc9d4c-2e78-498d-8f7a-b577c6369983"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "All actions that are not expressly denied are allowed"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "All actions that are not expressly allowed are denied"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "All actions must be expressly denied"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "None of the above"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The implicit deny principle ensures that access to an object is denied unless access has been expressly allowed (or explicitly granted) to a subject. It does not allow all actions that are not denied, and it doesn’t require all actions to be denied."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 14, Review Question 1\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What is the intent of least privilege?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.fd8fa020-0e29-43d5-a1dd-344e2254b2b1"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Enforce the most restrictive rights required by users to run system processes."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Enforce the least restrictive rights required by users to run system processes."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Enforce the most restrictive rights required by users to complete assigned tasks."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Enforce the least restrictive rights required by users to complete assigned tasks."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The principle of least privilege ensures that users (subjects) are granted only the most restrictive rights they need to perform their work tasks and job functions. Users don’t execute system processes. The least privilege principle does not enforce the least restrictive rights but rather the most restrictive rights."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 14, Review Question 2\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "A table includes multiple objects and subjects and it identifies the specific access each subject has to different objects. What is this table?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.8c37636c-359e-43ae-ac6d-074e6db84b7c"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Access Control List"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Access control matrix"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Federation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Creeping Privilege"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "An access control matrix includes multiple objects, and it lists subjects’ access to each of the objects. A single list of subjects for any specific object within an access control matrix is an access control list. A federation refers to a group of companies that share a federated identity management system for single sign-on. Creeping privileges refers to the excessive privileges a subject gathers over time."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 14, Review Question 3\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Who, or what, grants permissions to users in a DAC model?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.3b70e3de-19b1-489d-b037-1094c97adcba"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Administrators"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Access Controls list"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Assigned Labels"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "The data customers"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The data custodian (or owner) grants permissions to users in a Discretionary Access Control (DAC) model."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 14, Review Question 4\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following models is also known as an identity-based access control model?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.d3c14785-e47a-46ca-a7a7-f9c31f2c19e2"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "DAC"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "RBAC"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Rule-based access control"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "MAC"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A Discretionary Access Control (DAC) model is an identity-based access control model. It allows the owner (or data custodian) of a resource to grant permissions at the discretion of the owner. The Role Based Access Control (RBAC) model is based on role or group membership. The rule-based access control model is based on rules within an ACL. The Mandatory Access Control (MAC) model uses assigned labels to identify access."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 14, Review Question 4\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "A central authority determines which files a user can access. Which of the following best describes this?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.f5b7b065-2934-4c24-a0b9-dfdbb49dbb86"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Access Control List"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "An access control matrix"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Discretionary Access Control model"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Non discretionary access control model"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A nondiscretionary access control model uses a central authority to determine which objects (such as files) that users (and other subjects) can access."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 14, Review Question 5\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following statements is true related to the RBAC model?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.8688b741-2aa5-4a1a-a0e6-9a5bbc246daf"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "A RBAC model allows users membership in multiple groups."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "A RBAC model allows users membership in a single group."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "A RBAC model is nonhierarchical."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "A RBAC model uses labels."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A. The Role Based Access Control (RBAC) model is based on role or group membership, and users can be members of multiple groups. Users are not limited to only a single role. RBAC models are based on the hierarchy of an organization, so they are hierarchy based. The Mandatory Access Control (MAC) model uses assigned labels to identify access."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 14, Review Question 8\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is the best choice for a role within an organization using a RBAC model?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.369e9b55-ec3e-4288-a735-4f988ff5bf01"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Web Server"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Application"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Database"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Programmer"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A programmer is a valid role in a Role Based Access Control (RBAC) model. Administrators would place programmers’ user accounts into the Programmer role and assign privileges to this role. Roles are typically used to organize users, and the other answers are not users."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 14, Review Question 9\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What type of access control model is used on a firewall?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.97b87f54-80fd-480b-bb33-c4b436669ca3"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "MAC Model"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "DAC Model"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Rule Based Access Model"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "RBAC Model"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Firewalls use a rule-based access control model with rules expressed in an access control list. A Mandatory Access Control (MAC) model uses labels. A Discretionary Access Control (DAC) model allows users to assign permissions. A Role Based Access Control (RBAC) model organizes users in groups."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 14, Review Question 10\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What type of access controls rely on the use of labels?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.ea5c8909-852b-4e75-a88a-488b5001d16d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "DAC"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "NonDiscretionary"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "MAC"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "RBAC"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Mandatory Access Control (MAC) models rely on the use of labels for subjects and objects. Discretionary Access Control (DAC) models allow an owner of an object to control access to the object. Nondiscretionary access controls have centralized management such as a rule-based access control model deployed on a firewall. Role Based Access Control (RBAC) models define a subject’s access based on job-related roles."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 14, Review Question 12\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following best describes a characteristic of the MAC model?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.78dcb889-35c7-4238-9319-56e907986260"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Employs Explicit-Deny philosophy"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Permissive"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Rule Based"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Prohibitive"
   },
   {
    "name": "E",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Mandatory Access Control (MAC) model is prohibitive, and it uses an implicit-deny philosophy (not an explicit-deny philosophy). It is not permissive and it uses labels rather than rules."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 14, Review Question 13\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not a valid access control model?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.97a52d4a-818c-4e66-8388-dd022e09c20d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "DAC"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Nondiscretionary"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "MAC"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Compliance Based Access control model"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Compliance-based access control model is not a valid type of access control model. The other answers list valid access control models."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 14, Review Question 14\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What would an organization do to identify weaknesses?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.269dcb72-80ae-4bb5-a81a-5225e904933e"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Asset Valuation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Threat Modeling"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Vulnerability Analysis"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Access Review"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A vulnerability analysis identifies weaknesses and can include periodic vulnerability scans and penetration tests. Asset valuation determines the value of assets, not weaknesses. Threat modeling attempts to identify threats."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 14, Review Question 15\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following can help mitigate the success of an online brute-force attack?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.ba1829a4-557f-4c3c-9257-9045f2487510"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Rainbow Table"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Account Lockout"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Salting Passwords"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Encryption of Passwords"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "An account lockout policy will lock an account after a user has entered an incorrect password too many times, and this blocks an online brute-force attack. Attackers use rainbow tables in offline password attacks. Password salts reduce the effectiveness of rainbow tables. Encrypting the password protects the stored password but isn’t effective against a brute-force attack without an account lockout."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 14, Review Question 16\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following would provide the best protection against rainbow table attacks?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.42d73c30-5a31-4744-8d32-aa64e7fe5e93"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Hashing Passwords with MD5"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Salt and Pepper with Hashing"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Account Lockout"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Implement RBAC"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Using both a salt and pepper when hashing passwords provides strong protection against rainbow table attacks. MD5 is no longer considered secure, so it isn’t a good choice for hashing passwords. Account lockout helps thwart online password brute-force attacks, but a rainbow table attack is an offline attack. Role Based Access Control (RBAC) is an access control model and unrelated to password attacks."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 14, Review Question 17\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Management wants to ensure that the consultant has the correct priorities while doing her research. Of the following, what should be provided to the consultant to meet this need?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.1f19865a-48c0-49c1-bc88-74a54dd69c09"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Asset Valuation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Threat Modeling Results"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Vulnerability Analaysis Reports"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Audit Trails"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Asset valuation identifies the actual value of assets so that they can be prioritized. For example, it will identify the value of the company’s reputation from the loss of customer data compared with the value of the secret data stolen by the malicious employee. None of the other answers is focused on high-value assets. Threat modeling results will identify potential threats."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 14, Review Question 17\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 }
]