export default [
 {
  "name": "At which stage in the SDLC should a privacy impact assessment first be conducted",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDL.f74710ec-e2fb-4700-a394-0bcece067f39"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Initiation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Development/Acquisition"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Implementation/Assessment"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Operation/Maintenance"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Disposal"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "A small but talented company has developed proprietary code for you. You are concerned that in the future the employees and maybe even the company may break up and move on. What is the best way for you to protect the value of your purchased product?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDL.6c3c0336-0996-4766-b74f-2a87cae9daf8"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Demand the source code"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Hire the lead developer"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Only buy from well-established vendors"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Use a software escrow"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Software escrow is usually requested by the buyers, who intend to ensure the continuity of the software maintenance over time, even if the software house that has developed the application goes out of the business or fails to maintain and update the code."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "If a web application is not doing appropriate input validation what is the most likely consequence of this",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDL.a15bd4d6-af81-47b7-b62b-4f1e006cb17f"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "SQL Injection"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Memory Leaks"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "SYN Floods"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Cross Site Scripting"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Heartbleed"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "According to OWASP SQL injection is the most frequently occurring type of attack when there is inappropriate validation.\nCross Site Scripting also occurs but is less frequent.\n [The OWASP Top Ten provides this information( https://owasp.org/www-project-top-ten/2017/A1_2017-Injection )"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "You are designing a relational db and what to ensure that the design is logical, efficient, and optimized. WHich of the following processes should you follow to acheive this.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDL.fbbcc230-cb9a-48d6-b347-510d0c3035d7"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Top Down Approach"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Normalization"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Multiple Primary Keys"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Waterfall"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Baselines"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "This is a process where some data is converted into relational form by considering the constraints expected at different normal forms to make the data towards a normalized form. There are NF1,2,3,4,5"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following differentiates DOM based XSS from stored or reflected XSS attackes",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDL.13c68efb-0afc-46ed-8e39-17e70b46528b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "DOM based XSS is stored in the web server and sent to a victim when visiting the web page"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "DOM based XSS will not be visible in the HTML source of the page"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "DOM based XSS rely upon tricking a user into clickingon a malicious link"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "DOM based XSS are server side flaw and stored and reflected are client side"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Apparrently DOM based attacks write scripts into the DOM ...where they are not usually found. SOme browser versions dont handle this properly."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=hzFQap93VJM"
   }
  ],
  "value": ""
 },
 {
  "name": "You are completing design of the Security Architecture. What phase of the SDLC are you in?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDL.d0819d9f-58c2-4aca-b6fb-c4ef5775da6a"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Initiation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Development/Acquisition"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Implement/Assessment"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Operations & Maintenance"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Disposal"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=8A4c-vtz_U0"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following memory types is used by programs on your system.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDL.c26b10e5-59cd-4aa3-b254-5c9e3e008ddc"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Physical Addresses"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Logical Addresses"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Relative Addresses"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Indirect Addresses"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The OS manages memory and hides the actual physical or virtual (on disk) memory location behind a logical address"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=bs5xEHCmQPk"
   }
  ],
  "value": ""
 },
 {
  "name": "You have tasked your security team with reviewing an internal web application. If configured which of the following would be considered GREATEST weakness in the session management configuration",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDL.0d74f6d9-ab88-4f95-9fa2-d9aa62b84db4"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Sessions are tracked and managed in cookies"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "The server supports only HTTPS connections"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "The server does not run a client side firewall"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Sessions are managed via URL rewrites"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "If session info is in the URL sending a URL to someone means they access a site as yourself."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=1wkExUdaOVQ"
   }
  ],
  "value": ""
 },
 {
  "name": "An approach based on lean and agile principles in which business owners and the development, operations and quality assurance departments collaborate?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDL.47d92f97-ee2b-40e1-bcc5-e418b65543de"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Data Mining"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "DevOps"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Computer Virus"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Covert Channel"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.isc2.org/training/self-study-resources/flashcards/cissp/software-development-security"
   }
  ],
  "value": ""
 },
 {
  "name": "How can security violations due to object reuse be mitigated",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDL.a35aeae1-f2aa-433e-9b1c-7a613c31425b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Reboot the system after deleting a folder containing a large number of files."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Hold Option+Command while pressing delete on the MacOS system"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Hold the shift key while pressing delete on Windows"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Zeroize RAM after a process finishes using it."
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Delete the file and then create a new empty file with the same name"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "If we had secure information in RAM we zeroize it to eliminate possiblity it might be used by another process."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=XBOuWeR08QM"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is a characteristic of an interpreted programming language",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDL.fff298bd-93f6-4eb0-91bb-bb289a0e0faa"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Turns high level source code into binary executable"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Executes code one line at a time"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Are in the form of instructions executed directly by the hardware"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Converts assembly language into machine language"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=tx70EN2ph_Y"
   }
  ],
  "value": ""
 },
 {
  "name": "Software prototyping was introduced to overcome some limitations of the waterfall approach to software development.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDL.38faa4c5-d9d8-4874-bd9f-6f989b360c5d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Missing functionality may be identified"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Prototypes can be reused to build the actual application"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Requirements analysis is reduced"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Defects can be identified earlier, reducing time and cost of development"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "User feedback is quicker, allowing necessary changes to be identified sooner"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Flexibility of development allows project to be easily expand beyond plans"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A, D,E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": [
     {
      "value": "loss of the big picture"
     },
     {
      "value": "content in prototype doesnt make it into final code base"
     },
     {
      "value": "scope screep"
     }
    ]
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": "How do we maintain the bigger picture in our team (I think we lose site of this)"
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=ziefG4Qz9Pc&list=RDCMUCwUkAunxT1BNbmKVOSEoqYA&index=2"
   }
  ],
  "value": "Prototyping build successive iterations of an application that show its functionality often focusing on systems that have high levels of user interaction.\nThis approach has many benefits What are they. Choose 3"
 },
 {
  "name": "The Montreal Protocol, an international treaty in the 1980's endevours to protect the earths ozone layer from depletion. This includes the replacement of the Halon-base fire suppression systems. The EPA SNAP (Significant New Alternatives Policy) provides a list of alternatives. Which of the following are suitable Halon replacements according to SNAP? Pick 6",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.f93d44ab-dd45-46c4-a108-2dc68d55339f"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "BFR (Bromine Flame Retardent)"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Carbon Dioxide"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "FM-200"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Aero K"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Argonite"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "FM-100"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "FE-13"
   },
   {
    "name": "H",
    "attrs": [],
    "value": "HFC-32"
   },
   {
    "name": "I",
    "attrs": [],
    "value": "Inergen"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B,C,D,E,G,I"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=5_YqDgPJhW0&list=RDCMUCwUkAunxT1BNbmKVOSEoqYA&index=4"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following best describes the domains of a relation in a relational database?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.6632505b-509f-4b08-8ac4-855f681464a4"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "A named set of possible values for an attribute, all of the same type."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "All tuple in a relation"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "All the attributes of a relation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "The cardinality of a realtion"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "The degree of attributes in a relation"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=_JUfV0u_iPg"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is not a standard application hardening technique?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.b091a4cb-50c9-4822-8764-adac0f1b6063"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Encrypt Sensitive Information"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Conduct Cross Site Scripting"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Apply Security Patches Promptly"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Validate User Input"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-8-software-development-security-2018/quiz/urn:li:learningApiAssessment:4614258?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What software development methodology uses four stages in an iterative process?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.6653bd82-496f-47dd-9fe0-e604d66bb46b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Agile"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Spiral"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Waterfall"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "DevOps"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-8-software-development-security-2018/quiz/urn:li:learningApiAssessment:4613604?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What phase of the capability maturity model introduces the reuse of code across projects?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.5624c30b-3920-49e8-9446-265489d3e99e"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Optimizing"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Defined"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Initial"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Repeatable"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-8-software-development-security-2018/quiz/urn:li:learningApiAssessment:4613604?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What component of a change management program includes final testing that the software functions properly?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.2a834edc-72ae-466f-9576-3262b9f2f5db"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Change Management"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Iteration Management"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Request Management"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Release Management"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-8-software-development-security-2018/quiz/urn:li:learningApiAssessment:4613604?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the most effective defense against cross-site scripting attacks?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.24f41563-7e96-434d-bf89-d1df2441e505"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "input validation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "query parameterization"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "antivirus software"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "vulnerability scanning"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-8-software-development-security-2018/quiz/urn:li:learningApiAssessment:4615226?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What character is essential in input for a SQL injection attack?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.4d9232bd-daf5-47ba-b044-0e4e7b505ad8"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "exclamation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "quote"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "asterix"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "doublequotes"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The quote can be used to terminate one statement and start a new one."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-8-software-development-security-2018/quiz/urn:li:learningApiAssessment:4615226?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Alan is analyzing his web server logs and sees several strange entries that contain strings similar to ../../\" in URL requests. What type of attack was attempted against his server?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.554cd8e2-3b2c-4a71-a645-31c576f0b185"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "directory traversal"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "cross-site scriptiong"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "buffer overflow"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "SQL injection"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-8-software-development-security-2018/quiz/urn:li:learningApiAssessment:4615226?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which choice is not a significant risk associated with browser add-ons and extensions?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.385df37c-085f-49a7-ae08-9854d8c5d0ed"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "resale of legitmate extensions"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "sandbox execution"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "overly broad permissions"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "malicious author"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-8-software-development-security-2018/quiz/urn:li:learningApiAssessment:4615226?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Developers wishing to sign their code must have a ",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.c5cc83af-21a6-4887-968d-2001eac8faf2"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "patent"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "digital certificate"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "software license"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "shared secret"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-8-software-development-security-2018/quiz/urn:li:learningApiAssessment:4613603?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.e89f6f64-b183-4690-a800-460cf82b258e"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Unit Testing"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "UAT"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Load Testing"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Integration Testing"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-8-software-development-security-2018/quiz/urn:li:learningApiAssessment:4612787?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What portion of the change management process allows developers to prioritize tasks?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.041c2f7d-78bf-4aff-bc13-c28044ca3b52"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Release Control"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Configuration Control"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Request Control"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Change Audit"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The request control provides users with a framework to request changes and developers with the opportunity to prioritize those requests."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 21 Review Question 3\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What approach to failure management places the system in a high level of security?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.b9718aab-e6b0-4ed0-af48-31393104d65b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Fail Open"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Fail Mitigation"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Fail Secure"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Fail Clear"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "In a fail-secure state, the system remains in a high level of security until an administrator intervenes."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 21 Review Question 4\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What software development model uses a seven-stage approach with a feedback loop that allows progress one step backward?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.b12b3957-0f14-4db8-948f-788d52e18655"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Boyce Codd"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Waterfall"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Spiral"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Agile"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The waterfall model uses a seven-stage approach to software development and includes a feedback loop that allows development to return to the previous phase to correct defects discovered during the subsequent phase."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 21 Review Question 5\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Richard believes that a database user is misusing his privileges to gain information about the company’s overall business trends by issuing queries that combine data from a large number of records. What process is the database user taking advantage of?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.7f151b2f-6f5b-479d-9d3f-06530922a702"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Inference"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Contamination"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Polyinstantiation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Aggregration"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "In this case, the process the database user is taking advantage of is aggregation. Aggregation attacks involve the use of specialized database functions to combine information from a large number of database records to reveal information that may be more sensitive than the information in individual records"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 21 Review Question 8\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What database technique can be used to prevent unauthorized users from determining classified information by noticing the absence of information normally available to them?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.2be3aad3-a22e-4a39-9fc4-9a2160e131c3"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Inference"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Manipulation"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Polyinstantiation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Aggegration"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Polyinstantiation allows the insertion of multiple records that appear to have the same primary key values into a database at different classification levels."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 21 Review Question 9\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is not a principle of Agile development?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.d7f29669-e2f4-4ff9-bec5-b0db5aa012f8"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Satisfy the customer through early and continuous delivery"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Business people and developers work together"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Pay continuous attention to technical excellence"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Prioritize security over other requirements"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "In Agile, the highest priority is to satisfy the customer through early and continuous delivery of valuable software."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 21 Review Question 10\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What type of information is used to form the basis of an expert system’s decision-making process?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.a72b599e-0e60-4a7d-bef5-6df1ad474b78"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "A series of weighted layered computations"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Combined input from a number of human experts weighted according to past performance"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "A series of if/then rules codified in a knowledge base"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "A biological decision-making process that simulates the reasoning process used by the human mind"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Expert systems use a knowledge base consisting of a series of “if/then” statements to form decisions based on the previous experience of human experts."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 21 Review Question 11\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "In which phase of the SW-CMM does an organization use quantitative measures to gain a detailed understanding of the development process?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.1e7aded0-1c97-4149-8840-bd40d29ba247"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "initial"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "repeatable"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "defined"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "managed"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "In the Managed phase, level 4 of the SW-CMM, the organization uses quantitative measures to gain a detailed understanding of the development process."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 20 Review Question 12\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following acts as a proxy between an application and a database to support interaction and simplify the work of programmers?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.ab759ad0-8e73-40b8-bde4-9bfde359068d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "SDLC"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "ODBC"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "DSS"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Abstraction"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 20 Review Question 13\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "In what type of software testing does the tester have access to the underlying source code?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.d3c18c67-87f0-4ae2-8b64-2b87c7a6f1a8"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Static Testing"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Dynamic Testing"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Cross Site Scripting"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Black Box Testing"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 20 Review Question 14\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What type of chart provides a graphical illustration of a schedule that helps to plan, coordinate, and track project tasks?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.58c968d3-9cbd-44f3-ae30-4b9f3a19b653"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Gantt"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Venn"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Bar"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "PERT"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A. A Gantt chart is a type of bar chart that shows the interrelationships over time between projects and schedules. It provides a graphical illustration of a schedule that helps to plan, coordinate, and track specific tasks in a project."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 20 Review Question 15\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which database security risk occurs when data from a higher classification level is mixed with data from a lower classification level?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.6353856b-0129-4fa6-a425-f442013f3d69"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Aggregation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Inference"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Contamination"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Polyinstantiation"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Contamination is the mixing of data from a higher classification level and/or need-to-know requirement with data from a lower classification level and/or need-to-know requirement."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 20 Review Question 16\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What database security technology involves creating two or more rows with seemingly identical primary keys that contain different data for users with different security clearances?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.7857613f-efd6-471a-a1b7-022defccd868"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Polyinstantiation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Cell supresssion"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Aggregation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Views"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Database developers use polyinstantiation, the creation of multiple records that seem to have the same primary key, to protect against inference attacks."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 20 Review Question 17\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is not part of the change management process?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.f2140e07-3f8f-423b-8621-b49663d61efc"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Request Control"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Release Control"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Configuration Audit"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Change control"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Configuration audit is part of the configuration management process rather than the change control process."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 20 Review Question 18\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What transaction management principle ensures that two transactions do not interfere with each other as they operate on the same data?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.1810d72a-0316-4070-b379-05c5d40c212f"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Atomicity"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Consistency"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Isolation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Durability"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The isolation principle states that two transactions operating on the same data must be temporarily separated from each other such that one does not interfere with the other."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 20 Review Question 19\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Tom built a database table consisting of the names, telephone numbers, and customer IDs for his business. The table contains information on 30 customers. What is the degree of this table?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDLC.d59e90a0-89db-4ebb-ac8d-c6b3e507bd42"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "2"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "3"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Thirty"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Undefined"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The cardinality of a table refers to the number of rows in the table while the degree of a table is the number of columns."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 20 Review Question 20\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 }
]