export default [
 {
  "name": "Incident handling requires prioritization in order to address incidents in an appropriate manner and order. Which of the following is NOT an factor to determine prioritization",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.9c313238-d225-4876-a5d3-bf7da9f26d8b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Functional Impact of the Incident"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Information impact of the Incident"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Detectability of the Incident"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Recoverability from the Incident"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Which component of SCAP serves to provide a standardized reference for publicly known security vulnerabilities and exposures",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.04309bb7-a825-4774-9921-4f833c294a44"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "CVE"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "CCE"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "CPE"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "CVSSS"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "XCCDF"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Common Vulnerabilities and Exposures (CVE)\nCommon Configuration Enumeration (CCE) (prior web-site at MITRE)\nCommon Platform Enumeration (CPE)\nCommon Vulnerability Scoring System (CVSS)\nExtensible Configuration Checklist Description Format (XCCDF)\nOpen Vulnerability and Assessment Language (OVAL)"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not one of the rules of evidence",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.a5ef2a40-7588-45fb-8a0f-7434cad5e328"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Admissable"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Authentic"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Complete"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Auditable"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Reliable"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Believable"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": "Complete is an interesting idea which I think is missing in a lot of business presentations"
   }
  ],
  "value": ""
 },
 {
  "name": "Examine this IP package and indicated what type of attack it is. SourceIP: 200.16.44.200, DestIP: 200.16.44.200, SourcePort: 88, DestPort: 88, Syn: 1",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.84875bc9-012a-4cba-bbde-b6ccdfb21c70"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "TCP Syn Flood"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Fraggle"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "LAND"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Teardrop"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "MitM"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": [
     {
      "value": "A land attack is a remote denial-of-service (DOS) attack caused by sending a packet to a machine with the source host/port the same as the destination host/port."
     },
     {
      "value": "A teardrop attack is a denial-of-service (DoS) attack that involves sending fragmented packets to a target machine. Since the machine receiving such packets cannot reassemble them due to a bug in TCP/IP fragmentation reassembly, the packets overlap one another, crashing the target network device."
     },
     {
      "value": "MitM attach - Man in the middle attach"
     }
    ]
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Your system is configured to generate an audit log event whenever an attempt to log in with a disabled user account is made. What is this an example of?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.4f8f5ffc-34f1-4045-b61f-322f93a2510f"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "A performance baseline"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "A security breach"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Defense in depth"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "A clipping level"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Manadatory access control"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A clipping level is a threshold for normal mistakes a user may commit before investigation or notification begins. An understanding of the term clipping level is essential for mastery of the CISSP exam. A clipping level establishes a baseline violation count to ignore normal user errors."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Your company is cloud migrating many of its existing apps and services. Which of the following will allow moving of the apps and services to the cloud while still providing control of the underlying operation system on which the app/services run?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.ba6969f0-7697-4643-a976-e57fc4975c58"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Platform as a Service"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Infrastructure as a Service"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Software as a Service"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Storage as a Service"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "You are exploring deployment options for network based intrusion detection systems. One of your performance objectives is to minimize false positives as much as possible. Based on this requirement, which type of IDS should you considers?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.0452b5db-fea5-4351-adf0-d7a526d2e71a"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Behaviour Based"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Knowledge Based"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Host Based"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Anonomoly Based"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Application based"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Anomoly or Behaviour based systems are prone to false positives whereas Knowledge Based Systems are not because those are about match to known patterns where than just violations of 'normal' behavior."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=yLNBUJVOjAg"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following are most likely to decrease the effectiveness of an incident reporting system (Choose 2)",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.44255857-25aa-4a6a-9eb4-71ad49baa225"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Communicate the effectives of the incident reporting system"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Eliminate anonymity in reporting"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Simplified process for report submission"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Lack of clear communication on what needs to be reported"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Feedback on previously reported results"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B,D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=QYmoP77j6SY"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is most important in order to maintain the integrity and admissibilty of digital evidence?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.c75e5b90-ae79-4d90-b48b-ee47e3285e2d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Use on NIST approved algorithms for hashing files"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Maintain chain of custody"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Never interact with live system"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Hash all the files before examing the original disk"
   },
   {
    "name": "E",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=QYmoP77j6SY"
   }
  ],
  "value": ""
 },
 {
  "name": "You have been tasked with reducing the likelihood that nodes in your network can forward packets with spoofed source IP addresses. Which of the following is the best way to accomplish this.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.e630e3da-e229-4619-93a7-28ea550c98b2"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Use SNMP to generate a list of allowed MACs for each VLAN"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Implement ACLs on each router interface allowing only traffic sourced from the local segment."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Enable MLD snooping on Layer2 switches"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Configure Reverse Path forwarding on the routers"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "In Reverse Path Forward a router looks at the Source IP address of a packet and asks itself would it route a packet destined for the Source IP address via this interface. if they answer is no then the packet is dropeed."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=fncd6XVXkdY"
   }
  ],
  "value": ""
 },
 {
  "name": "You have a building which has a higher likelihood of electrical fire than other types. Given this what class of fire suppression system should you have.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.1fbf161f-7900-4be1-b11d-fb98662ba6f6"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Class A"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Class B"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Class C"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Class D"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Class K"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=vmIM3v22ivI"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following best describes an electrical blackout?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.ee088061-84f4-48b5-91cc-babc1867cde3"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "A reduction in voltage that lasts minutes or hours"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "A prolonged loss of electrical power"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "A momentary low-voltage condition lasting only a few seconds"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "A momentary loss of electrical power"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=vmIM3v22ivI"
   }
  ],
  "value": ""
 },
 {
  "name": "A new incident investigator is preparing her incident 'jump kit'. Which of the following is least likely to be in the kit.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.6cf35d92-9a2a-4e6c-bd59-792e655716d6"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Network Diagrams and List of critical equipment"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Hashes of critical files"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Baseline documentation for systems, applications and network equipment"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Copies of well-known and common malware for comparison"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "A laptop loaded with packet sniffers and forsenics software"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Drive Imaging Tools"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "There is probably no reason to have copies of malware"
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=g7HDe-gl_VM"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is NOT one of the Incident Response  Life Cycle",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.afc3ac91-1a99-4fde-aa4d-08ad8e008125"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Containment, Eradication and Recovery"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Preparation"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Detection and Analysis"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Prevention"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Post-Incident Assessment"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=g7HDe-gl_VM"
   }
  ],
  "value": ""
 },
 {
  "name": "Audit logs are generated by most computer systems. Which of the following is the most important reason to implement logging?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.146cdc14-e877-4a49-af2c-7d278b96dec3"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Mutual Authentication"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Individual Accountability"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Origin Authentication"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Preserve Data Integrity"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=R3JrsyWXrUc"
   }
  ],
  "value": ""
 },
 {
  "name": "In addition to IP addresses, DHCP servers provide network nodes with additional useful information such as DNS servers address, default gateway and netbios node type. Which of the following is not a legitimate attack that may be executed against your DHCP deployment? (Choose 3)",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.1e47f560-7d52-42ee-95fb-bb5784399a9d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "A rogue DHCP server on your network can offer IP addresses to legitmate users, thereby creating DoS of MitM situation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "An attacker may gain control of your DHCP server and reconfigure the options assigned to clients."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Using TCP redirects, an attacker can send client DHCP packets to a remote DHCP server"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "A rogue DHCP server can be used to reconfigure SMTP connection settings for internal email systems."
   },
   {
    "name": "E",
    "attrs": [],
    "value": "An attacker can request multiple IP addresses from the legitimate DHCP server thereby exhausing the IP address pool"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "An attacker can send negative acknowledgements whenever a client attempts to renew an IP address"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C,D,F"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "As DHCP is not TCP based (rather UDP) option C is not a concern.\nDHCP doesnt relate to SMTP\nIt would be necessary to be on the network link to spoof an acknowlegement."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=FpOLERNDFMM"
   }
  ],
  "value": ""
 },
 {
  "name": "A new admin in your enterprise  has asked you about an entry she discovered in an /etc/hosts file on a users ubuntu workstation.  The entry read: 'evilapp.evilsite.com: 127.0.0.1' Which of the following is the BEST response to the admin?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.9e0e361c-c974-4e01-a5ce-1279da996a67"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "The entry is null route to the hostname specified"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "it is a sinkhole entry to protect the the Linux system"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "The system has been comprised by an attacker"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "It forces traffic to that site to be route out the interface 127.0.0.1"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The entry prevents ask to that site."
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=jpasqBWKJCg"
   }
  ],
  "value": ""
 },
 {
  "name": "You have determined that a user current unknown has been modifying and in some cases deleting files without authorization. Which of the following will not help mitigate the problem. Choose two.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.636823fd-5712-4388-a771-363eafd8e4ac"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Encrypt the files to prevent unauthorized deletion."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Configure user level auditing for files/folders"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Digitally sign the files to prevent unauthorized modification."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Set appropriate file/folder permissions in the file system."
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Maintain backsup of the data."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A,C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=l1ZLBwog1xU"
   }
  ],
  "value": ""
 },
 {
  "name": "Packet Filtering Firewalls have several limitations that make them less appropriate than more modern solutions when protected internal resources from the internet threats. Which of the following are short comings of packet filtering? (Choose Two)",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.40b0d7db-49a3-4540-af57-353c98f8a574"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "They control access based on source IP address and cannot verify if the address is being spoofed."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "They use reverse path forwarding lookups."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "They are stateless"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "They do not support logging packets that match firewall rules."
   },
   {
    "name": "E",
    "attrs": [],
    "value": "They are stateful."
   },
   {
    "name": "F",
    "attrs": [],
    "value": "They defend against TCP syn flood attacks which reduces there effective throughtput."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A,C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=jP9hzYCbN5I"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following are UDP based? (Choose 6)",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.d2d2d072-d5a0-440a-834e-78756b6c9184"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "BGP"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "RADIUS"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "SMB"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "IMAP4"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "SNMP,"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "NTP"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "TFTP"
   },
   {
    "name": "H",
    "attrs": [],
    "value": "DNS"
   },
   {
    "name": "I",
    "attrs": [],
    "value": "SMTP"
   },
   {
    "name": "J",
    "attrs": [],
    "value": "DHCP"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B,E,F,G,H,J"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "RADIUS ports 1812 and 1813\nIMAP4 port 143 or 993(SSL)\nSNMP port 162\nNTP port 123\nDNS port 53\nTFTP port 69 (trival file transfer protocol)"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=jP9hzYCbN5I"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not a component of a configuration management system?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.3a546dca-ae00-4f0a-aa87-3fc474ac64f9"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Version Control Mechanism"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "System Threat Modeling"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Automated Deployment Scripts"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Change history logging"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=aLfDayIwZnQ&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=23"
   }
  ],
  "value": ""
 },
 {
  "name": "You are evaluating the merits of differential and incremental backup strategies.  Which of the following is true?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.3e2ec968-351f-4fbd-a258-a9e6f949c4a6"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Differential begin with a full back and incrementals do not"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Incremental backups do not evalate the archive bit when determining if a file should be backed up"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Differentials backups only backup files modified since the previous differenetial of full backup"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Compared to differentials, a complete restore will take longer if using an incremental strategy"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "A 'copy' backup cannot be used if using a differential backup strategy"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Differentials back up files since last full or last incremental backup.\n Incremental backups require full back and all incremental backups"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=Vq1ECMX-iG4"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following best defines a Recover Point Objective?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.8b1dc255-f09c-42cb-91f9-3681bf4bdd7c"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Maximum amount of time a business process can be unavailable"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Maximum amount of time to recover a business process"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "The amount of time needed to verify a system/data after recovery"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Measure of time indicating the maximum amount of data that can be lost"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "MTD - (Maximum Tolerable Downtime) - Maximum amount of time a business process can be unavailable\nRTO - Maximum amount of time to recover a business process\nWTR - (Work Receovery Time) - The amount of time needed to verify a system/data after recovery"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=stf9UlkYYn0"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of investigation would typically be launched in response to a report of high network latency?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.2fd0039e-5158-46ba-be95-802cb7cc70f1"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Civil"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Operational"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Regulatory"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Criminal"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4594149?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Server logs are an example of ",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.96644989-2185-4805-a905-2245e27ebe5c"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Testimonial"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Documentary"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Expert Opinion"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Real"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4594149?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which evidence source should be collected first when considering the order of volatility?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.5dad8d56-1dec-48cc-b8e2-eb6d83296b85"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Process Information"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Logs"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Memory Contents"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Temporary Files"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "What type of technology prevents a forensic examiner from accidentally corrupting evidence while creating an image of a disk?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.af4f8e91-3de9-4cec-aa0c-cbcabea7e624"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "hashing"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "evidence log"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "sealed container"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "write blocker"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4594149?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Three of these choices are data elements found in NetFlow data. Which is not?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.42671402-184d-4df4-a367-c268184a5df9"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Packet Contents"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Destination Address"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Source Address"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Amount of Data Transfered"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4594149?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Federal law requires U.S. businesses to report verified security incidents to US-CERT.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.2993d07c-eb9a-4ffc-af14-8aa0b055d57c"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "True"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "False"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4594149?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "During what phase of ediscovery does an organization share information with the other side?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.8877bf13-3d52-43b6-abd1-aca09eaf87fe"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Collection"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Analysis"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Production"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Preservation"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4594149?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "During what phase of continuous security monitoring does the organization define metrics?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.b0b02852-24ed-4cd7-8942-69c169f48cb7"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Analyze/Report"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Establish"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Implement"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Define"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4595102?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What DLP technique tags sensitive content and then watches for those tags in data leaving the organization?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.f0a0275e-3b63-4ecb-9912-181365697090"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Host Base DLP"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "watermarking"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "intrusion detection"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Pattern recogniation"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4595102?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of hypervisor runs directly on top of bare hardware?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.c6465ae8-e1bf-482d-ad2d-d23aaeadcc4b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Type 4"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Type 1"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Type 2"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Type 3"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4592711?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What additional patching requirements apply only in a virtualized environment?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.6bffd6bc-ccf6-4d3b-9844-7c6afd052ca6"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Patching the hypervisor"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Patching Firewalls"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Patching Apps"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Patching the OS"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4592711?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which public cloud computing tier places the most security responsibility on the vendor?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.385523fb-1054-43b9-8a87-c0cfa21d1bfc"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "IaaS"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "IDaaS"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "SaaS"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "PaaS"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4592711?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What security principle most directly applies to limiting information access?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.364679fc-a38a-458e-88bf-59b2d361b8d7"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Two person control"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "separation of duties"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "least privilege"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "need to known"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4594150?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What security principle requires two individuals to perform a sensitive action?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.0ca091d6-bcf5-4cfa-9a3b-36d171b6495f"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "2 person control"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "separation of duties"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "least privilege"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "need to know"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4594150?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following individuals would not normally be found on the incident response team?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.e0d10f59-044d-4d65-a36e-120ca32df26a"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Legal Counsel"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Human Resource Staff"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Information security professional"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "CEO"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4595103?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "During an incident response, what is the highest priority of first responders?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.2f0bac6f-a8d6-4a2a-9014-8a85f7f56f6a"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Identifying Root Cause"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Containing the damage"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Collecting Evidence"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Restoring operations"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4595103?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following tools is used primarily to perform network discovery scans?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.84238ca0-19f9-49fb-adc2-da62d00c76fc"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Nmap"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Nessus"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Metasploit"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "lsof"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 15 Review Question 1\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Adam recently ran a network port scan of a web server running in his organization. He ran the scan from an external network to get an attacker’s perspective on the scan. Which one of the following results is the greatest cause for alarm?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.7ea1883d-05a4-4d7e-a6d7-d37eadd8b069"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "80/filtered"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "22/filtered"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "433/open"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "1433/open"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Only open ports represent potentially significant security risks. Ports 80 and 443 are expected to be open on a web server. Port 1433 is a database port and should never be exposed to an external network."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 15 Review Question 2\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following factors should not be taken into consideration when planning a security testing schedule for a particular system?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.114ed533-cc87-491d-aa36-000d5c3f4dab"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Sensitivity of the information stored on the system"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Difficulty of performing the test"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Desire to experiment with new testing tools"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Desirability of the system to attackers"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The sensitivity of information stored on the system, difficulty of performing the test, and likelihood of an attacker targeting the system are all valid considerations when planning a security testing schedule. The desire to experiment with new testing tools should not influence the production testing schedule."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 15 Review Question 3\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is not normally included in a security assessment?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.3471e7d9-8b8e-48cd-8ab1-aed94e2b003f"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Vulnerability Scan"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Risk assessment"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Mitigation Vulnerabilities"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Threat assessment"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "C. Security assessments include many types of tests designed to identify vulnerabilities, and the assessment report normally includes recommendations for mitigation. The assessment does not, however, include actual mitigation of those vulnerabilities."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 15 Review Question 4\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Who is the intended audience for a security assessment report?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.e7317cc1-68b2-4199-a532-64bbff150c0d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Management"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Security Auditor"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Security Professional"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Customers"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Security assessment reports should be addressed to the organization’s management. For this reason, they should be written in plain English and avoid technical jargon."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 15 Review Question 5\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Beth would like to run an nmap scan against all of the systems on her organization’s private network. These include systems in the 10.0.0.0 private address space. She would like to scan this entire private address space because she is not certain what subnets are used. What network address should Beth specify as the target of her scan?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.aa0ddee5-f507-4c98-be38-5fef27e7e05d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "10.0.0.0/0"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "10.0.0.0/8"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "10.0.0.0/16"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "10.0.0.0/24"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The use of an 8-bit subnet mask means that the first octet of the IP address represents the network address. In this case, that means 10.0.0.0/8 will scan any IP address beginning with 10."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 15 Review Question 6\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Alan ran an nmap scan against a server and determined that port 80 is open on the server. What tool would likely provide him the best additional information about the server’s purpose and the identity of the server’s operator?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.ccbcfaea-b581-4729-b151-e072fa8064ea"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "SSH"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Web Browser"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "telnet"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "ping"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The server is likely running a website on port 80. Using a web browser to access the site may provide important information about the site’s purpose."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 15 Review Question 7\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What port is typically used to accept administrative connections using the SSH utility?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.f73b8168-e059-40fb-9c90-21dbe7982e1c"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "20"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "22"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "25"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "80"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 15 Review Question 8\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following tests provides the most accurate and detailed information about the security state of a server?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.61a46f1d-286d-4dba-8d71-398e9fafcd69"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Unauthenticated scan"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Port scan"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Half-open scan"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Authenticated scan"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Authenticated scans can read configuration information from the target system and reduce the instances of false positive and false negative reports."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 15 Review Question 9\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What type of network discovery scan only follows the first two steps of the TCP handshake?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.6a97e3f9-f4a9-4b0f-9f4f-5ba6cc71d95b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "TCP connection scan"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Xmas scan"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "TCP Syn Scan"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "TCP Ack San"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The TCP SYN scan sends a SYN packet and receives a SYN ACK packet in response, but it does not send the final ACK required to complete the three-way handshake.\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 974). Wiley. Kindle Edition."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 15 Review Question 10\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Matthew would like to test systems on his network for SQL injection vulnerabilities. Which one of the following tools would be best suited to this task?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.c16f7cdf-a681-48ca-9648-4fbf0cc068a7"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Port Scanner"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Network Vulnerablity Scanner"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Network discovery scanner"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Web vulnerablity scanner"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "SQL injection attacks are web vulnerabilities, and Matthew would be best served by a web vulnerability scanner. A network vulnerability scanner might also pick up this vulnerability, but the web vulnerability scanner is specifically designed for the task and more likely to be successful."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 15 Review Question 11\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Badin Industries runs a web application that processes e-commerce orders and handles credit card transactions. As such, it is subject to the Payment Card Industry Data Security Standard (PCI DSS). The company recently performed a web vulnerability scan of the application and it had no unsatisfactory findings. How often must Badin rescan the application?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.21ab0d2d-b99b-4b5e-9d57-2f8e7e556ab7"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Only if the appication changes"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "At least monthly"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "At least annually"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "There is no rescanning requirement"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "PCI DSS requires that Badin rescan the application at least annually and after any change in the application."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 15 Review Question 12\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Grace is performing a penetration test against a client’s network and would like to use a tool to assist in automatically executing common exploits. Which one of the following security tools will best meet her needs?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.03364d32-b755-42f9-a74c-5db90d774e5e"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "nnap"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "metasploit"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "nessus"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "snort"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": "Metasploit is an automated exploit tool that allows attackers to easily execute common attack techniques."
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 15 Review Question 13\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Paul would like to test his application against slightly modified versions of previously used input. What type of test does Paul intend to perform? Code review",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.01b1e58a-4bd5-401b-aa4e-f3c13310d709"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Code Review"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Application Vulnerability Review"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Mutation Fuzzing"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Generational Fuzzing"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Mutation fuzzing uses bit flipping and other techniques to slightly modify previous inputs to a program in an attempt to detect software flaws."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 15 Review Question 14\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Users of a banking application may try to withdraw funds that don’t exist from their account. Developers are aware of this threat and implemented code to protect against it. What type of software testing would most likely catch this type of vulnerability if the developers have not already remediated it?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.e43305b3-c5f3-4bbd-9a28-4430bd31496b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Misuse case testing"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "user interface testing"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "physical interface testing"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "security inteface testing"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Misuse case testing identifies known ways that an attacker might exploit a system and tests explicitly to see if those attacks are possible in the proposed code."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 15 Review Question 15\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What type of interface testing would identify flaws in a program’s command-line interface?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.30bb57df-122c-4c2d-b9da-2050bd254831"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Application Programming Interface Testing"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "User interface testing"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Physical Interface Testing"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Security Interface Testing"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "User interface testing includes assessments of both graphical user interfaces (GUIs) and command-line interfaces (CLIs) for a software program."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 15 Review Question 16\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "During what type of penetration test does the tester always have access to system configuration information?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.6095e0f2-9330-465b-8784-87654cdfcdb3"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Black Box"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "White Box"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Gray Box"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Red Box"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 15 Review Question 17\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is the final step of the Fagan inspection process?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.ce73bd95-5fd9-4064-97c7-e622a5f5cd46"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Inspection"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Rework"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Follow-Up"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "None of the above"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 15 Review Question 19\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What information security management task ensures that the organization’s data protection requirements are met effectively?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.1751890d-9647-4ee7-9706-576d6f0669f6"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Account Management"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Backup Verification"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Log Review"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Key Performance Indictors"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The backup verification process ensures that backups are running properly and thus meeting the organization’s data protection objectives."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 15 Review Question 20\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What is the most commonly used technique to protect against virus attacks?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.4f2c5d0f-ebfc-44a0-a264-ffcad16bc463"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Signature Detection"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Heuristic Detection"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Data integrity assurance"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Automated Reconstruction"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Signature detection mechanisms use known descriptions of viruses to identify malicious code resident on a system."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 21 Review Question 1\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "You are the security administrator for an e-commerce company and are placing a new web server into production. What network zone should you use?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.c243ab0b-1533-44e1-97bb-36d7467c0444"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Internet"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "DMZ"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Intranet"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Sandbox"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 21 Review Question 2\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following types of attacks relies on the difference between the timing of two events?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.3fcff942-b99a-409e-950b-b72be6942100"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Smurf"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "TOCTTOU"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Land"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Fraggle"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 21 Review Question 3\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following techniques is most closely associated with APT attacks?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.c0769ce8-16e4-4ffd-9bdc-03796b72e30d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Zero-day exploit"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Social Engineering"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Trojan Horse"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "SQL Injection"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "While an advanced persistent threat (APT) may leverage any of these attacks, they are most closely associated with zero-day attacks."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 21 Review Question 4\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What advanced virus technique modifies the malicious code of a virus on each system it infects?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.19d1f13f-d5f2-40a5-a9e6-af3e49667c5f"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Polymorphism"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Stealth"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Encryption"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Multipartitism"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "In an attempt to avoid detection by signature-based antivirus software packages, polymorphic viruses modify their own code each time they infect a system."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 21 Review Question 5\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following tools provides a solution to the problem of users forgetting complex passwords?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.158ee2fe-1418-4682-ad69-01141fa18d95"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "LastPass"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Crack"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Shadow password files"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Tripwire"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "LastPass is a tool that allows users to create unique, strong passwords for each service they use without the burden of memorizing them all."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 21 Review Question 6\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What type of application vulnerability most directly allows an attacker to modify the contents of a system’s memory?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.3aebeb50-1594-44b7-8432-c44833a4d67a"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Rootkit"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Back door"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "TOCTOU"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Buffer Overflow"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Buffer overflow attacks allow an attacker to modify the contents of a system’s memory by writing beyond the space allocated for a variable."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 21 Review Question 7\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What technique may be used to limit the effectiveness of rainbow table attacks?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.11b4470a-bbd9-4143-ae1c-f3386b956cab"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Hashing"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Salting"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Digital Signature"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Transport Encryption"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Salting passwords adds a random value to the password prior to hashing, making it impractical to construct a rainbow table of all possible values."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 21 Review Question 8\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What character should always be treated carefully when encountered as user input on a web form?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.9fe60b3a-6f02-4b31-983a-3b7a1d639658"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Exclamation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Ampersand"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Quote"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Apostrophy"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The single quote character (') is used in SQL queries and must be handled carefully on web forms to protect against SQL injection attacks."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 21 Review Question 10\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What database technology, if implemented for web forms, can limit the potential for SQL injection attacks?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.f16fc34a-034b-4c2b-a55f-0021aa908c04"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Triggers"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Stored Procedure"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Column Encryption"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Concurrency Control"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Developers of web applications should leverage database stored procedures to limit the application’s ability to execute arbitrary code. With stored procedures, the SQL statement resides on the database server and may only be modified by database administrators."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 21 Review Question 11\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What condition is necessary on a web page for it to be used in a cross-site scripting attack?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.5241b832-13e2-417b-ae1e-f9adef03c0e1"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Reflected Input"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Database driven content"
   },
   {
    "name": "C",
    "attrs": [],
    "value": ".Net Technology"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "CGI Scripts"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 21 Review Question 13\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What type of virus utilizes more than one propagation technique to maximize the number of penetrated systems?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.73d05727-bc7c-4609-a87c-1b7a3ec9b29e"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Stealth Virus"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Companion Virus"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Polymorhic Virus"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Multipartite virus"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Multipartite viruses use two or more propagation techniques (for example, file infection and boot sector infection) to maximize their reach."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 21 Review Question 14\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What worm was the first to cause major physical damage to a facility?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.d680d9d2-403b-495c-8656-4db8b8bdbe75"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Stuxnet"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Code Red"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Melissa"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "RTM"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Stuxnet was a highly sophisticated worm designed to destroy nuclear enrichment centrifuges attached to Siemens controllers."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 21 Review Question 16\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Ben’s system was infected by malicious code that modified the operating system to allow the malicious code author to gain access to his files. What type of exploit did this attacker engage in?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.f7042693-a796-4881-aba4-32b72fad9dd0"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Escalation of Privilege"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Back door"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Root kit"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Buffer overflow"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 21 Review Question 17\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "When designing firewall rules to prevent IP spoofing, which of the following principles should you follow?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.3225644f-3cf8-47a4-b102-e2186eea5b37"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Packets with internal source IP addresses don’t enter the network from the outside."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Packets with internal source IP addresses don’t exit the network from the inside."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Packets with public IP addresses don’t pass through the router in either direction."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Packets with external source IP addresses don’t enter the network from the outside."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Packets with internal source IP addresses should not be allowed to enter the network from the outside because they are likely spoofed."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp 21 Review Question 20\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 }
]