export default [
 {
  "name": "You need to provide internet connectivity fo workstations, devices and linux servers. You are allocated a public network with a /28 prefix. There are 2000 employess. Which of the following will allow this.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "CMS.d34f9706-00c3-4998-a860-7a661ca1dd9d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "NAT - Configure static translations"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "PAT = Translate MAC,IP & TCP/UDP Ports"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "HTTP Proxy - Forward all internet traffic to the proxy"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "NAT - translate ip addresses dynamically from a pool"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "PAT - Translate IP & TCP/UDP ports to one or more public addresses"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "SOCK Proxy - Relay all device traffic through a SOCK Proxy"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "As sessions are established the network device can map the device IP address and port to one of the 14 available public IPS and a specific port.\n NAT alone doesnt work because there arent enough IP addresses.\nHTTP Proxy is closed but only works for IP traffic\nSOCKS is possible but to uncommon to be used."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Your admin is concerned about data being compromised if the servers private key is obtained. Which of the following is the BEST way to mitigate this issue.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "CMS.19f87bbd-1169-4ee4-b9a8-c7a7c503f1db"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Increate key size to 2048"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Install a new certificate with a lifetime not longer than 90 days"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Enable perfect forward secrecy on the HTTPS server"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Enable certificate pinning"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Perfect forward secrecy is a mechanism to use empheral private/public key pairs over https rather than using a single key pair."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "802.2 and 802.3 correspond to which of the following standards",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "CMS.7facfd8b-8fd3-4f70-a83c-179e741af539"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "802.2 is a security standard for port based access control"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "802.3 is a wired ethernet standard for Media Access Control (MAC)"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "802.2 is a LAN standard for token ring"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "802.3 is a wireless standanrd for wireless LANs"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "802.2 is an internet standard for Logical Link Control"
   },
   {
    "name": "Fm",
    "attrs": [],
    "value": "802.3 is a wireless standard for bluetooth"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "802.2 is an wireless standard for WiMax"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B, E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "802.3 is standard for VLAN\n802.1x is port based access control\n802.5 is token ring\n802.15 is bluetooth\n802.16 is wimax\n802.1q is VLAN"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Which of these are NOT characteristics of IPV6",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "CMS.5612f193-37dd-4bdf-bc48-6109373e8a7c"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Fixed Size Header"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "No IPV6 header checksum"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "128 bit source address"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "16-bit TTL field in Header"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Extension headers"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "20 bit flow label"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "IPV6 Header is fixed to 40 bytes\nIPV6 does not have checksums as there are checksums at other layers\n128 bit source address\nThere is a 20 bit flow label in IPV6\n In IPV6 the TTL field from IPV4 has become Hop Limit. It is only 1 byte"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following are characteristics of 'star' topologies",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "CMS.226306b9-1453-4605-9b6b-d135f1a80ead"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Collision Free"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Resistant to multi-node failure"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Can only be used with copper cabling"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Limited reliance on central aggregegated device"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Collisions can happen between router/switch and node.\nAny cabling can be used.\nThere is reliance on the route/switch at the center of the star"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not likely to be found in a DMZ.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "CMS.63645adb-6e57-465f-aca4-6978cbc8ab8c"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Front end smtp relay"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "DNS"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Web Server"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "FTP Server"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Active Directory"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Front end smtp relay to do initial mail processing\nDNS - yes to provide resolution to the internet of your sites although DNS is generally provided by another provide\nWeb Server - yes\nFTP Server - yes\n AD Domain Controller - No"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": "Only in large organization does having a DMZ make sense. Even though does it make sense for a large organization to have their own DMS rather than the large scale of a service provider."
   }
  ],
  "value": ""
 },
 {
  "name": "You have developed a web app for a customer to view private information, you want to minimize chances attacker  within radio range will be able to eaves drop the wireless Lan and intercept their private data. Which is the best approach to acheive this.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "CMS.c7676d70-a7da-4319-b7a5-cbd7144cafc5"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Require SSH for all connections to the web server"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Implement TLS on the web server"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Advise customers to implement WPA2 with AES on their wireless Lans"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Digitally sign all traffic with RSA keys"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Require customers to connect vai IP Sec VPN"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Implement 802.1x on your switches and provide steps for your customers to do the same"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "Use an IDS to monitor for evidence of ARP flooding attacks"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=Qt02oe2f2yQ"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "WHich of the following firewall types provides you the LEAST amount of control over network traffic",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "CMS.3e1fc379-59d8-4624-8fe6-968c2da78d09"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Application Layer Firewalls"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "MAC filters on a L2 switch"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Packet Filtering routers"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Stateful firewalls"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Proxy Servers"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "MAC Filter is not a firewall\nPac"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=lbxGIFYrAqw"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of these best describes the function of the domain name system",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "CMS.db538a1a-8c72-4d28-af67-cc47c71cb938"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Resolve computer NetBios names to IP addresses"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Centralized database of domain names managed by ICANN"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "A distributed hierichal database for resolving fully qualified domain names"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "An LDAP database used for storing objects and their attributes"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "A port-to-service mapping agent that locates network services in a distributed network"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "You need to establish a transparent link between two physical sites. User at each site need to be able to able a variety of servers, printers and other IP enabled equipment at each location.  Minimizing user involvement is a key consideration.  Both sites have internet connectivity. WHich is the best option",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "CMS.573cc41f-5b97-468b-a478-b442e80e6d81"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "A SOCK5 proxy hosted at a cloud service provider"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Encrypted Remote Desktop Connections"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Configure a reverse proxy at each location to offer services to users"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Individual TLS VPN connections on a per-user basis"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "An IPSec VPN between the 2 sites"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is true regarding firewalls?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.dce89f0b-462c-466d-ba71-d4804898621e"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "They can protect a network at the internet layer of the TCP/IP networking model"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "They optimize performance at the internal/external trust boundary."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Firewalls eliminate all network based attacks from external networks"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Firewalls are optimized, standalone devices"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "They can be configured as the default gateway for external nodes"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "In the DoD TCP/IP Network model the internet layer is the network layer in the OSI Model."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is a benefit of implementing DNSSEC (DNS Security)",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.282baa90-2514-4ec7-ab44-c2273bfb2780"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Using encryption, DNSSEC prevents service providers from mining your DNS queries"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "DNSSEC prevents you from going to malicious web sites by redirecting your connections attempts"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "DNSSEC speeds up name resolution by compressing queries and answers"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "DNSSec authenticates server response using digital signatures"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "IPSec is comprised of a number of different protocols which work collectively to establish a level of security desired by a system administator. Which of the following components of an IPsec connection is responsible for authenticating parties and esablishing security associations?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COMS.cc06beda-880c-4536-98dc-e4d013e67a0f"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Authentication Header (AH)"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Encapsulating Security Payload (ESP)"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Internet Key Exchanges (IKE)"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "User Datagram Protocol (UDP)"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Diffie-Helman Key Exchange"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "IKE - negotiating key exchange\n AH - mechanism to provide integrity of a message\nESP - deals with confidentiality\nIKE may used Diffie Helman as the negotiated approach but IKE is the mechanism"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=QvZZ3UG2gGc"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following algorthms were considered by NIST to become the new Advance Encryption Standard (choose 4)",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.f49394ca-17fd-474c-9cfa-0b3677d88e20"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "RC6"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Blowfish"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Twofish"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Serpent"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "scrypt"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Rijndael"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "Whirlpool"
   },
   {
    "name": "H",
    "attrs": [],
    "value": "SHA-512"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A,C,F,D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "A server admin has reported a persistent and large number of half open TCP connections coming from many different Internet IP addresses. Which is the best way to address this issues?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COMS.f3ebec42-211a-4842-8e8c-914e011eea57"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Configure the app to retransmit slower connections over UDP"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Increase the installed RAM on the web server"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Decrease the size of the TCP backlog queue"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Enable reverse path forwarding on the internet router"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Enable SYN flood protection on an upstream firewall"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The upstream firewall keeps track of half open connections to a server and drops new ones if the limit is exceeded.\nNot a great solution because legitimate requests may be dropped in an attack."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=1wkExUdaOVQ"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is a characteristic of both TCP and UDP (choose 2)",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.9742214c-e77c-4e2d-8901-a788f1d6fb3c"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Both use sequence and acknowledgement numbers"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Both carrry DNS payloads"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Both implement windowing"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Both implement a header checksum"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Both use control bits to track connection state"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B,D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Normal DNS name resolution is over UDP but Zone Transfer occur over TCP\nYes both use a header checksum.\n Windowing (recipient requesting different packet size) only happens in TCP."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=HGtxaxO70rw"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not a benefit of FHRP (First Hop Redundancy Protocol) in your network infrastructure?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.b9a2bfd4-906d-4974-9c07-d337ebbf7e5e"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Hosts always have at least 2 gateway IP addresses for fault tolerance"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Routers share a virtual IP address allowing either one to use the address"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "A standy router providers failover support to the active router"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "An active router can be manually preempted for hardware maintenance tasks"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "FHRP - a virtual router is defined and the virtual gateway address is shared beween some number of routers. One router is active while the others are inactive until they sense the active one is offline and they take over.\n HSRP- Hot Standy Routing Protocol\nVRRP - Virtual Redundant Routing Protocol\nGLBP = Gateway Load Balancing Routing Protocol"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "ICMP Echo Request sent to the network broadcast address  of a spoofed victim causing all nodes to respond to the victim with an echo reply",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.8891d8b3-6afc-43a5-afd7-e34788d14376"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Smurf"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Bit"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Switches"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Frame"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.isc2.org/training/self-study-resources/flashcards/cissp/communication-and-network-security"
   }
  ],
  "value": ""
 },
 {
  "name": "Data represented at layer 4 of the open system interconnetion (OSI) model",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.c05c926b-7996-4c5a-bdb9-d1349a9dc564"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Switches"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Concentrators"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Firewalls"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Segment"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.isc2.org/training/self-study-resources/flashcards/cissp/communication-and-network-security"
   }
  ],
  "value": ""
 },
 {
  "name": "Provides a standard method for transporting multiprotocol datagrams over point to point",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.3b0071c1-e977-403d-892e-c5233ea0091a"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Multiprotocol Label Switching (MPLS)"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Port Address translation (PAT)"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Point to point protocol (PPP)"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Internet Control Message Protocol"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.isc2.org/training/self-study-resources/flashcards/cissp/communication-and-network-security"
   }
  ],
  "value": ""
 },
 {
  "name": "Separates network into three components: raw data, how the data is sent and what purpose the data serves. This involves focus on data, control and application management functions or 'planes'?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.072569a5-66ab-4fe6-b067-553bb06de65d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Port Address Translation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Software Defined WAN"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "WIFI"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Software Defined Networks"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.isc2.org/training/self-study-resources/flashcards/cissp/communication-and-network-security"
   }
  ],
  "value": ""
 },
 {
  "name": "What is a lightweight encapsulation protocol and lacks the reliable data transport of the TCP layer?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.ee198893-665a-4c25-84b2-cb944f0e8475"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Fibre Channel of Ethernet"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Port Address Translation"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "MultiProtocol Label Switching"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Code Division Multiple Access"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.isc2.org/training/self-study-resources/flashcards/cissp/communication-and-network-security"
   }
  ],
  "value": ""
 },
 {
  "name": "which of the following are not private IP addresses defined by RFC 1918? Choose all that apply",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.4b75e180-4320-42fa-8417-c99eb5facf04"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "172.32.31.45"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "192.188.4.3"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "10.16.31.22"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "169.254.67.89"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "172.29.35.9"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "131.107.33.1"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "192.168.44.200"
   },
   {
    "name": "H",
    "attrs": [],
    "value": "172.6.32.1"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A,B,D,F,H"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=se14Kog6raE"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following does Perfect Forward Secrecy not involve?  (choose several)",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.ff606ab3-cdbb-4852-85fc-e159cba954e6"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Port knocking"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "HTTPS"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Using empheral public private key"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "generation of multiple public private key pairs"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Using static public private key"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A,E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Perfect forward secrecy is a mechanism to use empheral private/public key pairs over https rather than using a single key pair."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "PJT made up"
   }
  ],
  "value": ""
 },
 {
  "name": "Security domains are critical constructs in a physical network and within alogical environment, as in an operating system. Which of the following best describes how addressing allows for isolation?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.fd9a9a1c-73da-4ad4-ae1e-7e0e9ca618be"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "In a network domains are isolated by using IP ranges and in an operating systemdomains can be isolated by using MAC addresses."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "In a network domains are isolated by using subnet masks and in an operating system domains can be isolated by using memory addresses."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Addressing is a way to implement ACLs, which enforce domain access attempts and boundaries"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Addressing creates virtual machines, which creates isolated domains."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Addressing is a way to enforce, \"if you dont have this address you dont belong here.\" network domains we use IP ranges and subnet masks to control how networksegments communicate to each other. Within software processes can only communicateto the address space that the security kernel will allow. Addressing is one way to enforce domain access and its boundaries."
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.briefmenow.org/isc2/which-of-the-following-best-describes-how-addressing-allows-for-isolation/"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following (pick 5) are not allowed to pass outbound through a firewall to the internet?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.813ccd83-2cc7-4242-afba-0d8c8b12268c"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "HTTP"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "FTP"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "SNMP"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "DNS"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "EIGRP"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "RADIUS"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "OSPF"
   },
   {
    "name": "H",
    "attrs": [],
    "value": "SSH"
   },
   {
    "name": "I",
    "attrs": [],
    "value": "SMTP"
   },
   {
    "name": "J",
    "attrs": [],
    "value": "LDAP"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C, E, F, G, J"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "EIGP - Enhance Interior Gateway Routing Protocol OSPF - Open Shortest Path First - A Routing protocol"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=78QfBIQJQZk"
   }
  ],
  "value": ""
 },
 {
  "name": "You have recently discovered that your 802.11 WLAN is 'RF visible' several floors above your office location. Which of the following will be most helpful to you when trying to reduce your RF cell size?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.d7652845-e09c-4dc3-855f-a12f3ffa344d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Configure PEAP"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Move the access point to the center of the office"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Reconsider antenna type and placement"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Change to a different channel"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Switch from 'fat' to 'then' APs"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Reduce the power produced by the AP"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C,F"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Fat AP have lots of extra features (nothing to do with RF cellsize)\nChannels are changed when there is interference.\n 2 main influences of RF cell size is annetenna and power"
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=eV4n-glv5Hc"
   }
  ],
  "value": ""
 },
 {
  "name": "You have 11 VLANs in your 6-switch network. Users needing to be assigned to the different VLANs are not all centralized.  Which of the following will you need to do in order to ensure both intra-VLAN and inter-VLAN communication (Choose 3)",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.c8907c2c-2141-453d-9d56-16d3755e0f8d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Configure Layer 2 Switch to route between the VLANs"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Route all traffic through the native VLAN"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Configure STP to tag the traffice on switch-to-switch links"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Configure a Layer-3 switch to route between the VLANs"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Allocate a unique subnet id for each VLAN"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Configure 802.1q on each port connected to a PC or server"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "Configure 802.1q on each trunk link"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D,G"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Layer 2 deals with MAC addresses so isnt helpful with routing\nSTP (Spanning Tree Protocol) is about loop avoidance.\n The 802.1q acheive intra routing (traffic between switches is tagged to pc on different switches are connected to VLAN)\nBy mapping subnets ID to VLANs you create the different network.\nYou can route between the networks via layer 3 routing acheive inter vlan routing (from VLAN 5 to 6)"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=eV4n-glv5Hc"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following are valid IP 6 address types?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.55e11197-69fe-4bd2-a30b-eff4f59c39e7"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Global Unicast"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "IPX"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Link-Local"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Broadcast"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Site-Local"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Unique-Local"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "NSAP-address"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A,C,F"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Global Unicast : UDP;\nLink-Local: equivalent to 127.0.0.1;\nUnique-Local - replace private addresses but are globally unique rather than stricly private;\nSite-Local - was a concept that was dropped as IPV6 matured;\nBroadcast - is not a concept in IPV but can be acheived via multicast;"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=7MqT17AV2Uc"
   }
  ],
  "value": ""
 },
 {
  "name": "Two different companies are preparing to connect their offices together via an Internet VPN. Before establishing the connection and beginning to share data, which of the following should be in place?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.4fbd12da-aef4-4d3e-aa6a-fa383915e74e"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "ISA"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "SLA"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "BIA"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "DLP"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "IDS"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "ISA - Interconnect Service Agreement"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=UlDgRaPpWTA"
   }
  ],
  "value": ""
 },
 {
  "name": "Company issued laptops with integrated VLAN adapters will be used outside the office and you assume the networks to which they connect will not be trusted. Which of the following is the best recommendation for securing the laptops and their transmitted data?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.6caa6ba8-200f-455c-8147-c58d29e0d9f8"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Enable full disk encryption"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Configure the systems to use PEAP when connecting to the untrusted WLANs"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Require users to establish a VPN connection when connected to an untrusted network"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Configure the clients to use AES when connecting to any WLAN"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Dont trust anyone"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=KwOC7beOyjY"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the last phase of the TCP 3 way handshake",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.d1c700fc-0d41-48ba-b82c-76055a340bae"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "SYN"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "NAK"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "ACK"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "SYN/ACK"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "SYN is sent first\nSYN/ACK is sent first from the destination host\nACK is then send from the initiator host"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.google.com.au/books/edition/ISC_2_CISSP_Certified_Information_System/mFhVDwAAQBAJ?hl=en&gbpv=1&dq=cissp+official+study+guide+mike+chapple&printsec=frontcover"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not a composition theory related to security models?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.07373ea5-11a1-47ce-bb19-5f0be709f232"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Cascading"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Iterative"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Feedback"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Hookup"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Cascading: Input for one system comes from the output of another system.\nFeedback: One system provides input to another system, which reciprocates by reversing those roles (so that system A first provides input for system B and then system B provides input to system A).\nHookup: One system sends input to another system but also sends input to external entities."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 286). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following are valid implementations of Triple DES (choose 2)?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.de7eaa6c-23e5-405c-b0f0-414df0f61382"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "DES-EEE. ALl 3 are unique. Key is effectively 168 bits."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "DES-EDE. Key1=Key2, Key 3 is unique"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "DES-EDE. Key1=Key3, Key 2 is unique"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "DES-EED. Key1=Key2, Key 3 is unique"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "DES-EEE. ALl 3 are the same. Keys are 168 bits"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A,C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "DES Keys are 56 bits.\nDES-EEE. Encryption-Encryption-Encryption\nDES-EDE. 2 keys used. First key is used in first and 3rd step."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=02rztIpWIdo"
   }
  ],
  "value": ""
 },
 {
  "name": "IPV6 introduces lots of new rules for address structures. WHich of the following addresses are valid destnation IPV6 address used for sending data t antoher not or nodes on the internet?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.ae7bdc45-b40e-427a-9dea-93d82f3d78d0"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "fe80::46c9:db66:2002"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "2002:46a8:8:722:d740:9be1:dfd1:d964"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "fda7:4967:fe1c:1::200"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "2620:0000:1234:cfg9:afc4:1:a:1100"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "3000:2341:5621:1:a84c::23::1"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "ff1e:40:2002:abcd:dead:beef:1:11ee"
   },
   {
    "name": "G",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B,F"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A is a local address which does not route globally\nB starts with 0 and has eight segments and is valid globally addressable\nC fda7 is example of locally routable address\nD has a g in in which is not a hex value\nE has double :: which is not allowed\nF ff (multi-cast) 1 (transient/temporary) e (global address) is valid\nG ff (multi-cast) 9 (local to the link) so not correct answer"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=ha5-i1TJ1f8&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=85"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of these ports numbers for well known services is correct",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.a7fa2a73-c330-40b2-9fa5-8bcffb0067e2"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "RDP-1433\nFTP-22\nSSH-21\nDNS-53\nSMTP-25"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "RDP-3389\nFTP-22\nSSH-21\nDNS-53\nSMTP-25"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "RDP-3389\nFTP-21\nSSH-22\nDNS-53\nSMTP-25"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "RDP-3389\nFTP-443\nSSH-21\nDNS-53\nSMTP-25"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/network-ports?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What TCP flag indicates that a packet is requesting a new connection?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.39790d74-81b3-4bcb-8cbc-6715920818d8"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "RST"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "URG"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "PSH"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "SYN"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4623063?u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What technology provides the translation that assigns public IP addresses to privately addressed systems that wish to communicate on the Internet?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.1692cd1b-b566-469e-bbeb-e03b941a0088"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "SSL"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "TLS"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "NAT"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "HTTP"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4623063?u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following ports is not normally used by email systems?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.e658aa3b-2895-4588-87ba-62821d005aa3"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "143"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "139"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "25"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "110"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "25 SMTP\n110 POP\n143 IMAP"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4623063?u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What command sends ICMP Echo Request packets?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.dc0805f6-9fa0-4d84-abfd-b42413e52e23"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "telnet"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "ftp"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "ssh"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "ping"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4623063?u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What network device can connect together multiple networks?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.379442a5-22ec-4b76-a6f5-d505c3ab0c68"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "switch"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "ap"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "router"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "wireless controller"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4623063?u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the default policy for a firewall",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.ee177746-209e-4fe2-a297-43fa6117b342"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Default Disallo"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Default Deny"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Default Prevent"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/firewalls?u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of these help you remember the OSI layers",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.5954f06f-e01c-4717-860d-eb11fe6a7297"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Please Do Not Teach Surly People Acronyms."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Kerckchoffs Principal"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "All Presidents Since Truman Never Did Pot"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Make Hay while the sun shines."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A,C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A - is from bottom up\nC - is from top down"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 444). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not a physical layer technology",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.78b374b1-1eaf-407a-aae1-9a80b59542bb"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "HSSI"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "SONET"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "X.21"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "V.25"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "ARP"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "ARP is data link layer\nHSSI - High Speed Serial Interface\nSONET - Synchronous Optical Network"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapter 11\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 445). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following are characteristics of a MAC address (choose 5)",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "32 bits in length"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "48 bits in length"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "written in hexdecimal format"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "written in decimal format"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "contains 24 bit OUI"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "contains 16 bit OUI"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "Multiple 802 standards use MAC address"
   },
   {
    "name": "H",
    "attrs": [],
    "value": "Only PCs and Servers have MAC addresses"
   },
   {
    "name": "I",
    "attrs": [],
    "value": "A nodes MAC address must be globally unique"
   },
   {
    "name": "J",
    "attrs": [],
    "value": "A nodes MAC address must be locally unique."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B,C,E,G,J"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "MAC are 48 bits, \nexpressed in hexidecimal\ncontainer 24 bit OUI - (Organizationally Unique Identifier)\n802.3 (ethernet) use MAC address\n802.11 (wireless) use MAC address\n802.15 (bluetooth) use MAC address"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=MYDlpLwPA7w&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=46"
   }
  ],
  "value": ""
 },
 {
  "name": "By what mechanism does an IPv6 node resolve an IP address to a MAC address?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.04cbd2d5-24fa-4d7f-9f13-06cfd0266db5"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "ARP"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "mDNS"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Solicited Node Multicast Address"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "SLAAC"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "MultiCast query to FF02::1"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "IPv6 does not use MAC addresses"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": [
     {
      "value": "ICMP multicast"
     },
     {
      "value": "Destination Address id fixed value plus ????"
     },
     {
      "value": "So the request only goes to the node that you are trying determine MAC address for\n Dont use ARP for IPV6\nmDNS is multicast DNS\n SLAAC - kind of like DHCP for IPV6\n FF02::1 - all nodes multicast (kinda like a broadcast)\n IPV6 and IPV4 are both Ethernet at layer 2 (Data Link Layer) and both use MAC."
     }
    ]
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=MYDlpLwPA7w&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=46"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of these are synomyms?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.97f1b61a-a8ed-4951-9784-a63e8672ee82"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Anomyly Detection"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Signature Detection"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Behavior Detection Systems"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Heuristic Detection Systems"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A,C,D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/network-intrusion-detection-and-prevention?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What security principle does a firewall implement with traffic when it does not have a rule that explicitly defines an action for that communication?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.278ad11c-296a-47dc-b1c3-950883cba67d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "least privilege"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "implicit deny"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "separation of duties"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "informed consent"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621456?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What network port is used for SSL/TLS VPN connections?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.5c26a4f1-a15f-4616-be82-aa785004a6e8"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "80"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "443"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "88"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "1521"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621456?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following functions is not normally found in a UTM device?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.84ed7e36-57f7-4e11-895b-abab4994de63"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Firewall"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Intrusion Detection"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "SSL TErmination"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Content Filtering"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621456?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Both IPV4 and IPV6 operate at Layer 3 of the OSI Model. WHich of the following is NOT a field in an IPv4 Header",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.ada873ff-0d49-4f8a-bbf1-31c5b9980e70"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "TTL"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Protocol ID"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Flow Label"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Version"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Source IP Address"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Fragment Offset"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "Checksum"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "TTL is a field in an IPV4 header but not in an IPV6 header\nProtocol is a field in an IPV4 header but not in an IPV6 header\nFlow Label is a field in an IPc6\nFragement offset exists in IPV4 but not IPV6\nChecksum exists in IPV4 and not IPV6"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=_JUfV0u_iPg"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is a public IP address?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.1a3a72de-ec0f-4f28-a49a-fb49a5c5ce48"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "172.18.144.144"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "142.19.15.4"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "192.168.14.129"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "10.194.99.16"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621453?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "How would a network technician write the subnet mask 255.255.0.0 in slash notation?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.9ee2447d-9443-45e5-bef7-6b0fb8a35baf"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "/8"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "/32"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "/16"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "/24"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621453?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following devices carries VLANs on a network?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.ce24b9e2-b511-4622-bcea-96d81580c873"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "hub"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "router"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "switch"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "firewall"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "VLANs are implement at layer 2"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621453?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the piece of software running on a device that enables it to connect to a NAC-protected network?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.ad5e3a8e-d86e-4ade-812a-06e1e0b95fee"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "SNMP agent"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "authentication server"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "authenticator"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "supplicant"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621453?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following controls would not constitute defense-in-depth for network access control?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.0d8e3107-e799-46e4-89a0-b6061c2d90f6"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "MAC filtering"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "802.1x"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "IDS"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "NAC"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "IDS is not about access control"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621453?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What control can administrators use when encryption is not feasible for a VoIP network?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.7a54ccc5-f122-4dda-a2b4-47a46017aab4"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "VOIP Firewall"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "VOIP Spoofing"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Separate voice VLANS"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "voice broadcasting"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4622081?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of storage network requires the use of dedicated connections?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.7afcedf8-3a8b-4019-9a22-90f1f45bdd71"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "iSSCI"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Fibre Channel over Ethernet"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "CIFS"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Fibre Channel"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4622081?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of firewall rule error occurs when a service is decommissioned but the related firewall rules are not removed?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.5d14e23b-8e75-49cc-b643-004775b3758a"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Shadowed Rule"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Orphaned Rule"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Promiscuous Rule"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Typographical Rule"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621454?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What router technology can be used to perform basic firewall functionality?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.d8e72c89-d51c-4941-b900-bac9b04ae760"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "spanning tree"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "access control lists"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "IPS"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "flood guard"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621454?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What technique should network administrators use on switches to limit the exposure of sensitive network traffic?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.1842cf52-869d-491d-9df7-a8fd7a4cbb1b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "spanning tree"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "loop prevention"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "VLAN pruning"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "VLAN hopping"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621454?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What technology can help prevent denial of service attacks on a network?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.10cfeb42-5eeb-4fd8-8025-9a0105bbde21"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "BGP"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Flood Guard"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "VLAN pruning"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "VLAN hopping"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621454?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What information is not found in network flow data?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.86391be3-56d2-454a-b3ff-abf583b0c458"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "source address"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "destination port"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "packet content"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "destination address"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621454?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What message can an SNMP agent send to a network management system to report an unusual event?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.06723673-69bd-40e0-ab53-e681f98b467a"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Trap"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Get Request"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Set Request"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Response"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621454?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of packet do participating systems send during a Smurf attack?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.52274c4d-2d62-43c0-aeae-1f4a4fbff25f"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "ICMP Information Reply"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "ICMP Status Check"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "ICMP Echo Request"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "ICMP Timestamp\nC"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4622080?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following techniques is useful in preventing replay attacks?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.f0447397-fdb0-414a-aaad-2c7db6fdc52d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Mobile Device Management"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Full Disk Encryption"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Session Tokens"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Man in the middle"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4622080?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What attack uses carefully crafted packets that have all available option flags set to 1?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.c86f8838-e1dd-40fa-861d-b13ae7fb5672"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "DNS Poisoning"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Christmas Tree"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "ARP Poisoning"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "URL Hijacking"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4622080?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is the most secure way for web servers and web browsers to communicate with each other?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.aa3596f0-2356-48ea-a8d4-c6a458e25493"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "SSLv1"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "SSLv2"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "TLS"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "SSLv3"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4620642?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What IPsec protocol provides confidentiality protection for the content of packets?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.e27197a0-c063-46d8-bbad-f06dee3817d9"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "ISAKMP"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "IKE"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "AH"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "ESP"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "In IPSEC, ESP protects confidentiality of packets while AH protects integrity of header and packets"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4620642?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Cindy would like to transfer files between two systems over a network. Which one of the following protocols performs this action over a secure, encrypted connection?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.8d18fa4e-2191-4ba4-9081-8a6722ab354c"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "FTP"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "SCP"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "TFTP"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "SSH"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4620642?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Gary is setting up a wireless network for his home office. He wants to limit access to the network to specific pieces of hardware. What technology can he use to achieve this goal?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.c5e91be2-9c47-4815-b0a5-59569684973b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "WEP"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "SSID broadcasting"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "MAC filtering"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "WPA"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4620642?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Brad is configuring a new wireless network for his small business. What wireless security standard should he use?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.529de368-e839-4851-817f-4b1e90a1b35b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "WPA2"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "WPA"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "WEP"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "WEP2"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4622079?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Fran is choosing an authentication protocol for her organization's wireless network. Which one of the following protocols is the most secure?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.8accd396-a064-435e-a5c8-2ba11833fc60"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "TACAS"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "PEAP"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "EAP-MD5"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "LEAP"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4622079?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "How many digits are allowed in a WiFi Protected Setup (WPS) PIN?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.400548d4-5bfd-437f-b802-5dc811f86846"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "11"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "4"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "6"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "8"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4622079?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Renee notices a suspicious individual moving around the vicinity of her company's buildings with a large antenna mounted in his car. Users are not reporting any problems with the network. What type of attack is likely taking place?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.c7978e16-7694-4851-85e7-2815cf0395ad"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "WPS Cracking"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Jamming"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "War Driving"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "War chalking"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4622079?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What toolkit enables attackers to easily automate evil twin attacks?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.1e8131e6-2ab1-4c40-b33d-54697d2abc33"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Karma"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "NIDS"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "HIPS"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "iStumbler"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4622079?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What command is used to apply operating system updates on some Linux distributions?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.210624f0-8094-4995-aa99-2733b8afd3e9"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "yum"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "update"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "systeminfo"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "ps"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4623062?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of malware prevention is most effective against known viruses?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.b2874552-aa8d-4beb-abfa-b1ff67db1d2f"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "heuristic detection"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "signature detection"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "anomaly detection"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "behavour analsysis"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4623062?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the name of the application control technology built-in to Microsoft Windows?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.e6c7ca35-8468-42d3-8481-7d6a0ed1161b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "BitContrl"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "BitLocker"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "AppLocker"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "AppControl"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4623062?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following security controls is built into Microsoft Windows?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.329d52a8-3c63-4746-9aa9-455cffccd3f8"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Host IDS"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Host IPS"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Host firewall"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "MDM"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4623062?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What additional patching requirements apply only in a virtualized environment?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.4aca9633-561e-4569-b0a7-1ce61bb2df4d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "patching the hypervisor"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "patching firewalls"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "patching applications"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "patching the operating system"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4623062?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of these describes how a code is not a cipher",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.54f2f196-0104-4ee9-a5ef-98d4888ba62a"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Hides information"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Allows efficient communication"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Provides integrity"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A code such as the '10-4' code used in police communication provides clarity and efficiency"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "PT"
   }
  ],
  "value": ""
 },
 {
  "name": "What encryption technique does WPA use to protect wireless communications?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.7f4371e6-75e6-4cdc-84f0-212528f7dc08"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "TKIP"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "DES"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "3DES"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "AES"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Wi-Fi Protected Access (WPA) uses the Temporal Key Integrity Protocol (TKIP) to protect wireless communications. WPA2 uses AES encryption."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapter 7 Review Question 8\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 272). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 }
]