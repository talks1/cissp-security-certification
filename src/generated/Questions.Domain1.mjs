export default [
 {
  "name": "Which of the following is a violation of the principles of least privelege (choose 2)?",
  "attrs": [],
  "items": [
   {
    "name": "Id",
    "attrs": [],
    "value": "SRM.d7a4a7cc-7ada-4092-9f8e-6fb76b0e8e1c"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Given an auditor read and write permissions for system log files?"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Installing access control units on elevators limting staff to job-related floors"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Requiring users to enter only a username and password to log into a system"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Placing a linux subsystem in the Domain Admins group in Active Directory"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Granting software developers access to production systems"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": [
     {
      "value": "A"
     },
     {
      "value": "D"
     }
    ]
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "I am tempted to answer E but the rationale that D is better is that E is more about separation of duties than violation of least privilege."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": [
     {
      "value": "its expensive"
     },
     {
      "value": "it requires excessive foresight to know what a person is expected to do and not do"
     },
     {
      "value": "it is slow as generally granting someone access to something is a slow process because it requires coordination"
     }
    ]
   }
  ],
  "value": ""
 },
 {
  "name": "Your risk analysis team is overwhelmed with the process of information gathering and calculating values for 'what if' scenarios. Which is the best course of action.",
  "attrs": [],
  "items": [
   {
    "name": "Id",
    "attrs": [],
    "value": "SRM.b1363405-31a5-4d46-a602-54e02c6603a0"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Increase the size of the team"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Switch to a fully qualitative approach"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Implement automated risk analysis tool"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Re-evalute the scope of the RA teams work"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Limit the number of risks considered."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": [
     {
      "value": "C"
     }
    ]
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Apparently there are helpful automated risk management tools which can multiple team effort to consider a wider set of information."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": "I am interested in what risk management tools can do and what data they bring to bear.\nIs there an opportunity for our team to develop risk management tool.\n Does our company have a risk management tool"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following would not be consider an indication of attack",
  "attrs": [],
  "items": [
   {
    "name": "Id",
    "attrs": [],
    "value": "SRM.a05df008-d665-4558-b048-af9ac10d8ddb"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Detection of an ongoing spear phishing campaign against employees"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "A NIDS detects a buffer overflow exploit in an inbound packet"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Unusual amounts of ssh traffic leaving a network"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Log files show the same username/password trying to log in to 20 different servers in 20 seconds"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "A zero day exploit has been identified for software widely used in your organization"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": "I am interested in what risk management tools can do and what data they bring to bear.\nIs there an opportunity for our team to develop risk management tool.\n Does our company have a risk management tool"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is NOT an element of a security planning mission statement?",
  "attrs": [],
  "items": [
   {
    "name": "Id",
    "attrs": [],
    "value": "SRM.5a9c811e-9cb2-448f-9542-15de619ab328"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Objectives Statement"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Background Statement"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Scope Statement"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Confidentiality Statement"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": [
     {
      "value": "D"
     }
    ]
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "In data processing systems, the value analysis should be performed in terms of which three properties?",
  "attrs": [],
  "items": [
   {
    "name": "Id",
    "attrs": [],
    "value": "SRM.ed16244f-a2e6-45e3-acef-57434d43e196"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Profit, loss, ROI"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Intentional, accidental, natural disaster"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Assets, personnel, services provided"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Availability, integrity, confidentiality"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A relates to considerations of business management.\nB related to considers of threat analysis.\nC relates to business impact analysis."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following techniques MOST clearly indicates whether specific risk reduction controls should be implemented?",
  "attrs": [],
  "items": [
   {
    "name": "Id",
    "attrs": [],
    "value": "SRM.3316234c-e3d1-4cf0-b51f-9a5d692bcfdb"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Threat and vulnerability analysis."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Risk evaluation."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "ALE calculation."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Countermeasure cost/benefit analysis."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": [
     {
      "value": "D"
     }
    ]
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "ALE only consider impact but doesnt consider costs to mitigate risk"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Place the following four elements of the Business Continuity Plan in the proper order.?",
  "attrs": [],
  "items": [
   {
    "name": "Id",
    "attrs": [],
    "value": "SRM.66aab13a-2ade-4d82-8ee1-161156a0d90f"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Scope and plan initiation, plan approval and implementation, business impact assessment, business continuity plan development"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Scope and plan initiation, business impact assessment, business continuity plan development, plan approval and implementation"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Business impact assessment, scope and plan initiation, business continuity plan development, plan approval and implementation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Plan approval and implementation, business impact assessment, scope and plan initiation, business continuity plan development"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": [
     {
      "value": "B"
     }
    ]
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Implement is last.\nScoping is first because its a management thing which does things like defined roles and responsibilities."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Houston-based Sea Breeze Industries has determined that there is a possibility that it may be hit by a hurricane once every 10 years. The losses from such an event are calculated to be $1 million. What is the SLE for this event?",
  "attrs": [],
  "items": [
   {
    "name": "Id",
    "attrs": [],
    "value": "SRM.b2a2b036-9fc3-4784-bf2c-686b82d08886"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "$1 million"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "$10 million"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "$100,000"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "$10,000"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": [
     {
      "value": "A"
     }
    ]
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "SLE is about a single loss.\nThe ARO is frequency.\nThe ALE is SLE * ARO"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "The primary goal of a security awareness program is:?",
  "attrs": [],
  "items": [
   {
    "name": "Id",
    "attrs": [],
    "value": "SRM.1d96aeb3-e95a-4366-9320-892c42431698"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "A platform for disclosing exposure and risk analysis"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "To make everyone aware of potential risk and exposure"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Platform for disclosing exposure and risk analysis."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "A way for communicating security procedures"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": [
     {
      "value": "B"
     }
    ]
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "You have changed your encryption algorithm for files on your server from 3DES to AES. Which type of control are you implementing. Choose two.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "IAM.6e3cf218-a0aa-49da-a5b9-2f9047225ce9"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Detective"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Corrective"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Preventative"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Administrative"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Technical"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Physical"
   },
   {
    "name": "Compensating",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C,E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Encryption prevents others from reading\nThis is technical control"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Crime Preventation through Environmental Design seeks to deter criminal activity through techniques for environment design. Which of the following are components of CPTEDs strategy",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.3e0a4ecd-9f3a-4ca7-a695-bc28c70f4734"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Natural Access Control"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "End User Security Awareness Training"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Building Code Security Reviews"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Community Activism"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Natural Territorial Enforcement"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Environmental Inconveniences"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "Natural Survelliance"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A,E,G"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Matural Access Control - making it obvious what is public vs private areas\nNatural Territorical Control - Making people feel they have ownership of the area\nNatural Survelliance - people more likely to create crime if they believe they are being seen doing it"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": "Is there an equivalent for this in user interface design"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is a preventative security control",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.8be5ea80-975d-4df5-82d7-ef07a5f1f5a6"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "CCTV Cameras"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "NTFS File Permissions"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Data backups"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "No Trepassing Signs"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Network Intrusion Detection"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "NTFS prevents certain actions\nCompared to No Trepassing which Deter someone but dont prevent\nBackups are a corrective control\nCCTV is a detective control\nIDS is a detective control"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "What would not be considered in the creation of an occupant emergency plan (OEP), Emergency Action plan",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM3996910f-775a-49f6-8b89-8839f2f9b956."
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Evacuation / Shelter in Place for individuals"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Defining locations for command center"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Contingency plans for alternate modes of communication"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Steps to ensure the primary operations of the facility continue to function"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "D would be part of a continuity of operations"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "An employee has written a script that changes values in a database before a billing application reads the values for payment processing . After the billing process runs , the malicious script changes the database back to its original values. What type of attack is this?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.be642a5c-a7be-41ce-bd4c-ff7e11590a25"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Time of CHeck/Time of Use"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Buffer Overflow"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "XSS"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Salami Attach"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Data Diddlin"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Salalmi attack is changing a number slightly over time and hoping its not noticed."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is a characteristic of a trade secret",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.4645437e-fb7b-44a8-8b06-52ea73e4385a"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Trade secrets are not afforded protection once published to the world"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Trade secrets are exempt from non-disclosure agreements"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Trade secrets are generally unknown to the world"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Trade secrets are protected for 20 years"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Trade secrets protect the original expression of the idea that than the idea itself"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A copyright protects the original expression of the idea rather than the idea itself"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=c0xg0LBBsI4"
   }
  ],
  "value": ""
 },
 {
  "name": "Who is ultimately responsible for accepting the risk associated with operating a system in your enterprise?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.89f6cd7f-dc59-4845-872e-7526a41e31ec"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "System Owner"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "ISSO"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Software Developer"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Authorizing Official"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "CIO"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D - this is someone who formally accepts the delivery"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A- System owner is responsible for multiple aspect of developing the system\nISSO - is responsible for security concerns during development\nCIO - is responsible for budgets"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=8A4c-vtz_U0"
   }
  ],
  "value": ""
 },
 {
  "name": "When you combine the SLE (Single Loss Expectancy) x ARO (Annualized Rate of Occurence) you get what",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.297af2a5-b7ed-44a8-aca5-164cb1ceb963."
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Return on Investment (ROI)"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Exposure Factor (EF)"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Risk"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Total Cost of Ownership"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Annualized Loss Expectancy (ALE)"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Exposure Factor is the percentage of an asset that is damaged when an incident occurs"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=ofBrEUGhmGA"
   }
  ],
  "value": ""
 },
 {
  "name": "In addition to Physical and Adminstration controls what are the other types of controls",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.c9a90254-c773-4c8e-bc6c-b3dd670ebb6b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Preventative"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Detective"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Corrective"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Technical"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Compensating"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Recovery"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=ofBrEUGhmGA"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is the most important objective acheived by an IT change management policy",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.7e205a4e-6937-4ec0-bb26-21409a4c9d44"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Minimize costs associated with changes"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Support rapid change while minimizing service disruption"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Create best practice to ensure that data confidentiality is preserved"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Provides guidance when emergency changes must be made"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Improved change documentation and post change reporting"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=CvdJSbjaBHY"
   }
  ],
  "value": ""
 },
 {
  "name": "All of the following should be determined during a BIA exception:",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.3a9fa221-919f-409c-8b8a-6b03aaca2b31"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "The backup strategies required for data recovery"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Which business processes a system supports"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "The financial impact to the business if a system becomes unavailable"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "The resources required to restore the system to operation"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "The maximum amount of time the system can be unavailable."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=HGtxaxO70rw"
   }
  ],
  "value": ""
 },
 {
  "name": "ISO/IEEE 15408 is an international standard for computer security certification. It provides a set of requirements for security functionality of IT products during security evaluation with the goal of providing a level of assurance that the product/system performs in a certain way. What is the common name for this standard?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.2284c248-cf87-4499-ba5f-a7c90144369b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "NIST"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "RMF"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "TCSEC"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "ITSEC"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Common Criteria"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Certification and Accreditation"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "PCI"
   },
   {
    "name": "H",
    "attrs": [],
    "value": "COBIT"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "https://en.wikipedia.org/wiki/Common_Criteria"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=vp7DA6TqDNw"
   }
  ],
  "value": ""
 },
 {
  "name": "Textbooks define 'due care' as the actions a reasonable and prudent person or organization would take in order to protect an asset. What is the primary reason due care is taken.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.9c275835-cd54-46fa-be04-d5319fefb7d9"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Minimize Downtime"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Reduce Liability"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Manage Costs"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Decrease Administrative Overhead"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Eliminate Risk"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "When participating in an activity you arent as liable if you have take due care."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": "This is a crappy answer. You take care to avoid making bad decisions that result in risk. By avoding bad decision you eliminate risk."
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=R3JrsyWXrUc"
   }
  ],
  "value": ""
 },
 {
  "name": "DDL (Data Definition Language) is the standard for the language to create structures in a database.  What is the term used to refer to the logical database structure.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SDL.fe1aaff9-7538-4d04-845a-6d921166a7aa"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "DCL"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Schema"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Normalization"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Polyinstantiation"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Cardinality"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Relational"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "SQL"
   },
   {
    "name": "H",
    "attrs": [],
    "value": "DML"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "DCL - Data Control Lanaguage - Commits, Rollback\nPolyinstantiation - Having records with the same primary key but separate column defines sensitivity. Record provided to end user depends upon sensitivity."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=FpOLERNDFMM"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following would be considered an adminstrative control?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.4b3b623d-646f-4ddc-8225-b837adfe2658"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Background Checks"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Network Firewall"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Audible Alarms"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Security Awareness Training"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Security Guards"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Risk Management"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "Encryption of Personnel Records"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A, D, F"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=IpnH7EXLGZ0"
   }
  ],
  "value": ""
 },
 {
  "name": "You are researching security solutions to provide overnight security for a large fenced in area that stores physical company assets. Your solution should act as a deterrent and provide the ability to gauge the level of corrective response. Which of the following is the most appropriate solution.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.52de8b1d-f4d9-4086-bc3e-894111d6b3be"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Use guard dogs"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "CCTV Cameras that record when movement is detected"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Install razer wire on top of fences"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Hire a security guard"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Only security guard provides a gauging of appropriate corrective action"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=irgya0lWmT8"
   }
  ],
  "value": ""
 },
 {
  "name": "You have received a PDF file that was signed using a self signed certificate. Which of the following should you do before viewing the file?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.5615f348-7a38-44cc-88a7-a10382912b02"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Hash the PDF and compare the result to the hash recovered from the signature"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Add the user certificates to the list of trusted certificaes on your system"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Manually verify the certificate using and out of band method"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Use the private key to validat the file signature before opening the file"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=irgya0lWmT8"
   }
  ],
  "value": ""
 },
 {
  "name": "What can HASH function tell you about a file change",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.346ecd5b-dfbb-468f-8ca2-8a211876b876"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "WHat in the file has been changed"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "The content has been changed"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "How much of the file has been changed"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "The file has been viewed"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "PT invented"
   }
  ],
  "value": ""
 },
 {
  "name": "WHich of the following are not Integrity controls?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.a9319e54-0a77-4f2b-9c33-caccad910fbf"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Hashing"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Digital Signature"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Access controls"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Digital Certificates"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Redundant components"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "OS Patching"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C,E,F"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Integrity are about detecting changes or non-repudiation"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "PT invented"
   }
  ],
  "value": ""
 },
 {
  "name": "A user at a terminal needs to be able to securely log into a system. Both the system login process and the user need to have confidence that untrusted software can eject itself into the authentication process.  What is this called",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.bdc84081-2b30-4323-9e9c-6c36b14657d3"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Trusted Path"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "API"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Covert Channel"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Protection Domain"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A trusted path is a channel established with strict standards to allow necessary communication to occur without exposing the TCB to security vulnerabilities. A trusted path also protects system users (sometimes known as subjects) from compromise as a result of a TCB interchange."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=H0_epnHrnlM&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=44"
   }
  ],
  "value": ""
 },
 {
  "name": "An IT Contingency Planning Process consists of 7 broad steps. Which of the following is one of those steps? Choose 2",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.f81231cb-37ae-442f-9541-656d2b0c6a34"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Define Metrics to be gathered"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Develop Recovery Strategies"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Respond to management with migitation steps"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Perform functionality and security testing"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Identifying Preventative Controls"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Obtain formal authorization to operate (ATO)"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B,E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "7 Steps of IT Contingency Planning Process:",
    "attrs": [],
    "value": [
     {
      "value": "Develop the contingency planning policy statement"
     },
     {
      "value": "Conduct the business impact analysis (BIA)"
     },
     {
      "value": "Identify preventive controls"
     },
     {
      "value": "Develop recovery strategies"
     },
     {
      "value": "Develop an IT contingency plan"
     },
     {
      "value": "Plan testing, training, and exercises"
     },
     {
      "value": "Plan maintenance"
     }
    ]
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=H0_epnHrnlM&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=44"
   }
  ],
  "value": ""
 },
 {
  "name": "What are all the following examples of : ISO 270001, NIST 800-53, COBIT",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.437bf04e-df99-4e88-868c-fc09dcbc385d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Security Policy"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Information Security Program"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Security Control Framework"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Risk Management Framework"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/control-frameworks?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is not one of the major principles of COBIT?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.15bf55f1-8c15-4e1d-b2c5-913686b4fca9"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Meeting stakholder needs"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "applying a single integrated framework"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "maximizing profitability"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "separately governance from management"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4592111?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "As  it relates to the GDPR, which of the following have been added under the requirement to protect persional information (Choose 2)",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.a3809290-fd10-429a-9c87-b2ee6294693f"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Physical Characeristics"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Genertics"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Identification Number"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Mental Status"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Location Data"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Economic Status"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "Cultural or Social Identity"
   },
   {
    "name": "H",
    "attrs": [],
    "value": "Online Identifiers"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "E,H"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Other items were in GDPR and Privacy Act"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=IYFp_5YptYg"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is a new requirement under the EU GDPR?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.bce309ce-7cc5-4082-8e04-d9e0ae2a59e8"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Data consent cannot be disclosed without data subjects consent"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Data can only be used for the purpose stated when collected"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Subject can access their data and make corrections"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Subjects have the right to be forgotten"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Subjects must consent to data collection"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Collected data should be kept secure from potential abuse"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Now you have the right to request your account to be completely removed. Accounts cannot be deactivated."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=IYFp_5YptYg"
   }
  ],
  "value": ""
 },
 {
  "name": "When making decisions about how to best secure user compueters and servers, which of the following is the most important consideration?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.64448da4-a58b-4e4f-9e16-6ed6aef81d9d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Security should not decrease the usability of the system"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Intangible risks should be mitigated first"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Should cover all regularoty requirements"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Cost must be manage and should make sense for the given risk"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "All risk should be eliminated by mitigating mechanisms"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=f0foB_Ng0xE&t=1s"
   }
  ],
  "value": ""
 },
 {
  "name": "When a data breach occurs in which situation is notification not required in many Data Breach notification laws (choose 3)?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.e0f7970b-7c32-478e-9005-d6d5a8fc9a8a"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Place of Birth"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Address"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Social Security Information"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Encrypted Credit Card Information"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Social Media Posts"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Drivers License Numbers"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "Transactions"
   },
   {
    "name": "H",
    "attrs": [],
    "value": "Bank Accounts"
   },
   {
    "name": "I",
    "attrs": [],
    "value": "Passwords"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D, G, I"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Generally if encrypted information is breach it is not considered a data breach if the encryption keys are not breached. It also has to be personally identifiable which is why paswords and transactions are not data breach notifications."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/data-breaches?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What law restricts government interception of private electronic communications?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.69ed37a2-38d2-413c-8691-1350f7793143"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "ITADA"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "EPCA"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "CFAA"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "GLBA"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "CFAA- Prevents unauthorized access\nITADA- Identity theft\nGLBA - Sharing financial information between branches"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4591465?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Dru recently developed a new name for a product that he will be selling. He would like to ensure that nobody else may use the same name. What type of intellectual property protection should he seek?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.d5f0c16c-2732-46fe-b3f6-228df7b274b7"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Patent"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Copyright"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Trademark"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Trade Secret"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4591465?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following regulations and agencies is not involved in the export control process?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.076184a2-eb3c-47e4-a69c-e42c387b5a35"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "ITAR"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "OFAC"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "EAR"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "FCC"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "ITAR - Defense restrictions\nEAR - Dual Use\nOFAC - restrictions on trade"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4591465?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What should an information policy NOT contain",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.0f5cc65f-5ca4-48b8-9dc3-1a7a0b964289"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Process for policy exceptions and violations"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Description of security roles"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Designation of individuals responsible for security"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Specific details of security controls"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Authority for the creation of policies"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "THe policies are not specific to be able to stand test of time"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": "What scope do policies apply to? Does one policy suit all"
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/security-policies?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What security principle prevents against an individual having excess security rights?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.24af6a88-fd23-4da8-968c-b2926a078a18"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Least Privilege"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Job Rotation"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Separation of duties"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Mandatory Vacations"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4592112?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following are NOT key principles of information security (choose 2)",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.3a2ec3ba-5e0f-480a-8466-f62b962c8604"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Principle of Least Privelege"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Locards Principle"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Need to know"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Separation of dutie"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Tranquility Principle"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B,E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/security-policies?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is an example of a business continuity control",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.d9473124-717b-4543-9c27-0ce1cf55df95"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Only allow a use to create a vendor or pay a vendor but not both"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Classify data from a system"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Succession plan for key personnel"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Establish information security policy"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Log user access to information systems"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": "What are the key personnel for the team?\nDoes our team have a succession plan?"
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Phil Tomlinson https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/business-continuity-controls?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following does NOT involves disk striping",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.f74187ef-83df-450d-91b8-898fb0c1b2d9"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "RAID 0"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "RAID 1"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "RAID 2"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "RAID 5"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "RAID 6"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "RAID 1 is pure mirroring"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Phil Tomlinson https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/high-availability-and-fault-tolerance?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "How many disks are required to implement disk striping with parity",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.220d74fc-5e64-499c-8d1b-7bf423040174"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "1"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "2"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "3"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "4"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "RAID 3 requires at least 2 disks to strip across and a 3rd for the parity information"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/high-availability-and-fault-tolerance?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "WHich of the following control types is RAID",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.16875f77-d734-4ba0-bbe9-be612ff9a9f2"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Access control"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Backup Control"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Fault Tolerance Control"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "High Availability Control"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Quality of Service Control"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "In CISSP material HA is separate from Fault Tolerance"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Phil Tomlinson https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/high-availability-and-fault-tolerance?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of control are we using if we supplement a single firewall with a second standby firewall ready to assume responsibility if the primary firewall fails?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.c21d6959-38a7-4fab-bcc4-d246e679c1c1"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Component Redunancy Control"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Load Balancing"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "High Availability Control"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Clustering"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4590564?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the minimum number of disks to acheive RAID level 5",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.499567ab-0538-44c9-983d-1a997b3dc3ac"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "1"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "2"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "3"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "4"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4590564?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Whats the first thing you should do if you observe a information security policy violation?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.f4b8895b-3a34-4606-8c05-ca0c0fb63a50"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Inform the violator of the policy"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Investigate the consequences of this violation"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Bring it up with your manager"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Do nothing"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Suggestion in this material is that need to be sensitive to causing more harm than good"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/improving-personnel-security?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not a way to protect sensitive employee information",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.11d5fd2c-b153-4509-b7cb-115c9e2fdb18"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Minimization"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Limitation"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Truncation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Encryption"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Masking"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The others are mechanism to limit and protect the information kept by an organization"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/employee-privacy?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following agreements is most directly designed to protect confidential information after an employee has left the organization?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.c402d128-bd67-47f7-8794-2a0fc12c8164"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "BAA"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Asset Return"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "SLA"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "NDA"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Non Disclosure Agreement"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4592113?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Whats the SLE of an asset where the EF is 25% and the AV is 2 million dollars?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.265f9110-6910-4a07-98a6-0c2837171b0c"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "250,000"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "500,000"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "1 million"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "ALE = EF * AV"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quantitative-risk-assessment?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is NOT a risk management strategy",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.440ec91f-8e23-47db-9049-8908f8cae8fe"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Risk Avoidance"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Risk Deterence"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Risk Acceptance"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Risk Mitigation"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Risk Transference"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Risk Assessment"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "F"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/five-possible-risk-management-actions?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following are operational controls",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.09a99e2b-b7a1-4af4-90cb-961f7579881d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Firewall"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Code Review"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "System Administrator"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Automatic Code Analysis"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Pentration Test"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B,C,E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Operational Controls are implemented by people\nTechnical Controls are implemented by technology"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/security-control-selection-and-implementation?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What two factors are used to evaluate a risk?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.1fa5cf55-a605-4926-9600-2a64c43f4a49"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Criticality and Likelihood"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Impact and Criticality"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Frequency and Likelihood"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Likelihood and Impact"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4590563?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the correct formula for computing the annualized loss expectancy?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.46dbe115-6907-456c-96f6-b2a92eeed023"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "ALE = ARO * AV"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "ALE = SLE * ARO"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "ALE = EF SLE ARO"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "ALE = AV - SLE"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4590563?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the first step in the NIST risk management framework?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.5efea285-a901-41a5-b759-d42c48d712f2"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Authorize Information System"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Categorize information system"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Monitor Security Controls"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Select security controls"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4590563?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What letter in STRIDE refers to kind of attacks attempting to deny responsibility for an action?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.44e27cc1-54dd-4703-8eb1-fa51443c0dd0"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "S"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "T"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "R"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "I"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "E"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "S-spoofing\nT-tampering\nR-repudiation\nI-information Disclosure\nD-denial of Service\nE-elevation of privilege"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4589757?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following RAID solutions provides the SMALLEST net usable disk space?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.12da5fde-ec0c-4ab4-b9d2-aea41a57b856"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "RAID 0"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "RAID 1"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "RAID 3"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "RAID 5"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "With mirroing there is loss of 50% of disk capacity with 2 disks and 66% loss if 3 disks\nWith striping and parity there is 1 of X disks lost so RAID 5 is more efficient in terms of disks space"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=KAYYmSvaIy0&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=43"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the purpose of the hamming code?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.25c0bde3-8f5c-4bc6-a1c3-d07de9a2d9a7"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "It is used for data transposition in encryption processes"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "It is used as data encoding mechanism for 802.11 WLANs"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "It is used to detect and correct errors in data"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "It is used to calculate CRC checksums in Ethernet frames"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=KAYYmSvaIy0&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=43"
   }
  ],
  "value": ""
 },
 {
  "name": "What would 'system response time', 'service availablity' and 'data preservation' all be examples of?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.021d83ca-9540-4801-b5ef-e5b1aefe2410"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "MOU"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "SLA"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "SLR"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "BPA"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "SLR - Service Level Requirements\nSLA - Service Level Agreement\nMOU - Letter to document aspects of a relationship...document for future understanding\nBPA - Business Partnership Agreement - separate responsibilities and renumernation for a joint development\nISA - Agreement on how organizations will interconnect"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/vendor-information-management?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following are not likely to be data ownership provisions in a contract",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.ac32a9e3-5c1a-4ec4-b2e3-3dc2042af930"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Customer retains unihibited data ownership"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Vendors right to use information is limited to activities performed on behalf of the customer"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Vendors right to use information is limited to activities performed without the customers knowledge"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Vendor must delete information at the end of the contract"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Vendor is not allowed to share information with 3rd parties"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The customer should know how a vendor uses the customers information."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": "Are their agreements in placed with asana, jira, github around data protection?"
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/vendor-information-management?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What activity would an MSSP not normally perform for an organization",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.f9adbf3d-262a-4299-b2e2-3601bcdfd8f2"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Manage an entire security infrastructure"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Monitor System Logs"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Manage firewalls and networks"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Monitor security performance"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Perform identity and access management"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "If a company uses an MSSP (Managed Security Service Provider) it will need to independently monitor performance of the MSSP."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/third-party-security-services?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of agreement is used to define availability requirements for an IT service that an organization is purchasing from a vendor?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.ffe33446-5a34-4a03-8598-55afb22ec393"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "MOU"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "SLA"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "BPA"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "ISA"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not a required component in support of accountability?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.ea1ad329-b35e-43c7-8bcc-7bed02f91297"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Auditing"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Privacy"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Authentication"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Authorization"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Accountability does not require privacy but rather non-repudiation"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.google.com.au/books/edition/ISC_2_CISSP_Certified_Information_System/mFhVDwAAQBAJ?hl=en&gbpv=1&dq=cissp+official+study+guide+mike+chapple&printsec=frontcover"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not a defense against collusion",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.ceed8f8d-f3a7-451d-8fe9-0c6206eaac6a"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Separation of duties"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Restricted job responsibilities"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Group user accounts"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Job rotation"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.google.com.au/books/edition/ISC_2_CISSP_Certified_Information_System/mFhVDwAAQBAJ?hl=en&gbpv=1&dq=cissp+official+study+guide+mike+chapple&printsec=frontcover"
   }
  ],
  "value": ""
 },
 {
  "name": "In what phase of the Capability Maturity Model for Software (SW-CMM) are quantitative measures utilized to gain a detailed understanding of the software development process.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.6ab5a63e-4e5f-4d64-a060-3839585ad518"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Repeatable"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Defined"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Managed"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Optimized"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Managed phase of the SW-CMM involves the use of quantitative development metrics. The Software Engineering Institute (SEI) defines the key process areas for this level"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Assessment Test Question 13\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. l). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is a layer in the ring protection system that is not normally implemented in practice?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.2d49ba54-ca14-497c-98f8-f5b1db535f66"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Layer 0"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Layer 1"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Layer 3"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Layer 4"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Ring Protection System is the defense in depth model\nLayer 0 is the security kernel.\nLayer 1 and 2 are device drivers.\nLayer 3 is the application\nLayer 4 doesnt exist"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.google.com.au/books/edition/ISC_2_CISSP_Certified_Information_System/mFhVDwAAQBAJ?hl=en&gbpv=1&dq=cissp+official+study+guide+mike+chapple&printsec=frontcover"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is best countered by adequate parameter checking?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.a73338b2-0a7c-4f36-9a09-1a7ea6ffc0ea"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Time of check to time of use"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Buffer overflow"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "SYN Flood"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Denial of Service"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.google.com.au/books/edition/ISC_2_CISSP_Certified_Information_System/mFhVDwAAQBAJ?hl=en&gbpv=1&dq=cissp+official+study+guide+mike+chapple&printsec=frontcover"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the logical operation shown here?    X: 0 1 1, Y: 0 1 0?   X v Y",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.1512e3fe-eb36-455a-b7d7-1fd6e0c10ce5"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "0 1 1"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "0 1 0"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "0 0 0"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "0 0 1"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "V or ~ is the logical OR operator"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "a73338b2-0a7c-4f36-9a09-1a7ea6ffc0ea"
   }
  ],
  "value": ""
 },
 {
  "name": "The collection of components in the TCB that work together to implement reference monitor function is the ",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.f160cfc2-c0af-481f-b657-f6511fbd5f6a"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Security Perimeter"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Security Kernel"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Access Matrix"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Constrained Interface"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Reference monitor is the aspect of enforcing access controls"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.google.com.au/books/edition/ISC_2_CISSP_Certified_Information_System/mFhVDwAAQBAJ?hl=en&gbpv=1&dq=cissp+official+study+guide+mike+chapple&printsec=frontcover"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is true?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.ac9449f1-c0f1-4938-822e-5c1b24b9a1e2"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "The less complex a system is the more vulnerabilities it has"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "The more complex a system is, the less assurance it provides"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "The less complex a system, the less trust it provides"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "The more complex a system, the less attack surface it provides"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.google.com.au/books/edition/ISC_2_CISSP_Certified_Information_System/mFhVDwAAQBAJ?hl=en&gbpv=1&dq=cissp+official+study+guide+mike+chapple&printsec=frontcover"
   }
  ],
  "value": ""
 },
 {
  "name": "System architecture, system integrity, convert channel analysis, trusted facility management, and trusted recovery are elements of what security criteria?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.cbac2557-f2f5-447d-87b7-f3429dc923b5"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Quality Assurance"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Operationational Assurance"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Lifecycle Assurance"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Assurance is the degree of confidence you can place in the satification of security needs. Operational assurance focuses on the basic features and architecture that lend themselves to support security"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.google.com.au/books/edition/ISC_2_CISSP_Certified_Information_System/mFhVDwAAQBAJ?hl=en&gbpv=1&dq=cissp+official+study+guide+mike+chapple&printsec=frontcover"
   }
  ],
  "value": ""
 },
 {
  "name": "Ring 0, from the design architecture security mechanism known as protection rings, can also be referred to as all but which of the following?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.6a71d9f1-be3e-4f32-a7f8-8de638d56bb3"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Priviledge Mode"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Supervisory Mode"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "System Mode"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "User Mode"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. xlv). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Audit trails, logs, CCTV, intrusion detection systems, antivirus software, penetration testing, password crackers, performance monitoring, and cyclic redundancy checks (CRCs) are examples of what?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.0b1fdb59-e955-4805-aee6-475689470516"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Preventative Controls"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Detective Controls"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Corrective Controls"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. xlvi). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which type of intrusion detection system (IDS) can be considered an expert system?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.4220dd3c-0d63-451e-8899-5034fb4b0662"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Host based"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Network based"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Knowlege Based System"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Behaviour Based System"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A behavior-based IDS can be labeled an expert system or a pseudo-artificial intelligence system because it can learn and make assumptions about events. In other words, the IDS can act like a human expert by evaluating current events against known events. A knowledge-based IDS uses a database of known attack methods to detect attacks. Both host-based and network-based systems can be either knowledge-based, behavior-based, or a combination of both."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. xlviii). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following are not included in BIA recovery timeframe assessments? (choose 2)",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.392d91a3-1bc0-4c29-90e0-750b99da5c59"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "RPO"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "MTBF"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "MTD"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "RTO"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "MTBSI"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "TTR"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B,E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Recovery doesnt worry about how frequently they occur.\nMTBF - Mean Time Betwee Failures\nMTBF - Mean Time Between System Incidents\n MTD - Maximum Tolerable Downtime\nTTR - Time to recover"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Why would you implement a logon banner (choose 3)?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.0c89ff40-c9bb-451c-8568-fff07346df3d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Provide a welcome message to connecting users?"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Notifying user of active monitoring"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Provider system information upon connection"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Deter hackers attempting to connect"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Establishing \"no expectation of privacy\""
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Defining who is allowed to acces the system"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B,E,F"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=U8gYmlF_1DY"
   }
  ],
  "value": ""
 },
 {
  "name": "WHich of the following mechahisms provides the greatest capacity for individual accountability?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.bc9a1ca8-d0de-40ba-a3f6-734a4fdbe5a2"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Hashing Files to ensure integrity"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Loggint activity per IP address"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Setting permissions on folders"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Individual sign on per user"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Limiting the number of employees that have keys to the building."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=U8gYmlF_1DY"
   }
  ],
  "value": ""
 },
 {
  "name": "What are the opposites of Confidentiality, Integrity and Availability",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.5fd1518c-a24a-4dac-9393-0ac5c3360aa1"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Alteration, Destruction and Disclosure"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Compromise, Inaccuracy and Destruction"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Disclosure, Alteration and Destruction"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Inaccuracy, Compomise, Disclosure"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Top Secret, Secret, Unclassifed"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=Vq1ECMX-iG4"
   }
  ],
  "value": ""
 },
 {
  "name": "Vulnerabilities and risks are evaluated based on their threats against which of the following?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.6e6f65c2-7d31-4d41-8128-f838abfe75bd"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "One or more of the CIA Triad"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Data usefulness"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Due care"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Extent of liability"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Vulnerabilities and risks are evaluated based on their threats against one or more of the CIA Triad principles."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp1 Review Question 2\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not considered a violation of confidentiality?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.1dcd2588-2900-4d84-a62c-909712911bca"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Stealing passwords"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Eavesdropping"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Hardware destruction"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Social Engineering"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Hardware destruction is a violation of availability and possibly integrity. Violations of confidentiality include capturing network traffic, stealing password files, social engineering, port scanning, shoulder surfing, eavesdropping, and sniffing."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp1 Review Question 4\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not true?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.afbe33bf-47c7-487a-9af8-43fd9f15ae88"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Violations of confidentiality include human error"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Violations of confidentiality include management oversight"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Violations of confidentiality are limited to direct intentional acts"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Violations of confidentiality can occur when a transmission is not properly encrypted."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Violations of confidentiality are not limited to direct intentional attacks. Many instances of unauthorized disclosure of sensitive or confidential information are due to human error, oversight, or ineptitude."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp1 Review Question 5\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "STRIDE is often used in relation to assessing threats against applications or operating systems. Which of the following is not an element of STRIDE?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.a43efd88-6245-4694-b506-25665c24d9ef"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Spoofing"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Elevation of privelege"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Repudiation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Disclosure"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Disclosure is not an element of STRIDE. The elements of STRIDE are spoofing, tampering, repudiation, information disclosure, denial of service, and elevation of privilege."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp1 Review Question 6\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "All but which of the following items requires awareness for all individuals affected?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.b06e843f-1eab-4356-b09a-21b913b15987"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Restricting personal email"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Recording Phone Conversations"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Gathering information about surfing habits"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "The backup mechanism used to retain email"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Users should be aware that email messages are retained, but the backup mechanism used to perform this operation does not need to be disclosed to them."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp1 Review Question 9\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What element of data categorization management can override all other forms of access control?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.d1a8fc80-1b17-4578-9dc2-302e978ac079"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Classification"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Physical Access"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Custodian Responsibility"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Taking Ownership"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Ownership grants an entity full capabilities and privileges over the object they own. The ability to take ownership is often granted to the most powerful accounts in an operating system because it can be used to overstep any access control limitations otherwise implemented."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp1 Review Question 10\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is the most important and distinctive concept in relation to layered security?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.5b0ebc21-dde2-417f-9564-10bf9fefe284"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Multiple"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Series"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Parallel"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Filter"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Layering is the deployment of multiple security mechanisms in a series. When security restrictions are performed in a series, they are performed one after the other in a linear fashion. Therefore, a single failure of a security control does not render the entire solution ineffective."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp1 Review Question 12\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not considered an example of data hiding?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.f801ec13-2de3-4c88-9e42-22fbc27458ed"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Preventing an authorized reader of an object from deleting that object"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Keeping a database from being accessed by unauthorized visitors"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Restricting a subject at a lower classification level from accessing data at a higher classification level"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Preventing an application from accessing hardware directly"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A. Preventing an authorized reader of an object from deleting that object is just an example of access control, not data hiding. If you can read an object, it is not hidden from you."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp1 Review Question 13\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What is the primary goal of change management?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.e9c88248-894e-43e8-a771-4e83b3805f27"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Maintaining documentation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Keeping users informed of change"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Allowing rollback of failed changes"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Preventing security compromises"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The prevention of security compromises is the primary goal of change management."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp1 Review Question 14\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What is the primary objective of data classification schemes?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.af9b6735-1a22-4c65-994a-d4a42988b276"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "To control access to objects for authorized subjects"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "To formalize and stratify the process of securing data based on assigned labels of importance and sensitivity"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "To establish a transaction trail for auditing accountability"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "To manipulate access controls to provide for the most efficient means to grant or restrict functionality"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp1 Review Question 5\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is typically not a characteristic considered when classifying data?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.f4300b72-7ded-4db3-8384-3dddf14b53a8"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Value"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Size of object"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Useful lifetime"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "National Security implications"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp1 Review Question 16\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What are the two common data classification schemes?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.2de77ea2-371d-4f15-b32f-80b8c95b8e2f"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Military and private sector"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Personal and government"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Private sector and unrestricted sector"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Classified and unclassifed"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Military (or government) and private sector (or commercial business) are the two common data classification schemes."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp1 Review Question 17\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is the lowest military data classification for classified data?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.e3e319e2-f691-4149-96a1-746c5bd90dd1"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Sensitive"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Secret"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Proprietary"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Private"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Of the options listed, secret is the lowest classified military data classification. Keep in mind that items labeled as confidential, secret, and top secret are collectively known as classified, and confidential is below secret in the list."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp1 Review Question 18\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which commercial business/private sector data classification is used to control information about individuals within an organization?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.91dfbe88-f177-42a1-9b65-244e2674537e"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Confidential"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Private"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Sensitive"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Propietary"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp1 Review Question 19\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Data classifications are used to focus security controls over all but which of the following?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.647c3dd9-63f9-48e5-a6a5-bcf9d5e70cb3"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Storage"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Processing"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Layering"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Transfer"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Layering is a core aspect of security mechanisms, but it is not a focus of data classifications."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp1 Review Question 20\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is the weakest element in any security solution?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.8d3ca788-809a-44d1-b756-fe118e1e2335"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Software products"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Internet Connection"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Security policies"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Humans"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp2 Review Question 1\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "When seeking to hire new employees, what is the first step?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.89fcf732-c0a8-43e5-8889-e6e94c61292b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Create Job Description"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Set Position Characteristics"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Screen Candidates"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Request referrals"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The first step in hiring new employees is to create a job description. Without a job description, there is no consensus on what type of individual needs to be found and hired."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp2 Review Question 2\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "When an employee is to be terminated, which of the following should be done?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.830b1cca-1aed-40a1-af10-086f62fe46b4"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Inform the employee a few hours before they are officially terminated."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Disable the employee’s network access just as they are informed of the termination."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Send out a broadcast email informing everyone that a specific employee is to be terminated."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Wait until you and the employee are the only people remaining in the building before announcing the termination."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "You should remove or disable the employee’s network user account immediately before or at the same time they are informed of their termination."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp2 Review Question 4\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "QUESTION",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.f429f56a-f210-4635-84ef-89dd28304c88"
   },
   {
    "name": "A",
    "attrs": [],
    "value": ""
   },
   {
    "name": "B",
    "attrs": [],
    "value": ""
   },
   {
    "name": "C",
    "attrs": [],
    "value": ""
   },
   {
    "name": "D",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp2 Review Question X\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "If an organization contracts with outside entities to provide key business functions or services, such as account or technical support, what is the process called that is used to ensure that these entities support sufficient security?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.f322fef5-68ce-4cb6-884d-9c52f75047a8"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Asset Identification"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Third Party Governance"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Exit Interview"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Qualitative Analysis"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Third-party governance is the application of security oversight on third parties that your organization relies on."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp2 Review Question X\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "A portion of the ???? is the logical and practical investigation of business processes and organizational policies. This process/policy review ensures that the stated and implemented business tasks, systems, and methodologies are practical, efficient, and cost-effective, but most of all (at least in relation to security governance) that they support security through the reduction of vulnerabilities and the avoidance, reduction, or mitigation of risk.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.d50d8db9-622b-4311-b9a9-9b5ad942c82a"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Hybrid Assessment"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Risk aversion process"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Countermeasure selection"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Documentation review"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A portion of the documentation review is the logical and practical investigation of business processes and organizational policies."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp2 Review Question 6\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following statements is not true?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.a49fe36e-90e1-4c74-bc7d-08d9048322e1"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "IT security can provide protection only against logical or technical attacks"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "THe process by which the goals of risk management are acheived is known as risk analysis"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Risks to an IT infarstructure are all computer based"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "An asset is anything used in a business process or task"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp2 Review Question 7\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not an element of the risk analysis process?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.a7aa8f62-5046-4052-847b-500ce71296c9"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Analyzing an environment for risks"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Creating a cost/benefit report for safeguards to present to upper management"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Selecting appropriate safeguards and implementing them"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Evaluating each threat event as to its likelihood of ocurring and cost of the resulting damage"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Risk analysis includes analyzing an environment for risks, evaluating each threat event as to its likelihood of occurring and the cost of the damage it would cause, assessing the cost of various countermeasures for each risk, and creating a cost/benefit report for safeguards to present to upper management. Selecting safeguards is a task of upper management based on the results of risk analysis. It is a task that falls under risk management, but it is not part of the risk analysis process."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp2 Review Question 8\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following would generally not be considered an asset in a risk analysis?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.6fcd9916-1dc5-406b-b9ac-1dbbfb726827"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "A development process"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "An IT infrastructure"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "A proprietary system resource"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Users personal files"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The personal files of users are not usually considered assets of the organization and thus are not considered in a risk analysis."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp2 Review Question 9\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following represents accidental or intentional exploitations of vulnerabilities?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.6d96df52-34f6-46a2-a8cf-5d13b77a8fc2"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Threat events"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Risks"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Threat agents"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Vulnerabilities"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Threat events are accidental or intentional exploitations of vulnerabilities."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp2 Review Question 10\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "When a safeguard or a countermeasure is not present or is not sufficient, what remains?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.5618b312-1ea8-4041-b315-c7cd2b6f70c0"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Vulnerability"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Exposure"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Risk"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Penetration"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp2 Review Question 11\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not a valid definition for risk?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.21ee9e81-194e-432c-9391-83d95e158e32"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "An assessment of probability, possible or chance"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Anything that removes vulneability or protects against one or more threats"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Risk = threat * vulnerability"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Every instance of exposure"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Anything that removes a vulnerability or protects against one or more specific threats is considered a safeguard or a countermeasure, not a risk."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp2 Review Question 12\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "When evaluating safeguards, what is the rule that should be followed in most cases?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.0b6cf8e9-7a5f-47c7-8eb1-23b6faf0c7d3"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "The expected annual cost of asset loss should not exceed the annual cost of safeguards"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "The annual costs of safeguards should equal the value of the asset."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "The annual costs of safeguards should not exceed the expected annual cost of asset loss."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "The annual costs of safeguards should not exceed 10 percent of the security budget."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The annual costs of safeguards should not exceed the expected annual cost of asset loss."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp2 Review Question 13\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "How is single loss expectancy (SLE) calculated?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.769f41bc-9a0a-45ec-8b8e-b43a20c3667f"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Threat + Vulnerability"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Asset Value * exposure factor"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Annual Rate of Occurance * vulneability"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Annualized Rate of Occurance Asset Value Exposure Factor"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "SLE is calculated using the formula SLE = asset value ($) exposure factor (SLE = AV EF)."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp2 Review Question 14\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "How is the value of a safeguard to a company calculated?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.20ac7b60-2c57-49fd-8f58-807b17d497c6"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "ALE before safeguard – ALE after implementing the safeguard – annual cost of safeguard"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "ALE before safeguard * ARO of safeguard"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "ALE after implementing safeguard + annual cost of safeguard – controls gap"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Total risk – controls gap"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The value of a safeguard to an organization is calculated by ALE before safeguard – ALE after implementing the safeguard – annual cost of safeguard (ALE1 – ALE2) – ACS ."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp2 Review Question 15\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What security control is directly focused on preventing collusion?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.030d5ed9-5777-419c-b6a8-285cd19622b8"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Principle of least privilege"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Job Description"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Separation of duties"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Qualitative Risk Analysis"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The likelihood that a co-worker will be willing to collaborate on an illegal or abusive scheme is reduced because of the higher risk of detection created by the combination of separation of duties, restricted job responsibilities, and job rotation."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp2 Review Question 16\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What process or event is typically hosted by an organization and is targeted to groups of employees with similar job functions?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.36dafe34-ca9f-4076-8080-421c777d7e02"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Education"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Awareness"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Training"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Termination"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Training is teaching employees to perform their work tasks and to comply with the security policy. Training is typically hosted by an organization and is targeted to groups of employees with similar job functions."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp2 Review Question 17\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not specifically or directly related to managing the security function of an organization?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.89c352c1-248e-4571-8849-b2536a01ddd0"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Worker job satisfaction"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Metrics"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Information Security Strategies"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Budget"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Managing the security function often includes assessment of budget, metrics, resources, and information security strategies,"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp2 Review Question 18\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "You’ve performed a basic quantitative risk analysis on a specific threat/vulnerability/risk relation. You select a possible countermeasure. When performing the calculations again, which of the following factors will change?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.8ce11ce9-2b60-47dd-9afb-23e5e70b3656"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "EF"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "SLE"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Asset Value"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "ARO"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A countermeasure directly affects the annualized rate of occurrence, primarily because the countermeasure is designed to prevent the occurrence of the risk, thus reducing its frequency per year."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp2 Review Question 20\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What is the first step that individusla responsible for the development of a business continuity plan should perform?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.1d509ace-668c-47ec-9aa0-d5a4ea513240"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "BCP Team Selection"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Business organization analysis"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Resource Requirements Analysis"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Legal and Regulatory assessment"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The business organization analysis helps the initial planners select appropriate BCP team members and then guides the overall BCP process."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp3 Review Question 1\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Once the BCP team is selected, what should be the first item placed on the team’s agenda?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.7eaf16d9-d8ac-4bc3-9f56-278734a2b136"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Business Impact Assessment"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Business organization analysis"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Resource Requirements Analysis"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Legal and Regulatory assessment"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The first task of the BCP team should be the review and validation of the business organization analysis initially performed by those individuals responsible for spearheading the BCP effort. This ensures that the initial effort, undertaken by a small group of individuals, reflects the beliefs of the entire BCP team."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp3 Review Question 2\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What is the term used to describe the responsibility of a firm’s officers and directors to ensure that adequate measures are in place to minimize the effect of a disaster on the organization’s continued viability?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.121395c6-3071-4381-b248-f19fe71cfb8b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Corporate Responsibility"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Review and validation of the business organizational analysis"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Due diligence"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Going concern responsilbity"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A firm’s officers and directors are legally bound to exercise due diligence in conducting their activities. This concept creates a fiduciary responsibility on their part to ensure that adequate business continuity plans are in place."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp3 Review Question 3\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following BIA terms identifies the amount of money a business expects to lose to a given risk each year?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.b5ae5554-2b53-46d0-ae79-b0bc314be1b8"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "ARO"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "SLE"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "ALE"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "EF"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The annualized loss expectancy (ALE) represents the amount of money a business expects to lose to a given risk each year. This figure is quite useful when performing a quantitative prioritization of business continuity resource allocation."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chp3 Review Question 6\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 }
]