export default [
 {
  "name": "Which of the of the following is a VALID technique for attacking smartcard",
  "attrs": [],
  "items": [
   {
    "name": "Id",
    "attrs": [],
    "value": "SAE.4a1cc150-1207-4b17-bce2-7490cfaa00c0"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Brute force attacks against symetric keys"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Factor the product of RSA primes"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Side channel attacks using differential power analysis"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Malware on users compute that will extract the private key when user logs in."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": [
     {
      "value": "C"
     }
    ]
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Apparently you can measure the power consumption of a device which reads a smart card. If you have the right keys the power usage goes up."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": "Is there any evidence this has been done successfully.\nDoes it scale well to really be a threat?\nThe hacker needs possession of the card.\nSeems very low risk."
   }
  ],
  "value": ""
 },
 {
  "name": "Which 2 should you consider before moving your companies accounting system to a cloud saas?",
  "attrs": [],
  "items": [
   {
    "name": "Id",
    "attrs": [],
    "value": "SAE.06b2c0ab-8470-471e-8b3f-0f461fd179c5"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "SLA (Service Level Agreement)"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "MOU (Memorandum of Understand)"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "NDA (Non Disclosure Agreement)"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Software License"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "ISA (Interconnection Service Agreement)"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": [
     {
      "value": "A, C"
     }
    ]
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is a key concern for ephemeral diffie-helman?",
  "attrs": [],
  "items": [
   {
    "name": "Id",
    "attrs": [],
    "value": "SAE.79fb5946-3cde-428d-9e3a-f69c8a102d9a"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Forward Secrecy is not available"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "No authentication"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Weak encryption"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Long term private key compromise"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "There is no authentication involved in diffie helman so it needs to be augemented to support this.\n Diffier Helman is about 2 parties being able to establish a secret key over unsecure channel. It uses Public Private Key Crypto to acheive this."
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=D5alTeDIg-Y"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the best way to control data when you only want users in certain location to access?",
  "attrs": [],
  "items": [
   {
    "name": "Id",
    "attrs": [],
    "value": "SAE.4c57f512-23d5-43bb-99a8-26a9142c5039"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "MAC"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "DAC"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "RBAC"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "ABAC"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": [
     {
      "value": "D"
     }
    ]
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Location attribute can be provided"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is NOT a property of the bell-lapula security model",
  "attrs": [],
  "items": [
   {
    "name": "Id",
    "attrs": [],
    "value": "SAE.babad34d-b39d-4eb7-b3e1-9225a2057e7c"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Simple Security Property"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Property Rule"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Tranquility Principle"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Strong Star Property"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Simple Integration Axiom"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Bell-Lapadula relates to confidentiality and not integrity."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is a major part of Trusted Computer Base?",
  "attrs": [],
  "items": [
   {
    "name": "Id",
    "attrs": [],
    "value": "SAE.6a01fced-175c-4544-ad0a-dda6b5d200a1"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "The software"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "The security subsystem"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "The operating system software"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "The reference monitor"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The trusted computing base (TCB) of a computer system is the set of all hardware, firmware, and/or software components that are critical to its security\nA reference monitor is a system component that enforces access controls on an object."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": "Access control for GeoDocs should be centralized into a security kernel as the trusted based for the security."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following are symmetric algorithms?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.0a11af5b-f328-44e5-a7cf-ddbea82796cd"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Serpent"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "RSA"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "MQV"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Blowfisth"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "RC5"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Diffie Helman"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A,D,E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Serpent, Blowfish are symmetric block ciphers\nRC5 is a symmetric stream cipher"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "You are sending an email ecrypted with a symmetric key. The symmetric key is encrypted using the recipients public key. What is the commmon term used to describe the encrypte message structure",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.be55d654-e5e3-499d-9a88-b695b53e152a"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Digital Signature"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Hashed Message Authentication Code (HMAC)"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Message Integrity Check (MIC)"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Digital Envelope"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "HMAC is taking a message, adding a shared secret and hashing it and sending"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "You are using an encryption schema which generate seemingly random bits which are then XOR'd with the plaintext data into order to produce ciphertext. Which type of algorithm is this?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.9249acaa-d979-4ace-be76-58d172682f38"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Stream Cipher"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "ECC"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Diffie-Helman"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "RSA"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Block Cipher"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Confidentiality is a critical component of modern distributed computing systems. WHich of the following presents the greatest challenge to providing confidentiality for such an environment?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.a1345cb3-480a-4f90-9b50-4893c8219940"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Heterogenity of systems"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Lack of protocol standardization"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Network scalability"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Inadequate system transparency"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Missing digital signatures"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Transmitting unencrypted data"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "F"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "other answers are problematic but doesnt directly related to confidentiality."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=CvdJSbjaBHY"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is an example of tunneling network traffic",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.80ba46b3-b6f6-4993-8943-8b330c2cc61a"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "NAT-T"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Masquerading"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "PAT (Port Address Translation)"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "SOCKS PROXY"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Stateless NAT64"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "NAT-T is a technique to wrap an encrypted IPSEC packet in UDP (as an example). UDP will be supported by NAT and thus be routable whereas the IPSEC by itself is not.\nMasquerading is another name for NAT"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=yLNBUJVOjAg"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following represents the best reason to upgrade your web application servers to TLS 1.3",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.6b623304-1239-48e4-ae30-79fbfd8d6e12"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "The SNI is encrypted by default in 1.3"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "The server certificate is encrypted when sent to the client"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "TLS 1.3 supports a larger number of legacy algorthms"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "The server uses certificate pinning to speed up connection times."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "With earlier versions of TLS the server certificate is not encrypted which means that the server you are access is visible in plain text. This may not be desired.\n SNI lets a client specify a hostname for the IP address it is targeting which allow one IP address to support many names.\nTLS 1.3 actually drops support for a number of legacy algorthms."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=P7j3lJDMFgg"
   }
  ],
  "value": ""
 },
 {
  "name": "In an effort to increase the security of SSH on your server you implement, a technique that requires users to connect to 3 seemingly random ports before connecting to port 22. Which of the following best describes this techqniue?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.37b5862f-996c-414b-86a1-23e7b8d968e2"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Firewalking"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Port mapping"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Port knocking"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Secret handshake"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Session triggering"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "kernel handshaking"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Port knocking is security by obsecurity."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=se14Kog6raE"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the term for hiding a message inside a large body of text",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.bfe26575-8dc5-45a3-8113-2fd842bd2b5c"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "One Time Pad"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Digest"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Null Cipher"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Steganography"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Block Cipher"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the followingis not an example of a side-channel attack?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.e319e162-8501-40cb-9acf-70599a7946f5"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Differential power analysis"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Electromagnetic emission"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Corruptive"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Timing"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "ome examples of side-channel attacks that have been carried out on smart cards are differential power analysis (examining the power emissions released during processing), electromagnetic analysis (examining the frequencies emitted), and timing (how long a specific process takes to complete). These types of attacks are used to uncover sensitive information about how a component works without trying to compromise any type of flaw or weakness. They are commonly used for data collection."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.briefmenow.org/isc2/which-of-the-followingis-not-an-example-of-a-side-channel-attack/"
   }
  ],
  "value": ""
 },
 {
  "name": "\"Subjects can access resources in domains of equal or lower trust levels.\" This is an easy sentence, but a difficult concept for many people to really understand.Which of the following is not an example of this concept?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.1d71adc6-dfad-4bb2-bea4-b76cd51329b1"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "The security officer can access over 80% of the files within a company."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "A contractor is only given access to three files on one file server."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "A security kernel process can access all processes within an operating system."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "A Guest account has access to all administrator accounts in the domain."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A subject can be a user, application, process or service – any active entity that isattempting to access a passive entity (object). Each of the answers providedexamples of how the more trusted subjects were given access to resources (a domain)that corresponded with its trust level. A Guest account does not have the trustlevel to access all administrator accounts."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.briefmenow.org/isc2/which-of-the-following-is-not-an-example-of-this-concept/"
   }
  ],
  "value": ""
 },
 {
  "name": "In the past 2 years your company has grown from locally maintained servers to a large number of cloud based instances. Managing system integrity has become more complex and error-prone process. As it relates to the this issue, which of the following broad approaches should you be implementing?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.48e331a5-6b22-43ba-9d41-0a49c52072ac"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Information Security Continuous Monitoring"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Risk Managment"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Application Threat Modeling"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Public Key Infrastructure"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Configuration Management"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=XBOuWeR08QM"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following does a hot site have which a warm site does not?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.9326b89a-e7b2-45d7-be30-ac2e1b6649a9"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Near Real Time Data"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Climate Control"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Physical Servers"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Telephones"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=tx70EN2ph_Y"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following are characteristics of elliptic curve cryptopgraphy (choose 4)?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.5fd35553-4735-4512-8885-a64593bb016f"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "It is stronger than RSA using significantly smaller key lengths"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "It has a large memory footprint"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "It can help conserve battery life in mobile?"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "It has a lower CPU overhead compared to RSA."
   },
   {
    "name": "E",
    "attrs": [],
    "value": "It is not support by most modern browsers"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "ECC was introduced as an alternative to AES"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "It can be used in Diffie Helman key exchange"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A,C,D,G"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "ECC uses smaller keys which required less computation power."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=78QfBIQJQZk"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is TRUE of  TLS, IPSEC and SSH?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.aedc686c-0b07-4410-86a4-8d77e56ac5d8"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "They are routeable and must be tunneled"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "They all operate at the transport layer"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "They are supported in ALL IPV4 nodes"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "They all offer confidentiality for data"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "They utilize only symmetric encryption"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "youtube.com/watch?v=l1ZLBwog1xU"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of these key lengths is available in the Rjindael encryption algorithm?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.b660f9e4-bfac-4b14-bb26-91eb21c55f47"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "64"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "72"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "128"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "168"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "192"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "256"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "512"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C,E,F"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Rjindeal supports key size in increments of 32 but these 3 are the only ones implemented."
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=KQ3qIRmJdbw"
   }
  ],
  "value": ""
 },
 {
  "name": "Mobile devices place an emphasis on battery consumption and frequently have limited processing power. What type of encryption is best suited to this",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.d2d79b54-cc60-4a2d-b766-f55add1fd1a2"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "RSA"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Diffie-Helman"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "EAP-TLS"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "PEAP"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "ECC"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "AES"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "Vernam Ciphers"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "ECC keys 12 times shorter than RSA keys are just as strong, so there is a efficiency savings"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=vxLsrsnJ9AU&list=RDCMUCwUkAunxT1BNbmKVOSEoqYA&index=3"
   }
  ],
  "value": ""
 },
 {
  "name": "Blowfish is an encryption algorthm originally designed as an alternative to DES. Which of the following are characteristics of Blowfish (Choose 4)?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.94a686da-cbca-456b-91b2-31672e380a28"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Key Size of 128,192,256"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Key Sizes from 32-448"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Symmetric"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Assymetric"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Patented"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Un-patented and license-free"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "Used in bcyrypt"
   },
   {
    "name": "H",
    "attrs": [],
    "value": "Used in scrypt"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B,C,F,G"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=vxLsrsnJ9AU&list=RDCMUCwUkAunxT1BNbmKVOSEoqYA&index=3"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following algorithms are asymmetric",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.7f008538-eefc-445d-b284-e62c82f2b477"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Advance Encryption Standard (AES)"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Blowfish"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Data Encryption Standard"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "El Gamal"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "RSA"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D,E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-the-basics-2/computerized-adaptive-testing-cat?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Cryptography provides many different types of protections for information. When utilized correctly, which of the following represent things that cryptography can do (Choose 6)",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.69167999-1b06-4123-a128-cecfcf54678d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Detect if a spreadsheet has been changed in an unauthorized way"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Provide confidentiality for a windows users accessing a linux web server via a web browswer"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Prevent a user from deleting a file they have no permission to access"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Provide a high degree of assurance that a remote system is who it says it is"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Recover data changed in an authorized manner."
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Prevent a thief from putting a stolen hard drive into a separate computer and recovering the information."
   },
   {
    "name": "G",
    "attrs": [],
    "value": "Check a script is unmodified before allowing it to be executed"
   },
   {
    "name": "H",
    "attrs": [],
    "value": "Reduced the effectiveness of ICMP based Denial of Service attacks"
   },
   {
    "name": "I",
    "attrs": [],
    "value": "Prevent an authorized user from exfiltrating data from a protected network"
   },
   {
    "name": "J",
    "attrs": [],
    "value": "Assist with complying with regulatory data security requirements"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A,B,D,F,G,J"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=f0foB_Ng0xE&t=1s"
   }
  ],
  "value": ""
 },
 {
  "name": "A use hashes a file using SHA1. The user makes a small change, renames it and emails to a friend.  Hashing the new file would give a particular value.  6 months later an investigator wants to determine if the image file before change is located on the original users computer. Which technique might help determine this. Choose the best answer",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.82070721-5c48-47f8-bbcb-54cb6942de4c"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Hash all the files using SHA-256 and look for a matching hash"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Look in the Sent Items folder in the users email program"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Search for images of similar size and file type."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "use a fuzzy hashing tool"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Create SHA1 hashes of each file and compare the last 8 bits of the hash value"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Fuzzy hashing - hashes part of the file and then can be used to calculate a percentage of similarity to another file"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": "This technique could be used to highlight changes in a document over time or since someone else last read the content."
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=zCe_pXZ9LFM"
   }
  ],
  "value": ""
 },
 {
  "name": "You have just received a digitally signed email message.What do you need in order to validate the integrity and authenticity of the message?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SRM.01ac8071-2c60-4081-b84b-185322c86e55"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Your pricate key"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Senders private key"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Root CAS private key"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Senders public key"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "The passphrase for the message."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=zCe_pXZ9LFM"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following best describes a zero day vulnerability?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.1b3ac04f-0db9-402b-8307-0b497c122573"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "An undisclosed vulnerability attackers can exploit to adversely affect systems"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Infecting a website frequented by a targeted group of users"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Gaining persistent control of a target system without the users knowledge"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Attacking a crypt system by exploiting its physicl implementation"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=UlDgRaPpWTA"
   }
  ],
  "value": ""
 },
 {
  "name": "A public facing web server has been infected with a kernel-level rootkit. Which of the following is the BEST recommendation on how the incident should be handled?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.a710ab56-a9f9-459f-ad61-9b0a2cf4da31"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Uninstall the rootkit and recover any correcupted data from backup"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Remove the system from the network and manually remove the rootkit files"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Reinstall the operating systems and restore data from a backup"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Install a rootkit removal program and use it to remove the rootkit"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=KwOC7beOyjY"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the length of a hash of a message produced with MD5 Hash?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.bfb644a9-c508-4a2d-8973-3012f0431f8c"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "64"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "128"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "256"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "384"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.google.com.au/books/edition/ISC_2_CISSP_Certified_Information_System/mFhVDwAAQBAJ?hl=en&gbpv=1&dq=cissp+official+study+guide+mike+chapple&printsec=frontcover"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is not a certification",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.2b4feb27-61c1-4f2a-a78a-e0dc955dcf08"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "ATO"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "IATO"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "IATT"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "DATO"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "NIST"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The others are authorizations/accrediations\nAuthorization To Operate (ATO)\nInterim Authorization to Operate (IATO)\nInterim Authorization to Test (IATT)\nDenial of AUthorization to Operate (DATO)"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering/security-requirements?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What security rule says that no subject should be able to read data at a higher level than the subject's security clearance?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.388c240e-649e-4214-9db0-a3b01598aa8b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Start integrity property"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "simple integrity property"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "simple security property"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Star security propery"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Integrity - No Read down, No write up\nSecurity - No read up, no write down"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering/quiz/urn:li:learningApiAssessment:4613498?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What layers are the reponsibility of the customer in a 'platform as a service' cloud model?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.953d4cc1-76f6-4f67-bde4-8e9124ba5c98"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Application"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Data Center"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Data"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Hardware"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "OS"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A,C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "IAAS - Vendors manages hardware - Customer manages OS, application and data\nPAAS - Vendor manages hardware and OS - Customer manages application and data\nSAAS - Vendor manages hardware, OS, Application - Customer manages data"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering/public-cloud-tiers?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following are not approaches to prevent sql injection attacks?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.2631b1c1-590d-4907-9ca3-60a9ae8a2c58"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Use parametrized sql queries"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Descape SQL characters in field inputs"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Use raw queries"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Bind parameters to queries"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Validate field inputs"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": "Should not use raw queries where input comes from an input field"
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "PJT"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the best defense against cross-site scripting attacks?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.5e12f151-dc0a-478d-ad6a-f501d38fd672"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "intrusion detection"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "input validation"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "network segmentation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "WPA2 encryption"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering/quiz/urn:li:learningApiAssessment:4614167?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is not an effective defense against XSRF attacks?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.8872a861-8493-41a1-8c8d-03ac2c8b79b9"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "network segmentation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "preventing HTTP GET requests"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Automatic Logouts"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "User Education"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering/quiz/urn:li:learningApiAssessment:4614167?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of fuzz testing captures real software input and modifies it?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.e056c234-8a68-4ecd-8ad1-fb4baf986fda"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Script Based Fuzzing"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Static Fuzzing"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Mutation Fuzzing"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Generation Fuzzing"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering/quiz/urn:li:learningApiAssessment:4614167?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Your web application is suspectible to XSS but cannot be re-written. Which of the following is the best option for you?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.4d1be06e-6fc1-48b4-a81d-7c6e29cda930"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Require all traffic to be encrypted."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Disable search and form submission"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Install a NIDS to monitor for XSS attackes"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Install a WAF between clients and server to act as a reverse proxy"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Install a statefull firewall to only allow established session to port 443"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "C is not the right answer because we dont just want detection. A WAF working as a web proxy can reject requests when it detects WAF."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=nKeRFLjBlc0"
   }
  ],
  "value": ""
 },
 {
  "name": "The core issues around BYOD relate to ",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.36e10bdd-d565-4ea1-9f10-b292f58c8e36"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "ownership"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "standards"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "administration"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "process"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering/quiz/urn:li:learningApiAssessment:4614168?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What technology can you use as a compensating control when it's not possible to patch an embedded system?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.ee0d5c51-9fb8-491e-9620-8a1787959df9"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "IDS"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Wrappers"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Log Analysis"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "SIEM"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Security Wrappers monitor requests to IOT devices only letting valid requests through."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering/quiz/urn:li:learningApiAssessment:4613500?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the most important control to apply to smart devices?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.08276296-067f-43dc-9b50-7f88b8c9f045"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "wrappers"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "intrusion detection"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "network segmentation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "application firewalls"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Same principle as Web DMZ. Isolate system."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering/quiz/urn:li:learningApiAssessment:4613500?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "When can non-repudiation not be acheived?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.f7536883-e971-420b-89f0-6a2094cd5209"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "using asymetric keys"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "PGP"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "using symetric keys"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Secure MIME"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "With symetric keys you can prove who generated anything."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "PT"
   }
  ],
  "value": ""
 },
 {
  "name": "Bob is planning to use a cryptographic cipher that rearranges the characters in a message. What type of cipher is Bob planning to use?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.5087193b-fc48-4abb-8da8-f5e848c2784b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "elliptic cipher"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "transposition cipher"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "stream cipher"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "substitution cipher"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering/quiz/urn:li:learningApiAssessment:4612688?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of these is Kerckchoff principle?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.c7296eb9-19c4-4280-93d1-9ee3a4cdd7b6"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "principle of seperation of duties"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "principle of least privilege"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Communication in event of breach"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Tranquility principle"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "In a security algorithm only the key should be secret"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "PT"
   }
  ],
  "value": ""
 },
 {
  "name": "Using symmetric encryption when must the keys be regenerated?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.7edd21a7-124d-4c91-bf3e-7d8cb9e56aa9"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "For each message"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "When the key expires"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "When a participant leaves a group"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Secret is compromised"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C,D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "You must regenerate new keys for any key that the participant knew and distribute to the group."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "PT"
   }
  ],
  "value": ""
 },
 {
  "name": "How many possible keys exist in a 4-bit key space?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.e9a96291-b56d-4969-8317-5fbc8b238591"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "4"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "8"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "16"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "128"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "2 ^ 4"
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 232). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What is the length of the cryptographic key used in the Data Encryption Standard (DES) cryptosystem?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.81497ae7-d17f-4c4c-b70b-0471d2aaf0d0"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "56"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "128"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "192"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "256"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 232). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following cannot be achieved by a secret key cryptosystem?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.d1f67934-7d75-411a-ba62-806323be13a0"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Non Repduation"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Confidentiality"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Authentication"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Key Distribution"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 232). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What is the output value of the mathematical function 16 mod 3?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.27f366f3-1f91-4b6c-8d0c-169fbfa00f20"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "0"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "1"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "3"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "5"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 233). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What block size is used by the 3DES encryption algorithm?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.294a2dfa-0d1b-4d21-9991-450de98459e4"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "32"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "64"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "128"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "256"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "3DES simply repeats the use of the DES algorithm three times. Therefore, it has the same block length as DES: 64 bits.\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 959). Wiley. Kindle Edition."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 233). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What is the minimum number of cryptographic keys required for secure two-way communications in symmetric key cryptography?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.0c001ef9-a889-4ec6-860e-a2fcbedd1aa8"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "1"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "2"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "3"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "4"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 233). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Dave is developing a key escrow system that requires multiple people to retrieve a key but does not depend on every participant being present. What type of technique is he using?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.1c575b1a-d985-42b1-8f4b-c1e68e6417e3"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Split Knowledge"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "M of N"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Work function"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Zero Knowledege Proof"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 233). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following Data Encryption Standard (DES) operating modes can be used for large messages with the assurance that an error early in the encryption/decryption process won’t spoil results throughout the communication?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.f9edeca3-c37b-4626-846d-2d9633ab26c2"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Cipher Block Chaining (CBC)"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Electronic Code Book (ECB)"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Cipher Feedback (CFB)"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Output feedback (OFB)"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Output feedback (OFB) mode prevents early errors from interfering with future encryption/decryption. Cipher Block Chaining and Cipher Feedback modes will carry errors throughout the entire encryption/decryption process. Electronic Code Book (ECB) operation is not suitable for large amounts of data."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 234). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Many cryptographic algorithms rely on the difficulty of factoring the product of large prime numbers. What characteristic of this problem are they relying on?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.bf28f942-4766-4e8c-b193-e78fa204788d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Diffusion"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Confusion"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "One Way function"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Kerckhoffs Principle"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A one-way function is a mathematical operation that easily produces output values for each possible combination of inputs but makes it impossible to retrieve the input values."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 234). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "How many keys are required to fully implement a symmetric algorithm with 10 participants?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.a9f40aaf-79e4-4ed8-924c-f3c829bff7af"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "10"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "20"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "45"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "100"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "n(n-1)/2"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 234). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What type of cryptosystem commonly makes use of a passage from a well-known book for the encryption key?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.a12df651-d86d-4f36-b5e7-22f3e294711d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Vernam Cipher"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Running key cipher"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Skiphack Cipher"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Blowfish Cipher"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 234). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which AES finalist makes use of prewhitening and postwhitening techniques?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.ef33177e-8e4f-4ba5-8703-349a676916d4"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Rjindael"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Twofish"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "blowfish"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Skipjack"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 235). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "How many encryption keys are required to fully implement an asymmetric algorithm with 10 participants?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.7bb96776-7f6d-4a50-8fdc-c6e903d75be2"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "10"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "20"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "45"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "100"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 235). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What key is actually used to encrypt the contents of a message when using PGP?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.9fa5abf7-22f2-40dd-83c2-3e88f05dc3ad"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "recipients public key"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "senders public key"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "randomly generated key"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "senders private key"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/quiz/urn:li:learningApiAssessment:4614169?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following encryption approaches is most susceptible to a quantum computing attack?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.7dc73fc8-3933-47fb-935f-dfeb93efd67e"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "quantum cryptography"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "RSA cryptography"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "elliptic curve cryptograhy"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "AES cryptography"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/quiz/urn:li:learningApiAssessment:4614169?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "The difficulty of solving what mathematical problem provides the security underlying the Diffie-Hellman algorithm?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.c7df2284-6093-44fa-a7f1-76b491f24776"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "prime factorization"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "travelling salesman"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "elliptic curve"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "graph isomorphism"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/quiz/urn:li:learningApiAssessment:4613497?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "In the early 1990s, the National Security Agency attempted to introduce key escrow using what failed technology?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.7ff155c3-98b1-4e9b-acf3-191f3a13a135"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Common certificates"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "DES"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Common critiera"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Clipper Chip"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/quiz/urn:li:learningApiAssessment:4613497?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What algorithm uses the Blowfish cipher along with a salt to strengthen cryptographic keys?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.8b4a6f11-fd90-4859-a85d-09a55b940a80"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Blowdart"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "PBKDF2"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Bcrypt"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "PBKDF2"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/quiz/urn:li:learningApiAssessment:4613497?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "The BIBA integrity model endeavours to ensure data integrity by only allowing authorized modifications. To do so subject and objects are grouped into authorization levels. Which of the following is one of Biba rules?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.ea15a35f-6fb9-4db6-bd13-0dfe31bf6998"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "No read down"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "No read up"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "No write down"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "No write up"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Strong star"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": [
     {
      "value": "Dont trust stuff read at a lower level in case its modified"
     },
     {
      "value": "Consequently dont write stuff for higher auth levels as they wont trust it"
     }
    ]
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=nJv45N8bMcs"
   }
  ],
  "value": ""
 },
 {
  "name": "In a computer system the ability for subject to interact with objects is controlled. Which of the following is responsible for exerting that control. Choose the best answer?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.4bb852a7-5386-4817-a7be-7e633fe100bf"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Reference Monitor"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Kerberos"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Memory Manager"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "API"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Security Control"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=nJv45N8bMcs"
   }
  ],
  "value": ""
 },
 {
  "name": "When a user receives a digitally signed message which of these wont be true?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.379b6147-5315-4520-95b8-715299ab6497"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "The owner of the public key is the person who signed the message"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "The device was sent from a particular device."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "The message was not altered after being signed"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "The receipient can prove these facts to a 3rd party."
   },
   {
    "name": "E",
    "attrs": [],
    "value": "The receipient can prove the email address of the sender"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B,E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "While the others are true, the message content cant confirm which device sent a message and if the certificate does not include the senders email address you can be sure of the senders email address"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "PT"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is not a barrier to using the web of trust (WoT) approach?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.f0f47dd3-42f9-469f-9084-6c118295da23"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Decentralized Approach"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Technical knowledge of users"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "use of weak crptography"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "high barrier to entry"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/quiz/urn:li:learningApiAssessment:4615121?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is not a possible hash length from the SHA-2 function?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.68a39515-6bdc-4dcb-8ba9-0d5ebe5fc239"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "512"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "128"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "256"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "224"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/quiz/urn:li:learningApiAssessment:4615121?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What standard governs the structure and content of digital certificates?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.82391306-4098-4b35-9da1-ada8728f2785"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "802.1x"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "x.509"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "x.500"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "802.11ac"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/quiz/urn:li:learningApiAssessment:4615121?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Harold works for a certificate authority and wants to ensure that his organization is able to revoke digital certificates that it creates. What is the most effective method of revoking digital certificates?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.9dde39c2-549d-4fea-9233-a0920f674ef5"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Transport Layer Security"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Online Certificate Status Protocol"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Certificate Revocation Bulletin"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Certificate Revocation Lists"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/quiz/urn:li:learningApiAssessment:4615121?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "An Hashed Message Authentication Code (HMAC) does not allow which of the following",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.1bd2a5b4-ec41-49a5-a158-7c5c79ca9b07"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Confirm integrity of the message"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Allow you to prove to 3rd party who sent the message"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Generation of Partial Digital Signature"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Viewing of plain text message content"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Because it encrypts the hash with a secret key it doesnt support non-repdiation"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "PT"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following are digital signature algorithms",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.e2b5674d-637b-40ca-a780-a81581c7ef0e"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "RSA"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "DSS"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Schnorrs Signature Algorthm"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Nyberg-Rueppel’s Signature Algorthm"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "X.509"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B,C,D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 249). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "As part of the rollout of smartcard system you have been tasked with providing a summary of features to senior management. Which of the following is true and could be included in the report",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.64b96b33-aec6-4b9d-9e83-9c007b8333bf"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Private keys should be marked as exportable so they can be transferred to new keys"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "The private key is encrypted with asymmetric key"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Smartcards hold multiple key pairs and their certificates"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Smart cards do not support assymetric keys with length shorter then 1024"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "If the card is lost the private can easily be retrieved with compatible smart card reader"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=mjgXGDE9_M8&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=14"
   }
  ],
  "value": ""
 },
 {
  "name": "How are the keys on the smart card used to authenticate the user? WHich is the best answer?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.66e41c62-9aad-4987-9234-ac510337e4bd"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "The smartcard calculates a hash of the user certificate and sends to the host computer"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "The computer validates the user certificate with standard PKI validation techniques"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "The user smartcard PIN unlocks the access to the user certificates"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "The user private key encrypts a challenge generated by the computer"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A computer has separate access to your public certificate (from AD for instance).\nWhen you plug in your smart card the computer issues challenge (some text) which the smart card encryptes and returns back to computer.\nThe computer checks it can use your public key to decrypt and get back to the challenge"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=mjgXGDE9_M8&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=14"
   }
  ],
  "value": ""
 },
 {
  "name": "What of the following are not one the 5 fundamentals of a hash algorthm?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.2cc696eb-ec0f-4ce6-8cf4-ab44e734f7c4"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Allow input of any length"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Collision free"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Easy to compute hash for any input"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Provide variable-length output"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "provide one-way functionality"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Hashes provide variable length ouput"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 269). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What is the length of SHA-1 vs SHA-2 and SHA-3 hash lengths?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.3666c73f-55b9-44ab-9ff0-9854703748fa"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "128-256-512"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "128-128-256"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "160-512-512"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "160-256-512"
   },
   {
    "name": "E",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "SHA-1 produces a 160-bit message digest whereas SHA-2 supports variable lengths, ranging up to 512 bits. SHA-3 improves upon the security of SHA-2 and supports the same hash lengths."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 269). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What are the components of the DSS standard",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.a0335572-b5d9-4fdd-939b-e1df1eb0c05c"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Hash using MD3 encrypt with AES"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Hash using SHA1-1,2,3 and encrypt with DSA,RSA,ECDSA"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Hash with PGP encrypt with Blowfish"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Hash with HAVAL and encrypt with Diffie Helman"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 269). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What approach does PGP take to encrypt emails",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.f77ca056-76a1-4604-991f-43857ad69348"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Generate secret key, encrypt message, encrypt secret key with recipients public key"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Encrypt message using recipients public key"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Hash message using MD5 encrypt message with hash"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 269). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What are the 2 modes of IPSEC",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.5006fa32-5b2f-4507-893a-bd2af13d1560"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Output feedback mode"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Tunnel Mode"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Network mode"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Transport Mode"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Presentation Mode"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B,D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "In IPsec transport mode, packet contents are encrypted for peer-to-peer communication. In tunnel mode, the entire packet, including header information, is encrypted for gateway-to-gateway communications."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 270). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What is known type of encryption attack",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.6b2c1fc2-93ab-435f-a412-e828bf12f919"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Man-in-the-middle"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Plaintext"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Meet-in-the-middle"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Brute Force Attack"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Reset Attack"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Chosen Plaintext Attach"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "Birthday attack"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "Replay attach"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Brute-force attacks are attempts to randomly find the correct cryptographic key. Known plaintext, chosen ciphertext, and chosen plaintext attacks require the attacker to have some extra information in addition to the ciphertext. The meet-in-the-middle attack exploits protocols that use two rounds of encryption. The man-in-the-middle attack fools both parties into communicating with the attacker instead of directly with each other. The birthday attack is an attempt to find collisions in hash functions. The replay attack is an attempt to reuse authentication requests."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 270). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What is an approach to data center design?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.34f8f5f8-7d2d-477f-ab57-7fd1fe669608"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Temperature and Humdity Mgt Approach"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "HVAC Approach"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Hot Aisle, Cool Aisle Approach"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Sustainability Approach"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "HVAC units pull warm air from aisle which expel heat from compute devices"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/data-center-environmental-protection?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the maximum acceptable temperature for a data center?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.72ebedd7-dd3d-45a7-add1-0cda8d7dec3b"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "64.6 Fahrenheit"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "80.6 Fahrenheit"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "74.8 Fahrenheit"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/quiz/urn:li:learningApiAssessment:4613502?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Fires are typically classified by fuels that start them. Which of the following is associated with fires caused by flammable , liquids  like gasoline, petroleum oil or propone?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.d92d2764-f1b3-4052-92cf-cfb92c3c78a8"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Class A"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Class B"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Class C"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Class D"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Class K"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": [
     {
      "value": "Class A- common cumbustible material - wood, trash"
     },
     {
      "value": "Class B - cumbustible liquids"
     },
     {
      "value": "Class C - Electrical"
     },
     {
      "value": "Class D - Combustible metals"
     },
     {
      "value": "Class K - Kitchen - fats"
     },
     {
      "value": "https://en.wikipedia.org/wiki/Fire_class"
     }
    ]
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "What class of fire extinguisher is designed to work on electrical fires?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.a8411004-c988-444c-ac3a-a4abf9161342"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "K"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": [
     {
      "value": "Class A- common cumbustible material - wood, trash"
     },
     {
      "value": "Class B - cumbustible liquids"
     },
     {
      "value": "Class C - Electrical"
     },
     {
      "value": "Class D - Combustible metals"
     },
     {
      "value": "Class K - Kitchen - fats"
     },
     {
      "value": "https://en.wikipedia.org/wiki/Fire_class"
     }
    ]
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/quiz/urn:li:learningApiAssessment:4613502?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following hashing algorithms produces output less than 200",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.a23d32d6-aa8e-4673-88be-f0632f06aafd"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "SHA2"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Whirlpool"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "SHA1"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "AES-CCMP"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "RC5"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "MD5"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C,F"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "SHA1 - 160 bit hash\nMD5 - 128 bit hash\n Whirlpool - 512\n AES-CCMP - not a hash\nRC5 - not a hash"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=stf9UlkYYn0"
   }
  ],
  "value": ""
 },
 {
  "name": "Brian computes the digest of a single sentence of text using a SHA-2 hash function. He then changes a single character of the sentence and computes the hash value again. Which one of the following statements is true about the new hash value?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.ed31c85c-7dcc-4dcd-8c72-656fc170613e"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "The new hash value will be one character different from the old hash value."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "The new hash value will share at least 50% of the characters of the old hash value."
   },
   {
    "name": "C",
    "attrs": [],
    "value": "The new hash value will be unchanged."
   },
   {
    "name": "D",
    "attrs": [],
    "value": "The new hash value will be completely different from the old hash value."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapter 7 Review QUestion 1\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which cryptographic algorithm forms the basis of the El Gamal cryptosystem?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.58a12dd9-3666-4b5d-85cd-aa44bb6f5cd0"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "RSA"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Diffie-Helman"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "3DES"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "IDEA"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The El Gamal cryptosystem extends the functionality of the Diffie-Hellman key exchange protocol to support the encryption and decryption of messages."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapter 7 Review QUestion 2 :\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "If Richard wants to send an encrypted message to Sue using a public key cryptosystem, which key does he use to encrypt the message?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.dce1f321-49a2-4f8b-9101-0bfd9c728bbd"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Richards Public Key"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Richards Private Key"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Sues public key"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Sues private key"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapter 7 Review QUestion 3\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "If a 2,048-bit plaintext message were encrypted with the El Gamal public key cryptosystem, how long would the resulting ciphertext message be?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.c8ca9146-cf0c-4716-b496-73f408237891"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "1024"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "2048"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "4096"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "8192"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The major disadvantage of the El Gamal cryptosystem is that it doubles the length of any message it encrypts. Therefore, a 2,048-bit plain-text message would yield a 4,096-bit ciphertext message when El Gamal is used for the encryption process."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapter 7 Review QUestion 4\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Acme Widgets currently uses a 1,024-bit RSA encryption standard companywide. The company plans to convert from RSA to an elliptic curve cryptosystem. If it wants to maintain the same cryptographic strength, what ECC key length should it use?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.c28283a3-48a5-4567-b001-41f6175a8c0c"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "160"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "512"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "1024"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "2048"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The elliptic curve cryptosystem requires significantly shorter keys to achieve encryption that would be the same strength as encryption achieved with the RSA"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapter 7 Review QUestion 5\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "John wants to produce a message digest of a 2,048-byte message he plans to send to Mary. If he uses the SHA-1 hashing algorithm, what size will the message digest for this particular message be?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.4982434d-73e5-49be-838d-cd06267900dc"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "160"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "512"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "1024"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "2048"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The SHA-1 hashing algorithm always produces a 160-bit message digest, regardless of the size of the input message. In fact, this fixed-length output is a requirement of any secure hashing algorithm."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapter 7 Review QUestion 6\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 272). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following technologies is considered flawed and should no longer be used?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.fc15eb81-8bd0-44b4-876d-d326ae9294fe"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "SHA-3"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "PGP"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "WEP"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "TLS"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The WEP algorithm has documented flaws that make it trivial to break. It should never be used to protect wireless networks."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapter 7 Review QUestion 7\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Richard received an encrypted message sent to him from Sue. Which key should he use to decrypt the message?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.759411ae-8c3c-4685-a2cc-da7b1c058fed"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Richards public key"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Richards private key"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Sues public key"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Sues private key"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapter 7 Review QUestion 9\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Richard wants to digitally sign a message he’s sending to Sue so that Sue can be sure the message came from him without modification while in transit. Which key should he use to encrypt the message digest?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.fcc210af-c43a-4a79-a74f-0319d6ab05d6"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Richards public key"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Richards private key"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Sues public key"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Sues private key"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapter 7 Review QUestion 10\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following algorithms is not supported by the Digital Signature Standard?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.5693599b-c8ac-4d9d-be9d-534d879a543d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "DSA"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "RSA"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "El gammal"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "ECC"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Digital Signature Standard allows federal government use of the Digital Signature Algorithm, RSA, or the Elliptic Curve DSA in conjunction with the SHA-1 hashing function to produce secure digital signatures."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapter 7 Review QUestion 11\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which International Telecommunications Union (ITU) standard governs the creation and endorsement of digital certificates for secure electronic communication?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.c927df0d-8486-4788-990d-47b5c11686f9"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "x.500"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "x.509"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "x.900"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "x.905"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapter 7 Review QUestion 12\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What cryptosystem provides the encryption/decryption technology for the commercial version of Phil Zimmerman’s Pretty Good Privacy secure email system?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.deb64a92-5c35-4a1d-8c55-09cd50553b9a"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "ROT13"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "IDEA"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "ECC"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "El Gammal"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Pretty Good Privacy uses a “web of trust” system of digital signature verification. The encryption technology is based on the IDEA private key cryptosystem."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapter 7 Review QUestion 13\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What type of cryptographic attack rendered Double DES (2DES) no more effective than standard DES encryption?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.105e99a2-f3bc-4ad7-8ce4-c95aa8549043"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Birthday Attack"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Chosen Cipher Text"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Meet in the middle"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Man in the middle"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "In the meet-in-the-middle attack, the attacker uses a known plaintext message. The plain text is then encrypted using every possible key (k1), and the equivalent ciphertext is decrypted using all possible keys (k2). When a match is found, the corresponding pair (k1, k2) represents both portions of the double encryption. This type of attack generally takes only double the time necessary to break a single round of encryption (or 2n rather than the anticipated 2n * 2n), offering minimal added protection."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapter 7 Review QUestion 15\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following tools can be used to improve the effectiveness of a brute-force password cracking attack?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.01582e2e-31ec-4c50-a17c-ceb4c0b3ca20"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Rainbow Tables"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Hierarchical Screening"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "TKIP"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Random Enhancement"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Rainbow tables contain precomputed hash values for commonly used passwords and may be used to increase the efficiency of password cracking attacks."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapter 7 Review QUestion 16\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following links would be protected by WPA encryption?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.14799fb1-0790-46db-88a1-8726e8a86ccb"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Firewall to Firewall"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Router to Firewall"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Client to Wireless Access Point"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Wireless Access Point To Router"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapter 7 Review QUestion 17\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What is the major disadvantage of using certificate revocation lists?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.4e501025-9160-4346-85b2-1018121edc4c"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Key Management"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Latency"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Record Keeping"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Vulnerability To Brute Force Attacks"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapter 7 Review QUestion 18\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following encryption algorithms is now considered insecure?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.1e2aed4e-2a47-4c2b-9918-4db59e4df757"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "El Gamal"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "RSA"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Elliptic Curve Cryptography"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Merkle-Hellman Knapsack"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The Merkle-Hellman Knapsack algorithm, which relies on the difficulty of factoring super-increasing sets, has been broken by cryptanalysts."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapter 7 Review QUestion 19\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What does IPsec define?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "COM.7ad2eeef-4063-4bc8-91bc-e1a9c64843f4"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "All possible security classifications for a specific configuration"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "A framework for setting up a secure communication channel"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "The valid transition states in the Biba model"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "TCSEC security categories"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapter 7 Review QUestion 19\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 }
]