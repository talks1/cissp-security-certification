export default [
 {
  "name": "ISO 27018:2019 provides guidance for cloud service providers on handling PII stored in their cloud. Which of the following is not a key principle definedin ISO 27018?",
  "attrs": [],
  "items": [
   {
    "name": "Id",
    "attrs": [],
    "value": "SAE.7b265a2b-b2cf-4308-88d3-d68f8f112379"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "CSPs require customer consent to to use PII"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Customers maintain control over how information is used"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Maintain encrypted backups of customer data"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Transparency regarding where data resides"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Communication in event of breach"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Independent annual audit"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": [
     {
      "value": "C"
     }
    ]
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "C is to detailed to be part of this specification"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is considered the most common form of fault-tolerant RAID technology?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.3fc2d7ce-6477-4626-b960-70abe0d13bfe"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "RAID 0"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "RAID 5"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "RAID 6"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "RAID 0+1"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "3 disks striping and parity survives 1 disk failure"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Which of the following is a responsibility of an information owner",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "ASE.15c7653f-b8db-45a8-9b2e-c2baf1566d24"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Perform Regular Backups of Data"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Identify classification level of data"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Develop an information classification policy"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Labelling information with its classification level"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Custodian does nightly backups and labels information\nPolicy more likely to be defined at the enterprise level"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "PCI DSS is a mandated information security standard that applies to organizations that accept credit cards.  The security requirements for compliance with PCI DSS allow merchants to only store and retain certain types of cardholder information.  Which are allowed to be stored by a merchant? (Choose 3)",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "ASE.50517f05-c6f7-414e-95e0-043619a9889d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Card Holders PIN"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Card Holders account number in plain text"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Card Expiration Date"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Full Magmentic Step Data"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "CAV2/CVS2/CVV2/CID"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Card Holders Name"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "Card Holders Account encrypted"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C,F,G"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=IpnH7EXLGZ0"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the most following is the MOST important security consideration when selecting a new computer facility?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "ASE.6bb00c0c-ead2-4f03-b58e-ed386609b571"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Local Law enforment response times"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Adjacent to competitor facilities"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Aircraft Flight Paths"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Utility Infrastructure"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Your fire system is being upgraded and you are considering options.  One of the requirements is  notification of the local fire department when there is an event. Which type of system will provide this functionality?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "ASE.e236b52c-28e0-4763-a96e-0d92cdb4fe20"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Protected Premises Fire Alaram System"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Central Station Fire Alarm System"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Auxilliary Fire Alarm System"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Public Fire Alarm System"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Municipal Fire Alarm System"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Combination Fire Alarm System"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Protected Premises Fire Alaram System is a local only solution\nCentral Station Fire Alarm System - alarm is monitoring company \nAuxiliary Fire Alarm System - alarm is local but also the fire department is notified"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=NlaheC1N5XQ"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is not a commonly used business classification level?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "ASE.874a3a76-32fa-4e03-b956-2003d16429d0"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Top Secret"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Sensitive"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Internal"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Highly Sensitive"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-2-asset-security-2/quiz/urn:li:learningApiAssessment:4581030?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following is not one of the GAPP Privacy Principles",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "ASE.a5f01d9e-4206-4a7f-83b3-d5e862b84bad"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Notice"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Management"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Quality"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Integrity"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Monitoring"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Choice"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Management - policy and procedures in place to protect privacy\nNotice - notify subject their information is being recorded\nChoice - subjects should be given choices around the data that is recorded and stored\nCollection - information should only be used for the purpose it is intended\nUse, Retentation and Disposal - \nAccess - subjects should be able to review and update\nDisclosure - only share information if that sharing is consistency and subject has agreed\nSecurity - must secure private information\nData Quality - reasonable steps to ensure private information is update to date\nMonitoring - monitoring must be in place"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-2-asset-security-2018/data-privacy?u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What U.S. federal government agency publishes security standards that are widely used throughout the government and private industry?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "ASE.4d27a5e7-3c0b-4c5c-82e0-412f10087f62"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "FCC"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "NIST"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "CIA"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "FTC"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-2-asset-security-2/quiz/urn:li:learningApiAssessment:4579766?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What letter is used to describe the file permissions given to all users on a Linux system?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "ASE.e39a7d27-ebd5-4d8f-b8c6-d7d840001f8f"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "u"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "o"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "g"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "r"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-2-asset-security-2/quiz/urn:li:learningApiAssessment:4579766?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the formula used to compute the ALE?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "ASE.a413e75e-6992-4e34-a6cb-9155bf3fdbf5"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "ALE = AV x EF x ARO"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "ALE = ARO x EF"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "ALE = AV x ARO"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "ALE = EF x ARO"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "SLE is AV x EF and ALE is SLE x ARO"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. xlvi). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What is the first step of the business impact assessment process?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "ASE.8c7c474d-8fcc-4e53-87a4-a421b41958f0"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Identification of priorities"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Likelihood assessment"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Risk identification"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Resource prioritization"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The priorities for the business must first be determined to order the risks subsequently identified"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. xlvi). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "What kind of recovery facility enables an organization to resume operations as quickly as possible, if not immediately, upon failure of the primary facility?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "ASE.14764538-bf76-44d4-87a1-7b85d36409b8"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Hot Site"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Warm Site"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Colde Site"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "All of the above"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. xlvii). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "You have been given a hard drive that spins at 15000RPM. You task is to erase the data such that the erase data cannot be recovered using readily available \"keyboard recovery\" tool.  At a minimum what must you do.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "ASE.d44626e8-f614-487b-ae8b-90ba24ed6701"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "use a data purging tool"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Physically destroy the drive"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "use a data clearing tool"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Format the drive using a different file system"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Purging is a more itense operation than clearing.\nClearing removes data in free space , swap space and any in use space."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=ha5-i1TJ1f8&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=85"
   }
  ],
  "value": ""
 }
]