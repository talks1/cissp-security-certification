export default [
 {
  "name": "Which of the following is key shortcoming of behaviour based (anomoly based) intrusion detection systems?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.a78fa02d-6d39-4893-9db6-38ebfa7e0f13"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "requires regular signature database updates"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "High number of false positives"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Limited to detecting signatures of known attacks"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Changes in network traffic pattern go unnoticed"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "You are developing an attack tree for a web application and as part of the process you are attempting to anticipate your potential attackers. Which of the following will you need to identify in order to characterize a likely adversary?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.4ad1a539-3c12-4891-a3a9-6caa07ef97bf"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Ease of vulnerability discovery"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Attacker Motive"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Damage Potential"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Opportunity"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Trust Boundaries"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Means"
   },
   {
    "name": "G",
    "attrs": [],
    "value": "Exploitability"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B,D,F"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "This is the basis of the MOM acronym. Means, opportunity and motive."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "Which is not true of penetration testing?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.4ce34ae8-70c4-4eb9-9473-1cdd886b8155"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Define clearly established boundaries before the test begins"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Typically performed after a vulnerability assessment is completed"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Utilize only passive tools combined with social engineering"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Define success criteria based on test objectives"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Rules of engagement are not defined in a black box test"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C,E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "A is important so you dont break things\nB is important so you can mitigate known weaknesses and mitigate before the penetration test\nFor E there are always limits which need to be discussed before attempting a penetration test"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   }
  ],
  "value": ""
 },
 {
  "name": "The \"Rules of Engagement\" are a critical component of any penetration test. Which is not something that needs to be agreed upon before  a pentest begins",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.deca50b7-b067-45c5-a42f-16bda760c66d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "The scope and range of the penetration test"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "The tools that will be used"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "The time frame/duration of the pen test"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Limits of liability of the pen testers"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "The tools dont really make any difference to the customer"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=wfDkTHDoPbI"
   }
  ],
  "value": ""
 },
 {
  "name": "In software development what is one of the primary differences between white-box and black-box testing?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.f1bb7f5c-d585-43eb-a9b4-9d9c2d3156c9"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "White box testing gives testers access to source code"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Black box testers fully deconstruct the app to identify vulnerabilities"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "White box testers are limited to pre-defined use cases"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Black box testers are more thorough and proficient"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "White box testing is done by developers"
   },
   {
    "name": "F",
    "attrs": [],
    "value": "Black box testing includes the line of business in the evaluation process"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Black box testing is more about the functionality\nWhite box testing is more about issues with the code base."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=ziefG4Qz9Pc&list=RDCMUCwUkAunxT1BNbmKVOSEoqYA&index=2"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of detected incident allows the most time for an investigation?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.ee13bc11-df97-4f1e-9697-df3630686b74"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Compromise"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Denial Of Service"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Malicious Code"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Scanning"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. xlvii). Wiley. Kindle Edition."
   }
  ],
  "value": ""
 },
 {
  "name": "As part of evidence collection process an investigator opens a terminal  on the suspect machine and issues commands to display the current network settings, ARP cache, resolver cache and routing table. He uses his cell phone to take pictures. Which of the following is true regarding his actions?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.be55d545-d60c-4760-b0ec-745a0b7854de"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "By viewing the data, disclosure rules have been violated."
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Running command has altered the system"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Chain of custody has been broken"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Photos are not considered legally authentic."
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Commands should only be executed through remote SSH."
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Dont rely on binaries on the system. The idea is to bring your own binaries on a read only media"
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=Ja4PxXHM7vw"
   }
  ],
  "value": ""
 },
 {
  "name": "A developer in your company has written a script that changes a numeric value before it is read and used by another program. After the value is read, the developers script changes the value back to its original value. What is this an example of?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SOP.32a11d5b-acd7-4b24-90cb-3b64cfd1b3b5"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Hacking"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Salalmi Slicing"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Time of Check/Time of Use"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Poly Instantiation"
   },
   {
    "name": "E",
    "attrs": [],
    "value": "Data Diddling"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "E"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.youtube.com/watch?v=Ja4PxXHM7vw"
   }
  ],
  "value": ""
 },
 {
  "name": "A ???? is an externally occurring force that jeopardizes the security of your systems.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.a00e916b-7537-4873-a83c-3fa6918a2410"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Vulnerability"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Countermeasure"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Risk"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Threat"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-6-security-assessment-and-testing-2018/quiz/urn:li:learningApiAssessment:4578328?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "In a ????  penetration test, the attacker has no prior knowledge of the environment.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.2ff07fc2-a832-4426-b6c8-c39e14016762"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "rainbox box"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "black box"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "white box"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "grey box"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-6-security-assessment-and-testing-2018/quiz/urn:li:learningApiAssessment:4578328?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Safe mode scans provide the most comprehensive look at system security.",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.17fe3314-3394-4ce2-ac19-ffbe4b08f7dd"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "TRUE"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "FALSE"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-6-security-assessment-and-testing-2018/quiz/urn:li:learningApiAssessment:4578328?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What is the first step of a Fagan inspection?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.6614d4cf-3c89-4077-a5e3-d58bd3b20808"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Planning"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Meeting"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Overview"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Preparation"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-6-security-assessment-and-testing-2018/quiz/urn:li:learningApiAssessment:4579120?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of fuzz testing captures real software input and modifies it?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.702f29ba-d39d-4d1c-aed7-b20f1799ba0d"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Mutation Fuzzing"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Switch Fuzzing"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Generation Fuzzing"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Twist Fuzzing"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-6-security-assessment-and-testing-2018/quiz/urn:li:learningApiAssessment:4579120?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What disaster recovery metric provides the targeted amount of time to restore a service after a failure?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.d1356dd4-9a5c-4fd7-8963-7542ff54f86f"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "RPO"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "MTO"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "RTO"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "TLS"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-6-security-assessment-and-testing-2018/quiz/urn:li:learningApiAssessment:4577404?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "What type of backup includes only those files that have changed since the most recent full or incremental backup?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.4aa036ef-245b-4491-ac42-175c45dcedb1"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "differential"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "partial"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "full"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "incremental"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "D"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": "Incremental backups only capture changed files since last incremental backup.\nDifferential backups capture changes sinces last full backup."
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-6-security-assessment-and-testing-2018/quiz/urn:li:learningApiAssessment:4577404?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "name": "Which one of the following disaster recovery tests involves the actual activation of the DR site?",
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAT.75ff3172-603b-48dc-9c93-0ebbf67365ae"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "read-through"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "parallel test"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "simulation"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "walk through"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "B"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-6-security-assessment-and-testing-2018/quiz/urn:li:learningApiAssessment:4577404?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.4ea5257d-ba1f-4b27-862c-cd009a6cb267"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "CRIs"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "KRIs"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "KPIs"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "CPIs"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "C"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-6-security-assessment-and-testing-2018/quiz/urn:li:learningApiAssessment:4577406?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 },
 {
  "attrs": [],
  "items": [
   {
    "name": "ID",
    "attrs": [],
    "value": "SAE.144a153c-a9d8-44c7-b359-9eeff23567cf"
   },
   {
    "name": "A",
    "attrs": [],
    "value": "Compensating"
   },
   {
    "name": "B",
    "attrs": [],
    "value": "Technical"
   },
   {
    "name": "C",
    "attrs": [],
    "value": "Administrative"
   },
   {
    "name": "D",
    "attrs": [],
    "value": "Logical"
   },
   {
    "name": "Answer",
    "attrs": [],
    "value": "A"
   },
   {
    "name": "Explanation",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Goal Implication",
    "attrs": [],
    "value": ""
   },
   {
    "name": "Source",
    "attrs": [],
    "value": "https://www.linkedin.com/learning/cissp-cert-prep-6-security-assessment-and-testing-2018/quiz/urn:li:learningApiAssessment:4577406?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675"
   }
  ],
  "value": ""
 }
]