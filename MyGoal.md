# First Goal

To pass the exam

# Second Goal 

Possible goals as I progress through this material:

Product Owners guide to CISSP
- focus on how the information is used to sell consulting time
- what are the implications for a software development team
- what are the implications for service delivery
- what are the prerequisites of the business idea (risk management) so that the idea moves into need for business continuity


While security is necessary it cant do more harm than good... how to avoid it getting in the way.
Availability is one triad of security and poor security prevents availability.


Opportunity Discovery and Evaluation


Best Practice - How certain are we in the CISSP answers.  Lets question best practice because the CISSP perspective is that of a large organization and it is very risk averse which may not be appropriate for the scale of our organization which needs to be more risk taking that a large organization.