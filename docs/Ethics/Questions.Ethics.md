

# Which of the of the following are not one of the ICS2 code of ethics
## Id
ETH.14c04a21-b094-4309-af29-248671ec2756
## A
Protect Society
## B
Use methods, tools, responsible
## C
Act honorable, honestly,justly, responsible and legally
## D
Provide diligent and competent services to principals
## E
Avoid competing professional and personal interests
## F
Advance and protect the profession

##  Answer
B,E
## Explanation


# Which of the following statements best aligns with ISC2 code of ethics (Choose Four)
## ID
EHT.45f534d7-1276-456a-ac81-fe0f4033bb6b
## A
Work to protect society, public trust and infraststructure
## B
Avoid using the internet as a test network, consider the potential outcomes of your actions
## C
be honest, act responsibly and within the confines of the law
## D
Deliver your product ontime , as defined and within the allowed budget
## E
Be decisive, confident and articulate when dealing with principles
## F
Safeguard your system using a complement of administrative technical and physical controls
## G
be competent in what you do and be diligent in the maintenance of that competenence
## H
Seek, through your actions, to improve the profession
##  Answer
A,C,G,H
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=vp7DA6TqDNw
