Domain 2 Agenda
- Introduction to Asset Security
- Data Classification
- Data Protection
- System Hardening and Baselining
- Threats to Data Storage
- Data Redundancy
- Secure Data Disposal


Data Security
 - Relates to 
    - Data at Rest
    - Data in Motion

- Data Security Controls
    - Policies
    - Encryption
    - Access Controls


Policies
- Classification Levels
- Data Storage
- Data Life Cycle policies
    - Data Retention
    - Data Disposal

Data Security Roles
    - Data Owner - bear responsibility for privacy, set policies and standards
    - Data Steward - handle implementation of policies
    - Data Custodian - 

Data Privacy - 
    Generally Accepted Privacy Principles
    Principles
        - Management - organization dealing with private information should have
        - Notice - notice that private information is being collected
        - Choice and Consent - 
        - Collection - only collect information which is disclosed to the owner of that data
        - Use, Rentation and Disposal
        - Access - Provide ability to update
        - Disclosure to 3rd party - only with consent
        - Security - private information should be protected
        - Qualty - private information is complete and update to date
        - Monitoring and Enforcement

Security Baselines
    - Mechanism to apply minimum security to a set of devices
    - Avoid configure these settings device by device
    
    - Sources of security standards
        -Vendors - Microsoft Security Compliance Manager
        - Government - NIST
        - Independent Organizations - Center for Internet Security Benchmarks

File Permissions
    NTFS
        - Full Permission
        - Read
        - Read and Execute
        - Write
        - Modify

Encryption
    - HSM - Hardware Security Modules
        - TPM - holds the encryption keys for whole disk encryption

Information Classification
    - Can classify systems 
        - Only able to process information at a specific level or lower
    - Labeling Requirements - Can apply to data or systems


Classification Systems
- Military -
    - Levels - Top Secret,
               Secret,
               Confidential,
               Unclassified
- Commercial 
    - Levels -  Confidential, 
                Private, 
                Sensitive, 
                Public