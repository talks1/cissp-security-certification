

# ISO 27018:2019 provides guidance for cloud service providers on handling PII stored in their cloud. Which of the following is not a key principle definedin ISO 27018?
## Id
SAE.7b265a2b-b2cf-4308-88d3-d68f8f112379
## A
CSPs require customer consent to to use PII
## B
Customers maintain control over how information is used
## C
Maintain encrypted backups of customer data
## D
Transparency regarding where data resides
## E
Communication in event of breach
## F
Independent annual audit
##  Answer
- C
## Explanation
C is to detailed to be part of this specification
## Goal Implication


# Which of the following is considered the most common form of fault-tolerant RAID technology?
## ID
SOP.3fc2d7ce-6477-4626-b960-70abe0d13bfe
## A
RAID 0
## B
RAID 5
## C
RAID 6
## D
RAID 0+1
##  Answer
B
## Explanation
3 disks striping and parity survives 1 disk failure
## Goal Implication


# Which of the following is a responsibility of an information owner
## ID
ASE.15c7653f-b8db-45a8-9b2e-c2baf1566d24
## A
Perform Regular Backups of Data
## B
Identify classification level of data
## C
Develop an information classification policy
## D
Labelling information with its classification level
##  Answer
B
## Explanation
Custodian does nightly backups and labels information
Policy more likely to be defined at the enterprise level
## Goal Implication

# PCI DSS is a mandated information security standard that applies to organizations that accept credit cards.  The security requirements for compliance with PCI DSS allow merchants to only store and retain certain types of cardholder information.  Which are allowed to be stored by a merchant? (Choose 3)
## ID
ASE.50517f05-c6f7-414e-95e0-043619a9889d
## A
Card Holders PIN
## B
Card Holders account number in plain text
## C
Card Expiration Date
## D
Full Magmentic Step Data
## E
CAV2/CVS2/CVV2/CID
## F
Card Holders Name
## G
Card Holders Account encrypted
##  Answer
C,F,G
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=IpnH7EXLGZ0

# Which one of the most following is the MOST important security consideration when selecting a new computer facility?
## ID
ASE.6bb00c0c-ead2-4f03-b58e-ed386609b571
## A
Local Law enforment response times
## B
Adjacent to competitor facilities
## C
Aircraft Flight Paths
## D
Utility Infrastructure
##  Answer
D
## Explanation
## Source


# Your fire system is being upgraded and you are considering options.  One of the requirements is  notification of the local fire department when there is an event. Which type of system will provide this functionality?
## ID
ASE.e236b52c-28e0-4763-a96e-0d92cdb4fe20
## A
Protected Premises Fire Alaram System
## B
Central Station Fire Alarm System
## C
Auxilliary Fire Alarm System
## D
Public Fire Alarm System
## E
Municipal Fire Alarm System
## F
Combination Fire Alarm System
##  Answer
C
## Explanation
Protected Premises Fire Alaram System is a local only solution
Central Station Fire Alarm System - alarm is monitoring company 
Auxiliary Fire Alarm System - alarm is local but also the fire department is notified
## Goal Implication
## Source
https://www.youtube.com/watch?v=NlaheC1N5XQ


# Which one of the following is not a commonly used business classification level?
## ID
ASE.874a3a76-32fa-4e03-b956-2003d16429d0
## A
Top Secret
## B
Sensitive
## C
Internal
## D
Highly Sensitive 
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-2-asset-security-2/quiz/urn:li:learningApiAssessment:4581030?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Which one of the following is not one of the GAPP Privacy Principles
## ID
ASE.a5f01d9e-4206-4a7f-83b3-d5e862b84bad
## A
Notice
## B
Management
## C
Quality
## D
Integrity
## E
Monitoring
## F
Choice
##  Answer
D
## Explanation
Management - policy and procedures in place to protect privacy
Notice - notify subject their information is being recorded
Choice - subjects should be given choices around the data that is recorded and stored
Collection - information should only be used for the purpose it is intended
Use, Retentation and Disposal - 
Access - subjects should be able to review and update
Disclosure - only share information if that sharing is consistency and subject has agreed
Security - must secure private information
Data Quality - reasonable steps to ensure private information is update to date
Monitoring - monitoring must be in place
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-2-asset-security-2018/data-privacy?u=25288675


# What U.S. federal government agency publishes security standards that are widely used throughout the government and private industry?
## ID
ASE.4d27a5e7-3c0b-4c5c-82e0-412f10087f62
## A
FCC
## B
NIST
## C
CIA
## D
FTC
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-2-asset-security-2/quiz/urn:li:learningApiAssessment:4579766?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What letter is used to describe the file permissions given to all users on a Linux system?
## ID
ASE.e39a7d27-ebd5-4d8f-b8c6-d7d840001f8f
## A
u
## B
o
## C
g
## D
r
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-2-asset-security-2/quiz/urn:li:learningApiAssessment:4579766?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What is the formula used to compute the ALE?
## ID
ASE.a413e75e-6992-4e34-a6cb-9155bf3fdbf5
## A
ALE = AV x EF x ARO
## B
ALE = ARO x EF
## C
ALE = AV x ARO
## D
ALE = EF x ARO
##  Answer
A
## Explanation
SLE is AV x EF and  ALE is SLE x ARO
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. xlvi). Wiley. Kindle Edition. 


# What is the first step of the business impact assessment process?
## ID
ASE.8c7c474d-8fcc-4e53-87a4-a421b41958f0
## A
Identification of priorities
## B
Likelihood assessment
## C
Risk identification
## D
Resource prioritization
##  Answer
A
## Explanation
The priorities for the business must first be determined to order the risks subsequently identified
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. xlvi). Wiley. Kindle Edition. 


# What kind of recovery facility enables an organization to resume operations as quickly as possible, if not immediately, upon failure of the primary facility?
## ID
ASE.14764538-bf76-44d4-87a1-7b85d36409b8
## A
Hot Site
## B
Warm Site
## C
Colde Site
## D
All of the above
##  Answer
A
## Explanation
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. xlvii). Wiley. Kindle Edition. 

# You have been given a hard drive that spins at 15000RPM. You task is to erase the data such that the erase data cannot be recovered using readily available "keyboard recovery" tool.  At a minimum what must you do.
## ID
ASE.d44626e8-f614-487b-ae8b-90ba24ed6701
## A
use a data purging tool
## B
Physically destroy the drive
## C
use a data clearing tool
## D
Format the drive using a different file system
##  Answer
C
## Explanation
Purging is a more itense operation than clearing.
Clearing removes data in free space , swap space and any in use space.
## Goal Implication
## Source
https://www.youtube.com/watch?v=ha5-i1TJ1f8&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=85