
## JBOD

Just a bunch of disks
No redundancy


## RAID 0 
Writes dat across multiple disks (striping) without parity.

## RAID 1 
mirroring,
1:1 drive ratio.
can survive a single disk failure only

called mirror when there is a single driver controller
called duplex when there are 2 drive controllers


## RAID 5
Data stripping with parity which lets you recover data if a single disk is lost.
3 drives are required
Most common
Hot swappable
Can withstand a single drive failure

## RAID 6
Data striping with double parity
4 disks required
can withstand 2 drive failures
