

# Which of the of the following is a VALID technique for attacking smartcard

## Id

SAE.4a1cc150-1207-4b17-bce2-7490cfaa00c0

## A
Brute force attacks against symetric keys

## B
Factor the product of RSA primes

## C
Side channel attacks using differential power analysis

## D
Malware on users compute that will extract the private key when user logs in.


##  Answer
- C

## Explanation

Apparently you can measure the power consumption of a device which reads a smart card.  If you have the right keys the power usage goes up.

## Goal Implication

Is there any evidence this has been done successfully.
Does it scale well to really be a threat?
The hacker needs possession of the card.
Seems very low risk.



# Which 2 should you consider before moving your companies accounting system to a cloud saas?

## Id

SAE.06b2c0ab-8470-471e-8b3f-0f461fd179c5

## A
SLA (Service Level Agreement)

## B
MOU (Memorandum of Understand)

## C
NDA (Non Disclosure Agreement)

## D
Software License 

## E
ISA (Interconnection Service Agreement)

##  Answer
- A, C

## Explanation


## Goal Implication



# Which of the following is a key concern for ephemeral diffie-helman?
## Id
SAE.79fb5946-3cde-428d-9e3a-f69c8a102d9a
## A
Forward Secrecy is not available
## B
No authentication
## C
Weak encryption
## D
Long term private key compromise
##  Answer
B
## Explanation
There is no authentication involved in diffie helman so it needs to be augemented to support this.

Diffier Helman is about 2 parties being able to establish a secret key over unsecure channel. It uses Public Private Key Crypto to acheive this.

## Source
https://www.youtube.com/watch?v=D5alTeDIg-Y


# What is the best way to control data when you only want users in certain location to access?

## Id

SAE.4c57f512-23d5-43bb-99a8-26a9142c5039

## A
MAC

## B
DAC

## C
RBAC

## D
ABAC


##  Answer
- D

## Explanation

Location attribute can be provided

## Goal Implication




# Which of the following is NOT a property of the bell-lapula security model
## Id
SAE.babad34d-b39d-4eb7-b3e1-9225a2057e7c
## A
Simple Security Property
## B
Property Rule
## C
Tranquility Principle
## D
Strong Star Property
## D
Simple Integration Axiom
##  Answer
D
## Explanation
Bell-Lapadula relates to confidentiality and not integrity.
## Goal Implication


# Which one of the following is a major part of Trusted Computer Base?
## Id
SAE.6a01fced-175c-4544-ad0a-dda6b5d200a1
## A
The software
## B
The security subsystem
## C
The operating system software
## D
The reference monitor
##  Answer
D
## Explanation
The trusted computing base (TCB) of a computer system is the set of all hardware, firmware, and/or software components that are critical to its security
A reference monitor is a system component that enforces access controls on an object.
## Goal Implication
Access control for GeoDocs should be centralized into a security kernel as the trusted based for the security.

# Which of the following are symmetric algorithms?
## ID
SAE.0a11af5b-f328-44e5-a7cf-ddbea82796cd
## A
Serpent
## B
RSA
## C
MQV
## D
Blowfisth
## E
RC5
## F
Diffie Helman
##  Answer
A,D,E
## Explanation
Serpent, Blowfish are symmetric block ciphers
RC5 is a symmetric stream cipher
## Goal Implication


# You are sending an email ecrypted with a symmetric key. The symmetric key is encrypted using the recipients public key. What is the commmon term used to describe the encrypte message structure
## ID
SAE.be55d654-e5e3-499d-9a88-b695b53e152a
## A
Digital Signature
## B
Hashed Message Authentication Code (HMAC)
## C
Message Integrity Check (MIC)
## D
Digital Envelope
##  Answer
D
## Explanation
HMAC  is taking a message, adding a shared secret and hashing it and sending
## Goal Implication

# You are using an encryption schema which generate seemingly random bits which are then XOR'd with the plaintext data into order to produce ciphertext. Which type of algorithm is this?
## ID
SAE.9249acaa-d979-4ace-be76-58d172682f38
## A
Stream Cipher
## B
ECC
## C
Diffie-Helman
## D
RSA
## E
Block Cipher
##  Answer
A
## Explanation
## Goal Implication


# Confidentiality is a critical component of modern distributed computing systems. WHich of the following presents the greatest challenge to providing confidentiality for such an environment?
## ID
SAE.a1345cb3-480a-4f90-9b50-4893c8219940
## A
Heterogenity of systems
## B
Lack of protocol standardization
## C
Network scalability
## D
Inadequate system transparency
## E
Missing digital signatures
## F
Transmitting unencrypted data
##  Answer
F
## Explanation
other answers are problematic but doesnt directly related to confidentiality.
## Goal Implication
## Source
https://www.youtube.com/watch?v=CvdJSbjaBHY



# Which of the following is an example of tunneling network traffic
## ID
SAE.80ba46b3-b6f6-4993-8943-8b330c2cc61a
## A
NAT-T
## B
Masquerading
## C
PAT (Port Address Translation)
## D
SOCKS PROXY
## E
Stateless NAT64
##  Answer
A
## Explanation
NAT-T is a technique to wrap an encrypted IPSEC packet in UDP (as an example). UDP will be supported by NAT and thus be routable whereas the IPSEC by itself is not.
Masquerading is another name for NAT

## Goal Implication
## Source
https://www.youtube.com/watch?v=yLNBUJVOjAg




# Which of the following represents the best reason to upgrade your web application servers to TLS 1.3
## ID
SAE.6b623304-1239-48e4-ae30-79fbfd8d6e12
## A
The SNI is encrypted by default in 1.3
## B
The server certificate is encrypted when sent to the client
## C
TLS 1.3 supports a larger number of legacy algorthms
## D
The server uses certificate pinning to speed up connection times.
##  Answer
B

## Explanation
With earlier versions of TLS the server certificate is not encrypted which means that the server you are access is visible in plain text. This may not be desired.

SNI lets a client specify a hostname for the IP address it is targeting which allow one IP address to support many names.
TLS 1.3 actually drops support for a number of legacy algorthms.
## Goal Implication
## Source
https://www.youtube.com/watch?v=P7j3lJDMFgg


# In an effort to increase the security of SSH on your server you implement, a technique that requires users to connect to 3 seemingly random ports before connecting to port 22. Which of the following best describes this techqniue?
## ID
COM.37b5862f-996c-414b-86a1-23e7b8d968e2
## A
Firewalking
## B
Port mapping
## C
Port knocking
## D
Secret handshake
## E
Session triggering
## F
kernel handshaking
##  Answer
C
## Explanation
Port knocking is security by obsecurity.
## Goal Implication
## Source
https://www.youtube.com/watch?v=se14Kog6raE


# What is the term for hiding a message inside a large body of text
## ID
SAE.bfe26575-8dc5-45a3-8113-2fd842bd2b5c
## A
One Time Pad
## B
Digest
## C
Null Cipher
## D
Steganography
## E
Block Cipher
##  Answer
C
## Explanation
## Goal Implication
## Source


# Which of the followingis not an example of a side-channel attack?
## ID
SAE.e319e162-8501-40cb-9acf-70599a7946f5
## A
Differential power analysis
## B
Electromagnetic emission
## C
Corruptive
## D
Timing
##  Answer
C
## Explanation
ome examples of side-channel attacks that have been carried out on smart cards are differential power analysis (examining the power emissions released during processing), electromagnetic analysis (examining the frequencies emitted), and timing (how long a specific process takes to complete). These types of attacks are used to uncover sensitive information about how a component works without trying to compromise any type of flaw or weakness. They are commonly used for data collection.
## Goal Implication
## Source
https://www.briefmenow.org/isc2/which-of-the-followingis-not-an-example-of-a-side-channel-attack/


# "Subjects can access resources in domains of equal or lower trust levels." This is an easy sentence, but a difficult concept for many people to really understand.Which of the following is not an example of this concept?
## ID
SAE.1d71adc6-dfad-4bb2-bea4-b76cd51329b1
## A
The security officer can access over 80% of the files within a company.
## B
A contractor is only given access to three files on one file server.
## C
A security kernel process can access all processes within an operating system.
## D
A Guest account has access to all administrator accounts in the domain.
##  Answer
D
## Explanation
A subject can be a user, application, process or service – any active entity that isattempting to access a passive entity (object). Each of the answers providedexamples of how the more trusted subjects were given access to resources (a domain)that corresponded with its trust level. A Guest account does not have the trustlevel to access all administrator accounts.
## Goal Implication
## Source
https://www.briefmenow.org/isc2/which-of-the-following-is-not-an-example-of-this-concept/

# In the past 2 years your company has grown from locally maintained servers to a large number of cloud based instances. Managing system integrity has become more complex and error-prone process. As it relates to the this issue, which of the following broad approaches should you be implementing?
## ID
SAE.48e331a5-6b22-43ba-9d41-0a49c52072ac
## A
Information Security Continuous Monitoring
## B
Risk Managment
## C
Application Threat Modeling
## D
Public Key Infrastructure
## E
Configuration Management
##  Answer
E
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=XBOuWeR08QM

# Which of the following does a hot site have which a warm site does not?
## ID
SAE.9326b89a-e7b2-45d7-be30-ac2e1b6649a9
## A
Near Real Time Data
## B
Climate Control
## C
Physical Servers
## D
Telephones
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=tx70EN2ph_Y


# Which of the following are characteristics of elliptic curve cryptopgraphy (choose 4)?
## ID
SAE.5fd35553-4735-4512-8885-a64593bb016f
## A
It is stronger than RSA using significantly smaller key lengths
## B
It has a large memory footprint
## C
It can help conserve battery life in mobile?
## D
It has a lower CPU overhead compared to RSA.
## E
It is not support by most modern browsers
## F
ECC was introduced as an alternative to AES
## G
It can be used in Diffie Helman key exchange
##  Answer
A,C,D,G
## Explanation
ECC uses smaller keys which required less computation power.
## Goal Implication
## Source
https://www.youtube.com/watch?v=78QfBIQJQZk


# Which of the following is TRUE of  TLS, IPSEC and SSH?
## ID
SAE.aedc686c-0b07-4410-86a4-8d77e56ac5d8
## A
They are routeable and must be tunneled
## B
They all operate at the transport layer
## C
They are supported in ALL IPV4 nodes
## D
They all offer confidentiality for data
## E
They utilize only symmetric encryption
##  Answer
D
## Explanation
## Goal Implication
## Source
youtube.com/watch?v=l1ZLBwog1xU

# Which of these key lengths is available in the Rjindael encryption algorithm?
## ID
SAE.b660f9e4-bfac-4b14-bb26-91eb21c55f47
## A
64
## B
72
## C
128
## D
168
## E
192
## F
256
## G
512
##  Answer
C,E,F
## Explanation
Rjindeal supports key size in increments of 32 but these 3 are the only ones implemented.
## Source
https://www.youtube.com/watch?v=KQ3qIRmJdbw

# Mobile devices place an emphasis on battery consumption and frequently have limited processing power. What type of encryption is best suited to this
## ID
SAE.d2d79b54-cc60-4a2d-b766-f55add1fd1a2
## A
RSA
## B
Diffie-Helman
## C
EAP-TLS
## D
PEAP
## E
ECC
## F
AES
## G
Vernam Ciphers
##  Answer
E
## Explanation
ECC keys 12 times shorter than RSA keys are just as strong, so there is a efficiency savings
## Goal Implication
## Source
https://www.youtube.com/watch?v=vxLsrsnJ9AU&list=RDCMUCwUkAunxT1BNbmKVOSEoqYA&index=3


# Blowfish is an encryption algorthm originally designed as an alternative to DES. Which of the following are characteristics of Blowfish (Choose 4)?
## ID
SAE.94a686da-cbca-456b-91b2-31672e380a28
## A
Key Size of  128,192,256
## B
Key Sizes from  32-448 
## C
Symmetric
## D
Assymetric
## E
Patented
## F
Un-patented and license-free
## G
Used in bcyrypt
## H
Used in scrypt
##  Answer
B,C,F,G
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=vxLsrsnJ9AU&list=RDCMUCwUkAunxT1BNbmKVOSEoqYA&index=3

# Which of the following algorithms are asymmetric
## ID
SAE.7f008538-eefc-445d-b284-e62c82f2b477
## A
Advance Encryption Standard (AES)
## B
Blowfish
## C
Data Encryption Standard
## D
El Gamal
## E
RSA
##  Answer
D,E
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-the-basics-2/computerized-adaptive-testing-cat?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Cryptography provides many different types of protections for information. When utilized correctly, which of the following represent things that cryptography can do (Choose 6)
## ID
SAE.69167999-1b06-4123-a128-cecfcf54678d
## A
Detect if a spreadsheet has been changed in an unauthorized way
## B
Provide confidentiality for a windows users accessing a linux web server via a web browswer
## C
Prevent a user from deleting a file they have no permission to access
## D
Provide a high degree of assurance that a remote system is who it says it is
## E
Recover data changed in an authorized manner. 
## F 
Prevent a thief from putting a stolen hard drive into a separate computer and recovering the information.
## G 
Check a script is unmodified before allowing it to be executed
## H
Reduced the effectiveness of ICMP based Denial of Service attacks
## I
Prevent an authorized user from exfiltrating data from a protected network
## J
Assist with complying with regulatory data security requirements
##  Answer
A,B,D,F,G,J
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=f0foB_Ng0xE&t=1s

# A use hashes a file using SHA1. The user makes a small change, renames it and emails to a friend.  Hashing the new file would give a particular value.  6 months later an investigator wants to determine if the image file before change is located on the original users computer. Which technique might help determine this. Choose the best answer
## ID
SAE.82070721-5c48-47f8-bbcb-54cb6942de4c
## A
Hash all the files using SHA-256 and look for a matching hash
## B
Look in the Sent Items folder in the users email program
## C
Search for images of  similar size and file type.
## D
use a fuzzy hashing tool
## E
Create SHA1 hashes of each file and compare the last 8 bits of the hash value
##  Answer
D
## Explanation
Fuzzy hashing - hashes part of the file and then can be used to calculate a percentage of similarity to another file
## Goal Implication
This technique could be used to highlight changes in a document over time or since someone else last read the content.
## Source
https://www.youtube.com/watch?v=zCe_pXZ9LFM

# You have just received a digitally signed email message.What do you need in order to validate the integrity and authenticity of the message?
## ID
SRM.01ac8071-2c60-4081-b84b-185322c86e55
## A
Your pricate key
## B
Senders private key
## C
Root CAS private key
## D
Senders public key
## E
The passphrase for the message.
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=zCe_pXZ9LFM


# Which of the following best describes a zero day vulnerability?
## ID
SAE.1b3ac04f-0db9-402b-8307-0b497c122573
## A
An undisclosed vulnerability attackers can exploit to adversely affect systems
## B
Infecting a website frequented by a targeted group of users
## C
Gaining persistent control of a target system without the users knowledge
## D
Attacking a crypt system by exploiting its physicl implementation
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=UlDgRaPpWTA

# A public facing web server has been infected with a kernel-level rootkit. Which of the following is the BEST recommendation on how the incident should be handled?
## ID
SAE.a710ab56-a9f9-459f-ad61-9b0a2cf4da31
## A
Uninstall the rootkit and recover any correcupted data from backup
## B
Remove the system from the network and manually remove the rootkit files
## C
Reinstall the operating systems and restore data from a backup
## D
Install a rootkit removal program and use it to remove the rootkit
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=KwOC7beOyjY


# What is the length of a hash of a message produced with MD5 Hash?
## ID
SAE.bfb644a9-c508-4a2d-8973-3012f0431f8c
## A
64
## B
128
## C
256
## D
384
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.google.com.au/books/edition/ISC_2_CISSP_Certified_Information_System/mFhVDwAAQBAJ?hl=en&gbpv=1&dq=cissp+official+study+guide+mike+chapple&printsec=frontcover

# Which of the following is not a certification
## ID
SAE.2b4feb27-61c1-4f2a-a78a-e0dc955dcf08
## A
ATO
## B
IATO
## C
IATT
## D
DATO
## E
NIST
##  Answer
E
## Explanation
The others are authorizations/accrediations
Authorization To Operate (ATO)
Interim Authorization to Operate (IATO)
Interim Authorization to Test (IATT)
Denial of AUthorization to Operate (DATO)
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering/security-requirements?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What security rule says that no subject should be able to read data at a higher level than the subject's security clearance?
## ID
SAE.388c240e-649e-4214-9db0-a3b01598aa8b
## A
Start integrity property
## B
simple integrity property
## C
simple security property
## D
Star security propery
##  Answer
C
## Explanation
Integrity  - No Read down, No write up
Security - No read up, no write down
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering/quiz/urn:li:learningApiAssessment:4613498?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What layers are the reponsibility of the customer in a 'platform as a service' cloud model?
## ID
SAE.953d4cc1-76f6-4f67-bde4-8e9124ba5c98
## A
Application
## B
Data Center
## C
Data
## D
Hardware
## E
OS
##  Answer
A,C
## Explanation
IAAS - Vendors manages hardware - Customer manages OS, application and data
PAAS - Vendor manages hardware and OS - Customer manages application and data
SAAS - Vendor manages hardware, OS, Application - Customer manages data
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering/public-cloud-tiers?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Which of the following are not approaches to prevent sql injection attacks?
## ID
SAE.2631b1c1-590d-4907-9ca3-60a9ae8a2c58
## A
Use parametrized sql queries
## B
Descape SQL characters in field inputs
## C
Use raw queries
## D
Bind parameters to queries
## E
Validate field inputs
##  Answer
C
## Explanation
## Goal Implication
Should not use raw queries where input comes from an input field
## Source
PJT

# What is the best defense against cross-site scripting attacks?
## ID
SAE.5e12f151-dc0a-478d-ad6a-f501d38fd672
## A
intrusion detection
## B
input validation
## C
network segmentation
## D
WPA2 encryption
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering/quiz/urn:li:learningApiAssessment:4614167?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Which one of the following is not an effective defense against XSRF attacks?
## ID
SAE.8872a861-8493-41a1-8c8d-03ac2c8b79b9
## A
network segmentation
## B
preventing HTTP GET requests
## C
Automatic Logouts
## D
User Education
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering/quiz/urn:li:learningApiAssessment:4614167?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What type of fuzz testing captures real software input and modifies it?
## ID
SAE.e056c234-8a68-4ecd-8ad1-fb4baf986fda
## A
Script Based Fuzzing
## B
Static Fuzzing
## C
Mutation Fuzzing
## D
Generation Fuzzing
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering/quiz/urn:li:learningApiAssessment:4614167?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Your web application is suspectible to XSS but cannot be re-written. Which of the following is the best option for you?
## ID
SAE.4d1be06e-6fc1-48b4-a81d-7c6e29cda930
## A
Require all traffic to be encrypted.
## B
Disable search and form submission
## C
Install a NIDS to monitor for XSS attackes
## D
Install a WAF between clients and server to act as a reverse proxy
## E
Install a statefull firewall to only allow established session to port 443
##  Answer
D
## Explanation
C is not the right answer because we dont just want detection.  A WAF working as a web proxy can reject requests when it detects WAF.
## Goal Implication
## Source
https://www.youtube.com/watch?v=nKeRFLjBlc0


# The core issues around BYOD relate to _____.
## ID
SAE.36e10bdd-d565-4ea1-9f10-b292f58c8e36
## A
ownership
## B
standards
## C
administration
## D
process
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering/quiz/urn:li:learningApiAssessment:4614168?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What technology can you use as a compensating control when it's not possible to patch an embedded system?
## ID
SAE.ee0d5c51-9fb8-491e-9620-8a1787959df9
## A
IDS
## B
Wrappers
## C
Log Analysis
## D
SIEM
##  Answer
B
## Explanation
Security Wrappers monitor requests to IOT devices only letting valid requests through.
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering/quiz/urn:li:learningApiAssessment:4613500?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What is the most important control to apply to smart devices?
## ID
SAE.08276296-067f-43dc-9b50-7f88b8c9f045
## A
wrappers
## B
intrusion detection
## C
network segmentation
## D
application firewalls
##  Answer
C
## Explanation
Same principle as Web DMZ.  Isolate system.
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering/quiz/urn:li:learningApiAssessment:4613500?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# When can non-repudiation not be acheived?
## ID
SAE.f7536883-e971-420b-89f0-6a2094cd5209
## A
using asymetric keys
## B
PGP
## C
using symetric keys
## D
Secure MIME
##  Answer
C
## Explanation
With symetric keys you can prove who generated anything.
## Goal Implication
## Source
PT


# Bob is planning to use a cryptographic cipher that rearranges the characters in a message. What type of cipher is Bob planning to use?
## ID
SAE.5087193b-fc48-4abb-8da8-f5e848c2784b
## A
elliptic cipher
## B
transposition cipher
## C
stream cipher
## D
substitution cipher
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering/quiz/urn:li:learningApiAssessment:4612688?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Which of these is Kerckchoff principle?
## ID
SAE.c7296eb9-19c4-4280-93d1-9ee3a4cdd7b6
## A
principle of seperation of duties
## B
principle of least privilege
## C
Communication in event of breach
## D
Tranquility principle
## E
In a security algorithm only the key should be secret
##  Answer
E
## Explanation
## Goal Implication
## Source
PT

# Using symmetric encryption when must the keys be regenerated?
## ID
SAE.7edd21a7-124d-4c91-bf3e-7d8cb9e56aa9
## A
For each message
## B
When the key expires
## C
When a participant leaves a group
## D
Secret is compromised
##  Answer
C,D
## Explanation
You must regenerate new keys for any key that the participant knew and distribute to the group.
## Goal Implication
## Source
PT

# How many possible keys exist in a 4-bit key space?
## ID
SAE.e9a96291-b56d-4969-8317-5fbc8b238591
## A
4
## B
8
## C
16
## D
128
##  Answer
C
## Explanation
2 ^ 4
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 232). Wiley. Kindle Edition. 


# What is the length of the cryptographic key used in the Data Encryption Standard (DES) cryptosystem?
## ID
SAE.81497ae7-d17f-4c4c-b70b-0471d2aaf0d0
## A
56
## B
128
## C
192
## D
256
##  Answer
A
## Explanation
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 232). Wiley. Kindle Edition. 

# Which one of the following cannot be achieved by a secret key cryptosystem?
## ID
SAE.d1f67934-7d75-411a-ba62-806323be13a0
## A
Non Repduation
## B
Confidentiality
## C
Authentication
## D
Key Distribution
##  Answer
A
## Explanation
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 232). Wiley. Kindle Edition. 


# What is the output value of the mathematical function 16 mod 3?
## ID
SAE.27f366f3-1f91-4b6c-8d0c-169fbfa00f20
## A
0
## B
1
## C
3
## D
5
##  Answer
B
## Explanation
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 233). Wiley. Kindle Edition. 

# What block size is used by the 3DES encryption algorithm?
## ID
SAE.294a2dfa-0d1b-4d21-9991-450de98459e4
## A
32
## B
64
## C
128
## D
256
##  Answer
B
## Explanation
3DES simply repeats the use of the DES algorithm three times. Therefore, it has the same block length as DES: 64 bits.
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 959). Wiley. Kindle Edition.
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 233). Wiley. Kindle Edition. 


# What is the minimum number of cryptographic keys required for secure two-way communications in symmetric key cryptography?
## ID
SAE.0c001ef9-a889-4ec6-860e-a2fcbedd1aa8
## A
1
## B
2
## C
3
## D
4
##  Answer
A
## Explanation
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 233). Wiley. Kindle Edition. 

# Dave is developing a key escrow system that requires multiple people to retrieve a key but does not depend on every participant being present. What type of technique is he using?
## ID
SAE.1c575b1a-d985-42b1-8f4b-c1e68e6417e3
## A
Split Knowledge
## B
M of N
## C
Work function
## D
Zero Knowledege Proof
##  Answer
B
## Explanation
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 233). Wiley. Kindle Edition. 

# Which one of the following Data Encryption Standard (DES) operating modes can be used for large messages with the assurance that an error early in the encryption/decryption process won’t spoil results throughout the communication?
## ID
SOP.f9edeca3-c37b-4626-846d-2d9633ab26c2
## A
Cipher Block Chaining (CBC)
## B
Electronic Code Book (ECB)
## C
Cipher Feedback (CFB)
## D
Output feedback (OFB)
##  Answer
D
## Explanation
Output feedback (OFB) mode prevents early errors from interfering with future encryption/decryption. Cipher Block Chaining and Cipher Feedback modes will carry errors throughout the entire encryption/decryption process. Electronic Code Book (ECB) operation is not suitable for large amounts of data.
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 234). Wiley. Kindle Edition. 


# Many cryptographic algorithms rely on the difficulty of factoring the product of large prime numbers. What characteristic of this problem are they relying on?
## ID
SAE.bf28f942-4766-4e8c-b193-e78fa204788d
## A
Diffusion
## B
Confusion
## C
One Way function
## D
Kerckhoffs Principle
##  Answer
C
## Explanation
A one-way function is a mathematical operation that easily produces output values for each possible combination of inputs but makes it impossible to retrieve the input values.
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 234). Wiley. Kindle Edition. 

# How many keys are required to fully implement a symmetric algorithm with 10 participants?
## ID
SAE.a9f40aaf-79e4-4ed8-924c-f3c829bff7af
## A
10
## B
20
## C
45
## D
100
##  Answer
C
## Explanation
n(n-1)/2
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 234). Wiley. Kindle Edition. 

# What type of cryptosystem commonly makes use of a passage from a well-known book for the encryption key?
## ID
SAE.a12df651-d86d-4f36-b5e7-22f3e294711d
## A
Vernam Cipher
## B
Running key cipher
## C
Skiphack Cipher
## D
Blowfish Cipher
##  Answer
B
## Explanation
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 234). Wiley. Kindle Edition. 

# Which AES finalist makes use of prewhitening and postwhitening techniques?
## ID
SAE.ef33177e-8e4f-4ba5-8703-349a676916d4
## A
Rjindael
## B
Twofish
## C
blowfish
## D
Skipjack
##  Answer
B
## Explanation
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 235). Wiley. Kindle Edition. 

# How many encryption keys are required to fully implement an asymmetric algorithm with 10 participants?
## ID
SAE.7bb96776-7f6d-4a50-8fdc-c6e903d75be2
## A
10
## B
20
## C
45
## D
100
##  Answer
B
## Explanation
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 235). Wiley. Kindle Edition. 


# What key is actually used to encrypt the contents of a message when using PGP?
## ID
SAE.9fa5abf7-22f2-40dd-83c2-3e88f05dc3ad
## A
recipients public key
## B
senders public key
## C
randomly generated key
## D
senders private key
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/quiz/urn:li:learningApiAssessment:4614169?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Which one of the following encryption approaches is most susceptible to a quantum computing attack?
## ID
SAE.7dc73fc8-3933-47fb-935f-dfeb93efd67e
## A
quantum cryptography
## B
RSA cryptography
## C
elliptic curve cryptograhy
## D
AES cryptography
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/quiz/urn:li:learningApiAssessment:4614169?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# The difficulty of solving what mathematical problem provides the security underlying the Diffie-Hellman algorithm?
## ID
SAE.c7df2284-6093-44fa-a7f1-76b491f24776
## A
prime factorization
## B
travelling salesman
## C
elliptic curve
## D
graph isomorphism
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/quiz/urn:li:learningApiAssessment:4613497?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# In the early 1990s, the National Security Agency attempted to introduce key escrow using what failed technology?
## ID
SAE.7ff155c3-98b1-4e9b-acf3-191f3a13a135
## A
Common certificates
## B
DES
## C
Common critiera
## D
Clipper Chip
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/quiz/urn:li:learningApiAssessment:4613497?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What algorithm uses the Blowfish cipher along with a salt to strengthen cryptographic keys?
## ID
SAE.8b4a6f11-fd90-4859-a85d-09a55b940a80
## A
Blowdart
## B
PBKDF2
## C
Bcrypt
## D
PBKDF2
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/quiz/urn:li:learningApiAssessment:4613497?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# The BIBA integrity model endeavours to ensure data integrity by only allowing authorized modifications. To do so subject and objects are grouped into authorization levels. Which of the following is one of Biba rules?
## ID
SAE.ea15a35f-6fb9-4db6-bd13-0dfe31bf6998
## A
No read down
## B
No read up
## C
No write down
## D
No write up
## E
Strong star
##  Answer
A
## Explanation
Biba Integrity no read down and no write up,
Bell Lapulda security model is no read up and no write down

Integratrity model is no read down and no write up
- Dont trust stuff read at a lower level in case its modified
- Consequently dont write stuff for higher auth levels as they wont trust it
## Goal Implication
## Source
https://www.youtube.com/watch?v=nJv45N8bMcs


# In a computer system the ability for subject to interact with objects is controlled. Which of the following is responsible for exerting that control. Choose the best answer?
## ID
SAE.4bb852a7-5386-4817-a7be-7e633fe100bf
## A
Reference Monitor
## B
Kerberos
## C
Memory Manager
## D
API
## E
Security Control
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=nJv45N8bMcs

# When a user receives a digitally signed message which of these wont be true?
## ID
SAE.379b6147-5315-4520-95b8-715299ab6497
## A
The owner of the public key is the person who signed the message
## B
The device was sent from a particular device.
## C
The message was not altered after being signed
## D
The receipient can prove these facts to a 3rd party.
## E
The receipient can prove the email address of the sender
##  Answer
B,E
## Explanation
While the others are true, the message content cant confirm which device sent a message and if the certificate does not include the senders email address you can be sure of the senders email address
## Goal Implication
## Source
PT


# Which one of the following is not a barrier to using the web of trust (WoT) approach?
## ID
SAE.f0f47dd3-42f9-469f-9084-6c118295da23
## A
Decentralized Approach
## B
Technical knowledge of users
## C
use of weak crptography
## D
high barrier to entry
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/quiz/urn:li:learningApiAssessment:4615121?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Which one of the following is not a possible hash length from the SHA-2 function?
## ID
SAE.68a39515-6bdc-4dcb-8ba9-0d5ebe5fc239
## A
512
## B
128
## C
256
## D
224
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/quiz/urn:li:learningApiAssessment:4615121?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What standard governs the structure and content of digital certificates?
## ID
SAE.82391306-4098-4b35-9da1-ada8728f2785
## A
802.1x
## B
x.509
## C
x.500
## D
802.11ac
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/quiz/urn:li:learningApiAssessment:4615121?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Harold works for a certificate authority and wants to ensure that his organization is able to revoke digital certificates that it creates. What is the most effective method of revoking digital certificates?
## ID
SAE.9dde39c2-549d-4fea-9233-a0920f674ef5
## A
Transport Layer Security
## B
Online Certificate Status Protocol
## C
Certificate Revocation Bulletin
## D
Certificate Revocation Lists
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/quiz/urn:li:learningApiAssessment:4615121?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# An Hashed Message Authentication Code (HMAC) does not allow which of the following
## ID
SAE.1bd2a5b4-ec41-49a5-a158-7c5c79ca9b07
## A
Confirm integrity of the message
## B
Allow you to prove to 3rd party who sent the message
## C
Generation of Partial Digital Signature
## D
Viewing of plain text message content
##  Answer
B
## Explanation
Because it encrypts the hash with a secret key it doesnt support non-repdiation
## Goal Implication
## Source
PT

# Which of the following are digital signature algorithms
## ID
SAE.e2b5674d-637b-40ca-a780-a81581c7ef0e
## A
RSA
## B
DSS
## C
Schnorrs Signature Algorthm
## D
Nyberg-Rueppel’s Signature Algorthm
## E
X.509
##  Answer
B,C,D
## Explanation
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 249). Wiley. Kindle Edition. 

# As part of the rollout of smartcard system you have been tasked with providing a summary of features to senior management. Which of the following is true and could be included in the report
## ID
SAE.64b96b33-aec6-4b9d-9e83-9c007b8333bf
## A
Private keys should be marked as exportable so they can be transferred to new keys
## B
The private key is encrypted with asymmetric key
## C
Smartcards hold multiple key pairs and their certificates
## D
Smart cards do not support assymetric keys with length shorter then 1024
## E
If the card is lost the private can easily be retrieved with compatible smart card reader
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=mjgXGDE9_M8&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=14


# How are the keys on the smart card used to authenticate the user? WHich is the best answer?
## ID
SAE.66e41c62-9aad-4987-9234-ac510337e4bd
## A
The smartcard  calculates a hash of the user certificate and sends to the host computer
## B
The computer validates the user certificate with standard PKI validation techniques
## C
The user smartcard PIN unlocks the access to the user certificates
## D
The user private key encrypts a challenge generated by the computer
##  Answer
D
## Explanation
A computer has separate access to your public certificate (from AD for instance).
When you plug in your smart card the computer issues challenge (some text) which the smart card encryptes and returns back to computer.
The computer checks it can use your public key to decrypt and get back to the challenge
## Goal Implication
## Source
https://www.youtube.com/watch?v=mjgXGDE9_M8&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=14


# What of the following are not one the 5 fundamentals of a hash algorthm?
## ID
SAE.2cc696eb-ec0f-4ce6-8cf4-ab44e734f7c4
## A
Allow input of any length
## B
Collision free
## C
Easy to compute hash for any input
## D
Provide variable-length output
## E
provide one-way functionality
##  Answer
D
## Explanation
Hashes provide variable length ouput
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 269). Wiley. Kindle Edition. 

# What is the length of SHA-1 vs SHA-2 and SHA-3 hash lengths?
## ID
SAE.3666c73f-55b9-44ab-9ff0-9854703748fa
## A
128-256-512
## B
128-128-256
## C
160-512-512
## D
160-256-512
## E
##  Answer
C
## Explanation
SHA-1 produces a 160-bit message digest whereas SHA-2 supports variable lengths, ranging up to 512 bits. SHA-3 improves upon the security of SHA-2 and supports the same hash lengths.
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 269). Wiley. Kindle Edition. 

# What are the components of the DSS standard
## ID
SAE.a0335572-b5d9-4fdd-939b-e1df1eb0c05c
## A
Hash using MD3 encrypt with AES
## B
Hash using SHA1-1,2,3 and encrypt with DSA,RSA,ECDSA
## C
Hash with PGP encrypt with Blowfish
## D
Hash with HAVAL and encrypt with Diffie Helman
##  Answer
B
## Explanation
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 269). Wiley. Kindle Edition. 

# What approach does PGP take to encrypt emails
## ID
SAE.f77ca056-76a1-4604-991f-43857ad69348
## A
Generate secret key, encrypt message, encrypt secret key with recipients public key
## B
Encrypt message using recipients public key
## C
Hash message using MD5 encrypt message with hash
##  Answer
A
## Explanation
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 269). Wiley. Kindle Edition. 

# What are the 2 modes of IPSEC
## ID
SAE.5006fa32-5b2f-4507-893a-bd2af13d1560
## A
Output feedback mode
## B
Tunnel Mode
## C
Network mode
## D
Transport Mode
## E
Presentation Mode
##  Answer
B,D
## Explanation
In IPsec transport mode, packet contents are encrypted for peer-to-peer communication. In tunnel mode, the entire packet, including header information, is encrypted for gateway-to-gateway communications.
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 270). Wiley. Kindle Edition. 

# What is known type of encryption attack
## ID
SAE.6b2c1fc2-93ab-435f-a412-e828bf12f919
## A
Man-in-the-middle
## B
Plaintext 
## C
Meet-in-the-middle
## D
Brute Force Attack
## E
Reset Attack
## F
Chosen Plaintext Attach
## G
Birthday attack
## G
Replay attach
##  Answer
E
## Explanation
Brute-force attacks are attempts to randomly find the correct cryptographic key. Known plaintext, chosen ciphertext, and chosen plaintext attacks require the attacker to have some extra information in addition to the ciphertext. The meet-in-the-middle attack exploits protocols that use two rounds of encryption. The man-in-the-middle attack fools both parties into communicating with the attacker instead of directly with each other. The birthday attack is an attempt to find collisions in hash functions. The replay attack is an attempt to reuse authentication requests.
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 270). Wiley. Kindle Edition. 

# What is an approach to data center design?
## ID
SAE.34f8f5f8-7d2d-477f-ab57-7fd1fe669608
## A
Temperature and Humdity Mgt Approach
## B
HVAC Approach
## C
Hot Aisle, Cool Aisle Approach
## D
Sustainability Approach
##  Answer
C
## Explanation
HVAC units pull warm air from aisle which expel heat from compute devices
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/data-center-environmental-protection?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What is the maximum acceptable temperature for a data center?
## ID
SAE.72ebedd7-dd3d-45a7-add1-0cda8d7dec3b
## A
64.6 Fahrenheit
## B
80.6 Fahrenheit
## C
74.8 Fahrenheit
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/quiz/urn:li:learningApiAssessment:4613502?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Fires are typically classified by fuels that start them. Which of the following is associated with fires caused by flammable , liquids  like gasoline, petroleum oil or propone?
## ID
SOP.d92d2764-f1b3-4052-92cf-cfb92c3c78a8
## A
Class A
## B
Class B
## C
Class C
## D
Class D
## E
Class K
##  Answer
B
## Explanation
- Class A- common cumbustible material - wood, trash
- Class B - cumbustible liquids 
- Class C - Electrical
- Class D - Combustible metals
- Class K - Kitchen - fats
- https://en.wikipedia.org/wiki/Fire_class
## Goal Implication

# What class of fire extinguisher is designed to work on electrical fires?
## ID
SAE.a8411004-c988-444c-ac3a-a4abf9161342
## A
A
## B
B
## C
C
## D
D
## E
K
##  Answer
C
## Explanation
- Class A- common cumbustible material - wood, trash
- Class B - cumbustible liquids 
- Class C - Electrical
- Class D - Combustible metals
- Class K - Kitchen - fats
- https://en.wikipedia.org/wiki/Fire_class
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-3-security-architecture-and-engineering-2018/quiz/urn:li:learningApiAssessment:4613502?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Which of the following hashing algorithms produces output less than 200
## ID
SAE.a23d32d6-aa8e-4673-88be-f0632f06aafd
## A
SHA2
## B
Whirlpool
## C
SHA1
## D
AES-CCMP
## E
RC5
## F
MD5
##  Answer
C,F
## Explanation
SHA1 - 160 bit hash
MD5 - 128 bit hash

Whirlpool - 512

AES-CCMP - not a hash
RC5 - not a hash

## Goal Implication
## Source
https://www.youtube.com/watch?v=stf9UlkYYn0


# Brian computes the digest of a single sentence of text using a SHA-2 hash function. He then changes a single character of the sentence and computes the hash value again. Which one of the following statements is true about the new hash value?
## ID
SAE.ed31c85c-7dcc-4dcd-8c72-656fc170613e
## A
The new hash value will be one character different from the old hash value.
## B
The new hash value will share at least 50% of the characters of the old hash value.
## C
The new hash value will be unchanged.
## D
The new hash value will be completely different from the old hash value.
##  Answer
D
## Explanation
## Goal Implication
## Source
Chapter 7 Review QUestion 1
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition. 


# Which cryptographic algorithm forms the basis of the El Gamal cryptosystem?
## ID
SAE.58a12dd9-3666-4b5d-85cd-aa44bb6f5cd0
## A
RSA
## B
Diffie-Helman
## C
3DES
## D
IDEA
##  Answer
B
## Explanation
The El Gamal cryptosystem extends the functionality of the Diffie-Hellman key exchange protocol to support the encryption and decryption of messages.
## Goal Implication
## Source
Chapter 7 Review QUestion 2 :
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition. 


# If Richard wants to send an encrypted message to Sue using a public key cryptosystem, which key does he use to encrypt the message?
## ID
SAE.dce1f321-49a2-4f8b-9101-0bfd9c728bbd
## A
Richards Public Key
## B
Richards Private Key
## C
Sues public key
## D
Sues private key
##  Answer
C
## Explanation
## Goal Implication
## Source
Chapter 7 Review QUestion 3
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition. 


# If a 2,048-bit plaintext message were encrypted with the El Gamal public key cryptosystem, how long would the resulting ciphertext message be?
## ID
SAE.c8ca9146-cf0c-4716-b496-73f408237891
## A
1024
## B
2048
## C
4096
## D
8192
##  Answer
C
## Explanation
The major disadvantage of the El Gamal cryptosystem is that it doubles the length of any message it encrypts. Therefore, a 2,048-bit plain-text message would yield a 4,096-bit ciphertext message when El Gamal is used for the encryption process.
## Goal Implication
## Source
Chapter 7 Review QUestion 4
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition. 

# Acme Widgets currently uses a 1,024-bit RSA encryption standard companywide. The company plans to convert from RSA to an elliptic curve cryptosystem. If it wants to maintain the same cryptographic strength, what ECC key length should it use?
## ID
SAE.c28283a3-48a5-4567-b001-41f6175a8c0c
## A
160
## B
512
## C
1024
## D
2048
##  Answer
A
## Explanation
The elliptic curve cryptosystem requires significantly shorter keys to achieve encryption that would be the same strength as encryption achieved with the RSA
## Goal Implication
## Source
Chapter 7 Review QUestion 5
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition. 

# John wants to produce a message digest of a 2,048-byte message he plans to send to Mary. If he uses the SHA-1 hashing algorithm, what size will the message digest for this particular message be?
## ID
SAE.4982434d-73e5-49be-838d-cd06267900dc
## A
160
## B
512
## C
1024
## D
2048
##  Answer
A
## Explanation
The SHA-1 hashing algorithm always produces a 160-bit message digest, regardless of the size of the input message. In fact, this fixed-length output is a requirement of any secure hashing algorithm.
## Goal Implication
## Source
Chapter 7 Review QUestion 6
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 272). Wiley. Kindle Edition. 

# Which one of the following technologies is considered flawed and should no longer be used?
## ID
SAE.fc15eb81-8bd0-44b4-876d-d326ae9294fe
## A
SHA-3
## B
PGP
## C
WEP
## D
TLS
##  Answer
C
## Explanation
The WEP algorithm has documented flaws that make it trivial to break. It should never be used to protect wireless networks.
## Goal Implication
## Source
Chapter 7 Review QUestion 7
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition. 


# Richard received an encrypted message sent to him from Sue. Which key should he use to decrypt the message?
## ID
COM.759411ae-8c3c-4685-a2cc-da7b1c058fed
## A
Richards public key
## B
Richards private key
## C
Sues public key
## D
Sues private key
##  Answer
B
## Explanation
## Goal Implication
## Source
Chapter 7 Review QUestion 9
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition. 


# Richard wants to digitally sign a message he’s sending to Sue so that Sue can be sure the message came from him without modification while in transit. Which key should he use to encrypt the message digest?
## ID
COM.fcc210af-c43a-4a79-a74f-0319d6ab05d6
## A
Richards public key
## B
Richards private key
## C
Sues public key
## D
Sues private key
##  Answer
B
## Explanation
## Goal Implication
## Source
Chapter 7 Review QUestion 10
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition. 

# Which one of the following algorithms is not supported by the Digital Signature Standard?
## ID
COM.5693599b-c8ac-4d9d-be9d-534d879a543d
## A
DSA
## B
RSA
## C
El gammal
## D
ECC
##  Answer
C
## Explanation
The Digital Signature Standard allows federal government use of the Digital Signature Algorithm, RSA, or the Elliptic Curve DSA in conjunction with the SHA-1 hashing function to produce secure digital signatures.
## Goal Implication
## Source
Chapter 7 Review QUestion 11
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition. 


# Which International Telecommunications Union (ITU) standard governs the creation and endorsement of digital certificates for secure electronic communication?
## ID
COM.c927df0d-8486-4788-990d-47b5c11686f9
## A
x.500
## B
x.509
## C
x.900
## D
x.905
##  Answer
B
## Explanation
## Goal Implication
## Source
Chapter 7 Review QUestion 12
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition. 


# What cryptosystem provides the encryption/decryption technology for the commercial version of Phil Zimmerman’s Pretty Good Privacy secure email system?
## ID
COM.deb64a92-5c35-4a1d-8c55-09cd50553b9a
## A
ROT13
## B
IDEA
## C
ECC
## D
El Gammal
##  Answer
B
## Explanation
 Pretty Good Privacy uses a “web of trust” system of digital signature verification. The encryption technology is based on the IDEA private key cryptosystem.
## Goal Implication
## Source
Chapter 7 Review QUestion 13
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition. 


# What type of cryptographic attack rendered Double DES (2DES) no more effective than standard DES encryption?
## ID
COM.105e99a2-f3bc-4ad7-8ce4-c95aa8549043
## A
Birthday Attack
## B
Chosen Cipher Text
## C
Meet in the middle
## D
Man in the middle
##  Answer
C
## Explanation
In the meet-in-the-middle attack, the attacker uses a known plaintext message. The plain text is then encrypted using every possible key (k1), and the equivalent ciphertext is decrypted using all possible keys (k2). When a match is found, the corresponding pair (k1, k2) represents both portions of the double encryption. This type of attack generally takes only double the time necessary to break a single round of encryption (or 2n rather than the anticipated 2n * 2n), offering minimal added protection.
## Goal Implication
## Source
Chapter 7 Review QUestion 15
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition. 

# Which of the following tools can be used to improve the effectiveness of a brute-force password cracking attack?
## ID
COM.01582e2e-31ec-4c50-a17c-ceb4c0b3ca20
## A
Rainbow Tables
## B
Hierarchical Screening
## C
TKIP
## D
Random Enhancement
##  Answer
A
## Explanation
Rainbow tables contain precomputed hash values for commonly used passwords and may be used to increase the efficiency of password cracking attacks.
## Goal Implication
## Source
Chapter 7 Review QUestion 16
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition. 


# Which of the following links would be protected by WPA encryption?
## ID
COM.14799fb1-0790-46db-88a1-8726e8a86ccb
## A
Firewall to Firewall
## B
Router to Firewall
## C
Client to Wireless Access Point
## D
Wireless Access Point To Router
##  Answer
C
## Explanation
## Goal Implication
## Source
Chapter 7 Review QUestion 17
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition. 


# What is the major disadvantage of using certificate revocation lists?
## ID
COM.4e501025-9160-4346-85b2-1018121edc4c
## A
Key Management
## B
Latency
## C
Record Keeping
## D
Vulnerability To Brute Force Attacks
##  Answer
B
## Explanation
## Goal Implication
## Source
Chapter 7 Review QUestion 18
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition. 


# Which one of the following encryption algorithms is now considered insecure?
## ID
COM.1e2aed4e-2a47-4c2b-9918-4db59e4df757
## A
El Gamal
## B
RSA
## C
Elliptic Curve Cryptography
## D
Merkle-Hellman Knapsack
##  Answer
D
## Explanation
The Merkle-Hellman Knapsack algorithm, which relies on the difficulty of factoring super-increasing sets, has been broken by cryptanalysts.
## Goal Implication
## Source
Chapter 7 Review QUestion 19
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition. 


# What does IPsec define?
## ID
COM.7ad2eeef-4063-4bc8-91bc-e1a9c64843f4
## A
All possible security classifications for a specific configuration
## B
A framework for setting up a secure communication channel
## C
The valid transition states in the Biba model
## D
TCSEC security categories
##  Answer
B
## Explanation
## Goal Implication
## Source
Chapter 7 Review QUestion 19
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 271). Wiley. Kindle Edition. 

