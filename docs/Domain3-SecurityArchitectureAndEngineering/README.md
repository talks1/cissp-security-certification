Cryptography Agenda
- Cryptography in History
- Security Services Provided by Cryptography
- Algorithm
- Elements of Cryptography Part 1
- Elements of Cryptography Part 2
- Principles of Secure Design
- Security Models Part 1
- Security Models Part 2
- Security Models Part 3
- Security Models Part 4
- System Architecture
- Evaluation Criteria Part 1
- Evaluation Criteria Part 2

Security Design Principles
- Incorporating security into design process
- Subject Object Mode
- Open vs Closed Systems and Open vs Closed Source
- Mechanisms to provide security 
    - confinement or sandboxing
    - bounds - boundaries of memory a process can use
    - isolation - programs should not alter memory or resources of other processes
- Failure Modes
    - Fail Open
    - Fail Closed
- Isolation
- Segmentation
    - Isolation of resources for different activities - Network, Memory, Process, VM

Security Models

    - Composition - flows of information
        - Cascading
        - Feedback
        - Hookup
    - NonIntererence - actions at higher security should not effect lower security levels
    - Lattice Based -
    - Discretionary/NonDiscretionary


    State Machine Model 
        - describes a system that is always secure no matter what state it is in. It’s based on the computer science definition of a finite state machine (FSM).
    Clark-Wilson  
        - integrity model
        - principles
            - well formed transactions and
            - separation of duties
        - subject/program/object
        - all requests for data go through a small set of programs which enforce security.
        - an integrity model that relies on auditing to ensure that unauthorized subjects cannot access objects and that authorized users access objects properly.        
    - Bell Lapadula - Confidentiality
        - Simple Security Property - No READ up
        - Star Security Property - No Write down
    - Biba - Integrity
        - Simple Integrity Property - No READ down
        - Star Integrity Property - No Write Up
    - Brewer Nash - Chinese Wall
    - Take - Grant
        - who can take security rights from others
        - who can grant right to access objects they can access
    - Sutherland
        - focuses on preventing interference in support of integrity. This model is based on the idea of defining a set of system states, initial states, and state transitions. Through the use of and limitations to only these predetermined secure states, integrity is maintained, and interference is prohibited.
    - Graham-Denning
        - secure creation and delete of objects
        
    - Goguen-Meseguer
        -  is based on predetermining the set or domain of objects that a subject can access. The set or domain is a list of those objects that a subject can access. This model is based on automation theory and domain separation. This means subjects are able to perform only predetermined actions against predetermined objects.

Security Modes (Review)
    - Single State processing - cant deal with information at different classification levels concurrently
    - Mutilevel processing - can deal with information at different classification levels concurrently
    - US government levels for systems which process classified information
        - Dedicated -  all users have clearance , approval and need to know all information
        - System High - all users have clearance , approval and need to know some information
        - Compartmented - all user have clearance for all information they will access and need to know for all the information they will access
        - Multi Level - some users do not have clearance for all information processed they access and have need to know for all information they will access.


Security Models - Government
    - Orange Book 
    - ITSEC and TSCEC - replaced by Common Criteria
    - Common Criteria
        - Protection Profiles - What purchases  want 
        - Security Targets - What vendors provide
        - Evaluation Assurance Levels -  How purchasers choose a vendor

Certification 
- product meets certain level of requirements but not considered in a specific environment, government wide decision

Accreditation 
- after certification, decision to operate in a certain environment
- the formal declaration by the Designated Approving Authority (DAA) that an IT system is approved to operate in a particular security mode using a prescribed set of safeguards at an acceptable level of risk.
- types of
    - Authorization To Operate (ATO)
    - Interim Authorization to Operate (IATO)
    - Interim Authorization to Test (IATT)
    - Denial of AUthorization to Operat (DATO)

Kernel - Ring Protection System  - (LInux Kernel)
    Layer  0 - Kernel
    Layer 1 - OS processes
    Layer 2 - device drivers    
    Layer 3 - Applications

Cloud Computing
- Hypervisors
    - Type 1 - Hypervisor run directly on the host
    - Type 2 - Hypervisors run on the host operation system

Models of Cloud Computing
- Private Cloud
- Public Cloud
- Hybrid Cloud

Public Cloud Tiers
- Software as a Service
- Platform as a Service
- Infrastructure as a Service


Memory
- ROM + RAM
- Segmentation Fault
- Memory Leak

API
- Covert storage Channel
    - ICMP echo request - Sending a ping with data packets
Covert Timing Channel
    - Port Knocking - Sending information via connecting to ports

High Availability
- using multiple systems to protect against failure
Load Balancing - this is not intended to provide high availability but rather spreading the load
- Redundant power supplies
- RAID - Redudant Arrays of Inexpensive Disks
    - RAID 1 - Mirroring - Requires 2 disks
    - RAID 5 - requires 3 disks
    - RAID is not a backup strategy



Client and Server Vulnerabilities
    Data Flow Control
        - Ensure limits to bandwidth and throughput are in place to protect servers

    Database
        - Aggregation Attack - aggregating separate pieces of information to deduce additional information
        - Inference Attack - Inferring information through delta changes and known constraints

Large Scale Computing
    Grid Computing - Centralized Node 
    P2P Computing - No centralized nodes


OWASP - Top Ten list
SANS - Top 25 list
CIS Benchmarks -

Attacks
-   SQL Injection - Use single quotes to form multiple queries from a single one.
    - Prevent using parameterized queries or not descaping sql characters in field inputs.

- XSS - Include Script tags in input fields that can display to another user.  The script will run on another users computer

- CSRF, XSRF -  Cross Site Request Forgery
    - Use GET requests (in image urls) to make illegitmate cross domain requests

- Fuzz Testing - Providing valid and invalid inputs to software

- Session HiJacking - Guessing the cookie value when it is not a random UUID


Mobile Applications
- GeoTagging - phones can tag images with GPS locations

IOT
- Use Network Segmentation  (DMZ to isolate IOT devices from internet and internal systems)


Symetric Encryption
- using shared key and n people who want to talk privately. This requires n (n-1)/2 keys.


Four Goals of Cryptography-
- Confidentiality
- Integrity
- Authentication
- Non Repudiation


Code is not the same as Cipher
Code - using a message for a specific purpose  - used for efficiency or security
Policy use the '10 code' for efficiency (10-4 I acknowledge)

Cipher - algorithm to encrypt/decrupt
- Stream Cipher
- Block Cipher

- Substitution Cipher - Change characters in some text
- Transposition Cipher - Change position of characters

- One Tim Pad -also know as a Vernam Cipher

Cryptographic Life Cycle
- NIST =    Initiation, Develop/Acquire, Implementation and Assessment, Operate and Maintain,  Sunset


DRM - Digital Rights - Renders content inaccessible without license
- Apple Fair Play

Kerckhoff Principle - On the keys should be secret in a cryptography algorithm.

Boolean Operations
X ^ Y = And operation
X v Y = Or operation
~X  = Not operation

One way function - multiply several number together, its difficult to know the original numbers.  Do this with prime and it is very hard.
Nonce - initialization vector - some randomization to ensure that similar block results in different cipher text 
Zero Knowlege Proof 

How algorithms are measured
Confusion  - relationship between plaintext and key is complex
Diffusion - change in plaintext results in change around cipher text

Trust
- Web Trust 
    -  Decentralized approach 
    - High barrier of entry to this network
- PKI
    - Certificate Authorities  (Trust Service Providers)

Hashing
- one way function 
- variable length to unique fixed length
- no 2 inputs provide the same output

- Hash Functions
    - MD2 - 128 bit digest
    - MD4 - 128 bit hash
    - MD5
        - invented by Ron Rivest
        - 128 bit hash
    - SHA 
        - produced by NIST
        - Secure Hash Algorthm
        - considered insecure
    - SHA -1
        - 160 bit digest
    - SHA-2
        - currently considered secure
        - 224,256,384,512 length hashes
        - SHA-2/256 - 256 bit digest
        - SHA-2/224 - 224 bit digest
        - SHA-2/512 - 512 bit digest
    - SHA-3
        - variable length output 
    - RIPEMD
    - HAVAL - Hash of Variable Length
        128, 160, 192, 224, and 256 bits

HMAC
    - relies on shared secret
    - combined with any message digest/hash function (above)
    - doesnt support non-repudiation has shared secret is used

Digital Signature
- Dont create a secret message.
- Create a message that could only have been created by a specific person

Digital Signature Algorythms
    - DSA
    - RSA DSA
    - ECC DSA
    - Schorrs
    - Nyberg-Reupel

Digital Certificate
- X.509 Standard
- Revoking Digital Certificate
    - CRL (Certificate Revocation List)
        - inefficient as it requires everyone to download CRL
    - OCSP (Online Certificate Status Protocol)
        - real time service that allows users to check if certificate is revoked
        - most browsers use this (except Chrome which uses googles on proprietary method)


Attacks
- Brute Force
- Keyspace - whats the number of guesses required to guess the key
- Plaintext attack - attacker has plaintext and its associated cypher test
- Chosen Plaintext attack - attacker has assess to generate cyphertext from plaintext he creates

- Salting - process of adding a random value to a password before computing hash which is stored and the salt stored separately.  Effectively extending the password and making cyrptograph frequency analysis less likely to be successful

- Meeting in the middle attach - attacker has known plaintext and encryption. Iterate all keys to find k1 and k2 which works for known plain text
- Man in the middle attack - Sit in between sender and receiver and relay messages

-Replay attacks - can be defeated by incporating a timestamp and expiration in messages

Data Centres
- Cooling Systems - Computing systems are more tolerant of heat - Data Centres are maintained at 64-84 degress
- Humidity  
    - High Humidity bad
    - Low Humidity - bad
    - DewPoint suite spot is 41.9 and 50.0 F
    Hot Aisle/Cool Aisle approach

- Fire Threat is extreme
    - Fires require - Heat, Fuel, Oxygen
    - Water removes heats
    
- Wet Pip System - 
- Dry Pipe System - minimum risk of burst pipe

- Chemicals - remove oxygen 

- Avoid placing Data Centre in flood zone

- Noise Interference
    - EMI - Data Centers need to be monitor - but are expensive to install
        - Common Mode Interference - between hot and ground wire
        - Traverse Mode Interface - between hot and neutral wire
    - RFI 
        - 


Physical Security
- Deterrent - Be aware of dog
- Preventative -  Block an intruder /fingerprint scanner
- Detective - CCTV

Mechanism of Actions
- Technical vs Administrative


Locks
- Key Locks
- Cipher Lock - Must enter the pattern
- Card Lock - Magnetic Stripe
 - Prone to tailgating or piggybacking - Mantraps to counter this
    - Piggyback is when someone authorized knowingly lets you through
    - Tailgating is when some authorized unknowingly lets you through

 Access Lists - need to be maintained

Phyiscal barriers
-  Fences
 - Bollards - block vehicles
 - Lighting
 - Signs

 Cabling 
 - should be protected

 Visitor Management
 - policies on who authorizes visitors
 - log vistor access


 Fire Alarm
    - Local - 
    - Centralized - alarm is raised to centralized monitoring site
    - Auxillary - emergency services are notified in addition to local or centralized alarm