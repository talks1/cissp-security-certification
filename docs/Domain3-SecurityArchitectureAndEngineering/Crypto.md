
Crypto Services
- Confidentiality
- Integrity
- Authenticity
- Non Repudiation
- Access Control

Crypto Terminology
- Confusion - Change one bit in the key and half of the bits in the cipher should change
- Diffusion - Change one bit in the plain text and half of the bits in the cipher shold change
- Avalanche - Measurement of confusion and diffusion in an algorithm

Secret Writing
- Hidden - Encoding in some other content
    - Stenography
    - null Cipher
- Srambled
    - One Way - Hashing
        - MD5 - 128 bit hash
        - SHA-1 - 160 bit hash
        - SHA-2  - 224,256,384512 bit hash
        - SHA-3 - 224,256,384512 bit hash
    - Two Way
        - Symmetric
            - Number of keys required n*(n-1)/2
            - Block
                - Operate on 
                    - P plain text 
                    - K encryption key
                To produce
                    - E encrypted block
                    - D decrypted block
                - Modes : ECB, CBC, CFB, OFB, CTR
                    - Modes are ways to make it hard for keys to be discoverable through patterns in the messages being sent
                        - ECB - Electronic Code Book
                                -Simply encrypt each P block with the key
                                - no initialization vector - fast but on good only for limited amounts of data
                                P1 K = E1  another P1 produces same K1
                        - CBC - Cipher Block Chaining - Use previous E block as key for next E
                                P1 K = E1 , P2 E1 = E2 
                                - the same P1 produces different Es
                        - CFB - streaming version of CBC-fill up buffer encrypt and send
                        - Output Feedback Mode
                            - instead of XOR the input text and previous ciphertext it uses a seed value. The seed value is processed by DES to produce new seed values
                        - CTR 
                            - use a counter rather than IV for each operation
                            - general purpose
                            - parallelizable
                        

                - Algorithms : 
                      - DES
                        - 56 bit key
                        - 64 bit block size
                        - Uses a Feistel Network that implements substitution and transposition
                      - 3DES
                        - DES-EEE - uses 3 56 bit keys as E(E(E(P,K1),K2),K3)
                        - DES-EDE - uses 3 56 bit keys as E(E(E(K1,P),K2),K3)
                        - DES-EEE2 - uses 2 56 bit keys as E(E(E(P,K1),K2),K1)
                        - DES-EDE2 - uses 2 56 bit keys as E(D(E(P,K1),K2),K1)
                                            
                    - IDEA - Internal Data Encryption Algorithm
                            - 128 Key
                            - Operates on 64 bit blocks
                            - Used in PGP

                    - Skipjack 
                        - 80 bit key
                        - 64 bit blocks
                        - encryption keys held in escrow

                    - blowfish
                        - Designed to replace DES
                        - in the public domain
                        - Designed by Bruce Schiender
                        - no longer considered secure
                        - max key length 384

                    - AES 
                        - block size 128, 
                        - key size 128,192,256
                    
                    - twofish
                        - 128 bit blocks
                        - key 256
                            - prewhitening
                            - postwhitening
                        - Designed by Bruce Scheider
                    -  serpent 
                        - block size 128, 
                        - key size 128,256
                    - RC6
                
            - Stream
                - Algorthms : RC4, RC5
                    - Wifi (WEP and WPA) use this
                    - Socket (SSL / TLS) use this
                    - Variable length key between 40-2048 bits

        - Asymmetric
            - Factoring 
                - RSA - variable key between 1024 and 4096
            - PGP
                - generate random key  and symmetric encrypte message
                - use public key to encrypt symmetric key
            - Discrete Log 
                - Diffie Helman
                - ECC (Elliptic Curve)
                - El Gamal
                - DSA
                - Quantum Computing
            - Digital Certificates
            - Digital Signatures
        - Substitution
            One Time Pad
                Vernam Cipher is an example of this from the days of Teletypes.
                Running key cipher - use content of book as the content and use the plain text as the key
        - Transposition Cipher
            Have a key - convert to number by replacing characters by their alphanetic order in the message
            Put the Plain text into columns with same length as key
            Send content from columns in order key numbers


Key Exchanges
    - Offline 
    - PKI
    - Diffie Helman
        - uses prime numbers
        - agree on large prime number and integer
        - each choose a secret random number
        - each perform calculation
    - EHCD - Elliptic Curve Diffie Helman - Variant using Elliptic Curve

Key Stretching - 
    - take a short password and stretch into a long encryption key
    - salt - adds value to password to make it more complex
    - Hashing - adds time to the verification process to slow down checking the password
    - PBKDF2 - Password Based Key Derivation v2 - adding a salt to a password