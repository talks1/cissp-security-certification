
## Lattice Based - Layers
    - Bell- Lapadula 

## Rule Based


## Star Model

## Role Based Access Control


## BIBA Model - Discretionary Access Control

Generally discretionary is an approach where the owner makes the decision about whether access is granted.

Focused on perserving the integrity of information

Lattice Based

No write up (* Integrity Axxiom)
No read down (Simple integration axiom)

Think defense chain of command.  Order come top down (not bottom up)

### Simple Integrity Property

Only Read up

### Star Integrity Property

Only Write Down


## Non Discretionary Access Control

Someone other that owner determine access.
Generally not a good idea

## Bell Lapadula - Mandatory Access Control

Focused on perserving the confidentiality of information
Lattice Based

Security model used by defense departments based on classifying information  (Top Secret, Official,....)

Focuses on confidentiality of classified information.

Object access is based on sensitivity

No Read Up (Simple Security Rule)
No write down (* Property Rule)

### Simple Security Property

To maintain confidentiality you can only read down (your level and below)

- confidentiality
- there are confidentiality layers
- read down, write up  ....also (no read up, no write down)

### Star Property

To maintain confidentiality you can only write at your level or up.

### Strong Star Property

You can only read/write at your own level


## Lipner Implementation


## Brewer and Nash

This model aims to eliminate conflicts of interest occuring by shutting off information which could cause a conflict of interest.

It is characterized by a dynamic security model.  Actions have subsequent re-actions.

It is about avoiding conflict of interest

If a consultant accesses information on Company A then they should notbe able to access information on Company B.

Chinese Wall

## Discretionary Access Control





## Simple Integrity Property
- integration
- there are integrity layers
- read up, write down

Tranquility Principal
- permissions should not change dynamically

