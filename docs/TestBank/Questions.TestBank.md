# What tool can an organization use to reduce or avoid risk when working with external vendors, consultants, or contractors?
## ID
034e318f-259a-4342-a5cc-f00081ae227d
## A
IPSEC
## B
VPN
## C
SLA
## D
BCP
##  Answer
C
## Explanation
A service-level agreement (SLA) is a contract with an external entity and is an important part of risk reduction and risk avoidance. By clearly defining the expectations and penalties for external parties, everyone involved knows what is expected of them and what the consequences are in the event of a failure to meet those expectations.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What form of testing stresses demands on the disaster recovery team to derive an appropriate response from a disaster scenario?
## ID
034e318f-259a-4342-a5cc-f00081ae227d024c2bbe-104a-40bc-8200-588107d4d294
## A
Full Interruption Test
## B
Parallel Test
## C
Simulation Test
## D
Structure Walk Through
##  Answer
C
## Explanation
Simulation tests are similar to the structured walk-throughs. In simulation tests, disaster recovery team members are presented with a scenario and asked to develop an appropriate response.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# An organization is hosting a website that accesses resources from multiple organizations. They want users to be able to access all of the resources but only log on once. What should they use?
## ID
tb475934.CISSPSG8E.be5.015
## A
Federal database for CIA
## B
Kerberos
## C
Diameter
## D
SSO
##  Answer
D
## Explanation
Single sign-on (SSO) allows a user to log on once and access multiple resources. This is not called a federal database but might be related to federation if multiple authentication systems must be integrated to establish the SSO. Kerberos is an effective SSO system within a single organization but not between organizations. Diameter is a newer alternative to RADIUS used for centralized authentication often for remote connections; thus, it is not appropriate for this scenario.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What form of proximity device is able to generate its own electricity from a magnetic field?
## ID
tb475934.CISSPSG8E.be3.107
## A
Passive Device
## B
Self Powered Device
## C
Field Powered Device
## D
Transponder
##  Answer
C
## Explanation
A field-powered device has electronics that activate when the device enters the electromagnetic field that the reader generates. Such devices actually generate electricity from an EM field to power themselves.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What aspect of security governance is based on the idea that senior management is responsible for the success or failure of a security endeavor?
## ID
tb475934.CISSPSG8E.be3.038
## A
Sarbanes-Oxley Act 2002
## B
COBIT
## C
Accreditation
## D
Top-down approach
##  Answer
D
## Explanation
he top-down approach is the aspect of security governance that is based on the idea that senior management is responsible for the success or failure of a security endeavor.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# In what scenario would you perform bulk transfers of backup data to a secure off-site location?
## ID
tb475934.CISSPSG8E.be5.007
## A
Incremental backup
## B
Differential backup
## C
Full backup
## D
Electronic vaulting
##  Answer
Electronic vaulting describes the transfer of backup data to a remote backup site in a bulk-transfer fashion.
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following is not a benefit of VLANs?
## ID
tb475934.CISSPSG8E.be5.046
## A
Traffic isolation
## B
Data/traffic encryption
## C
Traffic management
## D
Reduced vulnerability to sniffers
##  Answer
B
## Explanation
VLANs do not impose encryption on data or traffic. Encrypted traffic can occur within a VLAN, but encryption is not imposed by the VLAN.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following types of IDS is effective only against known attack methods?
## ID
tb475934.CISSPSG8E.be3.063
## A
Host based
## B
Network based
## C
Knowledge based
## D
Behavior based
##  Answer
C
## Explanation
A knowledge-based IDS is effective only against known attack methods, which is its primary drawback.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Administrators often consider the relationship between assets, risk, vulnerability, and threat under what model?
## ID
tb475934.CISSPSG8E.be6.038
## A
CIA Triad
## B
CIA Triplex
## C
Operations Security Tetrahedron
## D
Operations security triple
##  Answer
D
## Explanation
The operations security triple is a conceptual security model that encompasses three concepts (assets, risk, and vulnerability) and their interdependent relationship within a structured, formalized organization.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What form of intellectual property is used to protect words, slogans, and logos?
## ID
tb475934.CISSPSG8E.be4.084
## A
Patent
## B
Copyright
## C
Trademark
## D
Trade secret
##  Answer
C
## Explanation
Trademarks are used to protect the words, slogans, and logos that represent a company and its products or services.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What form of cloud service provides the customer with the ability to run their own custom code but does not require that they manage the execution environment or operating system?
## ID
tb475934.CISSPSG8E.be5.150
## A
SaaS
## B
PaaS
## C
IaaS
## D
SECaaS
##  Answer
B
## Explanation
Platform as a Service is the concept of providing a computing platform and software solution stack to a virtual or cloud-based service. Essentially, it involves paying for a service that provides all the aspects of a platform (that is, OS and complete solution package). A PaaS solution grants the customer the ability to run custom code of their choosing without needing to manage the environment.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following statements is true?
## ID
tb475934.CISSPSG8E.be1.055
## A
An open system does not allow anyone to view its programming code.
## B
A closed system does not define whether or not its programming code can be viewed.
## C
An open source program can only be distributed for free.
## D
A closed source program cannot be reverse engineered or decompiled.
##  Answer
B
## Explanation
A closed system is designed to work well with a narrow range of other systems, generally all from the same manufacturer. The standards for closed systems are often proprietary and not normally disclosed. However, a closed system (as a concept) does not define whether or not its programming code can be viewed. An open system (as a concept) also does not define whether or not its programming code can be viewed. An open source program can be distributed for free or for a fee. A closed source program can be reverse engineered or decompiled.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What technique may be used if an individual wants to prove knowledge of a fact to another individual without revealing the fact itself?
## ID
tb475934.CISSPSG8E.be2.084
## A
Split-knowledge proof
## B
Work function
## C
Digital signature
## D
Zero-knowledge proof
##  Answer
D
## Explanation
Zero-knowledge proofs confirm that an individual possesses certain factual knowledge without revealing the knowledge.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#



# Which one of the following is not a goal of cryptographic systems?
## ID
b475934.CISSPSG8E.be6.104
## A
Nonrepudiation
## B
Confidentiality
## C
Availability
## D
Integrity
##  Answer
C
## Explanation
The four goals of cryptographic systems are confidentiality, integrity, authentication, and nonrepudiation.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following is not considered a type of auditing activity?
## ID
tb475934.CISSPSG8E.be5.026
## A
Recording of event data
## B
Data reduction
## C
Log analysis
## D
Deployment of countermeasures
##  Answer
D
## Explanation
Deployment of countermeasures is not considered a type of auditing activity but is instead an attempt to prevent security problems. Auditing includes recording event data in logs, using data reduction methods to extract meaningful data, and analyzing logs.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following is an example of a code?
## ID
tb475934.CISSPSG8E.be4.096
## A
Rivest
## B
AES
## C
10 system
## D
Skipjack
##  Answer
C
## Explanation
The 10 system is a code used in radio communications for brevity and clarity.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# You are concerned about the risk that fire poses to your $25 million data center facility. Based on expert opinion, you determine that there is a 1 percent chance that fire will occur each year. Experts advise you that a fire would destroy half of your building. What is the single loss expectancy of your facility to fire?
## ID
b475934.CISSPSG8E.be5.059
## A
$25,000,000
## B
$12,500,000
## C
$250,000
## D
$125,000
##  Answer
B
## Explanation
The single loss expectancy is the amount of damage that would occur in one fire. The scenario states that a fire would destroy half the building, resulting in $12,500,000 of damage
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Your manager is concerned that the business impact assessment recently completed by the BCP team doesn’t adequately take into account the loss of goodwill among customers that might result from a particular type of disaster. Where should items like this be addressed?
## ID
tb475934.CISSPSG8E.be6.061
## A
Continuity strategy
## B
Quantitative analysis
## C
Likelihood assessment
## D
Qualitative analysis
##  Answer
D
## Explanation
The qualitative analysis portion of the business impact assessment (BIA) allows you to introduce intangible concerns, such as loss of customer goodwill, into the BIA planning process.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What strategy basically consists of multiple layers of antivirus, malware, and spyware protection distributed throughout a given network environment?
## ID
tb475934.CISSPSG8E.be6.039
## A
CIA Triad
## B
Concentric circle
## C
Operations security triple
## D
Separation of duties
##  Answer
B
## Explanation
A concentric circle security model comprises several mutually independent security applications, processes, or services that operate toward a single common goal.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Most of the higher-order security models, such as Bell-LaPadula and Biba, are based on which of the following?
## ID
tb475934.CISSPSG8E.be6.114
## A
Take-Grant model
## B
State machine model
## C
Brewer and Nash model
## D
Clark Wilson model
##  Answer
B
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which is not a part of an electronic access control lock?
## ID
tb475934.CISSPSG8E.be4.047
## A
An electromagnet
## B
A credential reader
## C
A door sensor
## D
A biometric scanner
##  Answer
D
## Explanation
An electronic access control (EAC) lock comprises three elements: an electromagnet to keep the door closed, a credential reader to authenticate subjects and to disable the electromagnet, and a door-closed sensor to reenable the electromagnet.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following is not a typical security concern with VoIP?
## ID
tb475934.CISSPSG8E.be1.030
## A
VLAN hopping
## B
Caller ID falsification
## C
Vishing
## D
SPIT
##  Answer
A
## Explanation
VLAN hopping is a switch security issue, not a VoIP security issue. Caller ID falsification, vishing, and SPIT (spam over Internet telephony) are VoIP security concerns.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What is a security perimeter? (Choose all that apply.)
## ID
tb475934.CISSPSG8E.c08.10
## A
The boundary of the physically secure area surrounding your system
## B
The imaginary boundary that separates the TCB from the rest of the system
## C
The network where your firewall resides
## D
Any connections to your computer system
##  Answer
A,B
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# When an intruder gains unauthorized access to a facility by asking an employee to hold open a door because their arms are full of packages, this is known as what type of attack?
## ID
tb475934.CISSPSG8E.be5.029
## A
Tailgating
## B
Masquerading
## C
Impersonation
## D
Piggybacking
##  Answer
D
## Explanation
Piggybacking is when an authorized person knowingly lets you through.
Tailgating is when an authorized persons unknowingly lets you through.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#



# Which of the following is a procedure designed to test and perhaps bypass a system’s security controls?
## ID
tb475934.CISSPSG8E.at.27
## A
Logging usage data
## B
War dialing
## C
Penetration testing
## D
Deploying secured desktop workstations
##  Answer
C
## Explanation
Penetration testing is the attempt to bypass security controls to test overall system security.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which one of the following storage devices is most likely to require encryption technology in order to maintain data security in a networked environment?
## ID
tb475934.CISSPSG8E.c09.11
## A
Hard disk
## B
Backup tape
## C
Removable drives
## D
RAM
##  Answer
C
## Explanation
Removable drives are easily taken out of their authorized physical location, and it is often not possible to apply operating system access controls to them. Therefore, encryption is often the only security measure short of physical security that can be afforded to them. Backup tapes are most often well controlled through physical security measures. Hard disks and RAM chips are often secured through operating system access controls.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following is not a form of spoofed traffic filtering?
## ID
tb475934.CISSPSG8E.be5.049
## A
Block inbound packets whose source address is an internal address
## B
Block outbound packets whose source address is an external address
## C
Block outbound packets whose source address is an unassigned internal address
## D
Block inbound packets whose source address is on a block/black list
##  Answer
D
## Explanation
Using a block list or black list is a valid form of security filtering; it is just not a form of spoofing filtering.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which one of the following alternate processing arrangements is rarely implemented?
## ID
tb475934.CISSPSG8E.be4.077
## A
Hot site
## B
Warm site
## C
Cold site
## D
MAA site
##  Answer
D
## Explanation
Mutual assistance agreements are rarely implemented because they are difficult to enforce in the event of a disaster requiring site activation.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What is a security risk of an embedded system that is not commonly found in a standard PC?
## ID
tb475934.CISSPSG8E.c09.05
## A
Software flaws
## B
Access to the internet
## C
Control of a mechanism in the physical world
## D
Power loss
##  Answer
C
## Explanation
Because an embedded system is in control of a mechanism in the physical world, a security breach could cause harm to people and property. This typically is not true of a standard PC. Power loss, internet access, and software flaws are security risks of both embedded systems and standard PCs.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which one of the following business impact assessment (BIA) tasks should be performed last?
## ID
tb475934.CISSPSG8E.be6.063
## A
Risk identification
## B
Resource prioritization
## C
Impact assessment
## D
Likelihood assessment
##  Answer
C
## Explanation
Resource prioritization is the final step of the business impact assessment (BIA) process.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#




# Which one of the following assumptions is not necessary before you trust the public key contained on a digital certificate?
## ID
tb475934.CISSPSG8E.be3.124
## A
The digital certificate of the CA is authentic.
## B
You trust the sender of the certificate.
## C
The certificate is not listed on a CRL.
## D
The certificate actually contains the public key.
##  Answer
B
## Explanation
You do not need to trust the sender of a digital certificate as long as the certificate meets the other requirements listed and you trust the certification authority.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What is the first priority in any business continuity plan?
## ID
tb475934.CISSPSG8E.be1.040
## A
Data restoration
## B
Personal safety
## C
Containing damage
## D
Activating an alternate site
##  Answer
B
## Explanation
The primary concern in any business continuity or disaster recovery effort is ensuring personal safety for everyone involved.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# On what port do DHCP clients request a configuration?
## ID
tb475934.CISSPSG8E.be2.005
## A
25
## B
110
## C
68
## D
443
##  Answer
C
## Explanation
Dynamic Host Configuration Protocol (DHCP) uses port 68 for client request broadcast and port 67 for server point-to-point response.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following is not one of the benefits of maintaining an object’s integrity?
## ID
tb475934.CISSPSG8E.be1.117
## A
Unauthorized subjects should be prevented from making modifications.
## B
Authorized subjects should be prevented from making unauthorized modifications.
## C
Objects should be available for access at all times without interruption to authorized individuals.
## D
Objects should be internally and externally consistent so that their data is a correct and true reflection of the real world and any relationship with any child, peer, or parent object is valid, consistent, and verifiable.
##  Answer
C
## Explanation
Option C is not an example of protecting integrity; it is the principle of availability.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which one of the following is not a major asset category normally covered by the BCP (business continuity plan)?
## ID
tb475934.CISSPSG8E.be3.135
## A
People
## B
Documentation
## C
Infrastructure
## D
Buildings/facilities
##  Answer
B
## Explanation
The BCP normally covers three major asset categories: people, infrastructure, and buildings/facilities.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which software development life cycle model allows for multiple iterations of the development process, resulting in multiple prototypes, each produced according to a complete design and testing process?
## ID
tb475934.CISSPSG8E.be4.010
## A
Software Capability Maturity model
## B
Waterfall model
## C
Development cycle
## D
Spiral model
##  Answer
D
## Explanation
The spiral model allows developers to repeat iterations of another life cycle model (such as the waterfall model) to produce a number of fully tested prototypes.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the composition theories refers to the management of information flow from one system to another?
## ID
tb475934.CISSPSG8E.be6.134
## A
Feedback
## B
Hookup
## C
Cascading
## D
Waterfall
##  Answer
C
## Explanation
Cascading is when input for one system comes from the output of another system. Feedback is when one system provides input to another system, which reciprocates by reversing those roles (so that system A first provides input for system B and then system B provides input to system A). Hookup is when one system sends input to another system but also sends input to external entities. Waterfall is a form of project management not related to information flow.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What is a data object passed from the Transport layer to the Network layer called when it reaches the Network layer?
## ID
tb475934.CISSPSG8E.be5.017
## A
Protocol data unit
## B
Segment
## C
Datagram
## D
Frame
##  Answer
C
## Explanation
A data object is called a datagram or a packet in the Network layer. It is called a PDU in layers 5 through 7. It is called a segment in the Transport layer and a frame in the Data Link layer.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following is not true regarding firewalls?
## ID
tb475934.CISSPSG8E.c11.13
## A
They are able to log traffic information.
## B
They are able to block viruses.
## C
They are able to issue alarms based on suspected attacks.
## D
They are unable to prevent internal attacks.
##  Answer
B
## Explanation
Most firewalls offer extensive logging, auditing, and monitoring capabilities as well as alarms and even basic IDS functions. Firewalls are unable to block viruses or malicious code transmitted through otherwise authorized communication channels, prevent unauthorized but accidental or intended disclosure of information by users, prevent attacks by malicious users already behind the firewall, or protect data after it passed out of or into the private network.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following cryptographic attacks can be used when you have access to an encrypted message but no other information?
## ID
tb475934.CISSPSG8E.be6.032
## A
Known plain-text attack
## B
Frequency analysis attack
## C
Chosen cipher-text attack
## D
Meet-in-the-middle attack
##  Answer
B
## Explanation
Frequency analysis may be used on encrypted messages. The other techniques listed require additional information, such as the plaintext or the ability to choose the ciphertext.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Federation is a means to accomplish ???.
## ID
tb475934.CISSPSG8E.be6.147
## A
Accountability logging
## B
ACL verification
## C
Single sign-on
## D
Trusted OS hardening
##  Answer
C
## Explanation
Federation or federated identity is a means of linking a subject’s accounts from several sites, services, or entities in a single account. Thus, it is a means to accomplish single sign-on. Accountability logging is used to relate digital activities to humans. ACL verification is a means to verify that correct permissions are assigned to subjects. Trusted OS hardening is the removal of unneeded components and securing the remaining elements.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What is an access object?
## ID
tb475934.CISSPSG8E.c08.05
## A
A resource a user or process wants to access
## B
A user or process that wants to access a resource
## C
A list of valid access rules
## D
The sequence of valid access types
##  Answer
A
## Explanation
An object is a resource a user or process wants to access. Option A describes an access object.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What type of trusted recovery process always requires the intervention of an administrator?
## ID
tb475934.CISSPSG8E.be6.130
## A
Automated
## B
Manual
## C
Function
## D
Controlled
##  Answer
B
## Explanation
A manual type of trusted recovery process (described in the Common Criteria) always requires the intervention of an administrator. Automated recovery can perform some type of trusted recovery for at least one type of failure. Function recovery provides automated recovery for specific functions and will roll back changes to a secure state if recovery fails. Controlled is not a specific type of trusted recovery process.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What is a threat modeling methodology that focuses on a risk-based approach instead of depending upon an aggregated threat model and that provides a method of performing a security audit in a reliable and repeatable procedure?
## ID
tb475934.CISSPSG8E.be3.003
## A
VAST
## B
Trike
## C
STRIDE
## D
DREAD
##  Answer
B
## Explanation
Trike is a threat modeling methodology that focuses on a risk-based approach instead of depending upon the aggregated threat model used in STRIDE and DREAD; it provides a method of performing a security audit in a reliable and repeatable procedure. Visual, Agile, and Simple Threat (VAST) is a threat modeling concept based on Agile project management and programming principles.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following does not need to be true in order to maintain the most efficient and secure server room?
## ID
tb475934.CISSPSG8E.c10.05
## A
It must be human compatible.
## B
It must include the use of nonwater fire suppressants.
## C
The humidity must be kept between 40 and 60 percent.
## D
The temperature must be kept between 60 and 75 degrees Fahrenheit.
##  Answer
A
## Explanation
A computer room does not need to be human compatible to be efficient and secure. Having a human-incompatible server room provides a greater level of protection against attacks.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following is not a valid access control model?
## ID
tb475934.CISSPSG8E.c14.14
## A
Discretionary Access Control model
## B
Nondiscretionary Access Control model
## C
Mandatory Access Control model
## D
Compliance-Based Access Control model
##  Answer
D
## Explanation
Compliance-Based Access Control model is not a valid type of access control model. The other answers list valid access control models.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Among the following scenarios, which course of action is insufficient to increase your posture against brute-force and dictionary-driven attacks?
## ID
tb475934.CISSPSG8E.be3.051
## A
Restrictive control over physical access
## B
Policy-driven strong password enforcement
## C
Two-factor authentication deployment
## D
Requiring authentication timeouts
##  Answer
D
## Explanation
Requiring authentication timeouts bears no direct result on password attack protection. Strong password enforcement, restricted physical access, and two-factor authentication help improve security posture against automated attacks.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# In a relational database, what type of key is used to uniquely identify a record in a table and can have multiple instances per table?
## ID
tb475934.CISSPSG8E.be4.101
## A
Candidate key
## B
Primary key
## C
Unique key
## D
Foreign key
##  Answer
A
## Explanation
A candidate key is a subset of attributes that can be used to uniquely identify any record in a table. No two records in the same table will ever contain the same values for all attributes composing a candidate key. Each table may have one or more candidate keys, which are chosen from column headings.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What sort of criminal act is committed when an attacker is able to intercept more than just network traffic?
## ID
tb475934.CISSPSG8E.be6.056
## A
Sniffing
## B
Eavesdropping
## C
Snooping
## D
War dialing
##  Answer
B
## Explanation
Eavesdropping is the ability to intercept network and telephony communications along with physical and electronic documents and even personal conversations.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What database security feature uses a locking mechanism to prevent simultaneous edits of cells?
## ID
tb475934.CISSPSG8E.be2.049
## A
Semantic integrity mechanism
## B
Concurrency
## C
Polyinstantiation
## D
Database partitioning
##  Answer
C
## Explanation
Concurrency uses a “lock” feature to allow an authorized user to make changes and then “unlock” the data elements only after the changes are complete. This is done so another user is unable able to access the database to view and/or make changes to the same elements at the same time.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What is system accreditation?
## ID
tb475934.CISSPSG8E.c08.02
## A
Formal acceptance of a stated system configuration
## B
A functional evaluation of the manufacturer’s goals for each hardware and software component to meet integration standards
## C
Acceptance of test results that prove the computer system enforces the security policy
## D
The process to specify secure communication between machines
##  Answer
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# When an attacker selects a target, they must perform reconnaissance to learn as much as possible about the systems and their configuration before launching attacks. What is the gathering of information from any publicly available resource, such as websites, social networks, discussion forums, file services, and public databases?
## ID
tb475934.CISSPSG8E.be5.148
## A
Banner grabbing
## B
Port scanning
## C
Open source intelligence
## D
Enumeration
##  Answer
C
## Explanation
Open source intelligence is the gathering of information from any publicly available resource. This includes websites, social networks, discussion forums, file services, public databases, and other online sources. This also includes non-Internet sources, such as libraries and periodicals.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What frequency does an 802.11n-compliant device employ that allows it to maintain support for legacy 802.11g devices?
## ID
tb475934.CISSPSG8E.be2.009
## A
5 GHz
## B
900 MHz
## C
7 GHz
## D
2.4 GHz
##  Answer
D
## Explanation
802.11n can use the 2.4 GHz and 5 GHz frequencies. The 2.4 GHz frequency is also used by 802.11g and 802.11b. The 5 GHz frequency is used by 802.11a, 802.11n, and 802.11ac
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# In a discussion of high-speed telco links or network carrier services, what does fault tolerance mean?
## ID
tb475934.CISSPSG8E.be2.012
## A
Error checking
## B
Redundancy
## C
Flow control
## D
Bandwidth on demand
##  Answer
B
## Explanation
In a discussion of high-speed telco links or network carrier services, fault tolerance means to have redundant connections.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following should be generally worker/user accessible?
## ID
tb475934.CISSPSG8E.be4.062
## A
Mission-critical data center
## B
Main workspaces
## C
Server vault
## D
Wiring closet
##  Answer
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Spoofing is primarily used to perform what activity?
## ID
tb475934.CISSPSG8E.be5.022
## A
Send large amounts of data to a victim
## B
Cause a buffer overflow
## C
Hide the identity of an attacker through misdirection
## D
Steal user account names and passwords
##  Answer
C
## Explanation
Spoofing grants the attacker the ability to hide their identity through misdirection and is used in many different types of attacks. Spoofing doesn’t send large amounts of data to a victim. It can be used as part of a buffer overflow attack to hide the identity of the attacker but doesn’t cause buffer overflows directly. If user account names and passwords are stolen, the attacker can use them to impersonate the user.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which access control system discourages the violation of security processes, policies, and procedures?
## ID
tb475934.CISSPSG8E.be3.052
## A
Detective access control
## B
Preventive access control
## C
Corrective access control
## D
Deterrent access control
##  Answer
D
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Abnormal or unauthorized activities detectable to an IDS include which of the following? (Choose all that apply.)
## ID
tb475934.CISSPSG8E.be1.137
## A
External connection attempts
## B
Execution of malicious code
## C
Access to controlled objects
## D
None of the above
##  Answer
A,B,C
## Explanation
IDSs watch for violations of confidentiality, integrity, and availability. Attacks recognized by IDSs can come from external connections (such as the Internet or partner networks), viruses, malicious code, trusted internal subjects attempting to perform unauthorized activities, and unauthorized access attempts from trusted locations.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which one of the following patterns of activity is indicative of a scanning attack?
## ID
tb475934.CISSPSG8E.be4.024
## A
Large number of blocked connection attempts on port 22
## B
Large number of successful connection attempts on port 80
## C
Large number of successful connection attempts on port 443
## D
Large number of disk failure events
##  Answer
A
## Explanation
A high number of blocked connection attempts may indicate that an attacker is scanning systems that do not offer a particular service on a particular port. Port 22 is the TCP port usually used by the Secure Shell (SSH) protocol, a common target of scanning attacks.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#



# What type of attack occurs when malicious users position themselves between a client and server and then interrupt the session and take it over?
## ID
b475934.CISSPSG8E.be4.052
## A
Man-in-the-middle
## B
Spoofing
## C
Hijack
## D
Cracking
##  Answer
C
## Explanation
In a hijack attack, which is an offshoot of a man-in-the-middle attack, a malicious user is positioned between a client and server and then interrupts the session and takes it over. A man-in-the middle attack doesn’t interrupt the session and take it over. Spoofing hides the identity of the attacker. Cracking commonly refers to discovering a password but can also mean any type of attack.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# You are implementing AES encryption for files that your organization plans to store in a cloud storage service and wish to have the strongest encryption possible. What key length should you choose?
## ID
tb475934.CISSPSG8E.be1.120
## A
192 bits
## B
256 bits
## C
512 bits
## D
1024 bits
##  Answer
B
## Explanation
The strongest keys supported by the Advanced Encryption Standard are 256 bits. The valid AES key lengths are 128, 192, and 256 bits.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What is the limit on the number of candidate keys that can exist in a single database table?
## ID
tb475934.CISSPSG8E.be6.081
## A
Zero
## B
One
## C
Two
## D
No limit
##  Answer
D
## Explanation
There is no limit to the number of candidate keys that a table can have. However, each table can have only one primary key.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What law protects the privacy rights of students?
## ID
tb475934.CISSPSG8E.be4.094
## A
HIPAA
## B
SOX
## C
GLBA
## D
FERPA
##  Answer
D
## Explanation
The Family Educational Rights and Privacy Act (FERPA) protects the rights of students and the parents of minor students.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#



# What rule of evidence states that a written agreement is assumed to contain all terms of the agreement?
## ID
tb475934.CISSPSG8E.be4.123
## A
Real evidence
## B
Best evidence
## C
Parol evidence
## D
Chain of evidence
##  Answer
C
## Explanation
The parol evidence rule states that when an agreement between parties is put into written form, the written document is assumed to contain all the terms of the agreement, and no verbal agreements may modify the written agreement.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Your database administrators recommend performing bulk transfer backups to a remote site on a daily basis. What type of strategy are they recommending?
## ID
tb475934.CISSPSG8E.be4.078
## A
Transaction logging
## B
Electronic vaulting
## C
Remote journaling
## D
Remote mirroring
##  Answer
B
## Explanation
Electronic vaulting uses bulk transfers to copy database contents to a remote site on a periodic basis.
Remote journaling is similar bit only involves transport incremental changes
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What is access?
## ID
tb475934.CISSPSG8E.be5.001
## A
Functions of an object
## B
Information flow from objects to subjects
## C
Unrestricted admittance of subjects on a system
## D
Administration of ACLs
##  Answer
B
## Explanation
Access is the transfer of information from an object to a subject. An object is a passive resource that does not have functions. Access is not unrestricted. Access control includes more than administration of access control lists (ACLs).
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# When it can be legally established that a human performed a specific action that was discovered via auditing, accountability has been established. What additional benefit is derived from this investigation and verification process?
## ID
tb475934.CISSPSG8E.be2.035
## A
Nonrepudiation
## B
Privacy
## C
Abstraction
## D
Redundancy
##  Answer
A
## Explanation
When audit trails legally prove accountability, then you also reap the benefit of nonrepudiation.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What means of risk response transfers the burden of risk to another entity?
## ID
tb475934.CISSPSG8E.be6.123
## A
Mitigation
## B
Assignment
## C
Tolerance
## D
Rejection
##  Answer
B
## Explanation
Risk assignment or transferring risk is the placement of the cost of loss a risk represents onto another entity or organization. Purchasing insurance and outsourcing are common forms of assigning or transferring risk.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# The absence of which of the following can result in the perception that due care is not being maintained?
## ID
tb475934.CISSPSG8E.be4.103
## A
Periodic security audits
## B
Deployment of all available controls
## C
Performance reviews
## D
Audit reports for shareholders
##  Answer
A
## Explanation
Failing to perform periodic security audits can result in the perception that due care is not being maintained. Such audits alert personnel that senior management is practicing due diligence in maintaining system security. An organization should not indiscriminately deploy all available controls but should choose the most effective ones based on risks. Performance reviews are useful managerial practices but not directly related to due care. Audit reports should not be shared with the public.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# A security management plan that discusses the needs of an organization to maintain security, the desire to improve control of authorized access, and the goal of implementing token-based security is what type of plan?
## ID
tb475934.CISSPSG8E.be3.076
## A
Functional
## B
Operational
## C
Strategic
## D
Tactical
##  Answer
C
## Explanation
A strategic plan is a long-term plan that is fairly stable. It defines the organization’s goals, mission, and objectives. It is useful for about five years if it is maintained and updated annually. The strategic plan also serves as the planning horizon. Long-term goals and visions of the future are discussed in a strategic plan.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which type of connection created by a packet-switching networking system reuses the same basic parameters or virtual pathway each time it connects?
## ID
tb475934.CISSPSG8E.be4.137
## A
Bandwidth on-demand connection
## B
Switched virtual circuit
## C
Permanent virtual circuit
## D
CSU/DSU connection
##  Answer
C
## Explanation
A PVC reestablishes a link using the same basic parameters or virtual pathway each time it connects. SVCs use unique settings each time. Bandwidth on-demand links can be either PVCs or SVCs. CSU/DSU is not a packet-switching technology.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# APTs are most closely related to what type of attack category?
## ID
tb475934.CISSPSG8E.be6.132
## A
Military attacks
## B
Thrill attacks
## C
Grudge attacks
## D
Insider attacks
##  Answer
A
## Explanation
Advanced persistent threats (APTs) are often associated with government and military actors.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# You are concerned about the risk that a hurricane poses to your corporate headquarters in south Florida. The building itself is valued at $15 million. After consulting with the National Weather Service, you determine that there is a 10 percent likelihood that a hurricane will strike over the course of a year. You hired a team of architects and engineers who determined that the average hurricane would destroy approximately 50 percent of the building. What is the annualized rate of occurrence (ARO)?
## ID
tb475934.CISSPSG8E.be5.037
## A
5 percent
## B
10 percent
## C
50 percent
## D
100 percent
##  Answer
B
## Explanation
The ARO is, quite simply, the probability that a given disaster will strike over the course of a year. According to the experts quoted in the scenario, this chance is 10 percent.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#



# Which of the following includes a record of system activity and can be used to detect security violations, software flaws, and performance problems?
## ID
tb475934.CISSPSG8E.be3.008
## A
Change logs
## B
Security logs
## C
System logs
## D
Audit trail
##  Answer
D
## Explanation
Audit trails provide a comprehensive record of system activity and can help detect a wide variety of security violations, software flaws, and performance problems. An audit trail includes a variety of logs, including change logs, security logs, and system logs, but any one of these individual logs can’t detect all the issues mentioned in the question.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What is the final stage in the life cycle of backup media, occurring after or as a means of sanitization?
## ID
tb475934.CISSPSG8E.be6.047
## A
Degaussing
## B
Destruction
## C
Declassification
## D
Defenestration
##  Answer
B
## Explanation
Destruction is the final stage in the life cycle of backup media. Destruction should occur after proper sanitization or as a means of sanitization.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# There are generally three forms of governance within an enterprise organization, all of which have common goals, such as to ensure continued growth and expansion over time and to maintain resiliency to threats and the market. Which of the following is not one of these common forms of governance?
## ID
tb475934.CISSPSG8E.be5.058
## A
IT
## B
Facility
## C
Corporate
## D
Security
##  Answer
B
## Explanation
The three common forms of governance are IT, corporate, and security. Facility is not usually considered a form of governance, or it is already contained within one of the other three.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#



# Biometric authentication devices fall under what top-level authentication factor type?
## ID
tb475934.CISSPSG8E.be1.122
## A
Type 1
## B
Type 2
## C
Type 3
## D
Type 4
##  Answer
C
## Explanation
Biometric authentication devices represent a Type 3 (something you are) authentication factor.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which environment requires exact, specific clearance for an object’s security domain?
## ID
tb475934.CISSPSG8E.be3.072
## A
Hierarchical environment
## B
Hybrid environment
## C
Compartmentalized environment
## D
Organizational environment
##  Answer
C
## Explanation
Compartmentalized environments require specific security clearances over compartments or domains instead of objects.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# When attempting to impose accountability on users, what key issue must be addressed?
## ID
tb475934.CISSPSG8E.be4.144
## A
Reliable log storage system
## B
Proper warning banner notification
## C
Legal defense/support of authentication
## D
Use of discretionary access control
##  Answer
C
## Explanation
To effectively hold users accountable, your security must be legally defensible. Primarily, you must be able to prove in a court that your authentication process cannot be easily compromised. Thus, your audit trails of actions can then be tied to a human.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#



# An employee retained access to sensitive data from previous job assignments. Investigators later caught him selling some of this sensitive data to competitors. What could have prevented the employee from stealing and selling the secret data?
## ID
tb475934.CISSPSG8E.be4.104
## A
Asset valuation
## B
Threat modeling
## C
Vulnerability analysis
## D
User entitlement audit
##  Answer
D
## Explanation
A user entitlement audit can detect when employees have excessive privileges. Asset valuation identifies the value of assets. Threat modeling identifies threats to valuable assets. Vulnerability analysis detects vulnerabilities or weaknesses that can be exploited by threats.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# The Roscommon Rangers baseball team is concerned about the risk that a storm might result in the cancellation of a baseball game. There is a 30 percent chance that the storm will occur, and if it does, the team must refund all single-game tickets because the game cannot be rescheduled. Season ticket holders will not receive a refund and account for 20 percent of ticket sales. The ticket sales for the game are $1,500,000. What is the single loss expectancy in this scenario?
## ID
tb475934.CISSPSG8E.be6.143
## A
$300,000
## B
$1,050,000
## C
$1,200,000
## D
$1,500,000
##  Answer
C
## Explanation
The single loss expectancy is calculated as the product of the exposure factor (80 percent) and the asset value ($1,500,000). In this example, the single loss expectancy is $1,200,000.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#



# The Bell-LaPadula access control model was designed to protect what aspect of security?
## ID
tb475934.CISSPSG8E.be6.028
## A
Confidentiality
## B
Integrity
## C
Accessibility
## D
Authentication
##  Answer
A
## Explanation
The Bell-LaPadula model is focused on maintaining the confidentiality of objects. Bell-LaPadula does not address the aspects of object integrity or availability.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Where is a good location for a turnstile?
## ID
tb475934.CISSPSG8E.be3.136
## A
Main entrance to a secure area
## B
Primary entrance for the public to enter a retail space
## C
On secondary or side exits
## D
On internal office intersections
##  Answer
C
## Explanation
Turnstiles are most appropriate on secondary or side exits where a security guard is not available or is unable to maintain constant surveillance. The other options listed are not as ideal for the use of a turnstile.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following elements is not necessary in the BCP documentation?
## ID
tb475934.CISSPSG8E.be4.059
## A
Risk acceptance details
## B
Emergency response guidelines
## C
Risk assessment
## D
Mobile site plan
##  Answer
D
## Explanation
Details of mobile sites are part of a disaster recovery plan, rather than a business continuity plan, since they are not deployed until after a disaster strikes.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Subjects can be held accountable for their actions toward other subjects and objects while they are authenticated to a system. What process facilitates this accountability?
## ID
tb475934.CISSPSG8E.be1.073
## A
Access controls
## B
Monitoring
## C
Account policies
## D
Performance review
##  Answer
B
## Explanation
Monitoring the activities of subjects and objects, as well as of core system functions that maintain the operating environment and the security mechanisms, helps establish accountability on the system.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Abnormal or unauthorized activities detectable to an IDS include which of the following? (Choose all that apply.)
## ID
tb475934.CISSPSG8E.be3.059
## A
External connection attempts
## B
Execution of malicious code
## C
Access to controlled objects
## D
None of the above
##  Answer
A,B,C
## Explanation
IDSs watch for violations of confidentiality, integrity, and availability. Attacks recognized by IDSs can come from external connections (such as the Internet or partner networks), viruses, malicious code, trusted internal subjects attempting to perform unauthorized activities, and unauthorized access attempts from trusted locations.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Using a network packet sniffer, you intercept a communication. After examining the IP packet’s header, you notice that the flag byte has the binary value of 00000100. What does this indicate?
## ID
tb475934.CISSPSG8E.be3.028
## A
A flooding attack is occurring.
## B
A new session is being initiated.
## C
A reset has been transmitted.
## D
A custom-crafted IP packet is being broadcast.
##  Answer
C
## Explanation
The flag value of 00000100 indicates a RST, or reset flag, has been transmitted. This is not necessarily an indication of malicious activity.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# An organization has a patch management program but wants to implement another method to ensure that systems are kept up-to-date. What could they use?
## ID
tb475934.CISSPSG8E.be5.067
## A
Change management program
## B
Configuration management program
## C
Port scanners
## D
Vulnerability scanners
##  Answer
D
## Explanation
Vulnerability scanners can check systems to ensure they are up-to-date with current patches (along with other checks) and are an effective tool to verify the patch management program. Change and configuration management programs ensure systems are configured as expected and unauthorized changes are not allowed. A port scanner is often part of a vulnerability scanner, but it will check only for open ports, not patches.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What type of evidence must be authenticated by a witness who can uniquely identify it or through a documented chain of custody?
## ID
tb475934.CISSPSG8E.be5.088
## A
Documentary evidence
## B
Testimonial evidence
## C
Real evidence
## D
Hearsay evidence
##  Answer
C
## Explanation
Real evidence must be either uniquely identified by a witness or authenticated through a documented chain of custody.   
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What type of malicious code uses a filename similar to that of a legitimate system file?
## ID
tb475934.CISSPSG8E.be4.113
## A
MBR
## B
Companion
## C
Stealth
## D
Multipartite
##  Answer
B
## Explanation
Companion viruses use filenames that mimic the filenames of legitimate system files.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What type of decision-making involves the emotional impact of events on a firm’s workforce and client base?
## ID
tb475934.CISSPSG8E.be2.131
## A
Quantitative decision-making
## B
Continuity decision-making
## C
Qualitative decision-making
## D
Impact decision-making
##  Answer
C
## Explanation
Qualitative decision-making takes non-numerical factors, such as emotional impact, into consideration.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# NAT allows the use of private IP addresses for internal use while maintaining the ability to communicate with the Internet. Where are the private IP addresses defined?
## ID
tb475934.CISSPSG8E.be5.087
## A
RFC 1492
## B
RFC 1661
## C
RFC 1918
## D
RFC 3947
##  Answer
C
## Explanation
NAT offers numerous benefits, including that you can use the private IP addresses defined in RFC 1918 in a private network and still be able to communicate with the Internet.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What form of access control is concerned with the data stored by a field rather than any other issue?
## ID
tb475934.CISSPSG8E.be4.049
## A
Content-dependent
## B
Context-dependent
## C
Semantic integrity mechanisms
## D
Perturbation
##  Answer
A
## Explanation
Content-dependent access control is focused on the internal data of each field.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#



# What is a divestiture?
## ID
tb475934.CISSPSG8E.be5.053
## A
Asset or employee reduction
## B
A distribution of profits to shareholders
## C
A release of documentation to the public
## D
A transmission of data to law enforcement during an investigation
##  Answer
A
## Explanation
A divestiture is an asset or employee reduction.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Of the following, which provides some protection against tampering?
## ID
tb475934.CISSPSG8E.be6.041
## A
Security token
## B
Capabilities list
## C
Security label
## D
Access matrix
##  Answer
C
## Explanation
A security label is usually a permanent part of the object to which it is attached, thus providing some protection against tampering. The other options do not offer such protection.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following is not a feature of packet switching?
## ID
tb475934.CISSPSG8E.be4.135
## A
Bursty traffic focused
## B
Fixed known delays
## C
Sensitive to data loss
## D
Supports any type of traffic
##  Answer
B
## Explanation
Packet switching has variable delays; circuit switching has fixed known delays.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What is the length of a patent in the United States?
## ID
tb475934.CISSPSG8E.be6.023
## A
7 years
## B
14 years
## C
20 years
## D
35 years
##  Answer
C
## Explanation
U.S. patents are generally issued for a period of 20 years.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# In which phase of the business impact assessment do you compute loss expectancies?
## ID
tb475934.CISSPSG8E.be4.058
## A
Risk assessment
## B
Likelihood assessment
## C
Impact assessment
## D
Resource prioritization
##  Answer
C
## Explanation
Loss expectancies are a measure of impact and are calculated during the impact assessment phase.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following security models is most often used for general commercial applications?
## ID
tb475934.CISSPSG8E.be4.099
## A
Brewer and Nash model
## B
Biba model
## C
Bell-LaPadula model
## D
Clark-Wilson model
##  Answer
D
## Explanation
Of the four models mentioned, Biba and Clark-Wilson are most commonly used for commercial applications because both focus on data integrity. Of these two, Clark-Wilson offers more control and does a better job of maintaining integrity, so it’s used most often for commercial applications. Bell-LaPadula is used most often for military applications. Brewer and Nash applies only to datasets (usually within database management systems) where conflict-of-interest classes prevent subjects from accessing more than one dataset that might lead to a conflict-of-interest situation.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# In the Biba model, what rule prevents a user from reading from lower levels of classification?
## ID
tb475934.CISSPSG8E.be2.083
## A
Star axiom
## B
Simple property
## C
No read up
## D
No write down
##  Answer
B
## Explanation
The Biba simple property rule is “no read down.” The Biba star axiom is "no write up". "No read up" is the simple rule for Bell LaPadula. "No write down" is the star rule for Bell LaPadula.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What is the most likely problem to using VPNs when a separate firewall is present?
## ID
tb475934.CISSPSG8E.be4.132
## A
You can’t filter on encrypted traffic.
## B
VPNs cannot cross firewalls.
## C
Firewalls block all outbound VPN connections.
## D
Firewalls greatly reduce the throughput of VPNs.
##  Answer
A
## Explanation
Firewalls are unable to filter on encrypted traffic within a VPN, which is a drawback. VPNs can cross firewalls. Firewalls do not have to always block outbound VPN connections. Firewalls usually only minimally affect the throughput of a VPN and then only when filtering is possible.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What are the only procedures that are allowed to modify a constrained data item (CDI) in the Clark-Wilson model?
## ID
tb475934.CISSPSG8E.be6.033
## A
Transformation procedures (TPs)
## B
Integrity verification procedure (IVP)
## C
Independent modification algorithm (IMA)
## D
Dependent modification algorithm (DMA)
##  Answer
A
## Explanation
Transformation procedures (TPs) are the only procedures that are allowed to modify a CDI. The limited access to CDIs through TPs forms the backbone of the Clark-Wilson integrity model.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What is the second phase of the IDEAL software development model?
## ID
tb475934.CISSPSG8E.be6.085
## A
Developing
## B
Diagnosing
## C
Determining
## D
Designing
##  Answer
B
## Explanation
The second phase of the IDEAL software development model is the Diagnosing stage.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following is not a critical member of a BCP team?
## ID
tb475934.CISSPSG8E.be5.091
## A
Legal representatives
## B
Information security representative
## C
Technical and functional experts
## D
Chief executive officer
##  Answer
C
## Explanation
While it is important to have executive-level support, it is not necessary (and quite unlikely!) to have the CEO on the team.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#



# What is the term used to refer to the formal assignment of responsibility to an individual or group?
## ID
tb475934.CISSPSG8E.be5.056
## A
Governance
## B
Ownership
## C
Take grant
## D
Separation of duties
##  Answer
B
## Explanation
Ownership is the formal assignment of responsibility to an individual or group.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# A process can function or operate as ????.
## ID
tb475934.CISSPSG8E.be1.054
## A
Subject only
## B
Object only
## C
Subject or object
## D
Neither a subject nor an object
##  Answer
C
## Explanation
A process can function or operate as a subject or object. In fact, many elements within an IT environment, including users, can be subjects or objects in different access control situations.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following algorithms/protocols provides inherent support for nonrepudiation?
## ID
tb475934.CISSPSG8E.be2.087
## A
HMAC
## B
DSA
## C
MD5
## D
SHA-1
##  Answer
B
## Explanation
The Digital Signature Algorithm (as specified in FIPS 186-2) is the only one of the algorithms listed here that supports true digital signatures, providing integrity verification and nonrepudiation. HMAC allows for the authentication of message digests but supports only integrity verification. MD5 and SHA-1 are message digest creation algorithms and can be used in the generation of digital signatures but provide no guarantees of integrity or nonrepudiation in and of themselves.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following groupings restricts access to roles?
## ID
tb475934.CISSPSG8E.be1.024
## A
Grouping subjects
## B
Grouping privileges
## C
Grouping programs
## D
Grouping objects
##  Answer
A
## Explanation
Role-based access control restricts access to roles by grouping subjects (such as users). Groups are assigned privileges but privileges aren’t grouped in roles. Programs aren’t grouped in roles. Objects such as files are often grouped within folders, but objects are not assigned as roles.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What category of malicious software includes rogue antivirus software?
## ID
tb475934.CISSPSG8E.be6.026
## A
Logic bombs
## B
Worms
## C
Trojan horses
## D
Spyware
##  Answer
C
## Explanation
Rogue antivirus software is an example of a Trojan horse. Users are tricked into installing it, and once installed, it steals sensitive information and/or prompts the user for payment.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Change management should ensure that which of the following is possible?
## ID
tb475934.CISSPSG8E.be3.088
## A
Duplicate security is imposed on other systems.
## B
Unauthorized changes to the system are prevented.
## C
Vulnerable resources are locked down when threatened
## D
Changes can be rolled back to a previous state.
##  Answer
D
## Explanation
Change management is responsible for making it possible to roll back any change to a previous secured state.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What form of password attack utilizes a preassembled lexicon of terms and their permutations?
## ID
tb475934.CISSPSG8E.be1.133
## A
Rainbow tables
## B
Dictionary word list
## C
Brute force
## D
Educated guess
##  Answer
B
## Explanation
Dictionary word lists are precompiled lists of common passwords and their permutations and serve as the foundation for a dictionary attack on accounts.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which conceptual security model offers the best preventive protection against viral infection and outbreak?
## ID
tb475934.CISSPSG8E.be4.044
## A
ISO/OSI reference model
## B
Concentric circle
## C
Operations security triple
## D
CIA Triad
##  Answer
B
## Explanation
A concentric circle security model represents the best practice known as defense in depth, a layered approach to protecting IT infrastructure.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What is the vulnerability or feature of a PBX system that allows for an external entity to piggyback onto the PBX system and make long-distance calls without being charged for the tolls?
## ID
tb475934.CISSPSG8E.be3.105
## A
Black box
## B
DTMF
## C
Vishing
## D
Remote dialing
##  Answer
D
## Explanation
Remote dialing (aka hoteling) is the vulnerability or feature of a PBX system that allows for an external entity to piggyback onto the PBX system and make long-distance calls without being charged for the tolls.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What Clark-Wilson model feature helps protect against insider attacks by restricting the amount of authority any user possesses?
## ID
tb475934.CISSPSG8E.be5.140
## A
Simple integrity property
## B
Time of use
## C
Need to know
## D
Separation of duties
##  Answer
D
## Explanation
The Clark-Wilson model enforces separation of duties to further protect the integrity of data. This model employs limited interfaces or programs to control and maintain object integrity.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What regulation formalizes the prudent man rule that requires senior executives to take personal responsibility for their actions?
## ID
tb475934.CISSPSG8E.be1.045
## A
CFAA
## B
Federal Sentencing Guidelines
## C
GLBA
## D
Sarbanes–Oxley
##  Answer
B
## Explanation
The Federal Sentencing Guidelines released in 1991 formalized the prudent man rule, which requires senior executives to take personal responsibility for ensuring the due care that ordinary, prudent individuals would exercise in the same situation.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What occurs when the relationship between the plain text and the key is complicated enough that an attacker can’t merely continue altering the plain text and analyzing the resulting cipher text to determine the key? (Choose all that apply.)
## ID
tb475934.CISSPSG8E.be3.106
## A
Confusion
## B
Transposition
## C
Polymorphism
## D
Diffusion
##  Answer
A,D
## Explanation
Confusion and diffusion are two principles underlying most cryptosystems.

Transposition is a way to acheive diffussion.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# On manual review systems, failure recognition is whose primary responsibility?
## ID
tb475934.CISSPSG8E.be4.061
## A
Tester or test-taker
## B
Client or representative
## C
Outsider or administrator
## D
Observer or auditor
##  Answer
D
## Explanation
The observer or auditor of a manual review system is directly responsible for recognizing the failure of that system.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What protocol manages the security associations used by IPsec?
## ID
tb475934.CISSPSG8E.be2.090
## A
ISAKMP
## B
SKIP
## C
IPComp
## D
SSL
##  Answer
A
## Explanation
The Internet Security Association and Key Management Protocol (ISAKMP) provides background security support services for IPsec, including managing security associations.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# The ???? model focuses on preventing interference in support of integrity. This model is based on the idea of defining a set of system states, initial states, and state transitions. Through the use of and limitations to only these predetermined secure states, integrity is maintained, and interference is prohibited.
## ID
tb475934.CISSPSG8E.be5.126
## A
Biba
## B
Take grant
## C
Goguen−Meseguer
## D
Sutherland
##  Answer
D
## Explanation
The Sutherland model focuses on preventing interference in support of integrity. This model is based on the idea of defining a set of system states, initial states, and state transitions. Through the use of and limitations to only these predetermined secure states, integrity is maintained, and interference is prohibited.

Biba relates to integrity and is non-discretionary lattice based model which applies simple star rule. No write down.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following models uses labels to define classifications levels of objects?
## ID
tb475934.CISSPSG8E.be4.120
## A
Mandatory access control model
## B
Discretionary access control model
## C
Role-based access control model
## D
Rule-based access control model
##  Answer
A
## Explanation
A mandatory access control model uses labels to classify objects such as data. Subjects with matching labels can access these objects. None of the other models uses labels.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which VPN protocol can be implemented as the payload encryption mechanism when L2TP is used to craft the VPN connection for an IP communication?
## ID
tb475934.CISSPSG8E.be2.017
## A
L2F
## B
PPTP
## C
IPsec
## D
SSH
##  Answer
C
## Explanation
IPsec is a VPN protocol that can be implemented as the payload encryption mechanism when L2TP is used to craft the VPN connection for an IP communication.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What cryptographic goal does the challenge-response protocol support?
## ID
tb475934.CISSPSG8E.be2.080
## A
Confidentiality
## B
Integrity
## C
Authentication
## D
Nonrepudiation
##  Answer
C
## Explanation
The challenge-response protocol is an authentication protocol that uses cryptographic techniques to allow parties to assure each other of their identity.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following definitions best explains the purpose of an intrusion detection system?
## ID
tb475934.CISSPSG8E.be4.011
## A
A product that inspects incoming and outgoing traffic across a network boundary to deny transit to unwanted, unauthorized, or suspect packets
## B
A device that provides secure termination or aggregation for IP phones, VoIP handsets, and softphones
## C
A device that, using complex content categorization criteria and content inspection, prevents potentially dangerous content from entering a network
## D
A product that automates the inspection of audit logs and real-time event information to detect intrusion attempts and possibly also system failures
##  Answer
D
## Explanation
An intrusion detection system (IDS) is a product that automates the inspection of audit logs and real-time event information to detect intrusion attempts. Option A defines a firewall, option B defines an IP telephony security gateway, and option C defines a content filtering system.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which IPsec protocol provides assurances of nonrepudiation?
## ID
tb475934.CISSPSG8E.be2.095
## A
AH
## B
ISAKMP
## C
DH
## D
ESP
##  Answer
A
## Explanation
The Authentication Header (AH) provides assurances of message integrity and nonrepudiation.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What form of infrastructure mode wireless networking deployment supports large physical environments through the use of a single SSID but numerous access points?
## ID
tb475934.CISSPSG8E.be4.048
## A
Stand-alone
## B
Wired extension
## C
Enterprise extended
## D
Bridge
##  Answer
C
## Explanation
Enterprise extended infrastructure mode exists when a wireless network is designed to support a large physical environment through the use of a single SSID but numerous access points.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following is the principle that objects are not disclosed to unauthorized subjects?
## ID
tb475934.CISSPSG8E.be3.067
## A
Integrity
## B
Nonrepudiation
## C
Sensitivity
## D
Confidentiality
##  Answer
D
## Explanation
Confidentiality is the principle that objects are not disclosed to unauthorized subjects.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What layer of the ring protection scheme includes programs running in supervisory mode?
## ID
tb475934.CISSPSG8E.be3.089
## A
Level 0
## B
Level 1
## C
Level 3
## D
Level 4
##  Answer
A
## Explanation
Supervisory mode programs are run by the security kernel, at Level 0 of the ring protection scheme.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Coordinated attack efforts against an infrastructure or system that limit or restrict its capacity to process legitimate traffic are what form of network-driven attack?
## ID
tb475934.CISSPSG8E.be5.023
## A
Denial of service
## B
Distributed denial of service
## C
Distributed reflective denial of service
## D
Differential denial of service
##  Answer
B
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# hat evidence standard do most civil investigations follow?
## ID
tb475934.CISSPSG8E.be1.058
## A
Beyond a reasonable doubt
## B
Beyond the shadow of a doubt
## C
Preponderance of the evidence
## D
Clear and convincing evidence
##  Answer
C
## Explanation
Criminal cases use 'beyond reasonable doubt'
while civil use 'more likely than not'
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Matthew recently completed writing a new song and posted it on his website. He wants to ensure that he preserves his copyright in the work. As a U.S. citizen, which of the following is the minimum that he must do to preserve his copyright in the song?
## ID
tb475934.CISSPSG8E.be6.080
## A
Register the song with the U.S. Copyright Office.
## B
Mark the song with the © symbol.
## C
Mail himself a copy of the song in a sealed envelope.
## D
Nothing.
##  Answer
D
## Explanation
Matthew is not required to do anything. Copyright protection is automatic as soon as he creates the work.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which source of interference is generated by electrical appliances, light sources, electrical cables and circuits, and so on?
## ID
tb475934.CISSPSG8E.be2.002
## A
Cross-talk noise
## B
Radio frequency interference
## C
Traverse mode noise
## D
Common mode noise
##  Answer
B
## Explanation
RFI
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which one of the following would be considered a system compromise?
## ID
tb475934.CISSPSG8E.be4.029
## A
Forged spam email appearing to come from your organization
## B
Unauthorized use of an account by the legitimate user’s relative
## C
Probing a network searching for vulnerable services
## D
Infection of a system by a virus
##  Answer
B
## Explanation
Sharing an account with a relative allows unauthorized access to a system, meeting the definition of a compromise. Forged spam email does not necessarily require access to your organization’s computing resources. Probing a network for vulnerable services is a scanning attack. Infection of a system by a virus is a malicious code attack.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What term describes the processor mode used to run the system tools used by administrators seeking to make configuration changes to a machine?
## ID
tb475934.CISSPSG8E.be1.032
## A
User mode
## B
Supervisory mode
## C
Kernel mode
## D
Privileged mode
##  Answer
A
## Explanation
All user applications, regardless of the security permissions assigned to the user, execute in user mode. Supervisory mode, kernel mode, and privileged mode are all terms that describe the mode used by the processor to execute instructions that originate from the operating system.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following is a means for IPv6 and IPv4 to be able to coexist on the same network? (Choose all that apply.)
## ID
tb475934.CISSPSG8E.be5.146
## A
Dual stack
## B
Tunneling
## C
IPsec v6
## D
NAT-PT
##  Answer
A,B,D
## Explanation
The means by which IPv6 and IPv4 can coexist on the same network is to use one or more of three primary options. Dual stack is to have most systems operate both IPv4 and IPv6 and use the appropriate protocol for each conversation. Tunneling allows most systems to operate a single stack of either IPv4 or IPv6 and use an encapsulation tunnel to access systems of the other protocol. Network Address Translation-Protocol Translation (NAT-PT) (RFC-2766) can be used to convert between IPv4 and IPv6 network segments similar to how NAT converts between internal and external addresses. There is no distinct IPsec version 6 because IPsec is native to IPv6, and IPsec does not assist or support a network operating both IPv4 and IPv6 on its own.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What is the best definition of a security model?
## ID
tb475934.CISSPSG8E.c08.12
## A
A security model states policies an organization must follow.
## B
A security model provides a framework to implement a security policy.
## C
A security model is a technical evaluation of each part of a computer system to assess its concordance with security standards.
## D
A security model is the process of formal acceptance of a certified configuration.
##  Answer
B
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which type of intrusion detection system (IDS) can be considered an expert system?
## ID
tb475934.CISSPSG8E.be6.075
## A
Host-based
## B
Network-based
## C
Knowledge-based
## D
Behavior-based
##  Answer
D
## Explanation
A behavior-based IDS can be labeled an expert system or a pseudo-artificial intelligence system because it can learn and make assumptions about events. In other words, the IDS can act like a human expert by evaluating current events against known events. A knowledge-based IDS uses a database of known attack methods to detect attacks. Both host-based and network-based systems can be either knowledge-based, behavior-based, or a combination of both.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following is the most important and distinctive concept in relation to layered security?
## ID
tb475934.CISSPSG8E.c01.12
## A
Multiple
## B
Series
## C
Parallel
## D
Filter
##  Answer
B
## Explanation
Layering is the deployment of multiple security mechanisms in a series. When security restrictions are performed in a series, they are performed one after the other in a linear fashion. Therefore, a single failure of a security control does not render the entire solution ineffective.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What phase of the Electronic Discovery Reference Model puts evidence in a format that may be shared with others?
## ID
tb475934.CISSPSG8E.be3.060
## A
Production
## B
Processing
## C
Review
## D
Presentation
##  Answer
A
## Explanation
Production places the information in a format that may be shared with others.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# The Jones Institute has six employees and uses a symmetric key encryption system to ensure confidentiality of communications. If each employee needs to communicate privately with every other employee, how many keys are necessary?
## ID
tb475934.CISSPSG8E.be2.078
## A
1
## B
6
## C
15
## D
30
##  Answer
C
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What is risk?
## ID
tb475934.CISSPSG8E.be2.038
## A
Any potential occurrence that can cause an undesirable or unwanted outcome
## B
The actual occurrence of an event that results in loss
## C
The likelihood that any specific threat will exploit a specific vulnerability to cause harm to an asset
## D
An instance of being exposed to asset loss due to a threat
##  Answer
C
## Explanation
Risk is the likelihood that any specific threat will exploit a specific vulnerability to cause harm to an asset.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#
## PoorQuestionAnswer
The answer given is that risk is a likelihood.   This seems to ignore that impact side of risks


# In which phase of the SW-CMM does an organization use quantitative measures to gain a detailed understanding of the development process?
## ID
tb475934.CISSPSG8E.c20.12
## A
Initial
## B
Repeatable
## C
Defined
## D
Managed
##  Answer
D
## Explanation
In the Managed phase, level 4 of the SW-CMM, the organization uses quantitative measures to gain a detailed understanding of the development process.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following describes a community cloud?
## ID
tb475934.CISSPSG8E.c09.06
## A
A cloud environment maintained, used, and paid for by a group of users or organizations for their shared benefit, such as collaboration and data exchange
## B
A cloud service within a corporate network and isolated from the internet
## C
A cloud service that is accessible to the general public typically over an internet connection
## D
A cloud service that is partially hosted within an organization for private use and that uses external services to offer resources to outsiders
##  Answer
A
## Explanation
A community cloud is a cloud environment maintained, used, and paid for by a group of users or organizations for their shared benefit, such as collaboration and data exchange. A private cloud is a cloud service within a corporate network and isolated from the internet. A public could is a cloud service that is accessible to the general public typically over an internet connection. A hybrid cloud is a cloud service that is partially hosted within an organization for private use and that uses external services to offer recourses to outsiders.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following does not reflect the protected elements under an access control methodology?
## ID
tb475934.CISSPSG8E.be1.061
## A
Integrity
## B
Scalability
## C
Accountability
## D
Confidentiality
##  Answer
B
## Explanation
Access control provides security through confidentiality, integrity, and accountability.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following is not a valid reason why log files should be protected?
## ID
tb475934.CISSPSG8E.be1.145
## A
Log files can be used to reconstruct events leading up to an incident.
## B
Attackers may try to erase their activity during or after an attack.
## C
Unprotected log files cannot be used as evidence.
## D
Log files include information on why an attack occurred.
##  Answer
D
## Explanation
Log files include information on what happened, when and where it happened, who was involved, and sometimes how, but they do not include why an attack occurred; they do not include the motivation of an attacker. Log files should be protected because they can be used to reconstruct events, attackers may try to modify them, and unprotected logs cannot be used as evidence.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What protocol is slowly replacing certificate revocation lists as a real-time method of verifying the status of a digital certificate?
## ID
tb475934.CISSPSG8E.be3.122
## A
OLAP
## B
LDAP
## C
OSCP
## D
BGP
##  Answer
C
## Explanation
The Online Certificate Status Protocol (OSCP) provides real-time query/response services to digital certificate users. This overcomes the latency inherent in the traditional certificate revocation list download and cross-check process.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What law protects the right of citizens to privacy by placing restrictions on the authority granted to government agencies to search private residences and facilities?
## ID
tb475934.CISSPSG8E.c04.06
## A
Privacy Act
## B
Fourth Amendment
## C
Second Amendment
## D
Gramm-Leach-Bliley Act
##  Answer
B
## Explanation
The Fourth Amendment to the U.S. Constitution sets the “probable cause” standard that law enforcement officers must follow when conducting searches and/or seizures of private property. It also states that those officers must obtain a warrant before gaining involuntary access to such property.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What are the well-known ports?
## ID
tb475934.CISSPSG8E.be3.087
## A
0 to 1,023
## B
80, 135, 110, 25
## C
0 to 65, 536
## D
32,000 to 65,536
##  Answer
A
## Explanation
Ports 0 to 1,023 are the well-known ports.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following security assessment and testing program components may be performed by security professionals in the IT organization?
## ID
tb475934.CISSPSG8E.be1.021
## A
Internal audit
## B
External audit
## C
Criminal investigation
## D
Assessment
##  Answer
D
## Explanation
IT staff may perform security assessments to evaluate the security of their systems and applications. Audits must be performed by internal or external auditors who are independent of the IT organization. Criminal investigations must be performed by certified law enforcement personnel.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What technique is commonly used by polymorphic viruses to escape detection?
## ID
tb475934.CISSPSG8E.be3.095
## A
Companion naming
## B
Encryption
## C
AV tampering
## D
Exploitation of administrative privileges
##  Answer
B
## Explanation
While viruses may use all of the techniques described in this question to escape detection, the use of encryption is characteristic of polymorphic viruses.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What Biba property protects a subject from accessing an object at a lower integrity level?
## ID
tb475934.CISSPSG8E.be6.035
## A
No read up
## B
No read down
## C
No write up
## D
No write down
##  Answer
B
## Explanation
The Simple Integrity Property states that a subject cannot read an object of a lower integrity level (no read down).
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# The _______________ model is based on predetermining the set or domain of objects that a subject can access. The set or domain is a list of those objects that a subject can access. This model is based on automation theory and domain separation. This means subjects are only able to perform predetermined actions against predetermined objects.
## ID
tb475934.CISSPSG8E.be2.100
## A
Clark-Wilson
## B
Goguen-Meseguer
## C
Graham-Denning
## D
Bell-LaPadula
##  Answer
B
## Explanation
The Goguen-Meseguer model is based on predetermining the set or domain of objects that a subject can access. The set or domain is a list of those objects that a subject can access. This model is based on automation theory and domain separation. This means subjects are able to perform only predetermined actions against predetermined objects.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Why do operating systems need security mechanisms?
## ID
tb475934.CISSPSG8E.be1.033
## A
Humans are perfect.
## B
Software is not trusted.
## C
Technology is always improving.
## D
Hardware is faulty.
##  Answer
B
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# In which IPsec mode is the content of an encapsulated packet encrypted but not the header?
## ID
tb475934.CISSPSG8E.be3.062
## A
Transport
## B
Tunnel
## C
Vector
## D
Transparent
##  Answer
A
## Explanation
In transport mode, the IP packet data is encrypted, but the header of the packet is not.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following combinations of terms defines the operations security triple?
## ID
tb475934.CISSPSG8E.be4.013
## A
Confidentiality, integrity, and availability (the CIA Triad)
## B
Authentication, authorization, and accounting (AAA)
## C
The relationship between assets, vulnerabilities, and threats
## D
Due care, due diligence, and operations controls
##  Answer
C
## Explanation
The primary purpose for operations security is to safeguard information assets that reside in a system day to day, to identify and safeguard any vulnerabilities that might be present in that system, and to prevent any exploitation of threats. Administrators often call the relationship between assets, vulnerabilities, and threats an operations security triple. CIA relates to a model used to develop security policy, and AAA defines a framework for managing access to computer resources, enforcing policy, auditing usage, and providing usage data for charge backs or billing. Due care, due diligence, and operations controls are part of operations security but not in the same sense as in the relationship between assets, vulnerabilities, and threats.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What type of virus modifies operating system routines to trick antivirus software into thinking everything is functioning normally?
## ID
tb475934.CISSPSG8E.be3.096
## A
Multipart viruses
## B
Stealth viruses
## C
Encrypted viruses
## D
Polymorphic viruses
##  Answer
B
## Explanation
Stealth viruses alter operating system file access routines so that when an antivirus package scans the system, it is provided with the information it would see on a clean system rather than with infected versions of data.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Your organization has recently implemented an SDN to separate the control plane from the data plane. Which of the following models will the SDN most likely use?
## ID
tb475934.CISSPSG8E.be4.125
## A
ABAC
## B
DAC
## C
MAC
## D
RBAC
##  Answer
A
## Explanation
A software-defined network (SDN) typically uses an attribute-based access control (ABAC) model. SDNs don’t normally use the discretionary access control (DAC), mandatory access control, or role-based access control (RBAC) models.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following is most directly associated with providing or supporting perfect forward secrecy?
## ID
tb475934.CISSPSG8E.be6.150
## A
PBKDF2
## B
ECDHE
## C
HMAC
## D
OCSP
##  Answer
B
## Explanation
Elliptic Curve Diffie-Hellman Ephemeral, or Elliptic Curve Ephemeral Diffie-Hellman (ECDHE), implements perfect forward secrecy through the use of elliptic curve cryptography (ECC). PBKDF2 is an example of a key-stretching technology not directly supporting perfect forward secrecy. HMAC is a hashing function. OCSP is used to check for certificate revocation.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which wireless security protocol makes use of AES cryptography?
## ID
tb475934.CISSPSG8E.be2.096
## A
WPS
## B
WEP
## C
WPA
## D
WPA2
##  Answer
WPA2 replaces RC4 and TKIP (used by the original WPA) with AES and CCMP cryptography.
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which network topology offers multiple routes to each node to protect from multiple segment failures?
## ID
tb475934.CISSPSG8E.be1.098
## A
Ring
## B
Star
## C
Bus
## D
Mesh
##  Answer
D
## Explanation
Mesh topologies provide redundant connections to systems, allowing multiple segment failures without seriously affecting connectivity.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What technology can be used to minimize the impact of a server failure immediately before the next backup was scheduled?
## ID
tb475934.CISSPSG8E.be4.081
## A
Clustering
## B
Differential backups
## C
Remote journaling
## D
Tape rotation
##  Answer
A
## Explanation
Clustering servers adds a degree of fault tolerance, protecting against the impact of a single server failure.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#



# A host organization that houses on-site security staff has what form of security system?
## ID
tb475934.CISSPSG8E.be6.088
## A
Auxiliary system
## B
Centralized system
## C
Localized system
## D
Proprietary system
##  Answer
D
## Explanation
This describes a proprietary system, which is almost the same concept as a central station, but with the host organization has its own onsite security staff waiting to respond to security breaches. This is not an auxiliary system as there is no mention of contacting official emergency services, like police or fire/rescue. This is not a centralized system, as that is not a type of security system. A central station is a security system with offsite monitoring by a professional security services company. The term centralized is often used in contrast to decentralized in describing how something is managed, such as access control or authentication. This is not a localized system as there is no mention of an audible alarm.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following is the type of antivirus response function that removes the malicious code but leaves damage unrepaired?
## ID
tb475934.CISSPSG8E.be2.068
## A
Cleaning
## B
Removal
## C
Stealth
## D
Polymorphism
##  Answer
B
## Explanation
Removal removes the malicious code but does not repair the damage caused by it. Cleaning not only removes the code, but it also repairs any damage the code has caused.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What type of attack allows the transmission of sensitive data between classification levels through the direct or indirect manipulation of a shared storage media?
## ID
tb475934.CISSPSG8E.be3.099
## A
Trojan horse
## B
Direct access to media
## C
Permission creep
## D
Covert channel
##  Answer
D
## Explanation
Covert channel attacks involve the illicit transfer of data by manipulating storage or timing channels.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Identification is the first step toward what ultimate goal?
## ID
tb475934.CISSPSG8E.be1.121
## A
Accountability
## B
Authorization
## C
Auditing
## D
Nonrepudiation
##  Answer
A
## Explanation
Accountability is the ultimate goal of a process started by identification.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What common vulnerability has no direct countermeasure and little safeguards or validators?
## ID
tb475934.CISSPSG8E.be4.067
## A
Theft
## B
Fraud
## C
Omission
## D
Collusion
##  Answer
C
## Explanation
Both omissions and errors are difficult aspects to protect against, particularly as they deal with human and circumstantial origins.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#



# Mark recently completed work on a piece of computer software that he thinks will be especially valuable. He wants to protect the source code against others rewriting it in a different form. What is the best form of intellectual property protection he could seek in this case?
## ID
tb475934.CISSPSG8E.be6.082
## A
Trademark
## B
Trade secret
## C
Copyright
## D
Patent
##  Answer
B
## Explanation
Mark’s best option is to treat the software source code as a trade secret. Copyright protection requires that he disclose the source in a public filing. Patent protection does not cover the actual program.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which process in the evaluation of a security system represents the formal acceptance of a certified configuration?
## ID
tb475934.CISSPSG8E.be2.079
## A
Certification
## B
Evaluation
## C
Accreditation
## D
Formal analysis
##  Answer
C
## Explanation
Accreditation is the formal declaration by the Designated Approving Authority (DAA) that an IT system is approved to operate in a particular security mode using a prescribed set of safeguards at an acceptable level of risk.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# In a(n) ___________ system, all protection mechanisms work together to process sensitive data for many types of users while maintaining a stable and secure computing environment.
## ID
tb475934.CISSPSG8E.be6.043
## A
Trusted
## B
Authorized
## C
Available
## D
Baseline
##  Answer
A
## Explanation
In a trusted system, all protection mechanisms work together to process sensitive data for many types of users while maintaining a stable and secure computing environment.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# The ????? model is based on the idea of defining a set of system states, initial states, and state transitions. Through the use of and limitations to only these predetermined secure states, integrity is maintained, and interference is prohibited.
## ID
tb475934.CISSPSG8E.be5.070
## A
Biba
## B
Sutherland
## C
Clark-Wilson
## D
Gramm-Leach-Bliley
##  Answer
B
## Explanation
The Sutherland model is based on the idea of defining a set of system states, initial states, and state transitions. Through the use of and limitations to only these predetermined secure states, integrity is maintained, and interference is prohibited.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which one of the following backup types does not alter the archive bit on backed-up files?
## ID
tb475934.CISSPSG8E.be6.101
## A
Full backup
## B
Remote journaling
## C
Incremental backup
## D
Differential backup
##  Answer
D
## Explanation
Differential backups store all files that have been modified since the time of the most recent full backup. They do not alter the archive bit.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which form of physical security control focuses on facility construction and selection, site management, personnel controls, awareness training, and emergency response and procedures?
## ID
tb475934.CISSPSG8E.be1.150
## A
Technical
## B
Physical
## C
Administrative
## D
Logical
##  Answer
C
## Explanation
Administrative physical security controls include facility construction and selection, site management, personnel controls, awareness training, and emergency response and procedures.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What is the primary function of a gateway as a network device?
## ID
tb475934.CISSPSG8E.be4.128
## A
Routing traffic
## B
Protocol translator
## C
Attenuation protection
## D
Creating virtual LANs
##  Answer
B
## Explanation
The gateway is a network device (or service) that works at the Application layer. However, an Application layer gateway is a very specific type of component. It serves as a protocol translation tool. For example, an IP-to-IPX gateway takes inbound communications from TCP/IP and translates them over to IPX/SPX for outbound transmission.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# If a specific step-by-step guide does not exist that prescribes how to accomplish a necessary task, which of the following is used to create such a document?
## ID
tb475934.CISSPSG8E.be2.126
## A
Policy
## B
Standard
## C
Procedure
## D
Guideline
##  Answer
D
## Explanation
A guideline offers recommendations on how standards and baselines are implemented and serves as an operational guide for both security professionals and users. Guidelines are flexible so they can be customized for each unique system or condition and can be used in the creation of new procedures (i.e., step-by-step guides).
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which one of the following items is not a critical piece of information in the chain of evidence?
## ID
tb475934.CISSPSG8E.be1.108
## A
General description of the evidence
## B
Name of the person collecting the evidence
## C
Relationship of the evidence to the crime
## D
Time and date the evidence was collected
##  Answer
C
## Explanation
The chain of evidence does not require that the evidence collector know or document the relationship of the evidence to the crime.
## Review
Yes
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# A tunnel mode VPN is used to connect which types of systems?
## ID
tb475934.CISSPSG8E.be5.011
## A
Hosts and servers
## B
Clients and terminals
## C
Hosts and networks
## D
Servers and domain controllers
##  Answer
C
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# The following eight primary protection rules or actions define the boundaries of what security model?
Securely create an object.
Securely create a subject.
Securely delete an object.
Securely delete a subject.
Securely provide the read access right.
Securely provide the grant access right.
Securely provide the delete access right.
Securely provide the transfer access right.
## ID
tb475934.CISSPSG8E.be2.120
## A
Graham-Denning
## B
Bell-LaPadula
## C
Take-Grant
## D
Sutherland
##  Answer
A
## Explanation
The Graham-Denning model is focused on the secure creation and deletion of both subjects and objects. Ultimately, Graham-Denning is a collection of eight primary protection rules or actions (listed in the question) that define the boundaries of certain secure actions.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which component of the CIA Triad has the most avenues or vectors of attack and compromise?
## ID
tb475934.CISSPSG8E.be2.028
## A
Confidentiality
## B
Integrity
## C
Availability
## D
Accountability
##  Answer
C
## Explanation
Availability has the most avenues or vectors of attack and compromise. Availability can be affected by damaging the resource, compromising the resource host, interfering with communications, or attacking the client.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# On a much smaller scale, _______________ is deployed to repair or restore capability, functionality, or resources following a violation of security policy.
## ID
tb475934.CISSPSG8E.be6.140
## A
Recovery access control
## B
Corrective access control
## C
Detective access control
## D
Compensation access control
##  Answer
A
## Explanation
Recovery access control is deployed to repair or restore resources, functions, and capabilities after a violation of security policies.
## Review
Yes
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Why should an enterprise network implement endpoint security?
## ID
tb475934.CISSPSG8E.be5.040
## A
To provide sufficient security on each individual host.
## B
Centralized security mechanisms are too expensive.
## C
Network security safeguards do not provide any protection for hosts.
## D
Hardware security options are ineffective against software exploits.
##  Answer
A
## Explanation
Endpoint security is implemented in order to provide sufficient security on each individual host, rather than relying on network-based security only.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# During threat modeling, several options exist for ranking or rating the severity and priority of threats. Which of the following not a threat modeling ranking system?
## ID
tb475934.CISSPSG8E.be3.036
## A
DREAD
## B
Probability * Damage Potential
## C
Qualitative analysis
## D
High/medium/low
##  Answer
C
## Explanation
Qualitative analysis is part of risk management/risk assessment, but it is not specifically a means of ranking or rating the severity and priority of threats under threat modelling. The three common means of ranking or rating the severity and priority of threats are DREAD, Probability * Damage Potential, and High/medium/low.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What government agency provides daily updates on wildfires in the United States?
## ID
tb475934.CISSPSG8E.be5.099
## A
FEMA
## B
NIFC
## C
USGS
## D
USFWS
##  Answer
B
## Explanation
The National Interagency Fire Center provides daily updates on wildfires occurring in the United States.
FEMA provides information on floods
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following encryption packages provides full disk encryption and is built into Microsoft Windows?
## ID
tb475934.CISSPSG8E.be6.031
## A
TrueCrypt
## B
PGP
## C
EFS
## D
BitLocker
##  Answer
D
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What network devices operate within the Physical layer?
## ID
tb475934.CISSPSG8E.be1.092
## A
Bridges and switches
## B
Firewalls
## C
Hubs and repeaters
## D
Routers
##  Answer
C
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What process state can be dependent on peripherals?
## ID
tb475934.CISSPSG8E.be5.130
## A
Ready
## B
Waiting
## C
Running
## D
Supervisory
##  Answer
B
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What type of disaster recovery test is the simplest to perform?
## ID
tb475934.CISSPSG8E.be4.114
## A
Structured walk-through
## B
Read-through
## C
Simulation
## D
Parallel
##  Answer
B
## Explanation
In the read-through test, you distribute copies of the disaster recovery plan to key personnel for review but do not actually meet or perform live testing.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Among the following concepts, which element is not essential for an audit report?
## ID
tb475934.CISSPSG8E.be5.052
## A
Audit purpose
## B
Audit scope
## C
Audit results
## D
Audit overview
##  Answer
D
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following is the least acceptable form of biometric device?
## ID
tb475934.CISSPSG8E.be6.003
## A
Iris scan
## B
Retina scan
## C
Fingerprint
## D
Facial geometry
##  Answer
B
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following is a documented set of best IT security practices crafted by Information Systems Audit and Control Association (ISACA) and IT Governance Institute (ITGI)?
## ID
tb475934.CISSPSG8E.be2.033
## A
ISO 17799
## B
COBIT
## C
OSSTMM
## D
Common Criteria (IS 15408)
##  Answer
B
## Explanation
Control Objectives for Information and Related Technology (COBIT) is a documented set of best IT security practices crafted by Information Systems Audit and Control Association (ISACA) and IT Governance Institute (ITGI).
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What testing exercise would you perform that involves personnel relocation and remote site activation?
## ID
tb475934.CISSPSG8E.be5.068  
## A
Parallel test
## B
Full-interruption test
## C
Structured walk-through
## D
Simulation test
##  Answer
A
## Explanation
Parallel tests represent the next level in testing and involve actually relocating personnel to the alternate recovery site and implementing site activation procedures.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#



# The act of ___________ is the practice of assessing the completeness and effectiveness of the security program.
## ID
tb475934.CISSPSG8E.be5.141
## A
Measuring and evaluating security metrics
## B
Monitoring performance
## C
Vulnerability analysis
## D
Crafting a baseline
##  Answer
A
## Explanation
The act of measuring and evaluating security metrics is the practice of assessing the completeness and effectiveness of the security program. This should also include measuring it against common security guidelines and tracking the success of its controls.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# How is metadata generated?
## ID
tb475934.CISSPSG8E.be2.121
## A
By creating a data warehouse
## B
By performing data mining
## C
By hosting a data mart
## D
By authoring a data dictionary
##  Answer
B
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What form of attack is always possible when using a non-802.1x implementation of a wireless network?
## ID
tb475934.CISSPSG8E.be3.129
## A
Password guessing
## B
Encryption cracking
## C
IV interception
## D
Packet replay attacks
##  Answer
A
## Explanation
Password guessing is always a potential attack if a wireless network is not otherwise using some other form of authentication, typically accessed via 802.1x.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What is the best type of water-based fire suppression system for a computer facility?
## ID
tb475934.CISSPSG8E.c10.19
## A
Wet pipe system
## B
Dry pipe system
## C
Preaction system
## D
Deluge system
##  Answer
C
## Explanation
A preaction system is a combination dry pipe/wet pipe system. The system exists as a dry pipe until the initial stages of a fire (smoke, heat, and so on) are detected, and then the pipes are filled with water. The water is released only after the sprinkler head activation triggers are melted by sufficient heat.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Your organization was recently attacked by an APT. A root-cause analysis indicated that the attack was successful because an employee responded inappropriately to a malicious spam email. What phase of incident response does a root-cause analysis occur?
## ID
tb475934.CISSPSG8E.be6.084
## A
Detection
## B
Response
## C
Mitigation
## D
Remediation
##  Answer
D
## Explanation
A root-cause analysis typically occurs during the remediation stage. Incidents are identified and verified in the detection stage. During incident response, personnel assess the damage and collect evidence. Personnel isolate compromised systems during the mitigation stage.
## Review
Yes
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# The ????? of a process consist of limits set on the memory addresses and resources it can access. This also states or defines the area within which a process is confined.
## ID
tb475934.CISSPSG8E.be2.105
## A
Isolation
## B
Bounds
## C
Confinement
## D
Authentication
##  Answer
B
## Explanation
The bounds of a process consist of limits set on the memory addresses and resources it can access. The bounds state or define the area within which a process is confined
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Methodical examination and review of environmental security and regulatory compliance, and a form of directive control, is known as what?
## ID
tb475934.CISSPSG8E.be5.035
## A
Penetration testing
## B
Compliance checking
## C
Auditing
## D
Ethical hacking
##  Answer
C
## Explanation
Auditing is the periodic examination and review of a network to ensure that it meets security and regulatory compliance.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# When NAC is used to manage an enterprise network, what is most likely to happen to a notebook system once reconnected to the intranet after it has been out of the office for six weeks while in use by an executive on an international business trip?
## ID
tb475934.CISSPSG8E.be3.025
## A
Reimaged
## B
Updated at next refresh cycle
## C
Quarantine
## D
User must reset their password
##  Answer
C
## Explanation
NAC often operates in a pre-admission philosophy in which a system must meet all current security requirements (such as patch application and antivirus updates) before it is allowed to communicate with the network. This often means systems that are not in compliance are quarantined or otherwise involved in a captive portal strategy in order to force compliance before network access is restored.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What is the most common reaction to the loss of physical and infrastructure support?
## ID
tb475934.CISSPSG8E.be5.117
## A
Deploying OS updates
## B
Vulnerability scanning
## C
Waiting for the event to expire
## D
Tightening of access controls
##  Answer
C
## Explanation
In most cases, you must simply wait until the emergency or condition expires and things return to normal. If physical and infrastructure support is lost, such as after a catastrophe, regular activity (including deploying updates, performing scans, or tightening controls) is not possible.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What form of interference is generated by a power differential between hot and ground wires?
## ID
tb475934.CISSPSG8E.be6.014
## A
Common mode noise
## B
Traverse mode noise
## C
Cross-talk noise
## D
Radio frequency interference
##  Answer
A
## Explanation
Common mode noise is generated by the difference in power between the hot and ground wires of a power source or operating electrical equipment.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Who is responsible for the day-to-day maintenance of objects?
## ID
tb475934.CISSPSG8E.be4.124
## A
The user
## B
The owner
## C
The custodian
## D
The administrator
##  Answer
C
## Explanation
A custodian is someone who has been assigned to or delegated the day-to-day responsibility of proper storage and protection of objects. A user is any subject who accesses objects on a system to perform some action or accomplish a work task. An owner is the person who has final corporate responsibility for the protection and storage of data. When discussing access to objects, three subject labels are used: user, owner, and custodian. Therefore, administrator is not an appropriate choice.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# The Goguen-Meseguer model is an ________ model based on predetermining the set or domain—a list of objects that a subject can access.
## ID
tb475934.CISSPSG8E.be5.111
## A
Integrity
## B
Confidentiality
## C
Non-interference
## D
Availability
##  Answer
A
## Explanation
The Goguen-Meseguer model is an integrity model based on predetermining the set or domain—a list of objects that a subject can access.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What programming environment offered by Microsoft includes the Common Language Interface?
## ID
tb475934.CISSPSG8E.be2.060
## A
COM
## B
DCOM
## C
.NET Framework
## D
ActiveX
##  Answer
C
## Explanation
The .NET Framework includes the Common Language Interface to support multiple programming languages.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# ???? is a form of programming attack that is used to either falsify information being sent to a visitor or cause their system to give up information without authorization.
## ID
tb475934.CISSPSG8E.be2.108
## A
SQL injection
## B
Buffer overflow
## C
DDoS
## D
XML exploitation
##  Answer
D
## Explanation
XML exploitation is a form of programming attack that is used to either falsify information being sent to a visitor or cause their system to give up information without authorization. 
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Third-party governance cannot be mandated for whom?
## ID
tb475934.CISSPSG8E.be4.147
## A
Internal entities
## B
External consultants and suppliers
## C
Subsidiaries
## D
Commercial competitors
##  Answer
D
## Explanation
Commercial competitors or any other entity that is not directly connected or related to the primary organization cannot have that organization’s third-party governance mandated or forced on them.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What type of information is not normally included in the risk acceptance/mitigation portion of BCP (business continuity plan) documentation?
## ID
tb475934.CISSPSG8E.be3.128
## A
Reasons for accepting risks
## B
Potential future events that might warrant reconsideration of the decision
## C
Identification of insurance policies that apply to a given risk
## D
Risk mitigation provisions and processes
##  Answer
C
## Explanation
Insurance policies are an example of risk assignment/transference and would not be described in the risk acceptance/mitigation section of the documentation.
## Review
Yes
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following is not a goal of the change control process of configuration management?
## ID
tb475934.CISSPSG8E.be5.093
## A
Implement changes in a monitored and orderly manner.
## B
Changes are cost effective.
## C
A formalized testing process is included to verify that a change produces expected results.
## D
All changes can be reversed.
##  Answer
B
## Explanation
While most business decisions need to include cost analysis, the change control process of configuration management is not directly concerned with cost effectiveness.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What security services are provided by Kerberos for authentication traffic?
## ID
tb475934.CISSPSG8E.be5.014
## A
Availability and nonrepudiation
## B
Confidentiality and nonrepudiation
## C
Confidentiality and integrity
## D
Availability and authorization
##  Answer
C
## Explanation
Kerberos provides confidentiality and integrity protection security services for authentication traffic using symmetric cryptography to encrypt tickets sent over the network to prove identification and provide authentication. The security services provide by Kerberos are not directly related to availability or nonrepudiation.
## Review
Yes
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# A team that knows substantial information about its target, including on-site hardware/software inventory and configuration details, is best described as what?
## ID
tb475934.CISSPSG8E.be6.055
## A
Zero knowledge
## B
Infinite knowledge
## C
Absolute knowledge
## D
Partial knowledge
##  Answer
D
## Explanation
Partial-knowledge teams possess a detailed account of organizational assets, including hardware and software inventory, prior to a penetration test.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What is the foundation of user and personnel security?
## ID
tb475934.CISSPSG8E.be2.047
## A
Background checks
## B
Job descriptions
## C
Auditing and monitoring
## D
Discretionary access control
##  Answer
B
## Explanation
Job descriptions are essential to user and personnel security. Only when it’s based on a job description does a background check have true meaning. Without a job description, auditing and monitoring cannot determine when a user performs tasks outside of their assigned work. Without a job description, administrators do not know what level of access to assign via DAC.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What type of intellectual property protection is best suited for computer software?
## ID
tb475934.CISSPSG8E.be6.121
## A
Copyright
## B
Trademark
## C
Patent
## D
Trade secret
##  Answer
D
## Explanation
Trade secrets are one of the best legal protections for computer software.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Of the following choices, what best describes the purpose of a honeypot or a honeynet?
## ID
tb475934.CISSPSG8E.be4.012
## A
To keep attackers away from real systems or networks they might otherwise attack
## B
To provide a lure for attackers by advertising the presence of a weak or insecure system or network
## C
To provide an isolated network for testing software
## D
To lure attackers into a bogus system or network environment and present sufficient material of apparent worth or interest to keep the attacker around long enough to track them down
##  Answer

## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# RSA encryption relies on the use of two ????.
## ID
tb475934.CISSPSG8E.be3.104
## A
One-way functions
## B
Prime numbers
## C
Hash functions
## D
Elliptic curves
##  Answer
B
## Explanation
The strength of RSA encryption relies on the difficulty of factoring the two large prime numbers used to generate the encryption key.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following passwords uses a challenge-response mechanism to create a one-time password?
## ID
tb475934.CISSPSG8E.be3.027
## A
Synchronous one-time passwords
## B
Asynchronous one-time passwords
## C
Strong static passwords
## D
Passphrases
##  Answer
B
## Explanation
An asynchronous token generates and displays one-time passwords using a challenge-response process to generate the password. A synchronous token is synchronized with an authentication server and generates synchronous one-time passwords. Static passwords are not one-time passwords but instead stay the same for a period of time. A passphrase is a static password created from an easy-to-remember phrase.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What security model is based on dynamic changes of user privileges and access based on user activity?
## ID
tb475934.CISSPSG8E.be2.104
## A
Sutherland
## B
Brewer–Nash
## C
Biba
## D
Graham–Denning
##  Answer
B
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What form of security planning is designed to focus on timeframes of approximately one year and may include scheduling of tasks, assignment of responsibilities, hiring plans, maintenance plans, and even acquisition plans?
## ID
tb475934.CISSPSG8E.be5.055
## A
Strategic
## B
Operational
## C
Administrative
## D
Tactical
##  Answer
D
## Explanation
Tactical planning is designed to focus on timeframes of approximately one year and may include scheduling of tasks, assignment of responsibilities, hiring plans, maintenance plans, and even acquisition plans
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What method is not integral to assuring effective and reliable security staffing?
## ID
tb475934.CISSPSG8E.be2.061
## A
Screening
## B
Bonding
## C
Training
## D
Conditioning
##  Answer
D
## Explanation
Screening, bonding, and training are all vital procedures for ensuring effective and reliable security staffing because they verify the integrity and validate the suitability of said staffers.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What part of the Common Criteria specifies the claims of security from the vendor that are built into a target of evaluation?
## ID
tb475934.CISSPSG8E.be2.119
## A
Protection profiles
## B
Evaluation assurance level
## C
Certificate authority
## D
Security target
##  Answer
D
## Explanation
Security targets (STs) specify the claims of security from the vendor that are built into a TOE.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#



# Who is responsible for authoring the principle that can be summed up as “the enemy knows the system”?
## ID
tb475934.CISSPSG8E.be6.108
## A
Rivest
## B
Schneier
## C
Kerckchoffs
## D
Shamir
##  Answer
C
## Explanation
Kerckchoffs’s principle states that a cryptographic system should remain secure even when all details of the system, except the key, are public knowledge.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# ????? is any hardware, software, or administrative policy or procedure that defines and enforces access and restriction rights on an organizational level.
## ID
tb475934.CISSPSG8E.be1.060
## A
Logical control
## B
Technical control
## C
Access control
## D
Administrative control
##  Answer
C
## Explanation
Access control is any hardware, software, or organizational administrative policy or procedure that grants or restricts access, monitors and records attempts to access, identifies users attempting to access, and determines whether access is authorized.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# A ???? contains levels with various compartments that are isolated from the rest of the security domain.
## ID
tb475934.CISSPSG8E.be1.129
## A
Hybrid environment
## B
Compartmentalized environment
## C
Hierarchical environment
## D
Security environment
##  Answer
A
## Explanation

## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What type of system is authorized to process data at different classification levels only when all users have authorized access to those classification levels?
## ID
tb475934.CISSPSG8E.be6.083
## A
Compartmented mode system
## B
System-high mode system
## C
Multilevel mode system
## D
Dedicated mode system
##  Answer
B
## Explanation
Systems running in system-high mode are authorized to process data at different classification levels only if all system users have access to the highest level of classification processed.
## Review
Yes
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What regulation formalizes the prudent man rule, requiring that senior executives of an organization take personal responsibility for ensuring due care?
## ID
tb475934.CISSPSG8E.be4.088
## A
National Information Infrastructure Protection Act
## B
Federal Information Security Management Act
## C
Information Security Reform Act
## D
Federal Sentencing Guidelines
##  Answer
D
## Explanation
The Federal Sentencing Guidelines formalized the prudent man rule and applied it to information security.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following actions should never occur during the detection phase of incident response?
## ID
tb475934.CISSPSG8E.be1.011
## A
Powering down a compromised system
## B
Disconnecting a compromised system from the network
## C
Isolating a compromised system from other systems on the network
## D
Preserving the system in a running state
##  Answer
A
## Explanation
You should never power down a compromised system during the early stages of incident response because this may destroy valuable evidence stored in volatile memory. The other answers include steps that will isolate a system without destroying evidence in typical scenarios.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What law requires that communications carriers cooperate with federal agencies conducting a wiretap?
## ID
tb475934.CISSPSG8E.be6.109
## A
CFAA
## B
CALEA
## C
EPPIA
## D
ECPA
##  Answer
B
## Explanation
The Communications Assistance to Law Enforcement Act (CALEA) requires all communications carriers to make wiretaps possible for law enforcement with an appropriate court order.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which one of the following is not a basic requirement for the admissibility of evidence?
## ID
tb475934.CISSPSG8E.be4.022
## A
Timely
## B
Relevant
## C
Material
## D
Competent
##  Answer
A
## Explanation
To be admissible, evidence must be relevant, material, and competent.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# A biometric system is matching subjects to a database using a one-to-many search. What is this providing?
## ID
tb475934.CISSPSG8E.be3.029
## A
Authentication
## B
Authorization
## C
Accountability
## D
Identification
##  Answer
D
## Explanation
Biometric systems using a one-to-many search provide identification by searching a database for a match. Biometric systems using a one-to-one search provide authentication. Biometric systems do not provide authorization or accountability.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#



# Which one of the following files might be modified or created by a companion virus?
## ID
tb475934.CISSPSG8E.be4.043
## A
COMMAND.EXE
## B
CONFIG.SYS
## C
AUTOEXEC.BAT
## D
WIN32.DLL
##  Answer
A
## Explanation
Companion viruses are self-contained executable files with filenames similar to those of existing system/program files but with a modified extension. The virus file is executed when an unsuspecting user types the filename without the extension at the command prompt.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following access control techniques uses labels to identify subjects and objects?
## ID
tb475934.CISSPSG8E.be6.107
## A
Discretionary access control
## B
Nondiscretionary access control
## C
Role-based access control
## D
Mandatory access control
##  Answer
D
## Explanation
Mandatory access control uses labels to identify subjects and objects and grants access based on matching labels. Discretionary access control requires all objects to have an owner. Nondiscretionary access control provides centralized access controlled by an administrator. Role-based access control provides access based on membership within a role.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Of the following choices, what is the most appropriate action to take during the mitigation phase of incident response?
## ID
tb475934.CISSPSG8E.be5.113
## A
Isolate and contain
## B
Gather evidence
## C
Report
## D
Restore service
##  Answer
A
## Explanation
During the mitigation phase of an incident, you would take steps to isolate and contain the incident. You would gather evidence during the response phase. Personnel notify appropriate people during the reporting phase. Servers are restored during the recovery phase.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# The purchasing of insurance is a form of ????.
## ID
tb475934.CISSPSG8E.be3.066
## A
Risk mitigation
## B
Risk assignment
## C
Risk acceptance
## D
Risk rejection
##  Answer
B
## Explanation
Insurance is a form of risk assignment or transference.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What technology may database developers use to restrict users’ access to a limited subset of database attributes or records?
## ID
tb475934.CISSPSG8E.be4.041
## A
Polyinstantiation
## B
Cell suppression
## C
Aggregation
## D
Views
##  Answer
D
## Explanation
Database views use SQL statements to limit the amount of information that users can view from a table.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# All of the following are implications of multilayer protocols except which one?
## ID
tb475934.CISSPSG8E.be3.020
## A
VLAN hopping
## B
Multiple encapsulation
## C
Filter evasion using tunneling
## D
Static IP addressing
##  Answer
D
## Explanation
Static IP addressing is not an implication of multilayer protocols; it is the identification feature of the IP protocol.

VLAN hopping is because it relies upon encapsulating a packet with one VLAN with another package for another VLAN.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# QOn what principle does a SYN flood attack operate?
## ID
tb475934.CISSPSG8E.be1.138
## A
Sending overly large SYN packets
## B
Exploiting a platform flaw in Windows
## C
Using an amplification network to flood a victim with packets
## D
Exploiting the TCP/IP three-way handshake
##  Answer
D
## Explanation
SYN flood attacks are targeted at the standard three-way handshake process used by TCP/IP to initiate communication sessions.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# The term personal area network is most closely associated with what wireless technology?
## ID
tb475934.CISSPSG8E.be2.008
## A
802.15
## B
802.11
## C
802.16
## D
802.3
##  Answer
A
## Explanation
802.15 (aka Bluetooth) creates personal area networks (PANs).
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which backup facility is large enough to support current operational capacity and load, including the supportive infrastructure?
## ID
tb475934.CISSPSG8E.be4.127
## A
Mobile site
## B
Cloud provider
## C
Hot site
## D
Cold site
##  Answer
C
## Explanation

## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What is the primary purpose of most malware today?
## ID
tb475934.CISSPSG8E.be2.073
## A
Infecting word processor documents
## B
Creating botnets
## C
Destroying data
## D
Sending spam
##  Answer
B
## Explanation
Most malware is designed to add systems to botnets, where they are later used for other nefarious purposes, such as sending spam or participating in distributed denial-of-service attacks.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Practicing the activities that maintain continued application of security protocol, policy, and procedure is also called what?
## ID
tb475934.CISSPSG8E.be6.053
## A
Due notice
## B
Due diligence
## C
Due care
## D
Due date
##  Answer
B
## Explanation
Due diligence is the action taken to apply, enforce, and perform responsibilities or rules as governed by organizational policy.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Among the following choices, what kind of IDS is considered an expert system?
## ID
tb475934.CISSPSG8E.be1.079
## A
Behavior-based
## B
Network-based
## C
Knowledge-based
## D
Host-based
##  Answer
A
## Explanation
A behavior-based intrusion detection system (IDS) can be labeled an expert system or a pseudo-artificial intelligence system because it can learn and make assumptions about events. In other words, the IDS can act like a human expert by evaluating current events against known events.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# During the deencapsulation procedure, the ________ layer strips its information and sends the message up to the Network layer.
## ID
tb475934.CISSPSG8E.be4.109
## A
Transport
## B
Data Link
## C
Presentation
## D
Ethernet
##  Answer
B
## Explanation
During the deencapsulation procedure, the Data Link layer strips its information and sends the message up to the Network layer.

I think this is trying to say the the deencapsulation process starts at the data link layer. 

Its not a good question.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following network devices is used to connect networks that are using different network protocols?
## ID
tb475934.CISSPSG8E.be1.088
## A
Bridge
## B
Router
## C
Switch
## D
Gateway
##  Answer
D
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following forms of authentication provides the strongest security?
## ID
tb475934.CISSPSG8E.be6.106
## A
Password and a PIN
## B
One-time password
## C
Passphrase and a smart card
## D
Fingerprint
##  Answer
C
## Explanation
C is 2 factor while the others are not.
Among these options, passphrase and a smart card provide the strongest authentication security because they deliver two-factor authentication. A password and a PIN are both in the same factor. Despite the security offered by one-time passwords, a two-factor authenticator is stronger.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What is the maximum key length supported by the Advanced Encryption Standard’s Rijndael encryption algorithm?
## ID
tb475934.CISSPSG8E.be2.076
## A
128 bits
## B
192 bits
## C
256 bits
## D
512 bits
##  Answer
C
## Explanation
The AES/Rijndael algorithm is capable of operating with 128-, 192-, or 256-bit keys. The algorithm uses a block size equal to the length of the key.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# When possible, operations controls should be ????.
## ID
tb475934.CISSPSG8E.be6.066
## A
Simple
## B
Administrative
## C
Preventive
## D
Transparent
##  Answer
D
## Explanation
When possible, operations controls should be invisible, or transparent, to users. This keeps users from feeling hampered by security and reduces their knowledge of the overall security scheme, thus further restricting the likelihood that users will violate system security deliberately.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# In what security mode must each user have access approval and valid need to know for all information processed by the system?
## ID
tb475934.CISSPSG8E.be5.128
## A
Dedicated mode
## B
System high mode
## C
Compartmented mode
## D
Multilevel mode
##  Answer
A
## Explanation
The scenario presented in the question describes the three characteristics of dedicated mode.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which security mode provides the most granular control over resources and users?
## ID
tb475934.CISSPSG8E.be5.131
## A
Dedicated
## B
System high
## C
Compartmented
## D
Multilevel
##  Answer
B
## Explanation
System high mode provides the most granular control over resources and users because it enforces clearances, requires need to know, and allows the processing of only single sensitivity levels. All the other levels either do not have unique need to know between users (dedicated), allow multiple levels of data processing (compartmented), or allow a wide number of users with varying clearance (multilevel).
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following is the weakest method of authentication?
## ID
tb475934.CISSPSG8E.be1.027
## A
Synchronous one-time passwords
## B
Asynchronous one-time passwords
## C
Strong static passwords
## D
Retina scans
##  Answer
B
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which one of the following is not a core principle of the Agile Manifesto?
## ID
tb475934.CISSPSG8E.be3.007
## A
Simplicity is essential.
## B
Build projects around all team members equally.
## C
Working software is the primary measure of progress.
## D
The best designs emerge from self-organizing teams.
##  Answer
B
## Explanation
The Agile Manifesto says that you should build projects around motivated individuals and give them the support they need.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Of the following choices, what is a primary benefit when images are used to deploy new systems?
## ID
tb475934.CISSPSG8E.be3.148
## A
Provides baseline for configuration management
## B
Improves patch management response times
## C
Reduces vulnerabilities from unpatched systems
## D
Provides documentation for changes
##  Answer
A
## Explanation
When images are used to deploy systems, the systems start with a common baseline, which is important for configuration management. Images don’t necessarily improve the evaluation, approval, deployment, and audits of patches to systems within the network. While images can include current patches to reduce their vulnerabilities, this is because the image provides a baseline. Change management provides documentation for changes.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following is not a benefit of tunneling?
## ID
tb475934.CISSPSG8E.be1.099
## A
Tunneling provides a connection method across untrusted systems.
## B
Traffic within a tunnel is isolated from inspection devices.
## C
Tunneling allows nonroutable traffic to be routed across other networks.
## D
Each encapsulated protocol includes its own error detection, error handling, acknowledgment, and session management features.
##  Answer
D
## Explanation
Tunneling is generally an inefficient means of communicating because all protocols include their own error detection, error handling, acknowledgment, and session management features, and using more than one protocol at a time just compounds the overhead required to communicate a single message.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following comes first?
## ID
tb475934.CISSPSG8E.be4.003
## A
Accreditation
## B
Assurance
## C
Trust
## D
Certification
##  Answer
C
## Explanation
Trust comes first. Trust is built into a system by crafting the components of security. Then assurance (in other words, reliability) is evaluated using certification and/or accreditation processes.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What form of interference is generated by a difference in power between hot and neutral wires of a power source?
## ID
tb475934.CISSPSG8E.be2.059
## A
Radio frequency interference
## B
Cross-talk noise
## C
Traverse mode noise
## D
Common mode noise
##  Answer
C
## Explanation
Traverse mode noise is generated by the difference in power between the hot and neutral wires of a power source or operating electrical equipment.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# The act of performing a(n) ???  in order to drive the security policy is the clearest and most direct example of management of the security function.
## ID
tb475934.CISSPSG8E.be5.137
## A
Ethical hacking exercise
## B
Full interruption test
## C
Risk assessment
## D
Public review of policy
##  Answer
C
## Explanation
The act of performing a risk assessment in order to drive the security policy is the clearest and most direct example of management of the security function.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which type of control provides extended options to existing controls and aids or supports administrative security policy?
## ID
tb475934.CISSPSG8E.be1.072
## A
Recovery access control
## B
Corrective access control
## C
Restorative access control
## D
Compensation access control
##  Answer
D
## Explanation
Compensation access control is deployed to provide various options to existing controls to help enforce and support a security policy.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Generally, a privacy policy is designed to protect what?
## ID
tb475934.CISSPSG8E.be5.060
## A
A user’s privacy
## B
The public’s freedom
## C
Intellectual property
## D
A company’s right to audit
##  Answer
D
## Explanation
The purpose of a privacy policy is to inform users where they do and do not have privacy for the primary benefit of the protection of the company’s right to audit and monitor user activity.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What amendment to the US Constitution protects individuals against wiretapping and invasions of privacy?
## ID
tb475934.CISSPSG8E.be4.091
## A
First
## B
Fourth
## C
Fifth
## D
Tenth
##  Answer
B
## Explanation
The Fourth Amendment, as interpreted by the courts, includes protections against wiretapping and other invasions of privacy.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which backup facility is large enough to support current operational capacity and load but lacks the supportive infrastructure?
## ID
tb475934.CISSPSG8E.be3.092
## A
Mobile site
## B
Service bureau
## C
Hot site
## D
Cold site
##  Answer
D
## Explanation
A cold site is any facility that provides only the physical space for recovery operations while the organization using the space provides its own hardware and software systems.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Alan conducted a vulnerability scan of a system and discovered that it is susceptible to a SQL injection attack. Which one of the following ports would an attacker most likely use to carry out this attack?
## ID
tb475934.CISSPSG8E.be4.057
## A
443
## B
565
## C
1433
## D
1521
##  Answer
A
## Explanation
While SQL injection attacks do target databases, they do so by using web servers as intermediaries. Therefore, SQL injection attacks take place over web ports, such as 80 and 443, and not database ports, such as 1433 and 1521.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# When conducting an internal investigation, what is the most common source of evidence?
## ID
tb475934.CISSPSG8E.be4.026
## A
Historical data
## B
Search warrant
## C
Subpoena
## D
Voluntary surrender
##  Answer
D
## Explanation
Internal investigations usually operate under the authority of senior managers, who grant access (i.e., voluntary surrender) to all information and resources necessary to conduct the investigation.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# During what phase of incident response do you collect evidence such as firewall logs?
## ID
tb475934.CISSPSG8E.be4.073
## A
Detection
## B
Response
## C
Compliance
## D
Remediation
##  Answer
B
## Explanation
Evidence collection takes place during the response phase of the incident. Incidents are identified and verified during the detection phase. Compliance with laws might occur during the reporting phase, depending on the incident. Personnel typically perform a root-cause analysis during the remediation phase.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What aspect of an organizational security policy, procedure, or process affects all other aspects?
## ID
tb475934.CISSPSG8E.be2.006
## A
Awareness training
## B
Logical security
## C
Disaster recovery
## D
Physical security
##  Answer
D
## Explanation
Physical security is the most prominent aspect of an organizational security policy because it directly and indirectly influences all other forms. Without physical security, no other processes and procedures are reliable.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which one of the following types of attack is most difficult to defend against?
## ID
tb475934.CISSPSG8E.be5.089
## A
Scanning
## B
Malicious code
## C
Grudge
## D
Distributed denial of service
##  Answer
D
## Explanation
It is difficult to defend against distributed denial-of-service attacks because of their sophistication and complexity.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# When you are configuring a wireless extension to an intranet, once you’ve configured WPA-2 with 802.1x authentication, what additional security step could you implement in order to offer additional reliable security?
## ID
tb475934.CISSPSG8E.be3.098
## A
Require a VPN
## B
Disable SSID broadcast.
## C
Issue static IP addresses.
## D
Use MAC filtering.
##  Answer
A
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which one of the following methods is not a valid method of destroying data on a hard drive?
## ID
tb475934.CISSPSG8E.be6.127
## A
Purging the drive with a software program
## B
Copying data over the existing data
## C
Removing the platters and shredding them
## D
Removing the platters and disintegrating them
##  Answer
B
## Explanation
Copying data over existing data is not reliable because data may remain on the drive as data remanence. Some software programs can overwrite the data with patterns of 1s and 0s to destroy the data. Shredding or disintegrating the platters will destroy the data.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What is the most important aspect of a biometric device?
## ID
tb475934.CISSPSG8E.be1.026
## A
Accuracy
## B
Acceptability
## C
Enrollment time
## D
Invasiveness
##  Answer
A
## Explanation
The most important aspect of a biometric factor is its accuracy. If a biometric factor is not accurate, it may allow unauthorized users into a system. Acceptability by users, the amount of time it takes to enroll, and the invasiveness of the biometric device are additional considerations but not as important as its accuracy.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What is confidentiality dependent on?
## ID
tb475934.CISSPSG8E.be1.115
## A
Integrity
## B
Availability
## C
Nonrepudiation
## D
Auditing
##  Answer
A
## Explanation
Without object integrity, confidentiality cannot be maintained. In fact, integrity and confidentiality depend on one another.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What phase of the Electronic Discovery Reference Model performs a rough cut of irrelevant information?
## ID
tb475934.CISSPSG8E.be1.059
## A
Collection
## B
Processing
## C
Review
## D
Analysis
##  Answer
B
## Explanation
Processing screens the collected information to perform a “rough cut” of irrelevant information, reducing the amount of information requiring detailed screening.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# A(n) ??? system is one in which all protection mechanisms work together to process sensitive data for many types of users while maintaining a stable and secure computing environment.
## ID
tb475934.CISSPSG8E.be3.057
## A
Assured
## B
Updated
## C
Protected
## D
Trusted
##  Answer
D
## Explanation
A trusted system is one in which all protection mechanisms work together to process sensitive data for many types of users while maintaining a stable and secure computing environment.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# How might you map out the organizational needs for transfer to or establishment in a new facility?
## ID
tb475934.CISSPSG8E.be2.003
## A
Inventory assessment
## B
Threat assessment
## C
Risk analysis
## D
Critical path analysis
##  Answer
D
## Explanation
Critical path analysis can be defined as the logical sequencing of a series of events such that planners and integrators possess considerable information for the decision-making process.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# The Clark-Wilson access model is also called a(n) ___________________ interface model.
## ID
tb475934.CISSPSG8E.be6.036
## A
Encrypted
## B
Triple
## C
Restricted
## D
Unrestricted
##  Answer
C
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What character, if eliminated from all web form input, would prevent the execution of many cross-site scripting attacks?
## ID
tb475934.CISSPSG8E.be6.098
## A
$
## B
&
## C
˂
##  Answer
<
## Explanation
This is required for the script tag
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What type of attack uses malicious email and targets a group of employees within a single company?
## ID
tb475934.CISSPSG8E.be2.116
## A
Phishing
## B
Spear phishing
## C
Whaling
## D
Vishing
##  Answer
B
## Explanation
Spear phishing targets a specific group of people such as a group of employees within a single company. Phishing goes to anyone without any specific target. Whaling is a form of phishing that targets high-level executives. Vishing is a form of phishing that commonly uses Voice over IP (VoIP).
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# A _______________ contains levels with various compartments that are isolated from the rest of the security domain.
## ID
tb475934.CISSPSG8E.be4.116
## A
Hybrid environment
## B
Compartmentalized environment
## C
Hierarchical environment
## D
Security environment
##  Answer
A
## Explanation
Hybrid environments combine both hierarchical and compartmentalized environments so that security levels have subcompartments.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What is the frequency of an IT infrastructure security audit or security review based on?
## ID
tb475934.CISSPSG8E.be3.011
## A
Asset value
## B
Administrator discretion
## C
Risk
## D
Level of realized threats
##  Answer
C
## Explanation
The frequency of an IT infrastructure security audit or security review is based on risk. You must establish the existence of sufficient risk to warrant the expense of, and interruption caused by, a security audit on a more or less frequent basis. Asset value and threats are part of risk but are not the whole picture, and assessments are not performed based only on either of these. A high-value asset with a low level of threats doesn’t present a high risk. Similarly, a low-value asset with a high level of threats doesn’t present a high risk. The decision to perform an audit isn’t usually relegated to an administrator.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# In what type of attack does the intruder initiate connections to both a client and a server?
## ID
tb475934.CISSPSG8E.be2.097
## A
Chosen plain-text attack
## B
Meet-in-the-middle attack
## C
Man-in-the-middle attack
## D
Replay attack
##  Answer
C
## Explanation
In the man-in-the-middle attack, a malicious individual sits between two communicating parties and intercepts all communications (including the setup of the cryptographic session).

Meet-in-the middle is about having the some plain text and some output ciphertext and iterating through all k1 and k2 keys to find the combination that produces what is known.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#



# Which of the following provides the best protection against mishandling media that contains sensitive information?
## ID
tb475934.CISSPSG8E.be1.047
## A
Marking
## B
Purging
## C
Sanitizing
## D
Retaining
##  Answer
A
## Explanation
Marking (or labeling) media is the best choice of the available answers to protect against mishandling media. When properly marked, personnel are more likely to handle media properly. Purging and sanitizing methods remove sensitive information but do not protect against mishandling. Data retention refers to how long an organization keeps the data, not how it handles the data.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What type of virus modifies its own code as it travels from system to system in an attempt to evade signature detection?
## ID
tb475934.CISSPSG8E.be3.004
## A
Polymorphic
## B
Encrypted
## C
Multipartite
## D
Stealth
##  Answer
A
## Explanation
Polymorphic viruses actually modify their own code as they travel from system to system. The virus’s propagation and destruction techniques remain the same, but the signature of the virus is somewhat different each time it infects a new system.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following is not true?
## ID
tb475934.CISSPSG8E.be2.039
## A
Policies, standards, baselines, guidelines, and procedures can be combined in a single document.
## B
Not all users need to know the security standards, baselines, guidelines, and procedures for all security classification levels.
## C
When changes occur, it is easier to update and redistribute only the affected material rather than update a monolithic policy and redistribute it.
## D
Higher up the formalization structure (that is, security policies), there are fewer documents because they are general broad discussions of overview and goals. Further down the formalization structure (that is, guidelines and procedures), there are many documents because they contain details specific to a limited number of systems, networks, divisions, areas, and so on.
##  Answer
A
## Explanation
Avoid combining policies, standards, baselines, guidelines, and procedures in a single document. Each of these structures must exist as a separate entity because each performs a different specialized function.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which is the most common form of perimeter security device or mechanism for any given business?
## ID
tb475934.CISSPSG8E.be1.086
## A
Security guards
## B
Fences
## C
Badges
## D
Lighting
##  Answer
D
## Explanation
Lighting is by far the most pervasive and basic element of security because it illuminates areas and makes signs of hidden danger visible to all.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What security principle states that a thorough understanding of a system’s operational details is not necessary for most routine activities?
## ID
tb475934.CISSPSG8E.be6.119
## A
Process isolation
## B
Abstraction
## C
Monitoring
## D
Hardware segmentation
##  Answer
B
## Explanation
Abstraction states that a detailed understanding of lower system levels is not a necessary requirement for working at higher levels.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# The University of Outer Mongolia runs a web application that processes student tuition payments via credit card and is subject to PCI DSS. The university does not wish to perform web vulnerability scans on a regular basis because they consider them too time-consuming. What technology may they put in place that eliminates the PCI DSS requirement for recurring web vulnerability scans?
## ID
tb475934.CISSPSG8E.be5.079
## A
Web application firewall
## B
Intrusion prevention system
## C
Network vulnerability scanner
## D
None. There is no exception to the recurring web vulnerability scan requirement.
##  Answer
A
## Explanation
PCI DSS allows organizations to choose between performing annual web vulnerability assessment tests or installing a web application firewall.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What form of mobile device management provides users with a list of approved devices from which to select the device to implement?
## ID
tb475934.CISSPSG8E.be5.005
## A
BYOD
## B
CYOD
## C
COPE
## D
OCSP
##  Answer
B
## Explanation
The concept of CYOD (choose your own device) provides users with a list of approved devices from which to select the device to implement. BYOD (bring your own device) is a policy that allows employees to bring their own personal mobile devices into work and use those devices to connect to (or through) the company network to business resources and/or the Internet. The concept of COPE (company-owned, personally enabled) is for the organization to purchase devices and provide them to employees. OCSP (Online Certificate Status Protocol) is used to check the revocation status of certificates.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What is the countermeasure cost/benefit equation?
## ID
tb475934.CISSPSG8E.be2.046
## A
SLE * ARO
## B
EF * AV * ARO
## C
(ALE1 – ALE2) – CM cost
## D
Total risk + controls gap
##  Answer
C
## Explanation
To make the determination of whether the safeguard is financially equitable, use the following countermeasure cost/benefit equation: (ALE before countermeasure – ALE after implementing the countermeasure) – annual cost of countermeasure = value of the countermeasure to the company.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What is the purpose of a security impact analysis in the context of change management?
## ID
tb475934.CISSPSG8E.be5.078
## A
To approve changes
## B
To reject changes
## C
To identify changes
## D
To review changes
##  Answer
D
## Explanation
A security impact analysis reviews change requests and evaluates them for potential negative impacts. All changes aren’t necessarily approved or rejected. The analysis doesn’t attempt to identify changes.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following describes putting similar elements into groups, classes, or roles that are assigned security controls, restrictions, or permissions as a collective?
## ID
tb475934.CISSPSG8E.be3.065
## A
Data classification
## B
Abstraction
## C
Superzapping
## D
Using covert channels
##  Answer
B
## Explanation
Abstraction describes putting similar elements into groups, classes, or roles that are assigned security controls, restrictions, or permissions as a collective.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following is not a valid issue to consider when evaluating a safeguard?
## ID
tb475934.CISSPSG8E.be3.074
## A
Cost/benefit analysis
## B
Compliance with existing baselines
## C
Legal liability and prudent due care
## D
Compatibility with IT infrastructure
##  Answer
B
## Explanation
New safeguards establish new baselines; thus, compliance with existing baselines is not a valid consideration point.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following is not a valid security measure to protect against brute-force and dictionary attacks?
## ID
tb475934.CISSPSG8E.be2.114
## A
Enforce strong passwords through a security policy.
## B
Maintain strict control over physical access.
## C
Require all users to log in remotely.
## D
Use two-factor authentication.
##  Answer
C
## Explanation
Requiring users to log in remotely does not protect against password attacks such as brute-force or dictionary attacks. Strong password policies, physical access control, and two-factor authentication all improve the protection against brute-force and dictionary password attacks.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which backup format stores only those files that have been set with the archive bit and have been modified since the last complete backup?
## ID
tb475934.CISSPSG8E.be4.004
## A
Differential backup
## B
Partial backup
## C
Incremental backup
## D
Full backup
##  Answer
A
## Explanation
Differential backups store all files that have been modified since the time of the most recent full backup; they affect only those files that have the archive bit turned on, enabled, or set to 1.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#



# An organization wants to implement a cloud-based service using a combination of two separate clouds. Which deployment model should they choose?
## ID
tb475934.CISSPSG8E.be3.018
## A
Community
## B
Hybrid
## C
Private
## D
Public
##  Answer
B
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following is not an element defined under the Clark-Wilson model?
## ID
tb475934.CISSPSG8E.be1.031
## A
Constrained data item
## B
Transformation procedures
## C
Redundant commit statement
## D
Integrity verification procedure
##  Answer
C
## Explanation
A constrained data item (CDI) is any data item whose integrity is protected by the security model. An unconstrained data item (UDI) is any data item that is not controlled by the security model. Any data that is to be input and hasn’t been validated, or any output, would be considered an unconstrained data item. An integrity verification procedure (IVP) is a procedure that scans data items and confirms their integrity. Transformation procedures (TPs) are the only procedures that are allowed to modify a CDI. The limited access to CDIs through TPs forms the backbone of the Clark-Wilson integrity model.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# A person who illicitly gains the trust or credentials from a trusted party has committed what criminal act?
## ID
tb475934.CISSPSG8E.be6.052
## A
Espionage
## B
Sabotage
## C
Reverse engineering
## D
Social engineering
##  Answer
D
## Explanation
Social engineering is an attempt to deceive an insider into performing questionable actions on behalf of some unauthorized outsider.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What procedure returns business facilities and environments to a working state?
## ID
tb475934.CISSPSG8E.be6.079
## A
Reparation
## B
Restoration
## C
Respiration
## D
Recovery
##  Answer
B
## Explanation
Disaster restoration involves restoring a business facility and environment to a workable state.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What is an access control list (ACL) based on?
## ID
tb475934.CISSPSG8E.be1.022
## A
An object
## B
A subject
## C
A role
## D
An account
##  Answer
A
## Explanation
An ACL is based on an object and includes a list of subjects that are granted access. A capability table is focused on a subject and includes a list of objects the subject can access. Roles and accounts are examples of subjects and may be included in an ACL, but they aren’t the focus.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Any process, mechanism, or tool that guides an organizational security implementation is what type of control?
## ID
tb475934.CISSPSG8E.be6.045
## A
Administrative control
## B
Corrective control
## C
Directive control
## D
Detective control
##  Answer
Directive controls guide an organizational security implementation and as such are control statements.
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which one of the following is not a part of documenting the business continuity plan?
## ID
tb475934.CISSPSG8E.be6.064
## A
Documentation of policies for continuity
## B
Historical record
## C
Job creation
## D
Identification of flaws
##  Answer
C
## Explanation
BCP documentation can be an arduous task, but it should not require the creation of a new position.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# The Twofish algorithm uses an encryption technique not found in other algorithms that XORs the plain text with a separate subkey before the first round of encryption. What is this called?
## ID
tb475934.CISSPSG8E.be1.063
## A
Preencrypting
## B
Prewhitening
## C
Precleaning
## D
Prepending
##  Answer
B
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What type of processing makes use of a multithreading technique at the operating system level?
## ID
tb475934.CISSPSG8E.be6.022
## A
Symmetric multiprocessing
## B
Multitasking
## C
Multiprogramming
## D
Massively parallel processing
##  Answer
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What attack involves an interruptive malicious user positioned between a client and server attempting to take over?
## ID
tb475934.CISSPSG8E.be1.082
## A
Man-in-the-middle
## B
Spoofing
## C
Hijacking
## D
Cracking
##  Answer
C
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Tom built a database table consisting of the names, telephone numbers, and customer IDs for his business. The table contains information on 30 customers. What is the cardinality of this table?
## ID
tb475934.CISSPSG8E.be4.036
## A
Two
## B
Three
## C
Thirty
## D
Undefined
##  Answer
C
## Explanation
The cardinality of a table refers to the number of rows in the table, whereas the degree of a table is the number of columns.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following requirements does not come from the Children’s Online Privacy Protection Act?
## ID
tb475934.CISSPSG8E.be6.105
## A
Children must be kept anonymous when participating in online forums.
## B
Parents must be provided with the opportunity to review information collected from their children.
## C
Parents must give verifiable consent to the collection of information about children.
## D
Websites must have a privacy notice.
##  Answer
A
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# What is a hardware-imposed network segmentation that requires a routing function to support intersegment communications otherwise known as?
## ID
tb475934.CISSPSG8E.be5.045  
## A
Subnet
## B
DMZ
## C
VLAN
## D
Extranet
##  Answer
C
## Explanation
A VLAN (virtual LAN) is a hardware-imposed network segmentation created by switches that requires a routing function to support communication between different segments.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# Which of the following is not considered an example of data hiding?
## ID
tb475934.CISSPSG8E.c01.13
## A
Preventing an authorized reader of an object from deleting that object
## B
Keeping a database from being accessed by unauthorized visitors
## C
Restricting a subject at a lower classification level from accessing data at a higher classification level
## D
Preventing an application from accessing hardware directly
##  Answer
A
## Explanation
Preventing an authorized reader of an object from deleting that object is just an example of access control, not data hiding. If you can read an object, it is not hidden from you.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# What process or event is typically hosted by an organization and is targeted to groups of employees with similar job functions?
## ID
tb475934.CISSPSG8E.c02.17
## A
Education
## B
Awareness
## C
Training
## D
Termination
##  Answer
C
## Explanation
Training is teaching employees to perform their work tasks and to comply with the security policy. Training is typically hosted by an organization and is targeted to groups of employees with similar job functions.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

# QWhich of the following represents accidental or intentional exploitations of vulnerabilities?
## ID
tb475934.CISSPSG8E.c02.10
## A
Threat events
## B
Risks
## C
Threat agents
## D
Breaches
##  Answer
C
## Explanation
Threat events are accidental or intentional exploitations of vulnerabilities.
Breaches are intentional bypassing of controls
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# You’ve performed a basic quantitative risk analysis on a specific threat/vulnerability/risk relation. You select a possible countermeasure. When performing the calculations again, which of the following factors will change?
## ID
tb475934.CISSPSG8E.c02.20
## A
Exposure factor
## B
Single loss expectancy
## C
Asset value
## D
Annualized rate of occurrence
##  Answer
D
## Explanation
A countermeasure directly affects the annualized rate of occurrence, primarily because the counter-measure is designed to prevent the occurrence of the risk, thus reducing its frequency per year.

What is changing here is the introduction of the countermeasure.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Which of the following attacks is the best example of a financial attack?
## ID
tb475934.CISSPSG8E.be6.137
## A
Denial of service
## B
Website defacement
## C
Port scanning
## D
Phone phreaking
##  Answer
Phone phreaking attacks are designed to obtain service while avoiding financial costs.
## Explanation
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#


# Once a system is compromised, _______________ is deployed to restore it to its previous known-good state.
## ID
tb475934.CISSPSG8E.be1.065
## A
Compensation access control
## B
Recovery access control
## C
Restorative access control
## D
Corrective access control
##  Answer
D
## Explanation
Corrective access control is deployed to restore systems to normal after an unwanted or unauthorized activity has occurred.
## Goal Implication
## Source
http://app.efficientlearning.com/pv5/v8/5/app/isc2/isc2-cisspsg8e.html?#

