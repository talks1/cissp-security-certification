

## FHRP

FHRP  - a virtual router is defined and the virtual gateway address is shared beween some number of routers. One router is active while the others are inactive until they sense the active one is offline and they take over.

These are specific protocols to realize FHRP
HSRP- Hot Standy Routing Protocol
VRRP - Virtual Redundant Routing Protocol
GLBP = Gateway Load Balancing Routing Protocol