

# You need to provide internet connectivity fo workstations, devices and linux servers. You are allocated a public network with a /28 prefix. There are 2000 employess. Which of the following will allow this.

## ID

CMS.d34f9706-00c3-4998-a860-7a661ca1dd9d

## A
NAT - Configure static translations

## B
PAT = Translate MAC,IP & TCP/UDP Ports

## C
HTTP Proxy - Forward all internet traffic to the proxy

## D
NAT - translate ip addresses dynamically from a pool

## E
PAT - Translate IP & TCP/UDP ports to one or more public addresses

## F
SOCK Proxy - Relay all device traffic through a SOCK Proxy

##  Answer
E

## Explanation

As sessions are established the network device can map the device IP address and port to one of the 14 available public IPS and a specific port.

NAT alone doesnt work because there arent enough IP addresses.
HTTP Proxy is closed but only works for IP traffic
SOCKS is possible but to uncommon to be used.

## Goal Implication




# Your admin is concerned about data being compromised if the servers private key is obtained. Which of the following is the BEST way to mitigate this issue.
## ID
CMS.19f87bbd-1169-4ee4-b9a8-c7a7c503f1db
## A
Increate key size to 2048
## B
Install a new certificate with a lifetime not longer than 90 days
## C
Enable perfect forward secrecy on the HTTPS server
## D
Enable certificate pinning
##  Answer
C
## Explanation
Perfect forward secrecy is a mechanism to use empheral private/public key pairs over https rather than using a single key pair.
## Goal Implication

# 802.2 and 802.3 correspond to which of the following standards
## ID
CMS.7facfd8b-8fd3-4f70-a83c-179e741af539
## A
802.2 is a security standard for port based access control
## B
802.3 is a wired ethernet standard for Media Access Control (MAC)
## C
802.2 is a LAN standard for token ring
## D
802.3 is a wireless standanrd for wireless LANs
## E
802.2 is an internet standard for Logical Link Control
## Fm 
802.3 is a wireless standard for bluetooth
## G
802.2 is an wireless standard for WiMax
##  Answer
B, E

## Explanation
802.3 is standard for VLAN
802.1x is port based access control
802.5 is token ring
802.15 is bluetooth
802.16 is wimax
802.1q is VLAN

## Goal Implication

# Which of these are NOT characteristics of IPV6
## ID
CMS.5612f193-37dd-4bdf-bc48-6109373e8a7c
## A
Fixed Size Header
## B
No IPV6 header checksum
## C
128 bit source address
## D
16-bit TTL field in Header
## E
Extension headers
## F
20 bit flow label
##  Answer
D
## Explanation
IPV6 Header is fixed to 40 bytes
IPV6 does not have checksums as there are checksums at other layers
128 bit source address
There is a 20 bit flow label in IPV6

In IPV6 the TTL field from IPV4 has become Hop Limit. It is only 1 byte

## Goal Implication




# Which of the following are characteristics of 'star' topologies
## ID
CMS.226306b9-1453-4605-9b6b-d135f1a80ead
## A
Collision Free
## B
Resistant to multi-node failure
## C
Can only be used with copper cabling
## D
Limited reliance on central aggregegated device
##  Answer
B
## Explanation
Collisions can happen between router/switch and  node.
Any cabling can be used.
There is reliance on the route/switch at the center of the star
## Goal Implication



# Which of the following is not likely to be found in a DMZ.
## ID

CMS.63645adb-6e57-465f-aca4-6978cbc8ab8c

## A
Front end smtp relay

## B
DNS

## C
Web Server

## D
FTP Server

## E
Active Directory

##  Answer
E


## Explanation
Front end smtp relay to do initial mail processing
DNS - yes to provide resolution to the internet of your sites although DNS is generally provided by another provide
Web Server - yes
FTP Server - yes

AD Domain Controller - No

## Goal Implication
Only in large organization does having a DMZ make sense.  Even though does it make sense for a large organization to have their own DMS rather than the large scale of a service provider.


# You have developed a web app for a customer to view private information, you want to minimize chances attacker  within radio range will be able to eaves drop the wireless Lan and intercept their private data. Which is the best approach to acheive this.

## ID
CMS.c7676d70-a7da-4319-b7a5-cbd7144cafc5
## A
Require SSH for all connections to the web server
## B
Implement TLS on the web server
## C
Advise customers to implement WPA2 with AES on their wireless Lans
## D
Digitally sign all traffic with  RSA keys
## E
Require customers to connect vai IP Sec  VPN
## F
Implement 802.1x on your switches and provide steps for your customers to do the same
## G
Use an IDS to monitor for evidence of ARP flooding attacks
##  Answer
B
## Source
https://www.youtube.com/watch?v=Qt02oe2f2yQ

## Explanation
## Goal Implication


# WHich of the following firewall types provides you the LEAST amount of control over network traffic
## ID
CMS.3e1fc379-59d8-4624-8fe6-968c2da78d09
## A
Application Layer Firewalls
## B
MAC filters on a L2 switch
## C
Packet Filtering routers
## D
Stateful firewalls
## E
Proxy Servers
##  Answer
C
## Explanation
MAC Filter is not a firewall
Pac
## Goal Implication
## Source
https://www.youtube.com/watch?v=lbxGIFYrAqw

# Which of these best describes the function of the domain name system
## ID
CMS.db538a1a-8c72-4d28-af67-cc47c71cb938
## A
Resolve computer NetBios names to IP addresses
## B
Centralized database of domain names managed by ICANN
## C
A distributed hierichal database for resolving fully qualified domain names
## D
An LDAP database used for storing objects and their attributes
## E
A port-to-service mapping agent that locates network services in a distributed network
##  Answer
C
## Explanation
## Goal Implication

# You need to establish a transparent link between two physical sites. User at each site need to be able to able a variety of servers, printers and other IP enabled equipment at each location.  Minimizing user involvement is a key consideration.  Both sites have internet connectivity. WHich is the best option
## ID
CMS.573cc41f-5b97-468b-a478-b442e80e6d81
## A
A SOCK5 proxy hosted at a cloud service provider
## B
Encrypted Remote Desktop Connections
## C
Configure a reverse proxy at each location to offer services to users
## D
Individual TLS VPN connections on a per-user basis
## E
An IPSec VPN between the 2 sites
##  Answer
E
## Explanation
## Goal Implication

# Which of the following is true regarding firewalls?
## ID
COM.dce89f0b-462c-466d-ba71-d4804898621e
## A
They can protect a network at the internet layer of the TCP/IP networking model
## B
They optimize performance at the internal/external trust boundary.
## C
Firewalls eliminate all network based attacks from external networks
## D
Firewalls are optimized, standalone devices
## E
They can be configured as the default gateway for external nodes
##  Answer
A
## Explanation
In the DoD TCP/IP Network model the internet layer is the network layer in the OSI Model.
## Goal Implication



# Which of the following is a benefit of implementing DNSSEC (DNS Security)
## ID
COM.282baa90-2514-4ec7-ab44-c2273bfb2780
## A
Using encryption, DNSSEC prevents service providers from mining your DNS queries
## B
DNSSEC prevents you from going to malicious web sites by redirecting your connections attempts
## C
DNSSEC speeds up name resolution by compressing queries and answers
## D
DNSSec authenticates server response using digital signatures
##  Answer
D
## Explanation
## Goal Implication


# IPSec is comprised of a number of different protocols which work collectively to establish a level of security desired by a system administator. Which of the following components of an IPsec connection is responsible for authenticating parties and esablishing security associations?
## ID
COMS.cc06beda-880c-4536-98dc-e4d013e67a0f
## A
Authentication Header (AH)
## B
Encapsulating Security Payload (ESP)
## C
Internet Key Exchanges (IKE)
## D
User Datagram Protocol (UDP)
## E
Diffie-Helman Key Exchange
##  Answer
C
## Explanation
IKE - negotiating key exchange

AH - mechanism to provide integrity of a message
ESP - deals with confidentiality
IKE may used Diffie Helman as the negotiated approach but IKE is the mechanism
## Goal Implication
## Source
https://www.youtube.com/watch?v=QvZZ3UG2gGc


# Which of the following algorthms were considered by NIST to become the new Advance Encryption Standard (choose 4)
## ID
COM.f49394ca-17fd-474c-9cfa-0b3677d88e20
## A
RC6
## B
Blowfish
## C
Twofish
## D
Serpent
## E
scrypt
## F
Rijndael
## G
Whirlpool
## H
SHA-512
##  Answer
A,C,F,D
## Explanation
## Goal Implication
## Source

# A server admin has reported a persistent and large number of half open TCP connections coming from many different Internet IP addresses. Which is the best way to address this issues?
## ID
COMS.f3ebec42-211a-4842-8e8c-914e011eea57
## A
Configure the app to retransmit slower connections over UDP
## B
Increase the installed RAM on the web server
## C
Decrease the size of the TCP backlog queue
## D
Enable reverse path forwarding on the internet router
## E
Enable SYN flood protection on an upstream firewall
##  Answer
E
## Explanation
The upstream firewall keeps track of half open connections to a server and drops new ones if the limit is exceeded.
Not a great solution because legitimate requests may be dropped in an attack.
## Goal Implication
## Source
https://www.youtube.com/watch?v=1wkExUdaOVQ


# Which of the following is a characteristic of both TCP and UDP (choose 2)
## ID
COM.9742214c-e77c-4e2d-8901-a788f1d6fb3c
## A
Both use sequence and acknowledgement numbers
## B
Both carrry DNS payloads
## C
Both implement windowing
## D
Both implement a header checksum
## E
Both use control bits to track connection state
##  Answer
B,D
## Explanation
Normal DNS name resolution is over UDP but Zone Transfer occur over TCP
Yes both use a header checksum.

Windowing (recipient requesting different packet size) only happens in TCP.
## Goal Implication
## Source
https://www.youtube.com/watch?v=HGtxaxO70rw


# Which of the following is not a benefit of FHRP (First Hop Redundancy Protocol) in your network infrastructure?
## ID
SOP.b9a2bfd4-906d-4974-9c07-d337ebbf7e5e
## A
Hosts always have at least 2 gateway IP addresses for fault tolerance
## B
Routers share a virtual IP address allowing either one to use the address
## C
A standy router providers failover support to the active router
## D
An active router can be manually preempted for hardware maintenance tasks
##  Answer
A
## Explanation
FHRP  - a virtual router is defined and the virtual gateway address is shared beween some number of routers. One router is active while the others are inactive until they sense the active one is offline and they take over.

HSRP- Hot Standy Routing Protocol
VRRP - Virtual Redundant Routing Protocol
GLBP = Gateway Load Balancing Routing Protocol

## Goal Implication
## Source



# ICMP Echo Request sent to the network broadcast address  of a spoofed victim causing all nodes to respond to the victim with an echo reply
## ID
COM.8891d8b3-6afc-43a5-afd7-e34788d14376
## A
Smurf
## B
Bit
## C
Switches
## D
Frame
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.isc2.org/training/self-study-resources/flashcards/cissp/communication-and-network-security


# Data represented at layer 4 of the open system interconnetion (OSI) model
## ID
COM.c05c926b-7996-4c5a-bdb9-d1349a9dc564
## A
Switches
## B
Concentrators
## C
Firewalls
## D
Segment
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.isc2.org/training/self-study-resources/flashcards/cissp/communication-and-network-security

# Provides a standard method for transporting multiprotocol datagrams over point to point
## ID
COM.3b0071c1-e977-403d-892e-c5233ea0091a
## A
Multiprotocol Label Switching (MPLS)
## B
Port Address translation (PAT)
## C
Point to point protocol (PPP)
## D
Internet Control Message Protocol
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.isc2.org/training/self-study-resources/flashcards/cissp/communication-and-network-security


# Separates network into three components: raw data, how the data is sent and what purpose the data serves. This involves focus on data, control and application management functions or 'planes'?
## ID
COM.072569a5-66ab-4fe6-b067-553bb06de65d
## A
Port Address Translation
## B
Software Defined WAN
## C
WIFI
## D
Software Defined Networks
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.isc2.org/training/self-study-resources/flashcards/cissp/communication-and-network-security


# What is a lightweight encapsulation protocol and lacks the reliable data transport of the TCP layer?
## ID
COM.ee198893-665a-4c25-84b2-cb944f0e8475
## A
Fibre Channel of Ethernet
## B
Port Address Translation
## C
MultiProtocol Label Switching
## D
Code Division Multiple Access
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.isc2.org/training/self-study-resources/flashcards/cissp/communication-and-network-security


# which of the following are not private IP addresses defined by RFC 1918? Choose all that apply
## ID
COM.4b75e180-4320-42fa-8417-c99eb5facf04
## A
172.32.31.45
## B
192.188.4.3
## C
10.16.31.22
## D
169.254.67.89
## E
172.29.35.9
## F
131.107.33.1
## G
192.168.44.200
## H
172.6.32.1
##  Answer
A,B,D,F,H
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=se14Kog6raE


# Which of the following does Perfect Forward Secrecy not involve?  (choose several)
## ID
COM.ff606ab3-cdbb-4852-85fc-e159cba954e6
## A
Port knocking
## B
HTTPS
## C
Using empheral public private key
## D
generation of multiple public private key pairs
## E
Using static public private key
##  Answer
A,E
## Explanation
Perfect forward secrecy is a mechanism to use empheral private/public key pairs over https rather than using a single key pair.
## Goal Implication
## Source
PJT made up


# Security domains are critical constructs in a physical network and within alogical environment, as in an operating system. Which of the following best describes how addressing allows for isolation?
## ID
COM.fd9a9a1c-73da-4ad4-ae1e-7e0e9ca618be
## A
In a network domains are isolated by using IP ranges and in an operating systemdomains can be isolated by using MAC addresses.
## B
In a network domains are isolated by using subnet masks and in an operating system domains can be isolated by using memory addresses.
## C
Addressing is a way to implement ACLs, which enforce domain access attempts and boundaries
## D
Addressing creates virtual machines, which creates isolated domains.
##  Answer
B
## Explanation
Addressing is a way to enforce, "if you dont have this address you dont belong here." network domains we use IP ranges and subnet masks to control how networksegments communicate to each other. Within software processes can only communicateto the address space that the security kernel will allow. Addressing is one way to enforce domain access and its boundaries.
## Source
https://www.briefmenow.org/isc2/which-of-the-following-best-describes-how-addressing-allows-for-isolation/


# Which of the following (pick 5) are not allowed to pass outbound through a firewall to the internet?
## ID
COM.813ccd83-2cc7-4242-afba-0d8c8b12268c
## A
HTTP
## B
FTP
## C
SNMP
## D
DNS
## E
EIGRP
## F
RADIUS
## G
OSPF
## H
SSH
## I
SMTP
## J
LDAP
##  Answer
C, E, F, G, J
## Explanation
EIGP - Enhance Interior Gateway Routing Protocol  
OSPF - Open Shortest Path First - A Routing protocol
## Goal Implication
## Source
https://www.youtube.com/watch?v=78QfBIQJQZk

# You have recently discovered that your 802.11 WLAN is 'RF visible' several floors above your office location. Which of the following will be most helpful to you when trying to reduce your RF cell size?
## ID
COM.d7652845-e09c-4dc3-855f-a12f3ffa344d
## A
Configure PEAP
## B
Move the access point to the center of the office
## C
Reconsider antenna type and placement
## D
Change to a different channel
## E
Switch from 'fat' to 'then' APs
## F
Reduce the power produced by the AP
##  Answer
C,F
## Explanation
Fat AP have lots of extra features (nothing to do with RF cellsize)
Channels are changed when there is interference.

2 main influences of RF cell size is annetenna and power
## Source
https://www.youtube.com/watch?v=eV4n-glv5Hc


# You have 11 VLANs in your 6-switch network. Users needing to be assigned to the different VLANs are not all centralized.  Which of the following will you need to do in order to ensure both intra-VLAN and inter-VLAN communication (Choose 3)
## ID
SOP.c8907c2c-2141-453d-9d56-16d3755e0f8d
## A
Configure Layer 2 Switch to route between the VLANs
## B
Route all traffic through the native VLAN
## C
Configure STP to tag the traffice on switch-to-switch links
## D
Configure a Layer-3 switch to route between the VLANs
## E
Allocate a unique subnet id for each VLAN
## F
Configure 802.1q on each port connected to a PC or server
## G
Configure 802.1q on each trunk link
##  Answer
D,G
## Explanation
Layer 2 deals with MAC addresses so isnt helpful with routing
STP (Spanning Tree Protocol) is about loop avoidance.


The 802.1q acheive intra routing (traffic between switches is tagged to pc on different switches are connected to VLAN)
By mapping subnets ID to VLANs you create the different network.
You can route between the networks via layer 3 routing acheive inter vlan routing (from VLAN 5 to 6)

## Goal Implication
## Source
https://www.youtube.com/watch?v=eV4n-glv5Hc


# Which of the following are valid IP 6 address types?
## ID
SRM.55e11197-69fe-4bd2-a30b-eff4f59c39e7
## A
Global Unicast
## B
IPX
## C
Link-Local
## D
Broadcast
## E
Site-Local
## F
Unique-Local
## G
NSAP-address
##  Answer
A,C,F
## Explanation

Global Unicast : UDP;
Link-Local: equivalent to 127.0.0.1;
Unique-Local - replace private addresses but are globally unique rather than stricly private;
Site-Local - was a concept that was dropped as IPV6 matured;
Broadcast - is not a concept in IPV but can be acheived via multicast;

## Goal Implication
## Source
https://www.youtube.com/watch?v=7MqT17AV2Uc

# Two different companies are preparing to connect their offices together via an Internet VPN. Before establishing the connection and beginning to share data, which of the following should be in place?
## ID
COM.4fbd12da-aef4-4d3e-aa6a-fa383915e74e
## A
ISA
## B
SLA
## C
BIA
## D
DLP
## E
IDS
##  Answer
A
## Explanation
ISA - Interconnect Service Agreement
## Goal Implication
## Source
https://www.youtube.com/watch?v=UlDgRaPpWTA


# Company issued laptops with integrated VLAN adapters will be used outside the office and you assume the networks to which they connect will not be trusted. Which of the following is the best recommendation for securing the laptops and their transmitted data?
## ID
SRM.6caa6ba8-200f-455c-8147-c58d29e0d9f8
## A
Enable full disk encryption
## B
Configure the systems to use PEAP when connecting to the untrusted WLANs
## C
Require users to establish a VPN connection when connected to an untrusted network
## D
Configure the clients to use AES when connecting to any WLAN
##  Answer
C
## Explanation
Dont trust anyone
## Goal Implication
## Source
https://www.youtube.com/watch?v=KwOC7beOyjY

# What is the last phase of the TCP 3 way handshake
## ID
COM.d1c700fc-0d41-48ba-b82c-76055a340bae
## A
SYN
## B
NAK
## C
ACK
## D
SYN/ACK
##  Answer
C
## Explanation
SYN is sent first
SYN/ACK is sent first from the destination host
ACK is then send from the initiator host
## Goal Implication
## Source
https://www.google.com.au/books/edition/ISC_2_CISSP_Certified_Information_System/mFhVDwAAQBAJ?hl=en&gbpv=1&dq=cissp+official+study+guide+mike+chapple&printsec=frontcover


# Which of the following is not a composition theory related to security models?
## ID
SAE.07373ea5-11a1-47ce-bb19-5f0be709f232
## A
Cascading
## B
Iterative
## C
Feedback
## D
Hookup
##  Answer
B
## Explanation
Cascading: Input for one system comes from the output of another system.
Feedback: One system provides input to another system, which reciprocates by reversing those roles (so that system A first provides input for system B and then system B provides input to system A).
Hookup: One system sends input to another system but also sends input to external entities.
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 286). Wiley. Kindle Edition. 


# Which of the following are valid implementations of Triple DES (choose 2)?
## ID
SOP.de7eaa6c-23e5-405c-b0f0-414df0f61382
## A
DES-EEE. ALl 3 are unique.  Key is effectively 168 bits.
## B
DES-EDE. Key1=Key2, Key 3 is unique
## C
DES-EDE. Key1=Key3, Key 2 is unique
## D
DES-EED. Key1=Key2, Key 3 is unique
## E
DES-EEE. ALl 3 are the same. Keys are 168 bits
##  Answer
A,C
## Explanation
DES Keys are 56 bits.
DES-EEE.  Encryption-Encryption-Encryption
DES-EDE. 2 keys used. First key is used in first and 3rd step.
## Goal Implication
## Source
https://www.youtube.com/watch?v=02rztIpWIdo

# IPV6 introduces lots of new rules for address structures. WHich of the following addresses are valid destnation IPV6 address used for sending data t antoher not or nodes on the internet?
## ID
COM.ae7bdc45-b40e-427a-9dea-93d82f3d78d0
## A
fe80::46c9:db66:2002
## B
2002:46a8:8:722:d740:9be1:dfd1:d964
## C
fda7:4967:fe1c:1::200
## D
2620:0000:1234:cfg9:afc4:1:a:1100
## E
3000:2341:5621:1:a84c::23::1
## F
ff1e:40:2002:abcd:dead:beef:1:11ee
## G
##  Answer
B,F
## Explanation
A is a local address which does not route globally
B  starts with 0 and has eight segments and is valid globally addressable
C fda7 is example of locally routable address
D has a g in in which is not a hex value
E has double :: which is not allowed
F ff (multi-cast) 1 (transient/temporary) e (global address) is valid
G ff (multi-cast) 9 (local to the link) so not correct answer
## Goal Implication
## Source
https://www.youtube.com/watch?v=ha5-i1TJ1f8&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=85


# Which of these ports numbers for well known services is correct
## ID
COM.a7fa2a73-c330-40b2-9fa5-8bcffb0067e2
## A
RDP-1433
FTP-22
SSH-21
DNS-53
SMTP-25
## B
RDP-3389
FTP-22
SSH-21
DNS-53
SMTP-25
## C
RDP-3389
FTP-21
SSH-22
DNS-53
SMTP-25
## D
RDP-3389
FTP-443
SSH-21
DNS-53
SMTP-25
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/network-ports?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What TCP flag indicates that a packet is requesting a new connection?
## ID
COM.39790d74-81b3-4bcb-8cbc-6715920818d8
## A
RST
## B
URG
## C
PSH
## D
SYN
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4623063?u=25288675

# What technology provides the translation that assigns public IP addresses to privately addressed systems that wish to communicate on the Internet?
## ID
COM.1692cd1b-b566-469e-bbeb-e03b941a0088
## A
SSL
## B
TLS
## C
NAT
## D
HTTP
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4623063?u=25288675

# Which one of the following ports is not normally used by email systems?
## ID
COM.e658aa3b-2895-4588-87ba-62821d005aa3
## A
143
## B
139
## C
25
## D
110
##  Answer
B
## Explanation
25 SMTP
110 POP
143 IMAP
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4623063?u=25288675

# What command sends ICMP Echo Request packets?
## ID
COM.dc0805f6-9fa0-4d84-abfd-b42413e52e23
## A
telnet
## B
ftp
## C
ssh
## D
ping
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4623063?u=25288675

# What network device can connect together multiple networks?
## ID
COM.379442a5-22ec-4b76-a6f5-d505c3ab0c68
## A
switch
## B
ap
## C
router
## D
wireless controller
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4623063?u=25288675

# What is the default policy for a firewall
## ID
COM.ee177746-209e-4fe2-a297-43fa6117b342
## A
Default Disallo
## B
Default Deny
## C
Default Prevent
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/firewalls?u=25288675

# Which of these help you remember the OSI layers
## ID
COM.5954f06f-e01c-4717-860d-eb11fe6a7297
## A
Please Do Not Teach Surly People Acronyms.
## B
Kerckchoffs Principal
## C
All Presidents Since Truman Never Did Pot
## D
Make Hay while the sun shines.
##  Answer
A,C
## Explanation
A - is from bottom up
C - is from top down
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 444). Wiley. Kindle Edition. 


# Which of the following is not a physical layer technology
## ID
COM.78b374b1-1eaf-407a-aae1-9a80b59542bb
## A
HSSI
## B
SONET
## C
X.21
## D
V.25
## E
ARP
##  Answer
E
## Explanation
ARP is data link layer
HSSI - High Speed Serial Interface
SONET - Synchronous Optical Network
## Goal Implication
## Source
Chapter 11
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 445). Wiley. Kindle Edition. 

# Which of the following are characteristics of a MAC address (choose 5)
## ID
COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e
## A
32 bits in length
## B
48 bits in length
## C
written in hexdecimal format
## D
written in decimal format
## E
contains 24 bit OUI
## F
contains 16 bit OUI
## G
Multiple 802 standards use MAC address
## H
Only PCs and Servers have MAC addresses
## I
A nodes MAC address must be globally unique
## J
A nodes MAC address must be locally unique.
##  Answer
B,C,E,G,J
## Explanation
MAC are 48 bits, 
expressed in hexidecimal
container 24 bit OUI - (Organizationally Unique Identifier)
802.3 (ethernet) use MAC address
802.11 (wireless) use MAC address
802.15 (bluetooth) use MAC address
## Goal Implication
## Source
https://www.youtube.com/watch?v=MYDlpLwPA7w&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=46


# By what mechanism does an IPv6 node resolve an IP address to a MAC address?
## ID
COM.04cbd2d5-24fa-4d7f-9f13-06cfd0266db5
## A
ARP
## B
mDNS
## C
Solicited Node Multicast Address
## D
SLAAC
## E
MultiCast query to FF02::1
## F
IPv6 does not use MAC addresses
##  Answer
## Explanation
Solicited Node Multicast Address
- ICMP multicast
- Destination Address id fixed value plus ????
- So the request only goes to the node that you are trying determine MAC address for


Dont use ARP for IPV6
mDNS is multicast DNS

SLAAC - kind of like DHCP for IPV6

FF02::1 - all nodes multicast (kinda like a broadcast)

IPV6 and IPV4 are both Ethernet at layer 2 (Data Link Layer) and both use MAC.

## Goal Implication
## Source
https://www.youtube.com/watch?v=MYDlpLwPA7w&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=46


# Which of these are synomyms?
## ID
COM.97f1b61a-a8ed-4951-9784-a63e8672ee82
## A
Anomyly Detection
## B
Signature Detection
## C
Behavior Detection Systems
## D
Heuristic Detection Systems
##  Answer
A,C,D
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/network-intrusion-detection-and-prevention?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What security principle does a firewall implement with traffic when it does not have a rule that explicitly defines an action for that communication?
## ID
COM.278ad11c-296a-47dc-b1c3-950883cba67d
## A
least privilege
## B
implicit deny
## C
separation of duties
## D
informed consent
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621456?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What network port is used for SSL/TLS VPN connections?
## ID
COM.5c26a4f1-a15f-4616-be82-aa785004a6e8
## A
80
## B
443
## C
88
## D
1521
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621456?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Which one of the following functions is not normally found in a UTM device?
## ID
COM.84ed7e36-57f7-4e11-895b-abab4994de63
## A
Firewall
## B
Intrusion Detection
## C
SSL TErmination
## D
Content Filtering
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621456?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Both IPV4 and IPV6 operate at Layer 3 of the OSI Model. WHich of the following is NOT a field in an IPv4 Header
## ID
COM.ada873ff-0d49-4f8a-bbf1-31c5b9980e70
## A
TTL
## B
Protocol ID
## C
Flow Label
## D
Version
## E
Source IP Address
## F
Fragment Offset
## G
Checksum
##  Answer
C
## Explanation
TTL is a field in an IPV4 header but not in an IPV6 header
Protocol is a field in an IPV4 header but not in an IPV6 header
Flow Label is a field in an IPc6
Fragement offset exists in IPV4 but not IPV6
Checksum exists in IPV4 and not IPV6
## Goal Implication
## Source
https://www.youtube.com/watch?v=_JUfV0u_iPg

# Which one of the following is a public IP address?
## ID
COM.1a3a72de-ec0f-4f28-a49a-fb49a5c5ce48
## A
172.18.144.144
## B
142.19.15.4
## C
192.168.14.129
## D
10.194.99.16
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621453?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# How would a network technician write the subnet mask 255.255.0.0 in slash notation?
## ID
COM.9ee2447d-9443-45e5-bef7-6b0fb8a35baf
## A
/8
## B
/32
## C
/16
## D
/24
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621453?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Which one of the following devices carries VLANs on a network?
## ID
COM.ce24b9e2-b511-4622-bcea-96d81580c873
## A
hub
## B
router
## C
switch
## D
firewall
##  Answer
C
## Explanation
VLANs are implement at layer 2
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621453?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What is the piece of software running on a device that enables it to connect to a NAC-protected network?
## ID
COM.ad5e3a8e-d86e-4ade-812a-06e1e0b95fee
## A
SNMP agent
## B
authentication server
## C
authenticator
## D
supplicant
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621453?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Which of the following controls would not constitute defense-in-depth for network access control?
## ID
COM.0d8e3107-e799-46e4-89a0-b6061c2d90f6
## A
MAC filtering
## B
802.1x
## C
IDS
## D
NAC
##  Answer
C
## Explanation
IDS is not about access control
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621453?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What control can administrators use when encryption is not feasible for a VoIP network?
## ID
COM.7a54ccc5-f122-4dda-a2b4-47a46017aab4
## A
VOIP Firewall
## B
VOIP Spoofing
## C
Separate voice VLANS
## D
voice broadcasting
##  Answer
C
## Explanation

## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4622081?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What type of storage network requires the use of dedicated connections?
## ID
COM.7afcedf8-3a8b-4019-9a22-90f1f45bdd71
## A
iSSCI
## B
Fibre Channel over Ethernet
## C
CIFS
## D
Fibre Channel
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4622081?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What type of firewall rule error occurs when a service is decommissioned but the related firewall rules are not removed?
## ID
COM.5d14e23b-8e75-49cc-b643-004775b3758a
## A
Shadowed Rule
## B
Orphaned Rule
## C
Promiscuous Rule
## D
Typographical Rule
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621454?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What router technology can be used to perform basic firewall functionality?
## ID
COM.d8e72c89-d51c-4941-b900-bac9b04ae760
## A
spanning tree
## B
access control lists
## C
IPS
## D
flood guard
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621454?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What technique should network administrators use on switches to limit the exposure of sensitive network traffic?
## ID
COM.1842cf52-869d-491d-9df7-a8fd7a4cbb1b
## A
spanning tree
## B
loop prevention
## C
VLAN pruning
## D
VLAN hopping
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621454?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What technology can help prevent denial of service attacks on a network?
## ID
COM.10cfeb42-5eeb-4fd8-8025-9a0105bbde21
## A
BGP
## B
Flood Guard
## C
VLAN pruning
## D
VLAN hopping
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621454?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What information is not found in network flow data?
## ID
COM.86391be3-56d2-454a-b3ff-abf583b0c458
## A
source address
## B
destination port
## C
packet content
## D
destination address
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621454?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What message can an SNMP agent send to a network management system to report an unusual event?
## ID
COM.06723673-69bd-40e0-ab53-e681f98b467a
## A
Trap
## B
Get Request
## C
Set Request
## D
Response
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4621454?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What type of packet do participating systems send during a Smurf attack?
## ID
COM.52274c4d-2d62-43c0-aeae-1f4a4fbff25f
## A
ICMP Information Reply
## B
ICMP Status Check
## C
ICMP Echo Request
## D
ICMP Timestamp
C
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4622080?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Which one of the following techniques is useful in preventing replay attacks?
## ID
COM.f0447397-fdb0-414a-aaad-2c7db6fdc52d
## A
Mobile Device Management
## B
Full Disk Encryption
## C
Session Tokens
## D
Man in the middle
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4622080?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What attack uses carefully crafted packets that have all available option flags set to 1?
## ID
COM.c86f8838-e1dd-40fa-861d-b13ae7fb5672
## A
DNS Poisoning
## B
Christmas Tree
## C
ARP Poisoning
## D
URL Hijacking
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4622080?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Which one of the following is the most secure way for web servers and web browsers to communicate with each other?
## ID
COM.aa3596f0-2356-48ea-a8d4-c6a458e25493
## A
SSLv1
## B
SSLv2
## C
TLS
## D
SSLv3
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4620642?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What IPsec protocol provides confidentiality protection for the content of packets?
## ID
COM.e27197a0-c063-46d8-bbad-f06dee3817d9
## A
ISAKMP
## B
IKE
## C
AH
## D
ESP
##  Answer
D
## Explanation
In IPSEC, ESP protects confidentiality of packets while AH protects integrity of header and packets
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4620642?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Cindy would like to transfer files between two systems over a network. Which one of the following protocols performs this action over a secure, encrypted connection?
## ID
COM.8d18fa4e-2191-4ba4-9081-8a6722ab354c
## A
FTP
## B
SCP
## C
TFTP
## D
SSH
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4620642?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Gary is setting up a wireless network for his home office. He wants to limit access to the network to specific pieces of hardware. What technology can he use to achieve this goal?
## ID
COM.c5e91be2-9c47-4815-b0a5-59569684973b
## A
WEP
## B
SSID broadcasting
## C
MAC filtering
## D
WPA
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4620642?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Brad is configuring a new wireless network for his small business. What wireless security standard should he use?
## ID
COM.529de368-e839-4851-817f-4b1e90a1b35b
## A
WPA2
## B
WPA
## C
WEP
## D
WEP2
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4622079?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Fran is choosing an authentication protocol for her organization's wireless network. Which one of the following protocols is the most secure?
## ID
COM.8accd396-a064-435e-a5c8-2ba11833fc60
## A
TACAS
## B
PEAP
## C
EAP-MD5
## D
LEAP
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4622079?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# How many digits are allowed in a WiFi Protected Setup (WPS) PIN?
## ID
COM.400548d4-5bfd-437f-b802-5dc811f86846
## A
11
## B
4
## C
6
## D
8
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4622079?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Renee notices a suspicious individual moving around the vicinity of her company's buildings with a large antenna mounted in his car. Users are not reporting any problems with the network. What type of attack is likely taking place?
## ID
COM.c7978e16-7694-4851-85e7-2815cf0395ad
## A
WPS Cracking
## B
Jamming
## C
War Driving
## D
War chalking
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4622079?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What toolkit enables attackers to easily automate evil twin attacks?
## ID
COM.1e8131e6-2ab1-4c40-b33d-54697d2abc33
## A
Karma
## B
NIDS
## C
HIPS
## D
iStumbler
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4622079?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What command is used to apply operating system updates on some Linux distributions?
## ID
COM.210624f0-8094-4995-aa99-2733b8afd3e9
## A
yum
## B
update
## C
systeminfo
## D
ps
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4623062?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What type of malware prevention is most effective against known viruses?
## ID
COM.b2874552-aa8d-4beb-abfa-b1ff67db1d2f
## A
heuristic detection
## B
signature detection
## C
anomaly detection
## D
behavour analsysis
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4623062?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What is the name of the application control technology built-in to Microsoft Windows?
## ID
COM.e6c7ca35-8468-42d3-8481-7d6a0ed1161b
## A
BitContrl
## B
BitLocker
## C
AppLocker
## D
AppControl
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4623062?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Which one of the following security controls is built into Microsoft Windows?
## ID
COM.329d52a8-3c63-4746-9aa9-455cffccd3f8
## A
Host IDS
## B
Host IPS
## C
Host firewall
## D
MDM
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4623062?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What additional patching requirements apply only in a virtualized environment?
## ID
COM.4aca9633-561e-4569-b0a7-1ce61bb2df4d
## A
patching the hypervisor
## B
patching firewalls
## C
patching applications
## D
patching the operating system
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-4-communication-and-network-security-2018/quiz/urn:li:learningApiAssessment:4623062?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Which of these describes how a code is not a cipher
## ID
COM.54f2f196-0104-4ee9-a5ef-98d4888ba62a
## A
Hides information
## B
Allows efficient communication
## C
Provides integrity
##  Answer
B
## Explanation
A code such as the '10-4' code used in police communication provides clarity and efficiency
## Goal Implication
## Source
PT



# What encryption technique does WPA use to protect wireless communications?
## ID
COM.7f4371e6-75e6-4cdc-84f0-212528f7dc08
## A
TKIP
## B
DES
## C
3DES
## D
AES
##  Answer
A
## Explanation
Wi-Fi Protected Access (WPA) uses the Temporal Key Integrity Protocol (TKIP) to protect wireless communications. WPA2 uses AES encryption.
## Goal Implication
## Source
Chapter 7 Review Question 8
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 272). Wiley. Kindle Edition. 