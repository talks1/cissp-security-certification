
The four goals of cryptographic systems 
    - confidentiality, 
    - integrity, 
    - authentication, and 
    - nonrepudiation.

- Introduction to Communications and Network Security
- The OSI Model Part 1
- The OSI Model Part 2
- The OSI Model Part 3
- The OSI Model Part 4
- The OSI Model Part 5
- The OSI Model Network Devices
- The OSI Model Collision Domains
- The OSI Model Layer 3 Protocols
- The OSI Model Layer 4
- The OSI Model Layers 5 and 6
- The OSI Model Layer 7
- The OSI Model Firewalls Part 1
- The OSI Model Firewalls Part 2
- The OSI Model Firewalls Part 3
- The OSI Model NAT/PAT
- Password Security
- Area Networks: LAN, WAN and MAN
- Remote Access
- General Routing Encapsulation (GRE)
- Wireless Security Part 1
- Wireless Security Part 2
- Wireless Security Part 3



IP-Internet Protocol - Network Layer
- addressing and packet delivery

Transport Protocol
- TCP and UDP
    - TCP-Tranmission Control Protocol
        - connection oriented
        -guaranteed delivery
        - 3 way handshake
            - SYN - request for new connection
            - FIN - request closure of connection
            - ACK - acknowledge SYN or FIN
            - First SYN from source to dest
            - Second SYN/ACK from dest to source
            - Third ACK from source to dest
TCP Flag Byte
00100000 - URG - Urgent
00010000 - ACK - Acknowledge
00001000 - PSH - Push Data
00000100 - RST - Reset a connection
00000010 - SYN - Start a connection
00000001 - FIN - Finish a connection



Well Known Ports
FTP - 21
SSH - 22
RDP-3389
Windows NetBios 137,138,139
DNS - 53
SMTP - 25
POP - 110
IMAP - 143
HTTP - 80
HTTPS - 443
DHCP-68, DHCP Server point to point request-67


ICMP-PING
TRACEROUTE


Alternative to TCP/IP
DNP3 - SCADA
- SCADA master station
- Low speed links (copper, radio waves)
- Remote Terminal Unit

Switches and Routers

Firewalls
Internet - Firewall - Internal
             |
            DMZ

Firewalls use stateful
Default Deny Policy - disallow connectivity not explicity allowed

Bridges - 
    - join different networks together.  THe networks are expected to have the same protocol.

Proxy Server
- anonymity - 
- caching
- security tasks

Web Securty Gateway 
- proxy between user and server
- scan requests and responses
- gateways are primarily responsible for protocol translation

VPNs
- site to site
    via IPSEC
- remote access VPN
    via SSL/TLS

VPN endpoints - firewall, route, server,  VPN concentrator


IDS - Intrusion Detection Detection
- checking for sql injections, unusual logins, malformed packets, botnet traffic

False Positive - False Alarm
False Negative - No detection

Signature Base Systems
- Low False Positive rate
Anomyly Detection System (aka Behavior Detection) (aka Heuristic Detection)
- high false positive error rate


Inband vs Out of Band IDS
- With inband deployments 
    - Adds a risk that issue with IDS cause loss of network connectivity
- With out of based / passive IDS
    - traffic is replicated to the device

Protocol Analyzers
- wireshark

UTM (Unified Threat Monitor)
- a firewall, IDS, IPS, URL filtering, Content inspection

CDNS
- can provide DDOS protections and some WAF features 

IP Address
Public Address
- ICANN - 

NAT - map public ip address to internal address
PAT - map port on public ip address onto internal address

VLANS
- work at layer 2 (e.g switches) without any router configuration.
- the switches have to have VLAN trunking enabled.


NAC - Network Access Control
- 802.1x
- check to validate device before letting it join a network
- preadmission philosophy - dont let devices join the network if missing requisites
- postadmission philosophy - allows/denies access based on user activity


Remote Access
- via TLS/SSH VPN
- remote shell - SSH
 - RDP

 Virtual Desktop
 - Amazon Workspaces

 Application Virtualization
 - Citrix,

 Screen Scraping
 - mainframe presentation

 Defense in Depth
 - organization should use mutiple overlapping security controls to acheive each of their security objectives

    - e.g Eavesdropping
        - use VPN
        - also use HTTPS
        - also segment network
    - e.g. access control
        - implement NAC
        - implement VLANS
        - MAC filtering
        - port security
    - e.g. Protect network perimeter
        - firewall
        - IPS
        - router ACLs

VOIP
- need to consider how to protect this traffic
- can use encryption but degrades service
- can use Network segment and NAC

Instant Messenging
- useful but not secure

SMS -
- no encryption
- no strong authentication


Storage Networks
- NAS - simple self contained
    - look like file services
- SAN - large scale disk array
    - look like devices
    - Fibre Channel
    - FC over ethernet
    - iSCSI



MPLS - uses fixed paths to route packets through the network
- layer 2.5 protocol
- first router is the LER - the tunnel is the LSP - Egress node is that last router
- 2 protocols
    - LDP - Label Distribution Protocol
    - RSVP-TE - Reservation Resource Protocol - Traffic Engineering
    

Secure Network
- Firewall
  - Common Firewall Rule Errors
    - Shadow Rules - should be removed / rearranged
    - Promiscuous Rules
- Routers
    - ACLs - blocking address by source network address
    
- Switches
    - Generally spread around the organization ...need to be secured
    - VLAN Pruning - avoid trunking vLANs to switch where users of that VLAN will not work
    -  Port Security - limiting access to a limited number of MAC addresses

- Maintaining Network Availability
  - SYN or MAC flooding - prevented with flood guards
  - Routing Loops - broadcast storm -  protected by STP - Spanning Tree Procol - the protocol eliminates redundant links

- Network Monitoring
 - Firewall Logs are very useful
 - Netflow Recording - keeps records of session traffic without recording all the packets
 - SIEM - systems that automate log analysis - combine information from firewall, servers

 - SNMP
    - Current version is SNPM version 3
    - Network Management System - central configuration of network devices
    - Agents - software running on the devices :  firewalls, routers, switches
    - GetRequest - retrieve configuration
    - SetRequest - push configuration to device
    - Trap - unusal information is available

Software Defined Network - SDN
 - Separates the Control Plane and Data Plan from each other
 - SDN Controller - takes commands on how the network should operate and propagates these changes out to devices
 - Supports strong network segmentation
 - Makes the network more programmable and therefore suspectible to attack

 Port Isolation or Private VLANs
 - prevents devices on the same switch from communicating.  Typically used in hotels so that devices in different rooms can only talk to internet and not to each other.

Network Attacks
- Denial of Service Attacks
  - Smurf Attack - Send PING ICMP request with a forged source address to many servers.  The services all send a response to the forged source address (which is the victim)
  - Amplified DDOS attach - Send request with forged source address to many servers where the response is a large responses.  The large responses are all sent to the victim.
    - Amplification Factor - Response to Request Ratio
 - LOIC - Low Orbit Ionic Cannon - popular tool for DDOS
- Eaves Dropping Attack
    - Man in the Middle - tricking client by DNS or ARP poisoning
    - Replay Attack - replay session sequence to login - prevented using a session token

- Christmas Tree Attack 
    - TCP Packet Headers where all flags are set
    - Different systems handle the christmas tree flag in a different way - can be used to identify the system

- ARP Poisoning 
    - ARP translates IP address to MAC address 

- TYPOSquatting - Register a domain name similar to well known sites


TLS- not a cryptographic algorthm ...instead it supports many possible algorthms and the server chooses which of the cipher suite it would like to use
- note it is possible to use TLS insecurely by using a weak on no-op encryption methd
- algorythm
    - client sends server supported cipher suites
    - server sends whcih of cipher suites it wants to use and the servers public certificate
    - client verifies public key and matches the domain name the connection was established to
    - client creates session encryption key and sends to server after encrypts using servers public key.
    - server uses it private key to decrypt

IPSEC-adds security to insecure TCP/IP
- uses 2 technologies to encrypt
 - ESP - encapsulating security payload
    - provides confidentiality and integrity for packet payloads
- Authentication Headers
    - provides integrity for packet headers
- 2 modes
    - Transport mode used for host to host communication
    - Tunnerl mode use for host to network communication

- Site to Site VPNs
    - transparent to user
- End User VPN to access corporate network
    - falling out of favour to TLS based access

Securing Network Connections
- Telnet insecure
- FTP insecure
- TFTP - trivial ftp
Instead use
- ssh,sftp

TOR - The Onion Router
- a browser to access dark web
- large list of tor relay nodes
- 3 nodes are selected at random for each request

- Forward Perfect Secrecy 
    - uses encryption to hide identity of participants from the relay nodes in a communication
    - destination packet encrypted with node 3 public key which is then encrypted with node2 which is then encrypted with node 1. 

- Washington Post provides 'SecureDrop' site which can be used to send information anonymously to the post.

WIFI
- The Standard
    - 802.11  - Standard - 2mbps
    - 802.11b -  11 mpbs
    - 802.11g - 22 mbs
    - 802.11n - 600 mbs
    - 802.11ac - 1 Gps
-Radio tranmission which can be picked up and eavesdropped on


- wireless access point modes
    - ad hoc - any 2 devices can communicate directly
    - infrastructure  - devices only communicate through access points

    - wireless mode - the access point doesnt connect to any wired network
    - wire mode - the access point enables access to wired network
        - enterprise wire mode - a single SSID across many access points.

- WIFI Security
    - SSID - Service Set ID - Identity of Network
        - can be disabled so network is not easily discoverable
    - MAC Filter
        - time consuming can be burden
        - not effective
    - VPN
    - Wireless Encryption 
        - WEP - weak encryption
        - WPA             
            - Based on RC4 (streaming cipher)
            - temporal key integrity protocol TKIP (uses different key for each packet)
        - WPA2 
            - Uses CCMP which uses AES and does not us TKIP
            
    - WIFI authentication
        - Shared Secret
            - provide password which is then stretched to 256 using PBFDF2 function
            - use of password is impractical in business and there is no way to revoke individual access        
        - EAP 
            - 802.1x/EAP
            - enterprise access protocol - use username/password via Radius 
            - LEAP (Lightweight EAP)
            - EAP
            - PEAP - protects EAP with TLS tunnel
        - Captive Portal
            - Redirect user to web page to grant them access
    - WIFI signal
        - antenna - omni directional -
        - 802.11ac - beam forming  - virtual directional antenna
        - power levels can be adjusted

    - WPS -
        - use short key to connect
        - only 11000 guesses to guess WPS secret    
        - dont use WPS

    - WIFI Attacks
        - Jamming - DOS
        - War Driving - drive around and information on WIFI networks
    -WIFI Risk
        - Rogue access points
        - Evil Twin access points - creation of access points with similar names to real APs but without the security

    - NFC attacks
        - Bluejacking - Spam your device 
        - Bluesnarfing - forced pairing across devices and download content

- Host Security
    - Enable security updates
    - Trusted Operating System - Certification via Common Criteria
    - harden OS but removing unnecessary systems and accounts 

    - Malware
        - Types
            - Virus - Require user actions
            - Worms - Dont require human action
            - Trojan Horse - 
            - Spyware - reports information
                - keystroke logging

        - Detection
            - Signature - be sure to update signatures
            - Behaviour based detection

        - Preventation
            - Windows Defender, Sophos
            - Browser Security
            - Spam Filtering

    - Application Control
        - White Listing 
        - Black Listing
        - Host Source Baselining

    - Firewall Control Access

    - hardware security 
        - use security locks
    
    - virtualization platform
        - snapshot - 
            - use sandboxes 
        - be sure to patch virtualization platform

    

Email 
- X.400 standard


Standards
802.1q is VLAN
802.1x is port based access control
802.2 is an internet standard for Logical Link Control
802.3 is a Wired Ethernet standard including Media Access Control (MAC)
802.5 is token ring
802.11 is wifi
802.15 is bluetooth
802.16 is wimax
