

128 Bit length

- 64 bits are network
- 64 bits are host

- 48 Bit Global routing prefix
- 16 bit subnet mask
- 64 bit interface


No broadbast

Unicast - point to point
Multicast - one to many
Anycast - one to all but traverses to nearest neighbor




Address Types
- Global Address 
    2001::/16
    anything starting with 2 or 3.
- Link Local Address
    FE80::/64
- Unique Local  (akin to private ip space
    FC00:/7)  FDA is an example of this
- FF1 is multiple

IPV6 are written in hex (characters 0-F)
Cannot use :: more than once in an address
