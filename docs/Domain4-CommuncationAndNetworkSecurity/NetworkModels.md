# DARPA Model
Early 1970s
TCP/IP

# OSI Model
Open Systems Interconnectivity Reference Model
late 1970s

Makes use of encapuslation in which each layer adds header/footer to data at high layer before passing to  next layer down.

## Application

Protocol 
    - HTTP
    - SSL
    - DNS
    - SSH

Data Object at this level is  Protocol Data Unit

## Presentation

The presentation layer ensures the information that the application layer of one system sends out is readable by the application layer of another system.
So it does conversion, character code translation, compression and encyrption

Telnet

Data Object at this level is  Protocol Data Unit

## Session

Authentication, Authorization

Protocols
- PPTP
- RPC
- ASP - Apple Talk Session Protocol


Data Object at this level is  Protocol Data Unit


## Transport

Joining packages together into streams


Connection/Stream
Same order delivery
Reliability
Flow Control 
MultiPlexing

Data Object at this level is  Segment

Protocols: 
    - TCP
    - UDP

Support 
- Simplex or 
- Duplex messaging

## Network

Moving packets around multiple nodes some which may be routers

Addressing,
Routing, 
Traffic Control

IP Address
Network

Protocols: 
    - NAT
    - IPSEC

Data Object at this level is  Datagram

## Data Link

Reliable transmission between 2 nodes

Protocols: 
    - ARP
    - PPP
    - VLAN

Data Object at this level is  Frame

## Physical

Device drivers that take packet of information and relay onto network.
X.21
HISSH - High Speed Serial Interface
SONET - Synchronous Optical Network
V.24 and V.35

# DoD TCP/IP Network Model

## Data Layer
same as OSI application/presentation and session combined

## Transport
same as OSI transport

## Internet Layer 
same as OSI network layer

## Network
same as OSI physical and data link