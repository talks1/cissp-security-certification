

# At which stage in the SDLC should a privacy impact assessment first be conducted

## ID

SDL.f74710ec-e2fb-4700-a394-0bcece067f39

## A
Initiation

## B
Development/Acquisition

## C
Implementation/Assessment

## D
Operation/Maintenance

## D
Disposal

##  Answer
A

## Explanation

## Goal Implication



# A small but talented company has developed proprietary code for you. You are concerned that in the future the employees and maybe even the company may break up and move on. What is the best way for you to protect the value of your purchased product?

## ID

SDL.6c3c0336-0996-4766-b74f-2a87cae9daf8

## A
Demand the source code

## B
Hire the lead developer

## C
Only buy from well-established vendors

## D
Use a software escrow



##  Answer
D

## Explanation

Software escrow is usually requested by the buyers, who intend to ensure the continuity of the software maintenance over time, even if the software house that has developed the application goes out of the business or fails to maintain and update the code.

## Goal Implication



# If a web application is not doing appropriate input validation what is the most likely consequence of this
## ID
SDL.a15bd4d6-af81-47b7-b62b-4f1e006cb17f
## A
SQL Injection
## B
Memory Leaks
## C
SYN Floods
## D
Cross Site Scripting
## E
Heartbleed

##  Answer
A

## Explanation
According to OWASP SQL injection is the most frequently occurring type of attack when there is inappropriate validation.
Cross Site Scripting also occurs but is less frequent.

[The OWASP Top Ten provides this information(https://owasp.org/www-project-top-ten/2017/A1_2017-Injection)

## Goal Implication



# You are designing a relational db and what to ensure that the design is logical, efficient, and optimized. WHich of the following processes should you follow to acheive this.
## ID
SDL.fbbcc230-cb9a-48d6-b347-510d0c3035d7
## A
Top Down Approach
## B
Normalization
## C
Multiple Primary Keys
## D
Waterfall
## E
Baselines
##  Answer
B
## Explanation
This is a process where some data is converted into relational form by considering the constraints expected at different normal forms to make the data towards a normalized form.  There are NF1,2,3,4,5
## Goal Implication


# Which of the following differentiates DOM based XSS from stored or reflected XSS attackes
## ID
SDL.13c68efb-0afc-46ed-8e39-17e70b46528b
## A
DOM based XSS is stored in the web server and sent to a victim when visiting the web page
## B
DOM based XSS will not be visible in the HTML source of the page
## C
DOM based XSS rely upon tricking a user into clickingon a malicious link
## D
DOM based XSS are server side flaw and stored and reflected are client side
##  Answer
B
## Explanation
Apparrently DOM based attacks write scripts into the DOM ...where they are not usually found. SOme browser versions dont handle this properly.
## Goal Implication
## Source
https://www.youtube.com/watch?v=hzFQap93VJM


# You are completing design of the Security Architecture. What phase of the SDLC are you in?
## ID
SDL.d0819d9f-58c2-4aca-b6fb-c4ef5775da6a
## A
Initiation
## B
Development/Acquisition
## C
Implement/Assessment
## D
Operations & Maintenance
## E
Disposal
##  Answer
B
## Explanation

## Goal Implication
## Source
https://www.youtube.com/watch?v=8A4c-vtz_U0

# Which of the following memory types is used by programs on your system.
## ID
SDL.c26b10e5-59cd-4aa3-b254-5c9e3e008ddc
## A
Physical Addresses
## B
Logical Addresses
## C
Relative Addresses
## D
Indirect Addresses
##  Answer
B
## Explanation
The OS manages memory and hides the actual physical or virtual (on disk) memory location behind a logical address
## Goal Implication
## Source
https://www.youtube.com/watch?v=bs5xEHCmQPk

# You have tasked your security team with reviewing an internal web application. If configured which of the following would be considered GREATEST weakness in the session management configuration
## ID
SDL.0d74f6d9-ab88-4f95-9fa2-d9aa62b84db4
## A
Sessions are tracked and managed in cookies
## B
The server supports only HTTPS connections
## C
The server does not run a client side firewall
## D
Sessions are managed via URL rewrites
##  Answer
D
## Explanation
If session info is in the URL sending a URL to someone means they access a site as yourself.
## Goal Implication
## Source
https://www.youtube.com/watch?v=1wkExUdaOVQ



# An approach based on lean and agile principles in which business owners and the development, operations and quality assurance departments collaborate?
## ID
SDL.47d92f97-ee2b-40e1-bcc5-e418b65543de
## A
Data Mining
## B
DevOps
## C
Computer Virus
## D
Covert Channel
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.isc2.org/training/self-study-resources/flashcards/cissp/software-development-security

# How can security violations due to object reuse be mitigated
## ID
SDL.a35aeae1-f2aa-433e-9b1c-7a613c31425b
## A
Reboot the system after deleting a folder containing a large number of files.
## B
Hold Option+Command while pressing delete on the MacOS system
## C
Hold the shift key while pressing delete on Windows
## D
Zeroize RAM after a process finishes using it.
## E
Delete the file and then create a new empty file with the same name
##  Answer
D
## Explanation
If we had secure information in RAM we zeroize it to eliminate possiblity it might be used by another process.
## Goal Implication
## Source
https://www.youtube.com/watch?v=XBOuWeR08QM

# Which of the following is a characteristic of an interpreted programming language
## ID
SDL.fff298bd-93f6-4eb0-91bb-bb289a0e0faa
## A
Turns high level source code into binary executable
## B
Executes code one line at a time
## C
Are in the form of instructions executed directly by the hardware
## D
Converts assembly language into machine language
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=tx70EN2ph_Y


# Software prototyping was introduced to overcome some limitations of the waterfall approach to software development.
Prototyping build successive iterations of an application that show its functionality often focusing on systems that have high levels of user interaction.
This approach has many benefits What are they. Choose 3
## ID
SDL.38faa4c5-d9d8-4874-bd9f-6f989b360c5d
## A
Missing functionality may be identified
## B
Prototypes can be reused to build the actual application
## C
Requirements analysis is reduced
## D
Defects can be identified earlier, reducing time and cost of development
## E
User feedback is quicker, allowing necessary changes to be identified sooner
## F 
Flexibility of development allows project to be easily expand beyond plans
##  Answer
A, D,E
## Explanation
Drawback of Agile approach (according to Colin Weaver) include
- loss of the big picture
- content in prototype doesnt make it into final code base
- scope screep
## Goal Implication
How do we maintain the bigger picture in our team (I think we lose site of this)
## Source
https://www.youtube.com/watch?v=ziefG4Qz9Pc&list=RDCMUCwUkAunxT1BNbmKVOSEoqYA&index=2


# The Montreal Protocol, an international treaty in the 1980's endevours to protect the earths ozone layer from depletion. This includes the replacement of the Halon-base fire suppression systems. The EPA SNAP (Significant New Alternatives Policy) provides a list of alternatives. Which of the following are suitable Halon replacements according to SNAP? Pick 6
## ID
SOP.f93d44ab-dd45-46c4-a108-2dc68d55339f
## A
BFR (Bromine Flame Retardent)
## B
Carbon Dioxide
## C
FM-200
## D
Aero K
## E
Argonite
## F
FM-100
## G
FE-13
## H
HFC-32
## I
Inergen
##  Answer
B,C,D,E,G,I
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=5_YqDgPJhW0&list=RDCMUCwUkAunxT1BNbmKVOSEoqYA&index=4


# Which of the following best describes the domains of a relation in a relational database?
## ID
SDLC.6632505b-509f-4b08-8ac4-855f681464a4
## A
A named set of possible values for an attribute, all of the same type.
## B
All tuple in a relation
## C
All the attributes of a relation
## D
The cardinality of a realtion
## E
The degree of attributes in a relation
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=_JUfV0u_iPg



# Which one of the following is not a standard application hardening technique?
## ID
SDLC.b091a4cb-50c9-4822-8764-adac0f1b6063
## A
Encrypt Sensitive Information
## B
Conduct Cross Site Scripting
## C
Apply Security Patches Promptly
## D
Validate User Input
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-8-software-development-security-2018/quiz/urn:li:learningApiAssessment:4614258?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What software development methodology uses four stages in an iterative process?
## ID
SDLC.6653bd82-496f-47dd-9fe0-e604d66bb46b
## A
Agile
## B
Spiral
## C
Waterfall
## D
DevOps
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-8-software-development-security-2018/quiz/urn:li:learningApiAssessment:4613604?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What phase of the capability maturity model introduces the reuse of code across projects?
## ID
SDLC.5624c30b-3920-49e8-9446-265489d3e99e
## A
Optimizing
## B
Defined
## C
Initial
## D
Repeatable
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-8-software-development-security-2018/quiz/urn:li:learningApiAssessment:4613604?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What component of a change management program includes final testing that the software functions properly?
## ID
SDLC.2a834edc-72ae-466f-9576-3262b9f2f5db
## A
Change Management
## B
Iteration Management
## C
Request Management
## D
Release Management
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-8-software-development-security-2018/quiz/urn:li:learningApiAssessment:4613604?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What is the most effective defense against cross-site scripting attacks?
## ID
SDLC.24f41563-7e96-434d-bf89-d1df2441e505
## A
input validation
## B
query parameterization
## C
antivirus software
## D
vulnerability scanning
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-8-software-development-security-2018/quiz/urn:li:learningApiAssessment:4615226?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What character is essential in input for a SQL injection attack?
## ID
SDLC.4d9232bd-daf5-47ba-b044-0e4e7b505ad8
## A
exclamation
## B
quote
## C
asterix
## D
doublequotes
##  Answer
B
## Explanation
The quote can be used to terminate one statement and start a new one.
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-8-software-development-security-2018/quiz/urn:li:learningApiAssessment:4615226?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Alan is analyzing his web server logs and sees several strange entries that contain strings similar to ../../" in URL requests. What type of attack was attempted against his server?
## ID
SDLC.554cd8e2-3b2c-4a71-a645-31c576f0b185
## A
directory traversal
## B
cross-site scriptiong
## C
buffer overflow
## D
SQL injection
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-8-software-development-security-2018/quiz/urn:li:learningApiAssessment:4615226?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Which choice is not a significant risk associated with browser add-ons and extensions?
## ID
SDLC.385df37c-085f-49a7-ae08-9854d8c5d0ed
## A
resale of legitmate extensions
## B
sandbox execution
## C
overly broad permissions
## D
malicious author
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-8-software-development-security-2018/quiz/urn:li:learningApiAssessment:4615226?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Developers wishing to sign their code must have a _____.
## ID
SDLC.c5cc83af-21a6-4887-968d-2001eac8faf2
## A
patent
## B
digital certificate
## C
software license
## D
shared secret
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-8-software-development-security-2018/quiz/urn:li:learningApiAssessment:4613603?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# _____ is usually the final stage in code testing.
## ID
SDLC.e89f6f64-b183-4690-a800-460cf82b258e
## A
Unit Testing
## B
UAT
## C
Load Testing
## D
Integration Testing
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-8-software-development-security-2018/quiz/urn:li:learningApiAssessment:4612787?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What portion of the change management process allows developers to prioritize tasks?
## ID
SDLC.041c2f7d-78bf-4aff-bc13-c28044ca3b52
## A
Release Control
## B
Configuration Control
## C
Request Control
## D
Change Audit
##  Answer
C
## Explanation
The request control provides users with a framework to request changes and developers with the opportunity to prioritize those requests.
## Goal Implication
## Source
Chp 21 Review Question 3
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition. 

# What approach to failure management places the system in a high level of security?
## ID
SDLC.b9718aab-e6b0-4ed0-af48-31393104d65b
## A
Fail Open
## B
Fail Mitigation
## C
Fail Secure
## D
Fail Clear
##  Answer
C
## Explanation
In a fail-secure state, the system remains in a high level of security until an administrator intervenes.
## Goal Implication
## Source
Chp 21 Review Question 4
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition. 


# What software development model uses a seven-stage approach with a feedback loop that allows progress one step backward?
## ID
SDLC.b12b3957-0f14-4db8-948f-788d52e18655
## A
Boyce Codd
## B
Waterfall
## C
Spiral
## D
Agile
##  Answer
B
## Explanation
The waterfall model uses a seven-stage approach to software development and includes a feedback loop that allows development to return to the previous phase to correct defects discovered during the subsequent phase.
## Goal Implication
## Source
Chp 21 Review Question 5
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition. 

# Richard believes that a database user is misusing his privileges to gain information about the company’s overall business trends by issuing queries that combine data from a large number of records. What process is the database user taking advantage of?
## ID
SDLC.7f151b2f-6f5b-479d-9d3f-06530922a702
## A
Inference
## B
Contamination
## C
Polyinstantiation
## D
Aggregration
##  Answer
## Explanation
In this case, the process the database user is taking advantage of is aggregation. Aggregation attacks involve the use of specialized database functions to combine information from a large number of database records to reveal information that may be more sensitive than the information in individual records
## Goal Implication
## Source
Chp 21 Review Question 8
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition. 


# What database technique can be used to prevent unauthorized users from determining classified information by noticing the absence of information normally available to them?
## ID
SDLC.2be3aad3-a22e-4a39-9fc4-9a2160e131c3
## A
Inference
## B
Manipulation
## C
Polyinstantiation
## D
Aggegration
##  Answer
C
## Explanation
Polyinstantiation allows the insertion of multiple records that appear to have the same primary key values into a database at different classification levels.
## Goal Implication
## Source
Chp 21 Review Question 9
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition. 


# Which one of the following is not a principle of Agile development?
## ID
SDLC.d7f29669-e2f4-4ff9-bec5-b0db5aa012f8
## A
Satisfy the customer through early and continuous delivery
## B
Business people and developers work together
## C
Pay continuous attention to technical excellence
## D
Prioritize security over other requirements
##  Answer
D
## Explanation
In Agile, the highest priority is to satisfy the customer through early and continuous delivery of valuable software.
## Goal Implication
## Source
Chp 21 Review Question 10
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition. 


# What type of information is used to form the basis of an expert system’s decision-making process?
## ID
SDLC.a72b599e-0e60-4a7d-bef5-6df1ad474b78
## A
A series of weighted layered computations
## B
Combined input from a number of human experts weighted according to past performance
## C
A series of if/then rules codified in a knowledge base
## D
A biological decision-making process that simulates the reasoning process used by the human mind
##  Answer
C
## Explanation
Expert systems use a knowledge base consisting of a series of “if/then” statements to form decisions based on the previous experience of human experts.
## Goal Implication
## Source
Chp 21 Review Question 11
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition. 


# In which phase of the SW-CMM does an organization use quantitative measures to gain a detailed understanding of the development process?
## ID
SDLC.1e7aded0-1c97-4149-8840-bd40d29ba247
## A
initial
## B
repeatable
## C
defined
## D
managed
##  Answer
D
## Explanation
In the Managed phase, level 4 of the SW-CMM, the organization uses quantitative measures to gain a detailed understanding of the development process.
## Goal Implication
## Source
Chp 20 Review Question 12
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition. 


# Which of the following acts as a proxy between an application and a database to support interaction and simplify the work of programmers?
## ID
SDLC.ab759ad0-8e73-40b8-bde4-9bfde359068d
## A
SDLC
## B
ODBC
## C
DSS
## D
Abstraction
##  Answer
B
## Explanation
## Goal Implication
## Source
Chp 20 Review Question 13
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition. 


# In what type of software testing does the tester have access to the underlying source code?
## ID
SDLC.d3c18c67-87f0-4ae2-8b64-2b87c7a6f1a8
## A
Static Testing
## B
Dynamic Testing
## C
Cross Site Scripting
## D
Black Box Testing
##  Answer
A
## Explanation
## Goal Implication
## Source
Chp 20 Review Question 14
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition. 

# What type of chart provides a graphical illustration of a schedule that helps to plan, coordinate, and track project tasks?
## ID
SDLC.58c968d3-9cbd-44f3-ae30-4b9f3a19b653
## A
Gantt
## B
Venn
## C
Bar
## D
PERT
##  Answer
A
## Explanation
A. A Gantt chart is a type of bar chart that shows the interrelationships over time between projects and schedules. It provides a graphical illustration of a schedule that helps to plan, coordinate, and track specific tasks in a project.
## Goal Implication
## Source
Chp 20 Review Question 15
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition. 

# Which database security risk occurs when data from a higher classification level is mixed with data from a lower classification level?
## ID
SDLC.6353856b-0129-4fa6-a425-f442013f3d69
## A
Aggregation
## B
Inference
## C
Contamination
## D
Polyinstantiation
##  Answer
C
## Explanation
Contamination is the mixing of data from a higher classification level and/or need-to-know requirement with data from a lower classification level and/or need-to-know requirement.
## Goal Implication
## Source
Chp 20 Review Question 16
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition. 

# What database security technology involves creating two or more rows with seemingly identical primary keys that contain different data for users with different security clearances?
## ID
SDLC.7857613f-efd6-471a-a1b7-022defccd868
## A
Polyinstantiation
## B
Cell supresssion
## C
Aggregation
## D
Views
##  Answer
A
## Explanation
Database developers use polyinstantiation, the creation of multiple records that seem to have the same primary key, to protect against inference attacks.
## Goal Implication
## Source
Chp 20 Review Question 17
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition. 

# Which one of the following is not part of the change management process?
## ID
SDLC.f2140e07-3f8f-423b-8621-b49663d61efc
## A
Request Control
## B
Release Control
## C
Configuration Audit
## D
Change control
##  Answer
C
## Explanation
Configuration audit is part of the configuration management process rather than the change control process.
## Goal Implication
## Source
Chp 20 Review Question 18
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition. 


# What transaction management principle ensures that two transactions do not interfere with each other as they operate on the same data?
## ID
SDLC.1810d72a-0316-4070-b379-05c5d40c212f
## A
Atomicity
## B
Consistency
## C
Isolation
## D
Durability
##  Answer
C
## Explanation
The isolation principle states that two transactions operating on the same data must be temporarily separated from each other such that one does not interfere with the other.
## Goal Implication
## Source
Chp 20 Review Question 19
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition. 

# Tom built a database table consisting of the names, telephone numbers, and customer IDs for his business. The table contains information on 30 customers. What is the degree of this table?

## ID
SDLC.d59e90a0-89db-4ebb-ac8d-c6b3e507bd42
## A
2
## B
3
## C
Thirty
## D
Undefined
##  Answer
B
## Explanation
The cardinality of a table refers to the number of rows in the table while the degree of a table is the number of columns.
## Goal Implication
## Source
Chp 20 Review Question 20
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition. 