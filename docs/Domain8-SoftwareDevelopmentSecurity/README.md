
Introduction to Software Development Security
Secure Design
Requirements to Writing Secure Code
Software Development Methodologies
Cloud Application Security
OWASP (Open Web Application Security Project)
Organizational Normative Framework
Object Oriented Programming
Database Introduction Part 1
Database Introduction Part 2
Database Introduction Part 3
Database Introduction Part 4
Database Introduction Part 5
Database Introduction Part 6


- Bake In Security
- System Life Cycle
    -  SDLC
            - Plan
            - Requirements
            - Design
            - Implementation
                - Waterfall
                - Agile
                - SecDevOps
            - Testing
                - Canary
                - Certification
            - Deployment
                - Accreditation  - The formal declaration by the Designated Approving Authority (DAA) that an IT system is approved to operate in a particular security mode using a prescribed set of safeguards at an acceptable level of risk.
    - Operation
    - Disposal


Trust - Assurance - Certificiation - Accrediation

- Matury Model
    - Ad Hoc
    - Repeatable
    - Defined
    - Managed
    - Optimized

- Obfuscation
    - Lexical
    - Data
    - Control 

- Software Vulnerabilities
    - Buffer Overflow
    - SQL Injection
    - XSS (Cross Site SCripting)
    - TOCTOU - time of check / time of use (Race Conditions)

- Secure Programming
    - Input validation
    - Session Management
    - Polyinstantiation 


Databases
    - ACID
        - Atomicity
        - Consistency
        - Isolation
        - Durability

## Reference

[NIST](https://doi.org/10.6028/NIST.SP.800-160v1)


Application Hardening
- Use proper authentication
- Encrypt sensitive data
- Validate user input
- Avoid and remediate known exploits

Application Configuration
- Settings
    - type and scope of encryption
    - users with access to the application
    - access granted to authorized users
    - security of underlying infrastructure
- Baselining
    - allow quick configuration of current settings and desired configuration settings

Development Methodologies
 - Quality software requires quality design
 - Models
    - Waterfall - linear rigid development 
        - System requirements
        - Software requirements
        - prelim design
        - detailed design
        - code
        - testing
        - operations
    - Spiral Model
        - Determine Requirements
        - Risk Assessment
        - Dev and Testing
        - Plan Next Phase
    - Agile
        - Individual interactions over processes and tools
        - Working Software over documentation
        - Customer collaboration over contract negotiation
        - Responding to change over following a plan
    

Capability Maturity Models
- Initial
- Repeatable
    - requirements mgt
    - planning
    - QA
    - oversight
    - config mgt
- Defined
    - formal documented practices
    - training programs
    - software engineering
    - peer reviews
- Managed
    - use quantity measures and understand there effectiveness
    - quantitative management
- Optimizing
    - continuous improvement
    - change management

- IDEAL Model
    - Initiating
    - Diagnosing
    - Establishing
    - Action
    - Learning


Operations
- Change Management

- Release Management
    - testing and verify
    - releases

DevOps
- Challenge
    - Developers want to acheive change
    - Operations want to prevent change
- Embraces Automation
- Infrastructure as Code


Cross Site Scripting
- enter script into input fields

SQL Injection
- update input fields so the generated SQL does multiple things
    - changing "select * from users where password='abc'" to  "select * from users where password='abc'; update users set password='hacked'"

Controls Designed to Prevent Privelege Escalation
- DEP - marking memory as non executable
- ASLR - address space layour randomization

Directory Traversal Attack
- Preventation
    - Use input validation
    - use strict file system permissions

Vulnerabilities
- arbitrary code execution
- remote code execution
Protection
- Limit Application Permissions
- Patch systems and application

Risk Analysis
- 


Configure Software Security
- 


`Security is often a trade off between user-friendliness and functionatilty`