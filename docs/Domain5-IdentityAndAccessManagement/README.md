Introduction to Identity and Access Management Part 1
Introduction to Identity and Access Management Part 2
Authentication Types Part 1: Something You Know
Authentication Types Part 2: Token Devices
Authentication Types Part 3: Memory Cards
Authentication Types Part 4: Something You Are
Strong Authentication
Social Media and the Introduction to Kerberos
Kerberos Components
The Kerberos Carnival Part 1
The Kerberos Carnival Part 2



Entity vs Identity
- Phil is an entity but could also be a staff member, could be alumnus and a student
- Identities have different attributes

Entities can be people or systems.

Identity
- who you claim to be
- username
Authentication
- proof that you are who you claim to be
- password
Authorization
- allowed to do something
- Access Control Lists
    - Rows Headings are the subjects
    - Columns are the ACLs
    - Cells are the capabilties for subject

|   Subject | Action |   Students | Configuration |
| --------- | --    | ---    | --- |
| Admin | Read  |  X | X |
| Admin | Write Data | X | X |
| User | Read| X | X |
| User | Write | | |



Authentication Factorys
- Type 1 : Something you know
- Type 2: Something you have
- Type 3 : Something you are

BioMetrics
- False Rejection Rate - Type 1 Error
- False Acceptance Rate - Type 2 Error

One Time Password - Something you known
- HOTP
    - Hmac Based One Time Password 
        - Combines shared secret  and incrementing counter
- TOTP
    - Time Based One Time Password
        - Combines shared secret and time of day
        - Clock sync is required
        - Google Authenticator

- Common Access Card
    - US Department of Defense

Remote Authentication Protocols
- PAP - assword Authentication Protocol - No encryption
- CHAP - Challenge Authentication Protocol - Prior knowledge of shared secret.
    - Each prove they know the secret without actually disclosing it
    - Server provides challenge.
    - Client hashes challenge with secret and returns to server
    - Servers does the same and check hash matches
- EAP - Extensible Authentication Protocol

Federation
    Trusts 
        - Direction
            - One Way
                Domain 2 trusts Domain 1 but not the other way around
            - Two Way
                Domain 2 trust Domain 1 and vice versa
        - Transititivity
            - Transitive
                - Domain 1 trusts Domain 2, while Domain 2 trusts Domain3 result is Domain 1 trusts Domain 3
            - Non Transitive
                - Domain 1 trusts Domain 2, while Domain 2 trusts Domain3 result is Domain 1 still does not trust Domain 3

Centralized Authentication Approaches
- RADIUS - Remote Access Dial In User Service
    - Radius Client is actually an application server or a router
    - Uses UDP
    - Only encrypts password

- TACACS - Terminal Access Controller Access Control System
    TACACS+ - Uses TCP and fully encrypts entire session

Kerberos and LDAP
- Ticket Based
- PORT
    - Kerebos - 88
    - LDAP 389
    - LDAPS 636
- NTLM - Hash Based Challenge Response
    - Weak Encryption
    - Vulnerable to Pass the Hash attach

SAML
    - Concepts
        - Principal
        - Identity
        - Provider
    - Benefits - True SSO
    - No credential access for service provider

Identity as a Service (IDaaS)
    - Directory Integration - Directory Synchronization to corporate on prem or on cloud directory
    - Application Integration 
        - Vendors - OneLogin

OAuth - Authorization Protocol not 

Open Id Connect - Authentication that works with OAuth

Certification Base Authentication
- Keys use for Authentication
    - Server sends random string to client, client encrypts using private key and sends to server, server decrypts using public key and checks its the same
- Basis of 
    - ssh and 
    - PKI based smart cards 
        - CAC
        - PIV
    - 802.1x standard

Accountability -
- Requires 
    - Identification - No shared accounts
    - Authentication - Strong protection of user accounts
    - Auditing - 
    - Logs - written in a secure way

Session Mgt
- use Timeouts and screen savers to disconnect users who have gone idle
- Timeouts - Prevents user accounts from being hijacked
    - time period based logout
    - check for activity and disconnect with no activity
    - require re-authentication for sensitive operations

Account and Privilege Mgt
- Principle of Least Privelege
- Principle of Separation of Duties
- Job Rotation
- Mandatory Vacation

Account Policies
- use Windows Group Policy to configure policies across the organization

Password Difficulty
- length
- complexity - different chars
- mandatory password expiration
- prevent use of previous passwords
- lock users out after to many incorrect attempts

Types of passwords
- Cognitive Passwords - a series of questions and answers

Principle of Least Privelege
- limits to damage that person can do
- restricts damagae an impersonatr can do
Separation of Duties
- performing any critical business function should require 2 people


Authorization
- Mandatory Access Control 
    - example SE Linux
- Discretionary Access Control
    - users can configure permissions as they see fit
    - resource owners set permissions by the creation of Access Control Lists
    - Windows NTFS provided DAC
- Database Access Control
    - SQL Server
        - Local User Accounts
        - Windows User Accounts
        - Mixed Mode - you can use both
    - Authorization 
        - DB Roles
- Controls
    - Default Deny (aka Implicit Deny)
        - if there is no explicit access granted, then deny
    - Rule Base Authorization
    - Role Based Auth
    - Time of Day - rstr


Password Attacks
    - /etc/password - password hashs are vulnerable
        - solution move passwords to /etc/shadow file which has tight restrictions

        - hashes have a collision problem when there is a large sample.
            - Birthday problem
    - Brute Force
    - Dictionary Attacks - try common
    - Rainbow Tables - precompute common passwords

Watering Hole Attacks

Social Engineering Attacks
- social engineernig works for a number of reasons
- Authority - displayin Authority gets people to trust you
- Intimidation -
- Consensus - When we dont know how to behave we look to others
- Scarcity - Get things before they run out
- Urgency - People feel pressured
- Familiarity / Liking  - people want to say yes to people they like

Impersonation Attacks
- SPAM - unsolicited
- Phishing - 
- Spear Phishing - targetted phishing - requires background knowledge 
- Whaling - highly targetted at senior executives
- Pharming - set up a fake website  , use typosquatting
- Voice Phishing - 
- SPIM - spam via instant messaging
- Spoofing - faking the identity of someone else