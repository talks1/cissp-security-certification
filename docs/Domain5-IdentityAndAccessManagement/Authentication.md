# Type of Authentication - Primary Authentication Factors

Type 1: Something you know
Type 2: Something you have
Type 3: Something you are (GPS)

Other types
- Somewhere you are

# Authentication Mechanisms

### PIV 
PIV - A physical smart card contain public private key pair and certificate published by trusted organization unit.
DerivedPIV - A virutal smart card stored on a device.

### Challenge Handshake Authentication Protocol

CHAP

Shared secret
Random authentication
Mutual client and server auhentication
3 way handshake - challenge - response - accept

Based on a username password so is brute forceable unless implemented within TLS connection.

CHAP is used for PPP connection historically
Now is used connecting to WIFI access point (protected EAP)


### One Time Password

Utilized with hard or soft token

#### Synchronous - 
- you are property to enter the current one time password

#### Asynchronous
you are prompted to enter a challege into the token which generates a response which you provide back.
More secure but not common.


### Authentication Assurance Level
1- least good
3- best