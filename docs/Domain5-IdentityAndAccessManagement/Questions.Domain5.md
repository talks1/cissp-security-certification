

# Which of the following issues is NOT addressed by Kerberos?

## ID

IAM.d0d13802-c128-46a9-a13f-d13f341d8f46

## A
Availability

## B
Confidentiality

## C
Integrity

## D
Authentication

##  Answer
A

## Explanation

Kereberos doesnt address how to make the system more robust.

## Goal Implication

How difficult is it for the right people to get access. Dont forget availability.



# Which of the following should NOT considered as key objectives of a smart card system for employees accessing facility and systems?
## ID
IAM.24b9cefe-ed9f-4129-944a-ed9bee2a64ac
## A
Mechanism should provide strong resistance to fraud,  tampering or other exploitation
## B
Mechanism should allow rapid authentication
## C
Credentials are issued by only authorization officials
## D
Credential recipients must be adequately verified prior to credentials being issues
## E
Mechanisms must support SHA-256 and AES-192 or greater.
##  Answer
E
## Explanation
AES-192 might not be involved in the smart card solution at all. All the other make sense for a system.
## Goal Implication

# Organization is replacing username/password with a smart card based system.Which of the following is least likely to be a requirement for issuance of a smart card?

## ID

IAM.ec78fd46-4a5d-462e-8ee4-a21d87d8e14e

## A
Requring person to appear in person for the registration process.

## B
Requring user to provide knowledge of current password

## C
Recording copy of the users fingerprint during the process

## D
Taking a photo of the user to be included on the smart card

## E
Requiring user to provide passport and another form of government id

##  Answer
B

## Explanation
WIth smart card issues we want to associate the current with a specific person and the current password is the thing least directly proving the identity of a person.

## Goal Implication



#  The Health Insurance Portability and Accountability Act (HIPAA), requires that medical providers keep which information private?

## ID

IAM.5158e50b-d1ae-4294-938a-e5f60f7eae33

## A
Medical

## B
Personal

## C
Personal and medical

## D
Insurance

##  Answer
C

## Explanation
Defines PHI (Personal Health Information)

## Goal Implication


# Which of the following is correct
## ID
IAM.26eb28e6-0f62-4916-b878-2dbfee03f9d4
## A
Authorization precedes Identification precedes Authentication
## B
Identification precedes precedes Authorization preceds Authentication
## C
Identification precedes precedes Authentication preceds Authorization
## D
Authentication precedes Authorization precedes Identification
##  Answer
C
## Explanation

## Goal Implication


# Which of the following access control mechanisms allows information owners to control access to resources by evaluating the subject, object and the environment?
## ID
IAM561697b1-1cb4-49cd-bc67-a45bbbe30cb8.
## A
Rule based access control
## B
Attribute based access control
## C
Role based access control
## D
Discretionary Access Control
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=wfDkTHDoPbI



# An organization uses PIV cards for desktop computer logins and physical access and wants to extend PIV-based authentication to the increasing number of company-issued smartphones and tablets. Which of the following will provide the best authentication security and the most seamless user experience?
## ID
IAM.d436fc7e-31bc-4217-9e0f-f276e9063f40
## A
MicroSD authentication tokens
## B
TOTP authentication using key fob or mobile app
## C
Derived PIV credentials stored securely on the device
## D
USB PIV card reader connected to the device
##  Answer
C
## Explanation
Derived PIV is like a virtual card on the device. A software solution.

Other options are more problematic as they are hardware based.
## Goal Implication
## Source
https://www.youtube.com/watch?v=fncd6XVXkdY


# A system in your enterprise does not support individual user passwords but multiple admins require access at least once a month. Which of the solutions is the best to acheive Accountability?
## ID
IAM.3283dee6-1985-45fe-834c-6605e5420732
## A
Isolate the legacy system to its own VLAN
## B
Change the password weekly and share with authorized users
## C
Utilize an enterprise password manager with password sharing features
## D
Remove the system from the network until a replacement can be identified
## E
Assign one admin to perform all tasks on the system
##  Answer
C
## Explanation
Password manager can record the admin requesting password. They can also change the password after each admin has done their work.
## Goal Implication
Implication is that for Aurecon IT we can say that LastPass should be a recommended approach for shared system access.
## Source
https://www.youtube.com/watch?v=jDPPxA6TthQ


# Which of the following are not characteristics of the challenge authentication protocol (CHAP)?
## ID
IAM.770f3370-d056-46f3-8353-5ddd04312277
## A
Challenges are encrypted using a symmetric algorithm
## B
Authentication is negotiated via a 3-way handshake
## C
Authenticator will randomly require re-authentication
## D
CHAP support mutual authentication by client and server
##  Answer
A
## Explanation
The challenge is hashed using the shared secret rather than encrypted.

The 3 way hanshake is challenge-response-accept

## Goal Implication
## Source
https://www.youtube.com/watch?v=P7j3lJDMFgg



# Which of the following are not principles of access control
## ID
IAM.c99147e4-2de5-4612-9186-a9490e03b317
## A
Principle of least privilege
## B
Locards Principle
## C
Need to know
## D
Separation of duties
## E
Tranquility principle
## F
Principle of accountability
##  Answer
C,E

## Explanation
Locards principle related to attackers always leaving some evidence
Tranquility principle relates to access control rules that are invariant

Principle of accountability is not a principle of access control

## Goal Implication
## Source
https://www.youtube.com/watch?v=BUcoABZzeQ4


# Select the authentication mechanisms which are characteristic based
## ID
IAM.4bd05ed4-c3f3-4186-b45e-f1d1d2a8a343
## A
Password
## B 
Rentinal Scan
## C
Soft token
## D
vascular pattern scanner
##  Answer
B,D
## Explanation
Characteristic based are some you are...biometric
## Goal Implication
## Source
I made this up

# Which of the following is not a mitigation technique that can be used to protect an application and its data.
## ID
SRM.82e6bd9f-2557-44c5-9ba9-b89c831e3a13
## A
Fuzzing
## B
ACLs
## C
Digital Signatures
## D
Disk Quotas
## E
Encryption
## F
Secure Logging

##  Answer
A
## Explanation
Fuzzing is a technique to spam an application with lots of random inputs to check if there are some vulnerabilities.  THis is not a migitation technique.
Interestingly I wouldnt consider 'secure logging' a migitation technique but this was indicated.
## Goal Implication
## Source
https://www.youtube.com/watch?v=6IIWcS8iye0


# Which resource-intensive access control mechanism implemented in databases, can allow/deny access to an object based on the data the object contains?
## ID
IAM.f49428d3-894d-4c25-aaa8-3665d739df19
## A
Discretionary Access Control
## B
Mandatory Access Control
## C
Role Based Acess Control
## D
View Based Access Control
## E
Content Dependent Access Control
## F
Context Based Access Control
##  Answer
E
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=WJWvcYv--OY


# What is an advantage of content-dependent access control in databases?
## ID
IAM.96369afd-0a44-4936-8ae8-2782b13ccf92
## A
Processing overhead.
## B
It ensures concurrency.
## C
It disallows data locking.
## D
Granular control.
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.briefmenow.org/isc2/what-is-an-advantage-of-content-dependent-access-control-in-databases/



# If security was not part of the development of a database, how is it usually handled?
## ID
IAM.80b59f91-e8a6-47ea-bf7f-aae9b60c6680
## A
Through cell suppression
## B
By a trusted back end
## C
By a trusted front end
## D
By views
##  Answer
C
## Explanation
Have to ensure security in the front end to compensate for the back end
## Goal Implication
## Source
https://www.briefmenow.org/isc2/if-security-was-not-part-of-the-development-of-a-database-how-is-it-usually-handled/

# The Active Directory domain adminstrator has created a security group called 'R&D- Project Z' added members of the group 'Project Z' team to the group. He then configures a Group Policy Object (GPO) that allows only that group  to acess 'Project Z' servers. What type of access control is this an example of?
## ID
IAM.4267a455-0292-4a85-be62-44f74fb8bbfa
## A
Discretionary
## B
Context-Dependent
## C
Non-Discretionary
## D
View Based
##  Answer
C
## Explanation
Non-Discretionary is another name for Role Based Access Control
## Source
https://www.youtube.com/watch?v=WJWvcYv--OY

# Which of the following is an example of multi-factor authentication
## ID
IAM.40b54081-909d-4fd1-93a3-674b1c43707e
## A
A split knowledge system
## B
A username with an iris scanner
## C
A smartcard and a PIN
## D
A password and a PIN
## E
A passphrase and a CAPTCHA challenge
## F
A passphrase and a pre-shared key
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=KLlz1SY7qGo&t=43s


# X.509 is the standard for which of the following?
## ID
IAM.8aa1c3e6-f08a-4695-b5ca-232a5c0fd9de
## A
Kerberos
## B
PKI Certificates
## C
Directory Services
## D
Diffie-Helman
## E
Risk Management Framework
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=KQ3qIRmJdbw

# One important criteria with biometrics is how acceptable they will be to your work force. Of the following authentication types which is most likely to be met with strong resistance from the average user.
## ID
IAM.87807170-05c0-4866-b498-59b06b16382b
## A
Iris Scan
## B
Hand Geometry
## C
Palm Scan
## D
Fingerprint Scan
## E
Retina Scan
## F
Voice Analysis
## G
Signature Dyanmics
##  Answer
E
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=5_YqDgPJhW0&list=RDCMUCwUkAunxT1BNbmKVOSEoqYA&index=4


# During what phase of the access control process does a user prove his or her identity?
## ID
IAM.b4e9ee30-4a94-4286-9c4e-2c6d4b2dbdab
## A
Authorization
## B
Identification
## C
Authentication
## D
Remediation
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4626246?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# How many separate roles are involved in a properly implemented registration and identity proofing process?
## ID
IAM.74d782ed-a6a3-4fcd-bf2d-8f2366fa4729
## A
1
## B
2
## C
3
## D
4
##  Answer
D
## Explanation
Request, Approval, Identity Proof and Issuance
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4626245?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Which one of the following is an example of multifactor authentication?
## ID
IAM.0f947655-5374-4afd-84b4-c74eeced675f
## A
ID Card and PIN
## B
retinal scan and fingerprint
## C
password and security questions
## D
ID Card and Key
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4625518?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Jane uses an authentication token that requires her to push a button each time she wishes to login to a system. What type of token is she using?
## ID
IAM.7c8da458-e0de-4714-9dd4-1d6aceee12b1
## A
HOTP
## B
SSL
## C
HMAC
## D
TOTP
##  Answer
A
## Explanation
HOTP - HMAC based One Time Password  uses shared secret and sequence number
TOTP - Uses shared secret and time of day
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4625518?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Which authentication protocol requires the use of external encryption to protect passwords?
## ID
IAM.1b8a32cb-5f08-4057-bef7-7955d3f9756b
## A
SAML
## B
PAP
## C
CHAP
## D
Kerberos
##  Answer
B
## Explanation
PAP has no encryption and therefore requires encryption in addition to its use
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4625518?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Which choice is not an example of federated authentication?
## ID
IAM.1af7f6d7-bd35-4788-bc44-d633b05ed5e0
## A
Google Accounts
## B
Twitter Accounts
## C
Facebook Connect
## D
RADIUS
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4625518?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Ricky would like to use an authentication protocol that fully encrypts the authentication session, uses the reliable TCP protocol and will work on his Cisco devices. What protocol should he choose?
## ID
IAM.47b9a1e4-774a-42f3-ba59-a04cf0893848
## A
TACACS
## B
RADIUS
## C
XTACACS
## D
TACACS+
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4625518?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# In the Kerberos protocol, what system performs authentication of the end user?
## ID
IAM.5b1b533c-12af-4a16-8927-8268092a3767
## A
TGT
## B
SS
## C
TGS
## D
AS
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4625518?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# In SAML, what organization performs authentication of the end user?
## ID
IAM.9342a92e-143e-40e7-a459-44f73f3e94b2
## A
Service Provider
## B
Principal
## C
Identity Provider
## D
Authentication Source
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4625518?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Which of the following are required for accountability?
## ID
IAM.53804faf-84c0-4d62-993b-299c33fae693
## A
Identification
## B
Authorization
## C
Authentication
## D
Confidentiality
## E
Secure Logging
## F
Auditing
## G
Scalability
## H
Availability
##  Answer
A,C,E,F
## Explanation
## Goal Implication
## Source
PT

# Which of the following are NOT a mechanism to increase account security
## ID
IAM.a1969183-1ede-4460-a003-858632776137
## A
Enforce minimum password length
## B
Enforce password expiry after set periods
## C
Enforce minimum password complexity
## D
Prevent password reuse on reset
## E
Require re-authentication on set time periods
## F
Lock out users if too many incorrect attempts occur
##  Answer
E
## Explanation
Require re-authentication on set time periods prevents session mis management but is not related to account security
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/password-policies?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Which of the following is not an important account management practice for security professionals?
## ID
IAM.68ce9b3a-4633-4371-a972-4b160917b052
## A
Privilege Creep
## B
Separation of Duties
## C
Least Privilege
## D
Mandatory Vacations
##  Answer
A
## Explanation
Privilege Creep is a symptom of inappropriately managing privileges while the others are practives for good account management.
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4624770?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What Windows mechanism allows the easy application of security settings to groups of users?
## ID
IAM.682a4be3-9472-4a58-956d-9706a100ba27
## A
SCEP
## B
GPO
## C
MMC
## D
ADUC
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4624770?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Which one of the following is not a normal account activity attribute to monitor?
## ID
IAM.b9dc3219-1d5a-427f-a97d-cf8afb2ea42b
## A
Login Time
## B
Incorrect Login Attempts
## C
Password
## D
Login Location
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4624770?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Tobias recently permanently moved from a job in accounting to a job in human resources but never had his accounting privileges revoked. What situation occurred in this case?
## ID
IAM.d0dbe92f-84ab-402c-96b1-899329d87b86
## A
Job Rotation
## B
Least Privilege
## C
Privilege Creep
## D
Separation of Duties
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4625517?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What command can administrators use to determine whether the SELinux kernel module is enabled?
## ID
IAM.bb58de21-d4eb-4cd5-9abc-530187f2a069
## A
secheck
## B
selmodule
## C
getenforce
## D
fsck
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4625517?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# In a discretionary access control system, individual users have the ability to alter access permissions.
## ID
IAM.893d3c77-2980-426e-9386-0a619f3974e9
## A
True
## B
False
##  Answer
A
## Explanation
Mandatory Access Control - users cannot change permissions
With Discretionary Access Controls - they can
## Goal Implication
## Source


# What file permission does NOT allow a user to launch an application?
## ID
IAM.9b6a4de5-fb7c-473b-bd8b-377f1bc28d6d
## A
read and execute
## B
read
## C
modify
## D
full control    
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4625517?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Dan is engaging in a password cracking attack where he uses precomputed hash values. What type of attack is Dan waging?
## ID
IAM.bb53e966-5c73-42ed-87af-23b8fe981099
## A
Rainbow Tables
## B
Hybrid
## C
Brute Force
## D
Dictionary
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4624771?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What type of website does the attacker use when waging a watering hole attack?
## ID
IAM.65aa0101-b702-47cc-8c80-c52db17d4cce
## A
software distribution site
## B
known malicious site
## C
site trusted by the end user
## D
hacker forum
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4624771?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# A social engineer calls an administrative assistant in your organization and obtains her password by threatening her that her boss' account will be deleted if she does not provide the password to assist with troubleshooting. What type of attack is the social engineer using?
## ID
IAM.cff2b913-4c3f-40cf-b1f1-4c085e90b40e
## A
scarcity
## B
liking
## C
social proof
## D
intimidation
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4624771?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What type of phishing attack focuses specifically on senior executives of a targeted organization?
## ID
IAM.df358c4c-0ae4-42fc-95c2-5c6d4c119f7e
## A
vishing
## B
spear phishing
## C
whaling
## D
pharming
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-5-identity-and-access-management-2018/quiz/urn:li:learningApiAssessment:4624771?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Which of the following would not be an asset that an organization would want to protect with access controls? 
## ID
IAM.6a5039d2-ce92-4753-8c9a-72af3de33b46
## A
Information
## B
Systems
## C
Devices
## D
Facilities
## E
None of the above
##  Answer
E
## Explanation
## Goal Implication
## Source
Chp 13 Review Question 1
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 618). Wiley. Kindle Edition. 

# Which of the following is true related to a subject? A subject is always a user account. The subject is always the entity that provides or hosts the information or data. The subject is always the entity that receives information about or data from an object. A single entity can never change roles between subject and object.
## ID
IAM.8ede983b-5750-4d24-a373-d3999224f448
## A
A subject is always a user account
## B
The subject is always the entity that hosts information or data
## C
The subject is always the entity that receives information about or data from an object
## D
A single entity can never change roles between subject and object
##  Answer
C
## Explanation
C. The subject is active and is always the entity that receives information about, or data from, the object. A subject can be a user, a program, a process, a file, a computer, a database, and so on. The object is always the entity that provides or hosts information or data. The roles of subject and
## Goal Implication
## Source
Chp 13 Review Question 2
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 618). Wiley. Kindle Edition. 


# Which of the following types of access control uses fences, security policies, security awareness training, and antivirus software to stop an unwanted or unauthorized activity from occurring?
## ID
IAM.46d03007-1312-48cf-ab82-8b6025d25020
## A
Preventative
## B
Detective
## C
Corrective
## D
Authoritative
##  Answer
A
## Explanation
## Goal Implication
## Source
Chp 13 Review Question 3
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 618). Wiley. Kindle Edition. 

# What type of access controls are hardware or software mechanisms used to manage access to resources and systems, and provide protection for those resources and systems?
## ID
IAM.e37e705c-a3c5-4195-af85-431d0a92b4bf
## A
Administrative
## B
Logical/Technical
## C
Physical
## D
Preventative
##  Answer
B
## Explanation
## Goal Implication
## Source
Chp 13 Review Question 4
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 618). Wiley. Kindle Edition. 


# Which of the following best expresses the primary goal when controlling access to assets?
## ID
IAM.092c642f-540d-4974-aa7e-b4a9d15f41cb
## A
Preserve Confidentiality, integrity, and availability of systems and data
## B
Ensure on valid objects can authenticate in the system
## C
Prevent unauthorized access to subjects
## D
Ensure that all subjects are authenticated
##  Answer
A
## Explanation
A primary goal when controlling access to assets is to protect against losses, including any loss of confidentiality, loss of availability, or loss of integrity. Subjects authenticate on a system, but objects do not authenticate. Subjects access objects, but objects do not access subjects. Identification and authentication is important as a first step in access control, but much more is needed to protect assets.
## Goal Implication
## Source
Chp 13 Review Question 5
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 618). Wiley. Kindle Edition. 

# A user logs in with a login ID and a password. What is the purpose of the login ID?
## ID
IAM.bf7b94d4-1ceb-4500-8a1a-f35d690fc0e8
## A
Authentication
## B
Authorization
## C
Accountability
## D
Identification
##  Answer
D
## Explanation
## Goal Implication
## Source
Chp 13 Review Question 6
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 618). Wiley. Kindle Edition. 

# Accountability requires all of the following items except one. Which item is not required for accountability?
## ID
IAM.0ac0ec8a-9e02-4870-be07-2468b83b3a0e
## A
Identitication
## B
Authentication
## C
Auditing
## D
Authorization
##  Answer
D
## Explanation
## Goal Implication
## Source
Chp 13 Review Question 7
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 618). Wiley. Kindle Edition. 

# Which of the following best identifies the benefit of a passphrase?
## ID
IAM.0fafaf7f-f9aa-4d67-a04d-ba77fc634bf7
## A
It is short
## B
It is easy to remember
## C
It includes a single set of characters
## D
It is easy to crack
##  Answer
B
## Explanation
## Goal Implication
## Source
Chp 13 Review Question 8
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 618). Wiley. Kindle Edition. 

# Which of the following is an example of a Type 2 authentication factor?
## ID
IAM.d0ef1137-a405-4f70-8ba6-17e2df588e18
## A
Something you have
## B
Something you are
## C
Something you do
## D
Something you know
##  Answer
A
## Explanation
A Type 2 authentication factor is based on something you have, such as a smartcard or token device. Type 3 authentication is based on something you are and sometimes something you do, which uses physical and behavioral biometric methods. Type 1 authentication is based on something you know, such as passwords or PINs.
## Goal Implication
## Source
Chp 13 Review Question 10
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 618). Wiley. Kindle Edition. 

# Your organization issues devices to employees. These devices generate onetime passwords every 60 seconds. A server hosted within the organization knows what this password is at any given time. What type of device is this?
## ID
IAM.a48f6c2b-0544-4b67-b3f7-879cbedec1ca
## A
Synchronous Token
## B
Asynchronous Token
## C
Smartcard
## D
Common Access Card
##  Answer
A
## Explanation
A synchronous token generates and displays onetime passwords, which are synchronized with an authentication server. An asynchronous token uses a challenge-response process to generate the onetime password. Smartcards do not generate onetime passwords, and common access cards are a version of a smartcard that includes a picture of the user.
## Goal Implication
## Source
Chp 13 Review Question 11
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 618). Wiley. Kindle Edition. 

# What does the CER for a biometric device indicate?
## ID
IAM.dda87e4d-a132-4596-bd50-884365717aaa
## A
The sensitivity is to high
## B
The sensitivity is to low
## C
It indicates the point where false rejection rate equals false acceptance rate
## D
When high enought, the biometric device is highly accurate
##  Answer
C
## Explanation
The point at which the biometric false rejection rate and the false acceptance rate are equal is the crossover error rate (CER). It does not indicate that sensitivity is too high or too low. A lower CER indicates a higher-quality biometric device, and a higher CER indicates a less accurate device.
## Goal Implication
## Source
Chp 13 Review Question 13
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 620). Wiley. Kindle Edition. 

# What is the primary purpose of Kerberos?
## ID
IAM.fe58e997-4925-4850-8273-cbb52add565b
## A
Confidentiality
## B
Integrity
## C
Authentication
## D
Accountability
##  Answer
C
## Explanation
## Goal Implication
## Source
Chp 13 Review Question 15
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 620). Wiley. Kindle Edition. 

# Which of the following is the best choice to support a federated identity management (FIM) system?
## ID
IAM.2496fbc4-8b48-4c31-9df6-cff19fd42710
## A
Kerberos
## B
HTML
## C
XML
## D
SAML
##  Answer
D
## Explanation
## Goal Implication
## Source
Chp 13 Review Question 16
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 620). Wiley. Kindle Edition. 

# What is the function of the network access server within a RADIUS architecture?
## ID
IAM.7af826d2-2d5c-490b-a2a8-40a04dbdb64e
## A
Authentication Server
## B
Client
## C
AAA server
## D
Firewall
## E
##  Answer
B
## Explanation
The network access server is the client within a RADIUS architecture. The RADIUS server is the authentication server and it provides authentication, authorization, and accounting (AAA) services. The network access server might have a host firewall enabled, but that isn’t the primary function.
## Goal Implication
## Source
Chp 13 Review Question 17
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 620). Wiley. Kindle Edition. 

# Which of the following AAA protocols is based on RADIUS and supports Mobile IP and VoIP?
## ID
IAM.7afb7b76-27b1-4425-8bb4-592e67ca03de
## A
Distributed access control
## B
Diameter
## C
TACACS+
## D
TACACS
##  Answer
B
## Explanation
Diameter is based on Remote Authentication Dial-in User Service (RADIUS), and it supports Mobile IP and Voice over IP (VoIP). Distributed access control systems such as a federated identity management system are not a specific protocol, and they don’t necessarily provide authentication, authorization, and accounting. TACACS and TACACS+ are authentication, authorization, and accounting (AAA) protocols, but they are alternatives to RADIUS, not based on RADIUS.
## Goal Implication
## Source
Chp 13 Review Question 18
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 620). Wiley. Kindle Edition. 

# Which of the following best describes an implicit deny principle?
## ID
IAM.d4bc9d4c-2e78-498d-8f7a-b577c6369983
## A
All actions that are not expressly denied are allowed
## B
All actions that are not expressly allowed are denied
## C
All actions must be expressly denied
## D
None of the above
##  Answer
B
## Explanation
The implicit deny principle ensures that access to an object is denied unless access has been expressly allowed (or explicitly granted) to a subject. It does not allow all actions that are not denied, and it doesn’t require all actions to be denied.
## Goal Implication
## Source
Chp 14, Review Question 1
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition. 

# What is the intent of least privilege?
## ID
IAM.fd8fa020-0e29-43d5-a1dd-344e2254b2b1
## A
Enforce the most restrictive rights required by users to run system processes.
## B
Enforce the least restrictive rights required by users to run system processes.
## C
Enforce the most restrictive rights required by users to complete assigned tasks.
## D
Enforce the least restrictive rights required by users to complete assigned tasks.
##  Answer
C
## Explanation
The principle of least privilege ensures that users (subjects) are granted only the most restrictive rights they need to perform their work tasks and job functions. Users don’t execute system processes. The least privilege principle does not enforce the least restrictive rights but rather the most restrictive rights.
## Goal Implication
## Source
Chp 14, Review Question 2
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition. 

# A table includes multiple objects and subjects and it identifies the specific access each subject has to different objects. What is this table?
## ID
IAM.8c37636c-359e-43ae-ac6d-074e6db84b7c
## A
Access Control List
## B
Access control matrix
## C
Federation
## D
Creeping Privilege
##  Answer
B
## Explanation
An access control matrix includes multiple objects, and it lists subjects’ access to each of the objects. A single list of subjects for any specific object within an access control matrix is an access control list. A federation refers to a group of companies that share a federated identity management system for single sign-on. Creeping privileges refers to the excessive privileges a subject gathers over time.
## Goal Implication
## Source
Chp 14, Review Question 3
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition. 

# Who, or what, grants permissions to users in a DAC model?
## ID
IAM.3b70e3de-19b1-489d-b037-1094c97adcba
## A
Administrators
## B
Access Controls list
## C
Assigned Labels
## D
The data customers
##  Answer
D
## Explanation
The data custodian (or owner) grants permissions to users in a Discretionary Access Control (DAC) model.
## Goal Implication
## Source
Chp 14, Review Question 4
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition. 

# Which of the following models is also known as an identity-based access control model?
## ID
IAM.d3c14785-e47a-46ca-a7a7-f9c31f2c19e2
## A
DAC
## B
RBAC
## C
Rule-based access control
## D
MAC
##  Answer
A
## Explanation
A Discretionary Access Control (DAC) model is an identity-based access control model. It allows the owner (or data custodian) of a resource to grant permissions at the discretion of the owner. The Role Based Access Control (RBAC) model is based on role or group membership. The rule-based access control model is based on rules within an ACL. The Mandatory Access Control (MAC) model uses assigned labels to identify access.
## Goal Implication
## Source
Chp 14, Review Question 4
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition. 

# A central authority determines which files a user can access. Which of the following best describes this?
## ID
IAM.f5b7b065-2934-4c24-a0b9-dfdbb49dbb86
## A
Access Control List
## B
An access control matrix
## C
Discretionary Access Control model
## D
Non discretionary access control model
##  Answer
D
## Explanation
A nondiscretionary access control model uses a central authority to determine which objects (such as files) that users (and other subjects) can access. 
## Goal Implication
## Source
Chp 14, Review Question 5
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition. 

# Which of the following statements is true related to the RBAC model?
## ID
IAM.8688b741-2aa5-4a1a-a0e6-9a5bbc246daf
## A
A RBAC model allows users membership in multiple groups.
## B
A RBAC model allows users membership in a single group.
## C
A RBAC model is nonhierarchical.
## D
A RBAC model uses labels.
##  Answer
A
## Explanation
A. The Role Based Access Control (RBAC) model is based on role or group membership, and users can be members of multiple groups. Users are not limited to only a single role. RBAC models are based on the hierarchy of an organization, so they are hierarchy based. The Mandatory Access Control (MAC) model uses assigned labels to identify access.
## Goal Implication
## Source
Chp 14, Review Question 8
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition. 

# Which of the following is the best choice for a role within an organization using a RBAC model?
## ID
IAM.369e9b55-ec3e-4288-a735-4f988ff5bf01
## A
Web Server
## B
Application
## C
Database
## D
Programmer
##  Answer
D
## Explanation
A programmer is a valid role in a Role Based Access Control (RBAC) model. Administrators would place programmers’ user accounts into the Programmer role and assign privileges to this role. Roles are typically used to organize users, and the other answers are not users.
## Goal Implication
## Source
Chp 14, Review Question 9
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition. 


# What type of access control model is used on a firewall?
## ID
IAM.97b87f54-80fd-480b-bb33-c4b436669ca3
## A
MAC Model
## B
DAC Model
## C
Rule Based Access Model
## D
RBAC Model
##  Answer
C
## Explanation
Firewalls use a rule-based access control model with rules expressed in an access control list. A Mandatory Access Control (MAC) model uses labels. A Discretionary Access Control (DAC) model allows users to assign permissions. A Role Based Access Control (RBAC) model organizes users in groups.
## Goal Implication
## Source
Chp 14, Review Question 10
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition. 


# What type of access controls rely on the use of labels?
## ID
IAM.ea5c8909-852b-4e75-a88a-488b5001d16d
## A
DAC
## B
NonDiscretionary
## C
MAC
## D
RBAC
##  Answer
C
## Explanation
Mandatory Access Control (MAC) models rely on the use of labels for subjects and objects. Discretionary Access Control (DAC) models allow an owner of an object to control access to the object. Nondiscretionary access controls have centralized management such as a rule-based access control model deployed on a firewall. Role Based Access Control (RBAC) models define a subject’s access based on job-related roles.
## Goal Implication
## Source
Chp 14, Review Question 12
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition.


# Which of the following best describes a characteristic of the MAC model?
## ID
IAM.78dcb889-35c7-4238-9319-56e907986260
## A
Employs Explicit-Deny philosophy
## B
Permissive
## C
Rule Based
## D
Prohibitive
## E
##  Answer
D
## Explanation
The Mandatory Access Control (MAC) model is prohibitive, and it uses an implicit-deny philosophy (not an explicit-deny philosophy). It is not permissive and it uses labels rather than rules.
## Goal Implication
## Source
Chp 14, Review Question 13
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition.

# Which of the following is not a valid access control model?
## ID
IAM.97a52d4a-818c-4e66-8388-dd022e09c20d
## A
DAC
## B
Nondiscretionary
## C
MAC
## D
Compliance Based Access control model
##  Answer
D
## Explanation
Compliance-based access control model is not a valid type of access control model. The other answers list valid access control models.
## Goal Implication
## Source
Chp 14, Review Question 14
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition.

# What would an organization do to identify weaknesses?
## ID
IAM.269dcb72-80ae-4bb5-a81a-5225e904933e
## A
Asset Valuation
## B
Threat Modeling
## C
Vulnerability Analysis
## D
Access Review
##  Answer
C
## Explanation
A vulnerability analysis identifies weaknesses and can include periodic vulnerability scans and penetration tests. Asset valuation determines the value of assets, not weaknesses. Threat modeling attempts to identify threats.
## Goal Implication
## Source
Chp 14, Review Question 15
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition.

# Which of the following can help mitigate the success of an online brute-force attack?
## ID
IAM.ba1829a4-557f-4c3c-9257-9045f2487510
## A
Rainbow Table
## B
Account Lockout
## C
Salting Passwords
## D
Encryption of Passwords
##  Answer
B
## Explanation
An account lockout policy will lock an account after a user has entered an incorrect password too many times, and this blocks an online brute-force attack. Attackers use rainbow tables in offline password attacks. Password salts reduce the effectiveness of rainbow tables. Encrypting the password protects the stored password but isn’t effective against a brute-force attack without an account lockout.
## Goal Implication
## Source
Chp 14, Review Question 16
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition.

# Which of the following would provide the best protection against rainbow table attacks?
## ID
IAM.42d73c30-5a31-4744-8d32-aa64e7fe5e93
## A
Hashing Passwords with MD5
## B
Salt and Pepper with Hashing
## C
Account Lockout
## D
Implement RBAC
##  Answer
B
## Explanation
Using both a salt and pepper when hashing passwords provides strong protection against rainbow table attacks. MD5 is no longer considered secure, so it isn’t a good choice for hashing passwords. Account lockout helps thwart online password brute-force attacks, but a rainbow table attack is an offline attack. Role Based Access Control (RBAC) is an access control model and unrelated to password attacks.
## Goal Implication
## Source
Chp 14, Review Question 17
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition.

# Management wants to ensure that the consultant has the correct priorities while doing her research. Of the following, what should be provided to the consultant to meet this need?
## ID
IAM.1f19865a-48c0-49c1-bc88-74a54dd69c09
## A
Asset Valuation
## B
Threat Modeling Results
## C
Vulnerability Analaysis Reports
## D
Audit Trails
##  Answer
A
## Explanation
Asset valuation identifies the actual value of assets so that they can be prioritized. For example, it will identify the value of the company’s reputation from the loss of customer data compared with the value of the secret data stolen by the malicious employee. None of the other answers is focused on high-value assets. Threat modeling results will identify potential threats.
## Goal Implication
## Source
Chp 14, Review Question 17
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 657). Wiley. Kindle Edition.

