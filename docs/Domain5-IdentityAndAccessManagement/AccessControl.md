
# Access Controls 
Enables management to specificy which users can access which systems and perform which operations and accountability of this activity.


## Reference Monitor Concept 

Subject - entity wanting to access
Object - service to be accessed
Rules - 
Logging and Montoring

Implemented in a security kernel


## Principle of Access Controls

- Separation of Duties
- Need to know
- Principle of Least Privilege

## Access Control Service

Identification
Authentication
Authorization
Auditing


