# Confidentiality

vs Disclosure

# Integrity

vs Alteration

# Availability

vs Destruction

DOS and DDOS are the attacks which directly target availabity

There is a related concept in Data Classification which is criticality. Critical information needs to be made available so it can be acted upon moreso that keeping it secret.

# Notes

The opposite of CIA is DAD

It is easy to get 2 of the 3.  Availability is an opposing force to Confidentiality and Integrity
