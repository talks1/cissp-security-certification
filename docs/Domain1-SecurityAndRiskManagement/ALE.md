


# ALE
Annualized Loss Expectancy

ALE = ARO * SLE 

# ARO

Annualized rate of occurance

If something happens multiple times in a year the number is greater than 1
If something happens less than once a year the number is less than one

# SLE

Single Loss Expectancy

SLE = Asset Value * Exposure Factor

# EF
Exposure Facture

Percent of asset at risk due to threat


