
# Mechanism Type of Control

- Preventative - to stop unwanted or unauthorized activity from occurring
- Detective - to discover unwanted or unauthorized activity
- Corrective - to restore systems to normal after an unwanted or unauthorized activity has occurred
- Deterrent - to discourage violation of security policies by individuals
- Recovery - to repair or restore resources
- Compensating - 
- Directive - to direct, confine, or control the action of subjects to force or encourage compliance with security policy.


Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 615). Wiley. Kindle Edition. 

# Application Type of Control

## Adminstrative

- Background Check
- Policies

## Technical

- Encryption
- Smart Cards

## Physical

- Locks
- Mantraps
- Cable protection
