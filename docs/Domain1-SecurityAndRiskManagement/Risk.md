## Terms
### Acceptable Risk

A sutiable level of risk commensurate with the potential benefits of the organizations operations as determined by senior management.

### Audit

Tools, processes and activities to review compliance

## Availability

Providing timely and reliable access to and use of information 

### Business Continuity

Actions, processes and tools for ensuring an organization can continue critical operations during an emergency

### BIA 

Business Impact Assessment 

Prioritized list of processes and services essential to the business

[NIST BIA](https://csrc.nist.gov/glossary/term/business_impact_analysis)

### Compliance

Adherence to a mandate, both the actions and the tools, processes and actions in adherence

### Confidentiality
Preserving authorized restrictions on informationa access and disclosure


## Roles

# Risk = Threat x Vulnerability

Quantitative Risk Analysis

Qualititative Risk Analysis



# Information Security Risk Management

ISRM


# Asset Value

Value of an asset

# Vulnerability

Absence of a safeguard

# Threat

Somthing that could pose loss to all of part of an asset

# Risk 

Probability that a threat materializes

# Controls

##
- Physical
- Technical

## Types

- Safeguards
- Countermeasures

## 
- Total Risk 
- Residual Risk
