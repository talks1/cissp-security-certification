

# Which of the following is a violation of the principles of least privelege (choose 2)?

## Id

SRM.d7a4a7cc-7ada-4092-9f8e-6fb76b0e8e1c

## A
Given an auditor read and write permissions for system log files?

## B
Installing access control units on elevators limting staff to job-related floors

## C
Requiring users to enter only a username and password to log into a system

## D
Placing a linux subsystem in the Domain Admins group in Active Directory

## E
Granting software developers access to production systems

##  Answer
- A
- D

## Explanation

I am tempted to answer E but the rationale that D is better is that E is more about separation of duties than violation of least privilege.

## Goal Implication

Principle of least privilege is problematic in a number of ways
- its expensive
- it requires excessive foresight to know what a person is expected to do and not do
- it is slow as generally granting someone access to something is a slow process because it requires coordination


# Your risk analysis team is overwhelmed with the process of information gathering and calculating values for 'what if' scenarios. Which is the best course of action.

## Id

SRM.b1363405-31a5-4d46-a602-54e02c6603a0

## A
Increase the size of the team

## B
Switch to a fully qualitative approach

## C
Implement automated risk analysis tool

## D
Re-evalute the scope of the RA teams work

## E
Limit the number of risks considered.

##  Answer
- C

## Explanation

Apparently there are helpful automated risk management tools which can multiple team effort to consider a wider set of information.

## Goal Implication

I am interested in what risk management tools can do and what data they bring to bear.
Is there an opportunity for our team to develop risk management tool.

Does our company have a risk management tool


# Which of the following would not be consider an indication of attack

## Id
SRM.a05df008-d665-4558-b048-af9ac10d8ddb
## A
Detection of an ongoing spear phishing campaign against employees
## B
A NIDS detects a buffer overflow exploit in an inbound packet
## C
Unusual amounts of ssh traffic leaving a network
## D
Log files show the same username/password trying to log in to 20 different servers in 20 seconds
## E
A zero day exploit has been identified for software widely used in your organization
##  Answer
C
## Explanation
## Goal Implication
I am interested in what risk management tools can do and what data they bring to bear.
Is there an opportunity for our team to develop risk management tool.

Does our company have a risk management tool
# Which of the following is NOT an element of a security planning mission statement?

## Id
SRM.5a9c811e-9cb2-448f-9542-15de619ab328
## A
Objectives Statement

## B
Background Statement

## C
Scope Statement

## D
Confidentiality Statement


##  Answer
- D

## Explanation

## Goal Implication




# In data processing systems, the value analysis should be performed in terms of which three properties?

## Id

SRM.ed16244f-a2e6-45e3-acef-57434d43e196

## A
Profit, loss, ROI

## B
Intentional, accidental, natural disaster

## C
Assets, personnel, services provided

## D
Availability, integrity, confidentiality

##  Answer
D

## Explanation

A relates to considerations of  business management.
B related to considers of threat analysis.
C relates to business impact analysis.

## Goal Implication




# Which of the following techniques MOST clearly indicates whether specific risk reduction controls should be implemented?

## Id

SRM.3316234c-e3d1-4cf0-b51f-9a5d692bcfdb

## A
Threat and vulnerability analysis.

## B
Risk evaluation.

## C
ALE calculation.

## D
Countermeasure cost/benefit analysis.

##  Answer
- D

## Explanation

ALE only consider impact but doesnt consider costs to mitigate risk

## Goal Implication



# Place the following four elements of the Business Continuity Plan in the proper order.?

## Id

SRM.66aab13a-2ade-4d82-8ee1-161156a0d90f

## A
Scope and plan initiation, plan approval and implementation, business impact assessment, business continuity plan development

## B
Scope and plan initiation, business impact assessment, business continuity plan development, plan approval and implementation

## C
Business impact assessment, scope and plan initiation, business continuity plan development, plan approval and implementation

## D
Plan approval and implementation, business impact assessment, scope and plan initiation, business continuity plan development

##  Answer
- B

## Explanation

Implement is last.
Scoping is first because its a management thing which does things like defined roles and responsibilities.

## Goal Implication




# Houston-based Sea Breeze Industries has determined that there is a possibility that it may be hit by a hurricane once every 10 years. The losses from such an event are calculated to be $1 million. What is the SLE for this event?

## Id

SRM.b2a2b036-9fc3-4784-bf2c-686b82d08886

## A
$1 million

## B
$10 million

## C
$100,000

## D
$10,000

##  Answer
- A

## Explanation

SLE is about a single loss.
The ARO is frequency.
The ALE is SLE * ARO

## Goal Implication



# The primary goal of a security awareness program is:?

## Id

SRM.1d96aeb3-e95a-4366-9320-892c42431698

## A
A platform for disclosing exposure and risk analysis

## B
To make everyone aware of potential risk and exposure

## C
Platform for disclosing exposure and risk analysis.

## D
A way for communicating security procedures

##  Answer
- B

## Explanation


## Goal Implication



# You have changed your encryption algorithm for files on your server from 3DES to AES. Which type of control are you implementing. Choose two.
## ID
IAM.6e3cf218-a0aa-49da-a5b9-2f9047225ce9
## A
Detective
## B
Corrective
## C
Preventative
## D
Administrative
## E
Technical
## F
Physical
## Compensating

##  Answer
C,E

## Explanation
Encryption prevents others from reading
This is technical control
## Goal Implication

 

# Crime Preventation through Environmental Design seeks to deter criminal activity through techniques for environment design. Which of the following are components of CPTEDs strategy
## ID
SRM.3e0a4ecd-9f3a-4ca7-a695-bc28c70f4734
## A
Natural Access Control
## B
End User Security Awareness Training
## C
Building Code Security Reviews
## D
Community Activism
## E
Natural Territorial Enforcement
## F
Environmental Inconveniences
## G
Natural Survelliance
##  Answer
A,E,G
## Explanation
Matural Access Control - making it obvious what is public vs private areas
Natural Territorical Control - Making people feel they have ownership of the area
Natural Survelliance - people more likely to create crime if they believe they are being seen doing it
## Goal Implication
Is there an equivalent for this in user interface design


# Which of the following is a preventative security control
## ID
SRM.8be5ea80-975d-4df5-82d7-ef07a5f1f5a6
## A
CCTV Cameras
## B
NTFS File Permissions
## C
Data backups
## D
No Trepassing Signs
## E
Network Intrusion Detection
##  Answer
B
## Explanation
NTFS prevents certain actions
Compared to No Trepassing which Deter someone but dont prevent
Backups are a corrective control
CCTV is a detective control
IDS is a detective control
## Goal Implication


# What would not be considered in the creation of an occupant emergency plan (OEP), Emergency Action plan
## ID
SRM3996910f-775a-49f6-8b89-8839f2f9b956.
## A
Evacuation / Shelter in Place  for individuals
## B
Defining locations for command center
## C
Contingency plans for alternate modes of communication
## D
Steps to ensure the primary operations of the facility continue to function
##  Answer
D
## Explanation
D would be part of a continuity of operations

## Goal Implication





# An employee has written a script that changes values in a database before a billing application reads the values for payment processing . After the billing process runs , the malicious script changes the database back to its original values. What type of attack is this?
## ID
SRM.be642a5c-a7be-41ce-bd4c-ff7e11590a25
## A
Time of CHeck/Time of Use
## B
Buffer Overflow
## C
XSS
## D
Salami Attach
## E
Data Diddlin
##  Answer
E
## Explanation
Salalmi attack is changing a number slightly over time and hoping its not noticed.
## Goal Implication

# Which of the following is a characteristic of a trade secret
## ID
SRM.4645437e-fb7b-44a8-8b06-52ea73e4385a
## A
Trade secrets are not afforded protection once published to the world
## B
Trade secrets are exempt from non-disclosure agreements
## C
Trade secrets are generally unknown to the world
## D
Trade secrets are protected for 20 years
## E
Trade secrets protect the original expression of the idea that than the idea itself
##  Answer
C
## Explanation
A copyright protects the original expression of the idea rather than the idea itself
## Goal Implication
## Source
https://www.youtube.com/watch?v=c0xg0LBBsI4


# Who is ultimately responsible for accepting the risk associated with operating a system in your enterprise?
## ID
SRM.89f6cd7f-dc59-4845-872e-7526a41e31ec
## A
System Owner
## B
ISSO
## C
Software Developer
## D
Authorizing Official
## E
CIO
##  Answer
D - this is someone who formally accepts the delivery
## Explanation
A- System owner is responsible for multiple aspect of developing the system
ISSO - is responsible for security concerns during development
CIO - is responsible for budgets
## Goal Implication
## Source
https://www.youtube.com/watch?v=8A4c-vtz_U0


# When you combine the SLE (Single Loss Expectancy) x ARO (Annualized Rate of Occurence) you get what
## ID
SRM.297af2a5-b7ed-44a8-aca5-164cb1ceb963.
## A
Return on Investment (ROI)
## B
Exposure Factor (EF)
## C
Risk
## D
Total Cost of Ownership
## E
Annualized Loss Expectancy (ALE)
##  Answer
E
## Explanation
Exposure Factor is the percentage of an asset that is damaged when an incident occurs
## Goal Implication
## Source
https://www.youtube.com/watch?v=ofBrEUGhmGA


# In addition to Physical and Adminstration controls what are the other types of controls
## ID
SRM.c9a90254-c773-4c8e-bc6c-b3dd670ebb6b
## A
Preventative
## B
Detective
## C
Corrective
## D
Technical
## E
Compensating
## F
Recovery
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=ofBrEUGhmGA

# Which of the following is the most important objective acheived by an IT change management policy
## ID
SRM.7e205a4e-6937-4ec0-bb26-21409a4c9d44
## A
Minimize costs associated with changes
## B
Support rapid change while minimizing service disruption
## C
Create best practice to ensure that data confidentiality is preserved
## D
Provides guidance when emergency changes must be made
## E
Improved change documentation and post change reporting
##  Answer
E
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=CvdJSbjaBHY



# All of the following should be determined during a BIA exception:
## ID
SRM.3a9fa221-919f-409c-8b8a-6b03aaca2b31
## A
The backup strategies required for data recovery
## B
Which business processes a system supports
## C
The financial impact to the business if a system becomes unavailable
## D
The resources required to restore the system to operation
## E
The maximum amount of time the system can be unavailable.
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=HGtxaxO70rw


# ISO/IEEE 15408 is an international standard for computer security certification. It provides a set of requirements for security functionality of IT products during security evaluation with the goal of providing a level of assurance that the product/system performs in a certain way. What is the common name for this standard?
## ID
SRM.2284c248-cf87-4499-ba5f-a7c90144369b
## A
NIST
## B
RMF
## C
TCSEC
## D
ITSEC
## E
Common Criteria
## F
Certification and Accreditation
## G
PCI
## H
COBIT
##  Answer
E
## Explanation
https://en.wikipedia.org/wiki/Common_Criteria
## Goal Implication

## Source
https://www.youtube.com/watch?v=vp7DA6TqDNw

# Textbooks define 'due care' as the actions a reasonable and prudent person or organization would take in order to protect an asset. What is the primary reason due care is taken.
## ID
SRM.9c275835-cd54-46fa-be04-d5319fefb7d9
## A
Minimize Downtime
## B
Reduce Liability
## C
Manage Costs
## D
Decrease Administrative Overhead
## E
Eliminate Risk
##  Answer
B
## Explanation
When participating in an activity you arent as liable if you have take due care.
## Goal Implication
This is a crappy answer. You take care to avoid making bad decisions that result in risk.  By avoding bad decision you eliminate risk.
## Source
https://www.youtube.com/watch?v=R3JrsyWXrUc

# DDL (Data Definition Language) is the standard for the language to create structures in a database.  What is the term used to refer to the logical database structure.
## ID
SDL.fe1aaff9-7538-4d04-845a-6d921166a7aa
## A
DCL
## B
Schema
## C
Normalization
## D
Polyinstantiation
## E
Cardinality
## F
Relational
## G
SQL
## H
DML
##  Answer
B
## Explanation
DCL - Data Control Lanaguage - Commits, Rollback
Polyinstantiation - Having records with the same primary key but separate column defines sensitivity. Record provided to end user depends upon sensitivity.
## Goal Implication
## Source
https://www.youtube.com/watch?v=FpOLERNDFMM

# Which of the following would be considered an adminstrative control?
## ID
SRM.4b3b623d-646f-4ddc-8225-b837adfe2658
## A
Background Checks
## B
Network Firewall
## C
Audible Alarms
## D
Security Awareness Training
## E
Security Guards
## F
Risk Management
## G
Encryption of Personnel Records
##  Answer
A, D, F 
## Explanation
## Source
https://www.youtube.com/watch?v=IpnH7EXLGZ0


# You are researching security solutions to provide overnight security for a large fenced in area that stores physical company assets. Your solution should act as a deterrent and provide the ability to gauge the level of corrective response. Which of the following is the most appropriate solution.
## ID
SRM.52de8b1d-f4d9-4086-bc3e-894111d6b3be
## A
Use guard dogs
## B
CCTV Cameras that record when movement is detected
## C
Install razer wire on top of fences
## D
Hire a security guard
##  Answer
D
## Explanation
Only security guard provides a gauging of appropriate corrective action
## Goal Implication
## Source
https://www.youtube.com/watch?v=irgya0lWmT8

# You have received a PDF file that was signed using a self signed certificate. Which of the following should you do before viewing the file?
## ID
SRM.5615f348-7a38-44cc-88a7-a10382912b02
## A
Hash the PDF and compare the result to the hash recovered from the signature
## B
Add the user certificates to the list of trusted certificaes on your system
## C
Manually verify the certificate using and out of band method
## D
Use the private key to validat the file signature before opening the file
##  Answer
C
## Explanation
## Source
https://www.youtube.com/watch?v=irgya0lWmT8

# What can HASH function tell you about a file change
## ID
SOP.346ecd5b-dfbb-468f-8ca2-8a211876b876
## A
WHat in the file has been changed
## B
The content has been changed
## C
How much of the file has been changed
## D
The file has been viewed
##  Answer
B
## Explanation
## Goal Implication
## Source
PT invented

# WHich of the following are not Integrity controls?
## ID
SOP.a9319e54-0a77-4f2b-9c33-caccad910fbf
## A
Hashing
## B
Digital Signature
## C
Access controls
## D
Digital Certificates
## E
Redundant components
## F
OS Patching
##  Answer
C,E,F
## Explanation
Integrity are about detecting changes or non-repudiation
## Goal Implication
## Source
PT invented

# A user at a terminal needs to be able to securely log into a system. Both the system login process and the user need to have confidence that untrusted software can eject itself into the authentication process.  What is this called
## ID
SRM.bdc84081-2b30-4323-9e9c-6c36b14657d3
## A
Trusted Path
## B
API
## C
Covert Channel
## D
Protection Domain
##  Answer
A
## Explanation
A trusted path is a channel established with strict standards to allow necessary communication to occur without exposing the TCB to security vulnerabilities. A trusted path also protects system users (sometimes known as subjects) from compromise as a result of a TCB interchange.
## Goal Implication
## Source
https://www.youtube.com/watch?v=H0_epnHrnlM&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=44



# An IT Contingency Planning Process consists of 7 broad steps. Which of the following is one of those steps? Choose 2
## ID
SRM.f81231cb-37ae-442f-9541-656d2b0c6a34
## A
Define Metrics to be gathered
## B
Develop Recovery Strategies
## C
Respond to management with migitation steps
## D
Perform functionality and security testing
## E
Identifying Preventative Controls
## F
Obtain formal authorization to operate (ATO)
##  Answer
B,E
## Explanation
7 Steps of IT Contingency Planning Process:
-------------------------------------------
1. Develop the contingency planning policy statement
2. Conduct the business impact analysis (BIA) 
3. Identify preventive controls
4. Develop recovery strategies
5. Develop an IT contingency plan 
6. Plan testing, training, and exercises 
7. Plan maintenance

## Goal Implication
## Source
https://www.youtube.com/watch?v=H0_epnHrnlM&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=44

# What are all the following examples of : ISO 270001, NIST 800-53, COBIT
## ID
SRM.437bf04e-df99-4e88-868c-fc09dcbc385d
## A
Security Policy
## B
Information Security Program
## C
Security Control Framework
## D
Risk Management Framework
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/control-frameworks?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Which one of the following is not one of the major principles of COBIT?
## ID
SRM.15bf55f1-8c15-4e1d-b2c5-913686b4fca9
## A
Meeting stakholder needs
## B
applying a single integrated framework
## C
maximizing profitability
## D
separately governance from management
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4592111?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# As  it relates to the GDPR, which of the following have been added under the requirement to protect persional information (Choose 2)
## ID
SRM.a3809290-fd10-429a-9c87-b2ee6294693f
## A
Physical Characeristics
## B
Genertics
## C
Identification Number
## D
Mental Status
## E
Location Data
## F
Economic Status
## G
Cultural or Social Identity
## H
Online Identifiers
##  Answer
E,H
## Explanation
Other items were in GDPR and Privacy Act
## Goal Implication
## Source
https://www.youtube.com/watch?v=IYFp_5YptYg

# Which of the following is a new requirement under the EU GDPR?
## ID
SRM.bce309ce-7cc5-4082-8e04-d9e0ae2a59e8
## A
Data consent cannot be disclosed without data subjects consent
## B
Data can only be used for the purpose stated when collected
## C
Subject can access their data and make corrections
## D
Subjects have the right to be forgotten
## E
Subjects must consent to data collection
## F
Collected data should be kept secure from potential abuse
##  Answer
D
## Explanation
Now you have the right to request your account to be completely removed. Accounts cannot be deactivated.
## Goal Implication
## Source
https://www.youtube.com/watch?v=IYFp_5YptYg


# When making decisions about how to best secure user compueters and servers, which of the following is the most important consideration?
## ID
SRM.64448da4-a58b-4e4f-9e16-6ed6aef81d9d
## A
Security should not decrease the usability of the system
## B
Intangible risks should be mitigated first
## C
Should cover all regularoty requirements
## D
Cost must be manage and should make sense for the given risk
## E
All risk should be eliminated by mitigating mechanisms
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=f0foB_Ng0xE&t=1s


# When a data breach occurs in which situation is notification not required in many Data Breach notification laws (choose 3)?
## ID
SRM.e0f7970b-7c32-478e-9005-d6d5a8fc9a8a
## A
Place of Birth
## B
Address
## C
Social Security Information
## D
Encrypted Credit Card Information
## E
Social Media Posts
## F
Drivers License Numbers
## G
Transactions
## H
Bank Accounts
## I
Passwords
##  Answer
D, G, I
## Explanation
Generally if encrypted information is breach it is not considered a data breach if the encryption keys are not breached. It also has to be personally identifiable which is why paswords and transactions are not data breach notifications.
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/data-breaches?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What law restricts government interception of private electronic communications?
## ID
SRM.69ed37a2-38d2-413c-8691-1350f7793143
## A
ITADA
## B
EPCA
## C
CFAA
## D
GLBA
##  Answer
B
## Explanation
CFAA- Prevents unauthorized access
ITADA- Identity theft
GLBA - Sharing financial information between branches
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4591465?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675



# Dru recently developed a new name for a product that he will be selling. He would like to ensure that nobody else may use the same name. What type of intellectual property protection should he seek?
## ID
SRM.d5f0c16c-2732-46fe-b3f6-228df7b274b7
## A
Patent
## B
Copyright
## C
Trademark
## D
Trade Secret
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4591465?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Which one of the following regulations and agencies is not involved in the export control process?
## ID
SRM.076184a2-eb3c-47e4-a69c-e42c387b5a35
## A
ITAR
## B
OFAC
## C
EAR
## D
FCC
##  Answer
D
## Explanation
ITAR - Defense restrictions
EAR - Dual Use
OFAC - restrictions on trade
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4591465?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What should an information policy NOT contain
## ID
SRM.0f5cc65f-5ca4-48b8-9dc3-1a7a0b964289
## A
Process for policy exceptions and violations
## B
Description of security roles
## C
Designation of individuals responsible for security
## D
Specific details of security controls
## E
Authority for the creation of policies
##  Answer
D
## Explanation
THe policies are not specific to be able to stand test of time
## Goal Implication
What scope do policies apply to?  Does one policy suit all
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/security-policies?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What security principle prevents against an individual having excess security rights?
## ID
SRM.24af6a88-fd23-4da8-968c-b2926a078a18
## A
Least Privilege
## B
Job Rotation
## C
Separation of duties
## D
Mandatory Vacations
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4592112?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Which of the following are NOT key principles of information security (choose 2)
## ID
SRM.3a2ec3ba-5e0f-480a-8466-f62b962c8604
## A
Principle of Least Privelege
## B
Locards Principle
## C
Need to know
## D
Separation of dutie
## E
Tranquility Principle
##  Answer
B,E
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/security-policies?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Which of the following is an example of a business continuity control
## ID
SRM.d9473124-717b-4543-9c27-0ce1cf55df95
## A
Only allow a use to create a vendor or pay a vendor but not both
## B
Classify data from a system
## C
Succession plan for key personnel
## D
Establish information security policy
## E
Log user access to information systems
##  Answer
C
## Explanation
## Goal Implication
What are the key personnel for the team?
Does our team have a succession plan?
## Source
Phil Tomlinson
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/business-continuity-controls?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Which of the following does NOT involves disk striping
## ID
SRM.f74187ef-83df-450d-91b8-898fb0c1b2d9
## A
RAID 0
## B
RAID 1
## C
RAID 2
## D
RAID 5
## E
RAID 6
##  Answer
B
## Explanation
RAID 1 is pure mirroring
## Goal Implication
## Source
Phil Tomlinson
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/high-availability-and-fault-tolerance?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# How many disks are required to implement disk striping with parity
## ID
SRM.220d74fc-5e64-499c-8d1b-7bf423040174
## A
1
## B
2
## C
3
## D
4
##  Answer
C
## Explanation
RAID 3 requires at least 2 disks to strip across and a 3rd for the parity information
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/high-availability-and-fault-tolerance?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# WHich of the following control types is RAID
## ID
SRM.16875f77-d734-4ba0-bbe9-be612ff9a9f2
## A
Access control
## B
Backup Control
## C
Fault Tolerance Control
## D
High Availability Control
## E
Quality of Service Control
##  Answer
C
## Explanation
In CISSP material HA is separate from Fault Tolerance
## Goal Implication
## Source
Phil Tomlinson
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/high-availability-and-fault-tolerance?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What type of control are we using if we supplement a single firewall with a second standby firewall ready to assume responsibility if the primary firewall fails?
## ID
SRM.c21d6959-38a7-4fab-bcc4-d246e679c1c1
## A
Component Redunancy Control
## B
Load Balancing
## C
High Availability Control
## D
Clustering
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4590564?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What is the minimum number of disks to acheive RAID level 5
## ID
SRM.499567ab-0538-44c9-983d-1a997b3dc3ac
## A
1
## B
2
## C
3
## D
4
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4590564?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Whats the first thing you should do if you observe a information security policy violation?
## ID
SRM.f4b8895b-3a34-4606-8c05-ca0c0fb63a50
## A
Inform the violator of the policy
## B
Investigate the consequences of this violation
## C
Bring it up with your manager
## D
Do nothing
##  Answer
C
## Explanation
Suggestion in this material is that need to be sensitive to causing more harm than good
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/improving-personnel-security?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Which of the following is not a way to protect sensitive employee information
## ID
SRM.11d5fd2c-b153-4509-b7cb-115c9e2fdb18
## A
Minimization
## B
Limitation        
## C
Truncation
## D
Encryption
## E
Masking 
##  Answer
C
## Explanation
The others are mechanism to limit and protect the information kept by an organization
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/employee-privacy?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Which one of the following agreements is most directly designed to protect confidential information after an employee has left the organization?
## ID
SRM.c402d128-bd67-47f7-8794-2a0fc12c8164
## A
BAA
## B
Asset Return
## C
SLA
## D
NDA
##  Answer
D
## Explanation
Non Disclosure Agreement
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4592113?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Whats the SLE of an asset where the EF is 25% and the AV is 2 million dollars?
## ID
SRM.265f9110-6910-4a07-98a6-0c2837171b0c
## A
250,000
## B
500,000
## C
1 million
##  Answer
B
## Explanation
ALE = EF * AV
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quantitative-risk-assessment?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Which of the following is NOT a risk management strategy
## ID
SRM.440ec91f-8e23-47db-9049-8908f8cae8fe
## A
Risk Avoidance
## B
Risk Deterence
## C
Risk Acceptance
## D
Risk Mitigation
## E
Risk Transference
## F
Risk Assessment
##  Answer
F
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/five-possible-risk-management-actions?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Which of the following are operational controls
## ID
SRM.09a99e2b-b7a1-4af4-90cb-961f7579881d
## A
Firewall
## B
Code Review
## C
System Administrator
## D
Automatic Code Analysis
## E
Pentration Test
##  Answer
B,C,E
## Explanation
Operational Controls are implemented by people
Technical Controls are implemented by technology
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/security-control-selection-and-implementation?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675



# What two factors are used to evaluate a risk?
## ID
SRM.1fa5cf55-a605-4926-9600-2a64c43f4a49
## A
Criticality and Likelihood
## B
Impact and Criticality
## C
Frequency and Likelihood
## D
Likelihood and Impact
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4590563?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What is the correct formula for computing the annualized loss expectancy?
## ID
SRM.46dbe115-6907-456c-96f6-b2a92eeed023
## A
ALE = ARO * AV
## B
ALE = SLE * ARO
## C
ALE = EF * SLE * ARO
## D
ALE = AV - SLE
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4590563?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What is the first step in the NIST risk management framework?
## ID
SRM.5efea285-a901-41a5-b759-d42c48d712f2
## A
Authorize Information System
## B
Categorize information system
## C
Monitor Security Controls
## D
Select security controls
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4590563?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What letter in STRIDE refers to kind of attacks attempting to deny responsibility for an action?
## ID
SRM.44e27cc1-54dd-4703-8eb1-fa51443c0dd0
## A
S
## B
T
## C
R
## D
I
## E
D
## F
E
##  Answer
C
## Explanation
S-spoofing
T-tampering
R-repudiation
I-information Disclosure
D-denial of Service
E-elevation of privilege
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/quiz/urn:li:learningApiAssessment:4589757?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Which of the following RAID solutions provides the SMALLEST net usable disk space?
## ID
SRM.12da5fde-ec0c-4ab4-b9d2-aea41a57b856
## A
RAID 0
## B
RAID 1
## C
RAID 3
## D
RAID 5
##  Answer
B
## Explanation
With mirroing there is loss of 50% of disk capacity with 2 disks and 66% loss if 3 disks
With striping and parity there is 1 of X disks lost so RAID 5 is more efficient in terms of disks space
## Goal Implication
## Source
https://www.youtube.com/watch?v=KAYYmSvaIy0&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=43

# What is the purpose of the hamming code?
## ID
SRM.25c0bde3-8f5c-4bc6-a1c3-d07de9a2d9a7
## A
It is used for data transposition in encryption processes
## B
It is used as data encoding mechanism for 802.11 WLANs
## C
It is used to detect and correct errors in data
## D
It is used to calculate CRC checksums in Ethernet frames
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=KAYYmSvaIy0&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=43


# What would 'system response time', 'service availablity' and 'data preservation' all be examples of?
## ID
SRM.021d83ca-9540-4801-b5ef-e5b1aefe2410
## A
MOU
## B
SLA
## C
SLR
## D
BPA
##  Answer
C
## Explanation
SLR - Service Level Requirements
SLA - Service Level Agreement
MOU - Letter to document aspects of a relationship...document for future understanding
BPA - Business Partnership Agreement - separate responsibilities and renumernation for a joint development
ISA - Agreement on how organizations will interconnect
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/vendor-information-management?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Which of the following are not likely to be data ownership provisions in a contract
## ID
SRM.ac32a9e3-5c1a-4ec4-b2e3-3dc2042af930
## A
Customer retains unihibited data ownership
## B
Vendors right to use information is limited to activities performed on behalf of the customer
## C
Vendors right to use information is limited to activities performed without the customers knowledge
## D
Vendor must delete information at the end of the contract
## E
Vendor is not allowed to share information with 3rd parties
##  Answer
C
## Explanation
The customer should know how a vendor uses the customers information.
## Goal Implication
Are their agreements in placed with asana, jira, github around data protection?
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/vendor-information-management?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675



# What activity would an MSSP not normally perform for an organization
## ID
SRM.f9adbf3d-262a-4299-b2e2-3601bcdfd8f2
## A
Manage an entire security infrastructure
## B
Monitor System Logs
## C
Manage firewalls and networks
## D
Monitor security performance
## E
Perform identity and access management
##  Answer
D
## Explanation
If a company uses an MSSP (Managed Security Service Provider) it will need to independently monitor performance of the MSSP.
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-1-security-and-risk-management-2/third-party-security-services?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What type of agreement is used to define availability requirements for an IT service that an organization is purchasing from a vendor?
## ID
SRM.ffe33446-5a34-4a03-8598-55afb22ec393
## A
MOU
## B
SLA
## C
BPA
## D
ISA
##  Answer
B
## Explanation
## Goal Implication
## Source

# Which of the following is not a required component in support of accountability?
## ID
SRM.ea1ad329-b35e-43c7-8bcc-7bed02f91297
## A
Auditing
## B
Privacy
## C
Authentication
## D
Authorization
##  Answer
B
## Explanation
Accountability does not require privacy but rather non-repudiation
## Goal Implication
## Source
https://www.google.com.au/books/edition/ISC_2_CISSP_Certified_Information_System/mFhVDwAAQBAJ?hl=en&gbpv=1&dq=cissp+official+study+guide+mike+chapple&printsec=frontcover


# Which of the following is not a defense against collusion
## ID
SRM.ceed8f8d-f3a7-451d-8fe9-0c6206eaac6a
## A
Separation of duties
## B
Restricted job responsibilities
## C
Group user accounts
## D
Job rotation
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.google.com.au/books/edition/ISC_2_CISSP_Certified_Information_System/mFhVDwAAQBAJ?hl=en&gbpv=1&dq=cissp+official+study+guide+mike+chapple&printsec=frontcover

# In what phase of the Capability Maturity Model for Software (SW-CMM) are quantitative measures utilized to gain a detailed understanding of the software development process.
## ID
SRM.6ab5a63e-4e5f-4d64-a060-3839585ad518
## A
Repeatable
## B
Defined
## C
Managed
## D
Optimized
##  Answer
C
## Explanation
The Managed phase of the SW-CMM involves the use of quantitative development metrics. The Software Engineering Institute (SEI) defines the key process areas for this level
## Goal Implication
## Source
Assessment Test Question 13
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. l). Wiley. Kindle Edition. 



# Which one of the following is a layer in the ring protection system that is not normally implemented in practice?
## ID
SRM.2d49ba54-ca14-497c-98f8-f5b1db535f66
## A
Layer 0
## B
Layer 1
## C
Layer 3
## D
Layer 4
##  Answer
B
## Explanation
Ring Protection System is the defense in depth model
Layer 0  is the security kernel.
Layer 1 and 2 are device drivers.
Layer 3 is the application
Layer 4 doesnt exist
## Goal Implication
## Source
https://www.google.com.au/books/edition/ISC_2_CISSP_Certified_Information_System/mFhVDwAAQBAJ?hl=en&gbpv=1&dq=cissp+official+study+guide+mike+chapple&printsec=frontcover


# Which of the following is best countered by adequate parameter checking?
## ID
SRM.a73338b2-0a7c-4f36-9a09-1a7ea6ffc0ea
## A
Time of check to time of use
## B
Buffer overflow
## C
SYN Flood
## D
Denial of Service
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.google.com.au/books/edition/ISC_2_CISSP_Certified_Information_System/mFhVDwAAQBAJ?hl=en&gbpv=1&dq=cissp+official+study+guide+mike+chapple&printsec=frontcover


# What is the logical operation shown here?    X: 0 1 1, Y: 0 1 0?   X v Y
## ID
SRM.1512e3fe-eb36-455a-b7d7-1fd6e0c10ce5
## A
0 1 1
## B
0 1 0
## C
0 0 0
## D
0 0 1
##  Answer
A
## Explanation
V or  ~ is the logical OR operator
## Goal Implication
## Source
a73338b2-0a7c-4f36-9a09-1a7ea6ffc0ea

# The collection of components in the TCB that work together to implement reference monitor function is the ______?
## ID
SRM.f160cfc2-c0af-481f-b657-f6511fbd5f6a
## A
Security Perimeter
## B
Security Kernel
## C
Access Matrix
## D
Constrained Interface
##  Answer
B
## Explanation
Reference monitor is the aspect of enforcing access controls
## Goal Implication
## Source
https://www.google.com.au/books/edition/ISC_2_CISSP_Certified_Information_System/mFhVDwAAQBAJ?hl=en&gbpv=1&dq=cissp+official+study+guide+mike+chapple&printsec=frontcover

# Which of the following is true?
## ID
SRM.ac9449f1-c0f1-4938-822e-5c1b24b9a1e2
## A
The less complex a system is the more vulnerabilities it has
## B
The more complex a system is, the less assurance it provides
## C
The less complex a system, the less trust it provides
## D
The more complex a system, the less attack surface it provides
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.google.com.au/books/edition/ISC_2_CISSP_Certified_Information_System/mFhVDwAAQBAJ?hl=en&gbpv=1&dq=cissp+official+study+guide+mike+chapple&printsec=frontcover


# System architecture, system integrity, convert channel analysis, trusted facility management, and trusted recovery are elements of what security criteria?
## ID
SRM.cbac2557-f2f5-447d-87b7-f3429dc923b5
## A
Quality Assurance
## B
Operationational Assurance
## C
Lifecycle Assurance
##  Answer
B
## Explanation
Assurance is the degree of confidence you can place in the satification of security needs. Operational assurance focuses on the basic features and architecture that lend themselves to support security
## Goal Implication
## Source
https://www.google.com.au/books/edition/ISC_2_CISSP_Certified_Information_System/mFhVDwAAQBAJ?hl=en&gbpv=1&dq=cissp+official+study+guide+mike+chapple&printsec=frontcover



# Ring 0, from the design architecture security mechanism known as protection rings, can also be referred to as all but which of the following?
## ID
SRM.6a71d9f1-be3e-4f32-a7f8-8de638d56bb3
## A
Priviledge Mode
## B
Supervisory Mode
## C
System Mode
## D
User Mode
##  Answer
D
## Explanation
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. xlv). Wiley. Kindle Edition. 


# Audit trails, logs, CCTV, intrusion detection systems, antivirus software, penetration testing, password crackers, performance monitoring, and cyclic redundancy checks (CRCs) are examples of what?
## ID
SRM.0b1fdb59-e955-4805-aee6-475689470516
## A
Preventative Controls
## B
Detective Controls
## C
Corrective Controls
##  Answer
B
## Explanation
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. xlvi). Wiley. Kindle Edition. 


# Which type of intrusion detection system (IDS) can be considered an expert system?
## ID
SRM.4220dd3c-0d63-451e-8899-5034fb4b0662
## A
Host based
## B
Network based
## C
Knowlege Based System
## D
Behaviour Based System
##  Answer
D
## Explanation
A behavior-based IDS can be labeled an expert system or a pseudo-artificial intelligence system because it can learn and make assumptions about events. In other words, the IDS can act like a human expert by evaluating current events against known events. A knowledge-based IDS uses a database of known attack methods to detect attacks. Both host-based and network-based systems can be either knowledge-based, behavior-based, or a combination of both.
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. xlviii). Wiley. Kindle Edition. 



# Which of the following are not included in BIA recovery timeframe assessments? (choose 2)
## ID
SRM.392d91a3-1bc0-4c29-90e0-750b99da5c59
## A
RPO
## B
MTBF
## C
MTD
## D
RTO
## E
MTBSI
## F
TTR
##  Answer
B,E
## Explanation
Recovery doesnt worry about how frequently they occur.
MTBF - Mean Time Betwee Failures
MTBF - Mean Time Between System Incidents

MTD - Maximum Tolerable Downtime
TTR - Time to recover
## Goal Implication
## Source


# Why would you implement a logon banner (choose 3)?
## ID
SRM.0c89ff40-c9bb-451c-8568-fff07346df3d
## A
Provide a welcome message to connecting users?
## B
Notifying user of active monitoring
## C
Provider system information upon connection
## D
Deter hackers attempting to connect
## E
Establishing "no expectation of privacy"
## F
Defining who is allowed to acces the system
##  Answer
B,E,F
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=U8gYmlF_1DY


# WHich of the following mechahisms provides the greatest capacity for individual accountability?
## ID
SRM.bc9a1ca8-d0de-40ba-a3f6-734a4fdbe5a2
## A
Hashing Files to ensure integrity
## B
Loggint activity per IP address
## C
Setting permissions on folders
## D
Individual sign on per user
## E
Limiting the number of employees that have keys to the building.
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=U8gYmlF_1DY

# What are the opposites of Confidentiality, Integrity and Availability
## ID
SRM.5fd1518c-a24a-4dac-9393-0ac5c3360aa1
## A
Alteration, Destruction and Disclosure
## B
Compromise, Inaccuracy and Destruction
## C
Disclosure, Alteration and Destruction
## D
Inaccuracy, Compomise, Disclosure
## E
Top Secret, Secret, Unclassifed
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=Vq1ECMX-iG4


# Vulnerabilities and risks are evaluated based on their threats against which of the following?
## ID
SRM.6e6f65c2-7d31-4d41-8128-f838abfe75bd
## A
One or more of the CIA Triad
## B
Data usefulness
## C
Due care
## D
Extent of liability
##  Answer
A
## Explanation
Vulnerabilities and risks are evaluated based on their threats against one or more of the CIA Triad principles.
## Goal Implication
## Source
Chp1 Review Question 2
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# Which of the following is not considered a violation of confidentiality?
## ID
SRM.1dcd2588-2900-4d84-a62c-909712911bca
## A
Stealing passwords
## B
Eavesdropping
## C
Hardware destruction
## D
Social Engineering
##  Answer
C
## Explanation
Hardware destruction is a violation of availability and possibly integrity. Violations of confidentiality include capturing network traffic, stealing password files, social engineering, port scanning, shoulder surfing, eavesdropping, and sniffing.
## Goal Implication
## Source
Chp1 Review Question 4
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# Which of the following is not true?
## ID
SRM.afbe33bf-47c7-487a-9af8-43fd9f15ae88
## A
Violations of confidentiality include human error
## B
Violations of confidentiality include management oversight
## C
Violations of confidentiality are limited to direct intentional acts
## D
Violations of confidentiality can occur when a transmission is not properly encrypted.
##  Answer
C
## Explanation
Violations of confidentiality are not limited to direct intentional attacks. Many instances of unauthorized disclosure of sensitive or confidential information are due to human error, oversight, or ineptitude.
## Goal Implication
## Source
Chp1 Review Question 5
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# STRIDE is often used in relation to assessing threats against applications or operating systems. Which of the following is not an element of STRIDE?
## ID
SRM.a43efd88-6245-4694-b506-25665c24d9ef
## A
Spoofing
## B
Elevation of privelege
## C
Repudiation
## D
Disclosure
##  Answer
D
## Explanation
Disclosure is not an element of STRIDE. The elements of STRIDE are spoofing, tampering, repudiation, information disclosure, denial of service, and elevation of privilege.
## Goal Implication
## Source
Chp1 Review Question 6
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 


# All but which of the following items requires awareness for all individuals affected?
## ID
SRM.b06e843f-1eab-4356-b09a-21b913b15987
## A
Restricting personal email
## B
Recording Phone Conversations
## C
Gathering information about surfing habits
## D
The backup mechanism used to retain email
##  Answer
D
## Explanation
Users should be aware that email messages are retained, but the backup mechanism used to perform this operation does not need to be disclosed to them.
## Goal Implication
## Source
Chp1 Review Question 9
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# What element of data categorization management can override all other forms of access control?
## ID
SRM.d1a8fc80-1b17-4578-9dc2-302e978ac079
## A
Classification
## B
Physical Access
## C
Custodian Responsibility
## D
Taking Ownership
##  Answer
D
## Explanation
 Ownership grants an entity full capabilities and privileges over the object they own. The ability to take ownership is often granted to the most powerful accounts in an operating system because it can be used to overstep any access control limitations otherwise implemented.
## Goal Implication
## Source
Chp1 Review Question 10
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# Which of the following is the most important and distinctive concept in relation to layered security?
## ID
SRM.5b0ebc21-dde2-417f-9564-10bf9fefe284
## A
Multiple
## B
Series
## C
Parallel
## D
Filter
##  Answer
A
## Explanation
Layering is the deployment of multiple security mechanisms in a series. When security restrictions are performed in a series, they are performed one after the other in a linear fashion. Therefore, a single failure of a security control does not render the entire solution ineffective.
## Goal Implication
## Source
Chp1 Review Question 12
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 


# Which of the following is not considered an example of data hiding?
## ID
SRM.f801ec13-2de3-4c88-9e42-22fbc27458ed
## A
Preventing an authorized reader of an object from deleting that object
## B
Keeping a database from being accessed by unauthorized visitors
## C
Restricting a subject at a lower classification level from accessing data at a higher classification level
## D
Preventing an application from accessing hardware directly
##  Answer
A
## Explanation
A. Preventing an authorized reader of an object from deleting that object is just an example of access control, not data hiding. If you can read an object, it is not hidden from you.
## Goal Implication
## Source
Chp1 Review Question 13
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# What is the primary goal of change management?
## ID
SRM.e9c88248-894e-43e8-a771-4e83b3805f27
## A
Maintaining documentation
## B
Keeping users informed of change
## C
Allowing rollback of failed changes
## D
Preventing security compromises
##  Answer
D
## Explanation
The prevention of security compromises is the primary goal of change management.
## Goal Implication
## Source
Chp1 Review Question 14
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 


# What is the primary objective of data classification schemes?
## ID
SRM.af9b6735-1a22-4c65-994a-d4a42988b276
## A
To control access to objects for authorized subjects
## B
To formalize and stratify the process of securing data based on assigned labels of importance and sensitivity
## C
To establish a transaction trail for auditing accountability
## D
To manipulate access controls to provide for the most efficient means to grant or restrict functionality
##  Answer
B
## Explanation
## Goal Implication
## Source
Chp1 Review Question 5
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 


# Which of the following is typically not a characteristic considered when classifying data?
## ID
SRM.f4300b72-7ded-4db3-8384-3dddf14b53a8
## A
Value
## B
Size of object
## C
Useful lifetime
## D
National Security implications
##  Answer
B
## Explanation
## Goal Implication
## Source
Chp1 Review Question 16
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# What are the two common data classification schemes?
## ID
SRM.2de77ea2-371d-4f15-b32f-80b8c95b8e2f
## A
Military and private sector
## B
Personal and government
## C
Private sector and unrestricted sector
## D
Classified and unclassifed
##  Answer
A
## Explanation
Military (or government) and private sector (or commercial business) are the two common data classification schemes.
## Goal Implication
## Source
Chp1 Review Question 17
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 


# Which of the following is the lowest military data classification for classified data?
## ID
SRM.e3e319e2-f691-4149-96a1-746c5bd90dd1
## A
Sensitive
## B
Secret
## C
Proprietary
## D
Private
##  Answer
A
## Explanation
Of the options listed, secret is the lowest classified military data classification. Keep in mind that items labeled as confidential, secret, and top secret are collectively known as classified, and confidential is below secret in the list.
## Goal Implication
## Source
Chp1 Review Question 18
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# Which commercial business/private sector data classification is used to control information about individuals within an organization?
## ID
SRM.91dfbe88-f177-42a1-9b65-244e2674537e
## A
Confidential
## B
Private
## C
Sensitive
## D
Propietary
##  Answer
B
## Explanation
## Goal Implication
## Source
Chp1 Review Question 19
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# Data classifications are used to focus security controls over all but which of the following?
## ID
SRM.647c3dd9-63f9-48e5-a6a5-bcf9d5e70cb3
## A
Storage
## B
Processing
## C
Layering
## D
Transfer
##  Answer
C
## Explanation
Layering is a core aspect of security mechanisms, but it is not a focus of data classifications.
## Goal Implication
## Source
Chp1 Review Question 20
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 


# Which of the following is the weakest element in any security solution?
## ID
SRM.8d3ca788-809a-44d1-b756-fe118e1e2335
## A
Software products
## B
Internet Connection
## C
Security policies
## D
Humans
##  Answer
D
## Explanation
## Goal Implication
## Source
Chp2 Review Question 1
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# When seeking to hire new employees, what is the first step?
## ID
SRM.89fcf732-c0a8-43e5-8889-e6e94c61292b
## A
Create Job Description
## B
Set Position Characteristics
## C
Screen Candidates
## D
Request referrals
##  Answer
A
## Explanation
The first step in hiring new employees is to create a job description. Without a job description, there is no consensus on what type of individual needs to be found and hired.
## Goal Implication
## Source
Chp2 Review Question 2
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# When an employee is to be terminated, which of the following should be done?
## ID
SRM.830b1cca-1aed-40a1-af10-086f62fe46b4
## A
Inform the employee a few hours before they are officially terminated.
## B
Disable the employee’s network access just as they are informed of the termination.
## C
Send out a broadcast email informing everyone that a specific employee is to be terminated.
## D
Wait until you and the employee are the only people remaining in the building before announcing the termination.
##  Answer
B
## Explanation
You should remove or disable the employee’s network user account immediately before or at the same time they are informed of their termination.
## Goal Implication
## Source
Chp2 Review Question 4
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 


# QUESTION
## ID
SRM.f429f56a-f210-4635-84ef-89dd28304c88
## A
## B
## C
## D
##  Answer
## Explanation
## Goal Implication
## Source
Chp2 Review Question X
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# If an organization contracts with outside entities to provide key business functions or services, such as account or technical support, what is the process called that is used to ensure that these entities support sufficient security?
## ID
SRM.f322fef5-68ce-4cb6-884d-9c52f75047a8
## A
Asset Identification
## B
Third Party Governance
## C
Exit Interview
## D
Qualitative Analysis
##  Answer
B
## Explanation
Third-party governance is the application of security oversight on third parties that your organization relies on.
## Goal Implication
## Source
Chp2 Review Question X
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 


# A portion of the ???? is the logical and practical investigation of business processes and organizational policies. This process/policy review ensures that the stated and implemented business tasks, systems, and methodologies are practical, efficient, and cost-effective, but most of all (at least in relation to security governance) that they support security through the reduction of vulnerabilities and the avoidance, reduction, or mitigation of risk.
## ID
SRM.d50d8db9-622b-4311-b9a9-9b5ad942c82a
## A
Hybrid Assessment
## B
Risk aversion process
## C
Countermeasure selection
## D
Documentation review
##  Answer
D
## Explanation
A portion of the documentation review is the logical and practical investigation of business processes and organizational policies.
## Goal Implication
## Source
Chp2 Review Question 6
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# Which of the following statements is not true?
## ID
SRM.a49fe36e-90e1-4c74-bc7d-08d9048322e1
## A
IT security can provide protection only against logical or technical attacks
## B
THe process by which the goals of risk management are acheived is known as risk analysis
## C
Risks to an IT infarstructure are all computer based
## D
An asset is anything used in a business process or task
##  Answer
C
## Explanation
## Goal Implication
## Source
Chp2 Review Question 7
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 


# Which of the following is not an element of the risk analysis process?
## ID
SRM.a7aa8f62-5046-4052-847b-500ce71296c9
## A
Analyzing an environment for risks
## B
Creating a cost/benefit report for safeguards to present to upper management
## C
Selecting appropriate safeguards and implementing them
## D
Evaluating each threat event as to its likelihood of ocurring and cost of the resulting damage
##  Answer
C
## Explanation
Risk analysis includes analyzing an environment for risks, evaluating each threat event as to its likelihood of occurring and the cost of the damage it would cause, assessing the cost of various countermeasures for each risk, and creating a cost/benefit report for safeguards to present to upper management. Selecting safeguards is a task of upper management based on the results of risk analysis. It is a task that falls under risk management, but it is not part of the risk analysis process.
## Goal Implication
## Source
Chp2 Review Question 8
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# Which of the following would generally not be considered an asset in a risk analysis?
## ID
SRM.6fcd9916-1dc5-406b-b9ac-1dbbfb726827
## A
A development process
## B
An IT infrastructure
## C
A proprietary system resource
## D
Users personal files
##  Answer
D
## Explanation
The personal files of users are not usually considered assets of the organization and thus are not considered in a risk analysis.
## Goal Implication
## Source
Chp2 Review Question 9
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# Which of the following represents accidental or intentional exploitations of vulnerabilities?
## ID
SRM.6d96df52-34f6-46a2-a8cf-5d13b77a8fc2
## A
Threat events
## B
Risks
## C
Threat agents
## D
Vulnerabilities
##  Answer
A
## Explanation
Threat events are accidental or intentional exploitations of vulnerabilities.
## Goal Implication
## Source
Chp2 Review Question 10
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# When a safeguard or a countermeasure is not present or is not sufficient, what remains?
## ID
SRM.5618b312-1ea8-4041-b315-c7cd2b6f70c0
## A
Vulnerability
## B
Exposure
## C
Risk
## D
Penetration
##  Answer
A
## Explanation
## Goal Implication
## Source
Chp2 Review Question 11
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# Which of the following is not a valid definition for risk?
## ID
SRM.21ee9e81-194e-432c-9391-83d95e158e32
## A
An assessment of probability, possible or chance
## B
Anything that removes vulneability or protects against one or more threats
## C
Risk = threat * vulnerability
## D
Every instance of exposure
##  Answer
B
## Explanation
Anything that removes a vulnerability or protects against one or more specific threats is considered a safeguard or a countermeasure, not a risk.
## Goal Implication
## Source
Chp2 Review Question 12
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 


# When evaluating safeguards, what is the rule that should be followed in most cases?
## ID
SRM.0b6cf8e9-7a5f-47c7-8eb1-23b6faf0c7d3
## A
The expected annual cost of asset loss should not exceed the annual cost of safeguards
## B
The annual costs of safeguards should equal the value of the asset.
## C
The annual costs of safeguards should not exceed the expected annual cost of asset loss.
## D
The annual costs of safeguards should not exceed 10 percent of the security budget.
##  Answer
C
## Explanation
The annual costs of safeguards should not exceed the expected annual cost of asset loss.
## Goal Implication
## Source
Chp2 Review Question 13
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# How is single loss expectancy (SLE) calculated?
## ID
SRM.769f41bc-9a0a-45ec-8b8e-b43a20c3667f
## A
Threat + Vulnerability
## B
Asset Value * exposure factor
## C
Annual Rate of Occurance * vulneability
## D
Annualized Rate of Occurance * Asset Value * Exposure Factor
##  Answer
B
## Explanation
SLE is calculated using the formula SLE = asset value ($) * exposure factor (SLE = AV * EF).
## Goal Implication
## Source
Chp2 Review Question 14
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# How is the value of a safeguard to a company calculated?
## ID
SRM.20ac7b60-2c57-49fd-8f58-807b17d497c6
## A
ALE before safeguard – ALE after implementing the safeguard – annual cost of safeguard
## B
ALE before safeguard * ARO of safeguard
## C
ALE after implementing safeguard + annual cost of safeguard – controls gap
## D
Total risk – controls gap
##  Answer
A
## Explanation
The value of a safeguard to an organization is calculated by ALE before safeguard – ALE after implementing the safeguard – annual cost of safeguard [(ALE1 – ALE2) – ACS].
## Goal Implication
## Source
Chp2 Review Question 15
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# What security control is directly focused on preventing collusion?
## ID
SRM.030d5ed9-5777-419c-b6a8-285cd19622b8
## A
Principle of least privilege
## B
Job Description
## C
Separation of duties
## D
Qualitative Risk Analysis
##  Answer
C
## Explanation
The likelihood that a co-worker will be willing to collaborate on an illegal or abusive scheme is reduced because of the higher risk of detection created by the combination of separation of duties, restricted job responsibilities, and job rotation.
## Goal Implication
## Source
Chp2 Review Question 16
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 


# What process or event is typically hosted by an organization and is targeted to groups of employees with similar job functions?
## ID
SRM.36dafe34-ca9f-4076-8080-421c777d7e02
## A
Education
## B
Awareness
## C
Training
## D
Termination
##  Answer
C
## Explanation
Training is teaching employees to perform their work tasks and to comply with the security policy. Training is typically hosted by an organization and is targeted to groups of employees with similar job functions.
## Goal Implication
## Source
Chp2 Review Question 17
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 


# Which of the following is not specifically or directly related to managing the security function of an organization?
## ID
SRM.89c352c1-248e-4571-8849-b2536a01ddd0
## A
Worker job satisfaction
## B
Metrics
## C
Information Security Strategies
## D
Budget
##  Answer
A
## Explanation
Managing the security function often includes assessment of budget, metrics, resources, and information security strategies,
## Goal Implication
## Source
Chp2 Review Question 18
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# You’ve performed a basic quantitative risk analysis on a specific threat/vulnerability/risk relation. You select a possible countermeasure. When performing the calculations again, which of the following factors will change?
## ID
SRM.8ce11ce9-2b60-47dd-9afb-23e5e70b3656
## A
EF
## B
SLE
## C
Asset Value
## D
ARO
##  Answer
D
## Explanation
A countermeasure directly affects the annualized rate of occurrence, primarily because the countermeasure is designed to prevent the occurrence of the risk, thus reducing its frequency per year.
## Goal Implication
## Source
Chp2 Review Question 20
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# What is the first step that individusla responsible for the development of a business continuity plan should perform?
## ID
SRM.1d509ace-668c-47ec-9aa0-d5a4ea513240
## A
BCP Team Selection
## B
Business organization analysis
## C
Resource Requirements Analysis
## D
Legal and Regulatory assessment
##  Answer
B
## Explanation
The business organization analysis helps the initial planners select appropriate BCP team members and then guides the overall BCP process.
## Goal Implication
## Source
Chp3 Review Question 1
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 


# Once the BCP team is selected, what should be the first item placed on the team’s agenda?
## ID
SRM.7eaf16d9-d8ac-4bc3-9f56-278734a2b136
## A
Business Impact Assessment
## B
Business organization analysis
## C
Resource Requirements Analysis
## D
Legal and Regulatory assessment
##  Answer
B
## Explanation
The first task of the BCP team should be the review and validation of the business organization analysis initially performed by those individuals responsible for spearheading the BCP effort. This ensures that the initial effort, undertaken by a small group of individuals, reflects the beliefs of the entire BCP team.
## Goal Implication
## Source
Chp3 Review Question 2
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# What is the term used to describe the responsibility of a firm’s officers and directors to ensure that adequate measures are in place to minimize the effect of a disaster on the organization’s continued viability?
## ID
SRM.121395c6-3071-4381-b248-f19fe71cfb8b
## A
Corporate Responsibility
## B
Review and validation of the business organizational analysis
## C
Due diligence
## D
Going concern responsilbity
##  Answer
C
## Explanation
A firm’s officers and directors are legally bound to exercise due diligence in conducting their activities. This concept creates a fiduciary responsibility on their part to ensure that adequate business continuity plans are in place.
## Goal Implication
## Source
Chp3 Review Question 3
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 

# Which one of the following BIA terms identifies the amount of money a business expects to lose to a given risk each year?
## ID
SRM.b5ae5554-2b53-46d0-ae79-b0bc314be1b8
## A
ARO
## B
SLE
## C
ALE
## D
EF
##  Answer
C
## Explanation
The annualized loss expectancy (ALE) represents the amount of money a business expects to lose to a given risk each year. This figure is quite useful when performing a quantitative prioritization of business continuity resource allocation.
## Goal Implication
## Source
Chp3 Review Question 6
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 45). Wiley. Kindle Edition. 


