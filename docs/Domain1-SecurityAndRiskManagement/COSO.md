# Committee of Sponsoring Organization of the Treadway Commission

Considers how to combat fraud in a organization.

[Avoiding Judgement Traps and Oversight](./references/COSO-EnhancingBoardOversight_r8_Web-ready.pdf)
[Source](https://www.coso.org/documents/COSO-EnhancingBoardOversight_r8_Web-ready%20%282%29.pdf)

Common Judgement Traps
- Overconfidendence Tendency
- Confirmation Tendency
- Anchoring Tendency
- Availability Tendency

Mitigations
- Seek opposing evidence
- Question expert opinions
- Encourage opposing points of view

Frames - people make different choices based on how a problem is solved
- patients accept riskier treatment if risk of dying is presented over likelihood of survival

Professional Judgement Process
- Define the problem
- Consider Alternatives
- Gather and evaluate information
- Reach a conclusion
- Articulate rationale