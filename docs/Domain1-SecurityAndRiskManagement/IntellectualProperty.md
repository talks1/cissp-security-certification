
## Trade Secret

Dont need to be disclosed
Maintined indefinitely

e.g. Coke formula

## Patent

Must be disclosed
Maintained for  period of time (20 years)
Not renewable
Must be explicitly applied for

## Copyright

Must be disclosed
Use of some content in publication
Maintined for a period of time
Automatically acheived without any registration or marking with copyright sign.

## Trademarks

Nike swoosh
Can be registered for 10 years and can be renewed every ten years indefinitely
Maintainable indefinitely
Acquired by registratering

