7 Steps of IT Contingency Planning Process:
-------------------------------------------
1. Develop the contingency planning policy statement
2. Conduct the business impact analysis (BIA) 
3. Identify preventive controls
4. Develop recovery strategies
5. Develop an IT contingency plan 
6. Plan testing, training, and exercises 
7. Plan maintenance
