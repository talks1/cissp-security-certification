

# Identify

## Identify Assets

### Asset Valuation
- Initial + Maintenance Costs
- Organizational value 
- Public value -
- Contribution to revenue - rather than costs how an asset contributes to revenue

## Identify Threats 

Threat Modeling - STRIDE 
- Spoofing
- Tampering
- Repudiation
- Information Disclosure
- Denial of Service
- Elevation of Privilege



## Identify existing controls
## Identify vulnerabilities
## Identify consequences

# Asset

# Response

# Control and Reporting

