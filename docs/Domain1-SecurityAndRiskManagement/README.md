Operations security triple - What administrators often call the relationship between assets, vulnerabilities, and threats

Due Care - using reasonable care to protect the interests of an organization.
Due diligence - the actions taken to apply, enforce, and perform responsibilities or rules as governed by organizational policy.

Security Governance
    Security Control Frameworks
        COBIT
        ISO 27001
        NIST 800-53

Compliance and Ethics
    Criminal
    Civil
    Adminstrative
    Private Regulation - PCIDSS is an example

    Privacy Compliance
        - USA
            - HIPAA - PHI (Protected Health Information)
            - FERPA - Education Records
            - GLBA - Financial Information - Limits sharing information between branches
            - COPPA - Privacy for children under 13
            - Private Act - Applies only to Federal Government
        - Europe
            - GDPR General Digital Privacy Regulation or GEneral Data Protection Regulation
                - Processing must be lawful, transparent and fair
                - Data must be collected for a legitimate, specific purpose
                - Collection the minimum amount of information
                - Ensure the accuracy of the information
                - Must delete information when not needed
                - Organization must protect information they collect
            - Privacy Shield - covers US companies doing business in Europe

                
    Criminal Compliance
        - CALEA - Communications Assistance to Law Enforcement Act
        - CFAA - Computer Fraud and Abuse Act
            - makes hacking illegal
            - Prohibits unauthorized access
            - Prohibits creation of malicious code ,
            - makes it illegal to criminalized damage to federal information systems 
        - ECPA - Restricts interception of communication
        - ITADA - Make identity theft a crime

    Intellectual Property
        Copyright - 70 years after death
        Trademark 
            - indefinite
            - Nike swoosh
        Patent 
            - public disclosure
            - must be
                - Novel
                - Useful
                - Non obvious
            - valid for 20 years
        Trade Secret
            e.g. coke recipe

    Export Controls
        - ITAR - defensive technologies
        - EAR - dual use technologies
        - OFAC - restrictions on transactions

    Security Framework
        Policies, Standards, Guidelines, Procedures
        Policy- Require compliance but point to standards
        Standards - Must be followed
            - CIS - Center for Internet Security - starting point for standards
        Guidlines - Optional
        Procedures - May be mandatory or optional


    Significant Policies
        - Acceptable Use Possible (aka Responsible Use Policy)

    Key Principles of Information Security

        Principle of Least Privilege - Assigment of permission necessary to do a job, granting excessive permissions increases possible of authorized activity.
        Separation of duties - Prevent users from holding two conflicting permissions  (e.g. no one person should have ability to create vendors and pay them)
        Mandatory Vacations - 
        Job Rotation - 

    Business Continuity
        Protection of availability
        BIA 
            - Identify Critical Business Process
            - Related Critical IT systems
            - Perform risk assessment - ALE for the system.
            - Then perform cost benefit analysis

        BCP Controls
            - Redundancy
                based on SPOF (Single Point of Failure Analysis)
            - Succession planning

        High Available - having redundant components
        Fault Tolerance - 
            Reduncy Power Supply
            RAID  - Redunant Array of Inexpensive Disks
                RAID0 - Disk striping - Entire disk available but no redundancy
                RAID1 - Disk mirroring - Loss of 50% of disk but get redundancy
                RAID3 - Striping with dedicated parity disk - Loss of 1 of x disk
                RAID5 - Disk striping with parity - 3 disks - Lose 1 of x disks (1 disk in 10 if you have 10)
                RAID6 - Duplicate parity blocks - Mimimum of 3 disks


Personnel Security
    Employee Privacy -
        Information Includes: 
            - Background Checks
            - Social Security Numbers
            - Salary and Payroll information
            - health records
        Via
            Minimization - keep only what it needed
            Limit - to data that those who need it
            Encryption
            Masking 


Risk Assessment - Identifying and Triaging Risks
    Threat - external source which jeopardized terms
    Vulnerability - weakness in your systems
    Risks - when an environment contains vulnerable and there is a threat

    Identify Risks by Impact and Likelihood
        Qualitative - Low, Medium , High
        Quantitive - a number
            Asset Valuation Technique
                - Original Cost of Asset
                - Depreciated Cost
                - Replacement Cost
            Exposure Factor - percentage of asset damaged by a threat
            SLE - actual damage if a risk occurs once
            ALE = SLE * ARO (Annulized Rate of Occurrence)

            For Repairable assets
                MTTF - Mean time to failures
                MTBF - Mean time between failures
                MTTR - Mean time to restore

            Risk Management Strategies
                Risk avoidance
                Risk mitigation
                Risk transference
                Risk acceptance
                    In Government "System Authorization" is the acceptance of risk on delivery of a system
                Risk deterrence
                    
            Security Controls
                Defence in Depth - applying multiple overlapping controls to acheive the same objective

                Deterrent 
                    - deter attacks 
                    - example 
                        - security camera
                        - penalty signs
                Preventative
                    - examples
                        - fences, locks
                Detective
                    - detect breaches
                    - example IDS
                Compensating 
                    - in addition to other controls
                    - examples
                        - network encryption in addition to db encryption for PII
                Corrective 
                    - restore a sysem
                    - examples
                        - antivirus solution
                        - configuration management
                Recovery
                    - extended corrective controls
                Directive
                    - designed to control actions
                    - examples
                        - policies
                

                Administrative
                Technical - carried out by technology - Firewall
                Operational - carried out by individuals - System Admin
                Management - to improve the risk management process itself

                False Positive
                False Negative
            
            Risk Management Framework
                NIST 800-37
                    Gather Details Technical and Organization
                    Step 1 - Categorize - Impact Assessment
                    Step 2 - Select Security Controls
                    Step 3 - Implement Security Controls
                    Step 4 - Assess Security Controls
                    Step 5 - Authorized
                    Step 6 - Monitor

Threat Modeling
    STRIDE (used by microsoft) is about thread modeling
        Spoofing - faking something (email, ip address)
        Tampering - unauthorized changes
        Repudation - deny responsibility for a change
        Information Disclosure - 
        Denial Of Service - deny authorized users from access
        Elevation of privelege - attempt to excede privilege
    DREAD
        Disaster, Reproduciability, Exploitability, Affected Users, and Discoverability
    Trike
        Security audit in a reliable and repeatable procedure

    Reduction Analysis
        Breaking a system down into components and analzing each

Vendor Agreement
    SLR - Service Level Requirements
    SLA - Service Level Agreement

    Types of Agreements    
        MOU - Letter to document aspects of a relationship...document for future understanding
        BPA - Business Partnership Agreement - separate responsibilities and renumernation for a joint development
        ISA - Agreement on how organizations will interconnect

    Data Ownership
        Provisions
            - Customer retains unihibited data ownership
            - Vendors right to use information is limited to activities performed on behalf of the customer
            - Vendors right to use information is limited to activities performed without the customers knowledge
            - Vendor must delete information at the end of the contract
            - Vendor is not allowed to share information with 3rd parties

    Managed Security Service Providers
        

Domain 1 Agenda
- Principals of Security
    - Information Security Program Part 1
    - Information Security Program Part 2

- Security Governance
    - Security Strategy
    - Security Blueprints

- Information Security Program
    - Policies, Procedures, Guidelines
    - Roles and Responsibilities

- Information Security Risk Management
    - Idenficiation
    - Assessment
    - Mitigation
    - Monitoring


- Legal Considerations
- Knowledge Transfer


Business Continuity Plan
- More strategic and senior management focused than Disaster Recovery Plan
- Phases
    - Scope and Planning
        - organization analysis
        - select of BCP team
        - assessment of resources
        - analysis of legal and regulatory landscape
    - Business Impact Assessment
        - Steps
            - Identify Priorities
            - Risk Identification
            - Likelihood Assessment
            - Impact Assessment
            - Resource priortization
    - Continuity Plan
        - Strategy development
        - Processes and provisions
    - Approval and Implementation
        - Approval
        - Implementation
        - Training


- Roles and Responsibilities
- Risk Definitions
- Risk Identification
- Risk Assessment and Analysis
- Risk Mitigation and Response
- Risk Monitoring and Reporting
- The CISSP Mindset Part 1
- The CISSP Mindset Part 2
- Introduction to Business Continuity and Disaster Recovery Planning
- Business Continuity Planning Part 1
- Business Continuity Planning Part 2
- BCP Step 1: Project Scope and Planning Part 1
- BCP Step 1: Project Scope and Planning Part 2
- BCP Step 2: Business Impact Assessment Part 1
- BCP Step 2: Business Impact Assessment Part 2
- BCP Steps 3 and 4: Community Planning, Approval and Implementation
- BCP Sub Plans
- Creation of BCP and DRPVIRTUAL LAB
- Developing the Teams
- Types of Tests