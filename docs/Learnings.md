# Lessons Learned from Study For , Taking and Passing the CISSP Exam

The test was hard.  135 questions , none of them easy. There was no indication how I was doing. Took me the whole 3 hours.  I thought I had failed when time ran out.

Very few questions did I feel confident on.

I didnt feel that my preparation covered all the material that was in the test despite having access to the official study guide and official test bank.
I know this because I have captured a large number of questions (~1500) from the material as I studied it and I have no questions covering a number of topics that appear in the test.  There were some references in the material for some of these subjects.

I do question the value of this approach to certification...its a leap of faith.
- Decide to go for the certification
- Struggle to find what is the right material to study
- Study like crazy for a long period of time with the main problem being not the material but the judgmentalism in the questions.
    - the questions are like those images where there are more than one image.  If you look at it a certain way you see some picture , but you can also switch you point of view and see a different image.  Effectively I think that by taking so many practice questions I became better at the type of question rather than being a better cyber security expert
    - I have yet to see any discussion on the validity of the answers to CISSP questions. No data to back up the judgements which are rampant in the questions.
- take the exam
    - a very difficult experience
    - leave the exam desk feeling shattered and uncertain
- somehow I passed
    - turn to religion


Surely there is a way to quality people that is
- more incremental
- more transparent
- less about perservance
- less about luck

## Topics on the Test which I was not prepared for

- [System and Organizaton Controls Audits](https://www.aicpa.org/interestareas/frc/assuranceadvisoryservices/sorhome.html)
    - Type 1 and Type 2

- [Statements on Standards for Attestation Engagment(SSAE)](https://en.wikipedia.org/wiki/SSAE_16)


- [RASP](https://www.rapid7.com/fundamentals/runtime-application-self-protection/)