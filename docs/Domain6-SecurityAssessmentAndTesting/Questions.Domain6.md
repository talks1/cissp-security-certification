

# Which of the following is key shortcoming of behaviour based (anomoly based) intrusion detection systems?

## ID
SAT.a78fa02d-6d39-4893-9db6-38ebfa7e0f13
## A
requires regular signature database updates
## B
High number of false positives
## C
Limited to detecting signatures of known attacks
## D
Changes in network traffic pattern go unnoticed
##  Answer
B
## Explanation
## Goal Implication

# You are developing an attack tree for a web application and as part of the process you are attempting to anticipate your potential attackers. Which of the following will you need to identify in order to characterize a likely adversary? 
## ID
SAT.4ad1a539-3c12-4891-a3a9-6caa07ef97bf
## A
Ease of vulnerability discovery
## B
Attacker Motive
## C
Damage Potential
## D
Opportunity
## E
Trust Boundaries
## F
Means
## G
Exploitability
##  Answer
B,D,F
## Explanation
This is the basis of the MOM acronym. Means, opportunity and motive.
## Goal Implication


# Which is not true of penetration testing?
## ID
SAT.4ce34ae8-70c4-4eb9-9473-1cdd886b8155
## A
Define clearly established boundaries before the test begins
## B
Typically performed after a vulnerability assessment is completed
## C
Utilize only passive tools combined with social engineering
## D
Define success criteria based on test objectives
## E
Rules of engagement are not defined in a black box test
##  Answer
C,E
## Explanation
A is important so you dont break things
B is important so you can mitigate known weaknesses and mitigate before the penetration test
For E there are always limits which need to be discussed before attempting a penetration test
## Goal Implication


# The "Rules of Engagement" are a critical component of any penetration test. Which is not something that needs to be agreed upon before  a pentest begins
## ID
SAT.deca50b7-b067-45c5-a42f-16bda760c66d
## A
The scope and range of the penetration test
## B
The tools that will be used
## C
The time frame/duration of the pen test
## D
Limits of liability of the pen testers
##  Answer
B
## Explanation
The tools dont really make any difference to the customer
## Goal Implication
## Source
https://www.youtube.com/watch?v=wfDkTHDoPbI


# In software development what is one of the primary differences between white-box and black-box testing?
## ID
SAT.f1bb7f5c-d585-43eb-a9b4-9d9c2d3156c9
## A
White box testing gives testers access to source code
## B
Black box testers fully deconstruct the app to identify vulnerabilities
## C
White box testers are limited to pre-defined use cases
## D
Black box testers are more thorough and proficient
## E
White box testing is done by developers
## F
Black box testing includes the line of business in the evaluation process
##  Answer
A
## Explanation
Black box testing is more about the functionality
White box testing is more about issues with the code base.
## Goal Implication
## Source
https://www.youtube.com/watch?v=ziefG4Qz9Pc&list=RDCMUCwUkAunxT1BNbmKVOSEoqYA&index=2

# What type of detected incident allows the most time for an investigation?
## ID
SAT.ee13bc11-df97-4f1e-9697-df3630686b74
## A
Compromise
## B
Denial Of Service
## C
Malicious Code
## D
Scanning
##  Answer
D
## Explanation
## Goal Implication
## Source
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. xlvii). Wiley. Kindle Edition. 



# As part of evidence collection process an investigator opens a terminal  on the suspect machine and issues commands to display the current network settings, ARP cache, resolver cache and routing table. He uses his cell phone to take pictures. Which of the following is true regarding his actions?
## ID
SAT.be55d545-d60c-4760-b0ec-745a0b7854de
## A
By viewing the data, disclosure rules have been violated.
## B
Running command has altered the system
## C
Chain of custody has been broken
## D
Photos are not considered legally authentic.
## E
Commands should only be executed through remote SSH.
##  Answer
B
## Explanation
Dont rely on binaries on the system. The idea is to bring your own binaries on a read only media
## Goal Implication
## Source
https://www.youtube.com/watch?v=Ja4PxXHM7vw


# A developer in your company has written a script that changes a numeric value before it is read and used by another program. After the value is read, the developers script changes the value back to its original value. What is this an example of?
## ID
SOP.32a11d5b-acd7-4b24-90cb-3b64cfd1b3b5
## A
Hacking
## B
Salalmi Slicing
## C
Time of Check/Time of Use
## D
Poly Instantiation
## E
Data Diddling
##  Answer
E
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=Ja4PxXHM7vw

# A ???? is an externally occurring force that jeopardizes the security of your systems.
## ID
SAT.a00e916b-7537-4873-a83c-3fa6918a2410
## A
Vulnerability
## B
Countermeasure
## C
Risk
## D
Threat
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-6-security-assessment-and-testing-2018/quiz/urn:li:learningApiAssessment:4578328?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# In a ????  penetration test, the attacker has no prior knowledge of the environment.
## ID
SAT.2ff07fc2-a832-4426-b6c8-c39e14016762
## A
rainbox box
## B
black box
## C
white box
## D
grey box
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-6-security-assessment-and-testing-2018/quiz/urn:li:learningApiAssessment:4578328?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Safe mode scans provide the most comprehensive look at system security.
## ID
SAT.17fe3314-3394-4ce2-ac19-ffbe4b08f7dd
## A
TRUE
## B
FALSE
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-6-security-assessment-and-testing-2018/quiz/urn:li:learningApiAssessment:4578328?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675



# What is the first step of a Fagan inspection?
## ID
SAT.6614d4cf-3c89-4077-a5e3-d58bd3b20808
## A
Planning
## B
Meeting
## C
Overview
## D
Preparation
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-6-security-assessment-and-testing-2018/quiz/urn:li:learningApiAssessment:4579120?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What type of fuzz testing captures real software input and modifies it?
## ID
SAT.702f29ba-d39d-4d1c-aed7-b20f1799ba0d
## A
Mutation Fuzzing
## B
Switch Fuzzing
## C
Generation Fuzzing
## D
Twist Fuzzing
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-6-security-assessment-and-testing-2018/quiz/urn:li:learningApiAssessment:4579120?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What disaster recovery metric provides the targeted amount of time to restore a service after a failure?
## ID
SAT.d1356dd4-9a5c-4fd7-8963-7542ff54f86f
## A
RPO
## B
MTO
## C
RTO
## D
TLS
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-6-security-assessment-and-testing-2018/quiz/urn:li:learningApiAssessment:4577404?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What type of backup includes only those files that have changed since the most recent full or incremental backup?
## ID
SAT.4aa036ef-245b-4491-ac42-175c45dcedb1
## A
differential
## B
partial
## C
full
## D
incremental
##  Answer
D
## Explanation
Incremental backups only capture changed files since last incremental backup.
Differential backups capture changes sinces last full backup.
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-6-security-assessment-and-testing-2018/quiz/urn:li:learningApiAssessment:4577404?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Which one of the following disaster recovery tests involves the actual activation of the DR site?
## ID
SAT.75ff3172-603b-48dc-9c93-0ebbf67365ae
## A
read-through
## B
parallel test
## C
simulation
## D
walk through
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-6-security-assessment-and-testing-2018/quiz/urn:li:learningApiAssessment:4577404?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# _____ demonstrate the success of a security program.
## ID
SAE.4ea5257d-ba1f-4b27-862c-cd009a6cb267
## A
CRIs
## B
KRIs
## C
KPIs
## D
CPIs
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-6-security-assessment-and-testing-2018/quiz/urn:li:learningApiAssessment:4577406?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# _____ controls mitigate the risk of exceptions to security policies.
## ID
SAE.144a153c-a9d8-44c7-b359-9eeff23567cf
## A
Compensating
## B
Technical
## C
Administrative
## D
Logical
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-6-security-assessment-and-testing-2018/quiz/urn:li:learningApiAssessment:4577406?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675