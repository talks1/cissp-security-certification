The 6 Security Assessments and Testing Objectives
Vulnerability Assessments and Penetration Testing
Vulnerability Scanning
    - Fuzzing - seems to be a technique by which you supply lots of random inputs to an application to see if there are weird behaviours
Testing Guidelines
Rules of Engagement Part 1
Rules of Engagement Part 2
Protocol Analyzers (Sniffers) and Privacy
8IDS Part 1
IDS Part 2
IDS Part 3


Vulnerability Ability Tools
- Passive Tools : Observe Active
- Active Tools : Interact with machines


- Honey Pots - active decoy machines
- HoneyNets - decoy networks - normally unused.  If someone connects they are probably malicious

- Protocol Analyzers
    - Wireshark
- Port Scanners
    - Nmap
- Vulnerabilty Scanner
    - Nessus

Assessment
 - Threats 
 - Vulnerabilities - what could undermine confidentiality, integrity or availability
 - Risks - 

- Likelihood
- Impact


- Assessment Techniques
    - Baseline Report - initial view of system security status
        - provides report of current system status
            - Microsoft Security Analyzer
            - CIS Baseline - compare current configuration against a baseline
    - Attach Surface Review
        - use scanners to check for known vulnerabilities
    - Code Review
    - Architecture Review
- Penetration Test
    - attacking a system on purpose
        - Discovery Phase
        - Attack Phase
    -types
        - white box - full knowlege of the system being targetted
        - black box -  no knowledge of the system being targetted
        - grey box -


SIEM -
- Central secure collection point for logs
- SIEM detects patterns  (log correlation) across multiple systems


Softwate Testing
    - Code Review
        - Fagan Inspection Model
            - Planning - Schedule Review
            - Overview - assigning roles to participants
            - Preparation - different roles review code
            - Meeting - formal identification of defects
            - Rework - 
            - Follow up
    - Code Tests
        - Types
            - Static
                - Code examined automatically but not run
            - Dynamic
                - Execution of the code
                - Synthentic transactions
        - Fuzz Testing
            - automatically generating input values
                - Generation Fuzzing
                - Mutatation Testing
            - available software - ZAP (Zed Attach Proxy)
        - API Tests
        - Mis Use Case Tests
        - Test Coverage

Disaster Recovery
- RTO - Recovery Time Objective
- RPO - Recovery Point Objective - Maximum time of data lost

Backups 
- Tape
- Disk to Disk backups
- Cloud backups
- Backup Types
    - Full Backups
    - Differential - Copy of all changed data since last full backup
    - Incremental - Include only those files that have changed

- Backup Media Management
    -GFS - Grandfather- Father - Son Media
- Backup Verification

- Disaster Recovery Sites
    - Site Types
        - Hot Site - Always running and can be switched to in a moments notice
        - Cold Site - Activating may required weeks to start up        
        - Warm Site - Activiating may take hours or days
        - MAA Site - Agreement between different companies to allow one to use another companies in the event of a disaster.  Not useful in practice.
    - Provide a location to store backups

- Disaster Recovery Tests
    - Test Types
        - Read Throughs - Get individuals to review their checklist
        - Walkthroughs - Get team together to review there checklist
        - Simulation - Team talks about how to handle specific scenarios
        - Full Interruption Tests

- Collecting Security Process Data
 
 - Security Metrics
   -Types
    - KPIs - a look backward
        - ITIL
            - Decrease in breaches
            - Decrease in breach impact
            - Increase in strong SLA
            - Number of new controls
            - Time to implement controls
            - Number of major incidents
            - Number of incident - related outages
            - Number of training events
            - Number of of shortcomings identified during security tests
    - KRIs - a look forward
        - Consider risk in terms of these criteria
            - Business Impactment
            - Effort to implement, measure and support
            - Reliability
            - Sensitivity

- Audit
    - Internal - work for the company
    - External - Independent
        - Price Waterhouse Coopes
        - Deloite
        - KPMG

- Control Management
 - Exception Management
 


