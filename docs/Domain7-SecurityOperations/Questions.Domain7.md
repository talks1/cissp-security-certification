

# Incident handling requires prioritization in order to address incidents in an appropriate manner and order. Which of the following is NOT an factor to determine prioritization

## ID

SOP.9c313238-d225-4876-a5d3-bf7da9f26d8b

## A
Functional Impact of the Incident

## B
Information impact of the Incident

## C
Detectability of the Incident

## D
Recoverability from the Incident

##  Answer
C

## Explanation


## Goal Implication



# Which component of SCAP serves to provide a standardized reference for publicly known security vulnerabilities and exposures

## ID

SOP.04309bb7-a825-4774-9921-4f833c294a44

## A
CVE

## B
CCE

## C
CPE

## D
CVSSS

## D
XCCDF

##  Answer
A

## Explanation
Common Vulnerabilities and Exposures (CVE)
Common Configuration Enumeration (CCE) (prior web-site at MITRE)
Common Platform Enumeration (CPE)
Common Vulnerability Scoring System (CVSS)
Extensible Configuration Checklist Description Format (XCCDF)
Open Vulnerability and Assessment Language (OVAL)

## Goal Implication



# Which of the following is not one of the rules of evidence
## ID
SOP.a5ef2a40-7588-45fb-8a0f-7434cad5e328
## A
Admissable
## B
Authentic
## C
Complete
## D
Auditable
## E
Reliable
## F
Believable

##  Answer
D

## Explanation

## Goal Implication
Complete is an interesting idea which I think is missing in a lot of business presentations



# Examine this IP package and indicated what type of attack it is. SourceIP: 200.16.44.200, DestIP: 200.16.44.200, SourcePort: 88, DestPort: 88, Syn: 1
## ID
SOP.84875bc9-012a-4cba-bbde-b6ccdfb21c70
## A
TCP Syn Flood
## B
Fraggle
## C
LAND
## D
Teardrop
## E
MitM
##  Answer
C
## Explanation

-A Fraggle Attack is a denial-of-service (DoS) attack that involves sending a large amount of spoofed UDP traffic to a router's broadcast address within a network.
-A DNS amplification attach - Send a query to DNS server with a different IP response address.  The DNS server responds to the request which is larger than the query. This can overwhelm the network.
- A land attack is a remote denial-of-service (DOS) attack caused by sending a packet to a machine with the source host/port the same as the destination host/port.
- A teardrop attack is a denial-of-service (DoS) attack that involves sending fragmented packets to a target machine. Since the machine receiving such packets cannot reassemble them due to a bug in TCP/IP fragmentation reassembly, the packets overlap one another, crashing the target network device.
- MitM attach - Man in the middle attach

## Goal Implication




# Your system is configured to generate an audit log event whenever an attempt to log in with a disabled user account is made. What is this an example of?
## ID
SOP.4f8f5ffc-34f1-4045-b61f-322f93a2510f
## A
A performance baseline
## B
A security breach
## C
Defense in depth
## D
A clipping level
## E
Manadatory access control
##  Answer
D
## Explanation
A clipping level is a threshold for normal mistakes a user may commit before investigation or notification begins. An understanding of the term clipping level is essential for mastery of the CISSP exam. A clipping level establishes a baseline violation count to ignore normal user errors.
## Goal Implication

# Your company is cloud migrating many of its existing apps and services. Which of the following will allow moving of the apps and services to the cloud while still providing control of the underlying operation system on which the app/services run?
## ID
SOP.ba6969f0-7697-4643-a976-e57fc4975c58
## A
Platform as a Service
## B
Infrastructure as a Service
## C
Software as a Service
## D
Storage as a Service
##  Answer
B
## Explanation
## Goal Implication

# You are exploring deployment options for network based intrusion detection systems. One of your performance objectives is to minimize false positives as much as possible. Based on this requirement, which type of IDS should you considers?
## ID
SOP.0452b5db-fea5-4351-adf0-d7a526d2e71a
## A
Behaviour Based
## B
Knowledge Based
## C
Host Based
## D
Anonomoly Based
## E
Application based
##  Answer
B
## Explanation
Anomoly or Behaviour based systems are prone to false positives whereas Knowledge Based Systems are not because those are about match to known patterns where than just violations of 'normal' behavior.
## Goal Implication
## Source
https://www.youtube.com/watch?v=yLNBUJVOjAg


# Which of the following are most likely to decrease the effectiveness of an incident reporting system (Choose 2)
## ID
SOP.44255857-25aa-4a6a-9eb4-71ad49baa225
## A
Communicate the effectives of the incident reporting system
## B
Eliminate anonymity in reporting
## C
Simplified process for report submission
## D
Lack of clear communication on what needs to be reported
## E
Feedback on previously reported results
##  Answer
B,D
## Explanation

## Goal Implication
## Source
https://www.youtube.com/watch?v=QYmoP77j6SY

# Which of the following is most important in order to maintain the integrity and admissibilty of digital evidence?
## ID
SOP.c75e5b90-ae79-4d90-b48b-ee47e3285e2d
## A
Use on NIST approved algorithms for hashing files
## B
Maintain chain of custody
## C
Never interact with live system
## D
Hash all the files before examing the original disk
## E
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=QYmoP77j6SY

# You have been tasked with reducing the likelihood that nodes in your network can forward packets with spoofed source IP addresses. Which of the following is the best way to accomplish this.
## ID
SOP.e630e3da-e229-4619-93a7-28ea550c98b2
## A
Use SNMP to generate a list of allowed MACs for each VLAN
## B
Implement ACLs on each router interface allowing only traffic sourced from the local segment.
## C
Enable MLD snooping on Layer2 switches
## D
Configure Reverse Path forwarding on the routers
##  Answer
D
## Explanation
In Reverse Path Forward a router looks at the Source IP address of a packet and asks itself would it route a packet destined for the Source IP address via this interface.  if they answer is no then the packet is dropeed.
## Goal Implication
## Source
https://www.youtube.com/watch?v=fncd6XVXkdY


# You have a building which has a higher likelihood of electrical fire than other types. Given this what class of fire suppression system should you have.
## ID
SOP.1fbf161f-7900-4be1-b11d-fb98662ba6f6
## A
Class A
## B
Class B
## C
Class C
## D
Class D
## E
Class K
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=vmIM3v22ivI


# Which of the following best describes an electrical blackout?
## ID
SOP.ee088061-84f4-48b5-91cc-babc1867cde3
## A
A reduction in voltage that lasts minutes or hours
## B
A prolonged loss of electrical power
## C
A momentary low-voltage condition lasting only a few seconds
## D
A momentary loss of electrical power
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=vmIM3v22ivI

# A new incident investigator is preparing her incident 'jump kit'. Which of the following is least likely to be in the kit.
## ID
SAT.6cf35d92-9a2a-4e6c-bd59-792e655716d6
## A
Network Diagrams and List of critical equipment
## B
Hashes of critical files
## C
Baseline documentation for systems, applications and network equipment
## D
Copies of well-known and common malware for comparison
## E
A laptop loaded with packet sniffers and forsenics software
## F
Drive Imaging Tools
##  Answer
D
## Explanation
There is probably no reason to have copies of malware
## Source
https://www.youtube.com/watch?v=g7HDe-gl_VM

# Which one of the following is NOT one of the Incident Response  Life Cycle
## ID
SOP.afc3ac91-1a99-4fde-aa4d-08ad8e008125
## A
Containment, Eradication and Recovery
## B
Preparation
## C
Detection and Analysis
## D
Prevention
## E
Post-Incident  Assessment
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=g7HDe-gl_VM

# Audit logs are generated by most computer systems. Which of the following is the most important reason to implement logging?
## ID
SOP.146cdc14-e877-4a49-af2c-7d278b96dec3
## A
Mutual Authentication
## B
Individual Accountability
## C
Origin Authentication
## D
Preserve Data Integrity
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=R3JrsyWXrUc

# In addition to IP addresses, DHCP servers provide network nodes with additional useful information such as DNS servers address, default gateway and netbios node type. Which of the following is not a legitimate attack that may be executed against your DHCP deployment? (Choose 3)
## ID
SOP.1e47f560-7d52-42ee-95fb-bb5784399a9d
## A
A rogue DHCP server on your network can offer IP addresses to legitmate users, thereby creating DoS of MitM situation
## B
An attacker may gain control of your DHCP server and reconfigure the options assigned to clients.
## C
Using TCP redirects, an attacker can send client DHCP packets to a remote DHCP server
## D
A rogue DHCP server can be used to reconfigure SMTP connection settings for internal email systems.
## E
An attacker can request multiple IP addresses from the legitimate DHCP server thereby exhausing the IP address pool
## F
An attacker can send negative acknowledgements whenever a client attempts to renew an IP address
##  Answer
C,D,F
## Explanation
As DHCP is not TCP based (rather UDP) option C is not a concern.
DHCP doesnt relate to SMTP
It would be necessary to be on the network link to spoof an acknowlegement.
## Goal Implication
## Source
https://www.youtube.com/watch?v=FpOLERNDFMM

# A new admin in your enterprise  has asked you about an entry she discovered in an /etc/hosts file on a users ubuntu workstation.  The entry read: 'evilapp.evilsite.com: 127.0.0.1' Which of the following is the BEST response to the admin?
## ID
SOP.9e0e361c-c974-4e01-a5ce-1279da996a67
## A
The entry is null route to the hostname specified
## B
it is a sinkhole entry to protect the the Linux system
## C
The system has been comprised by an attacker
## D
It forces traffic to that site to be route out the interface 127.0.0.1
##  Answer
B
## Explanation
The entry prevents ask to that site.
## Source
https://www.youtube.com/watch?v=jpasqBWKJCg

# You have determined that a user current unknown has been modifying and in some cases deleting files without authorization. Which of the following will not help mitigate the problem. Choose two.
## ID
SOP.636823fd-5712-4388-a771-363eafd8e4ac
## A
Encrypt the files to prevent unauthorized deletion.
## B
Configure user level auditing for files/folders
## C
Digitally sign the files to prevent unauthorized modification.
## D
Set appropriate file/folder permissions in the file system.
## E
Maintain backsup of the data.
##  Answer
A,C
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=l1ZLBwog1xU

# Packet Filtering Firewalls have several limitations that make them less appropriate than more modern solutions when protected internal resources from the internet threats. Which of the following are short comings of packet filtering? (Choose Two)
## ID
SOP.40b0d7db-49a3-4540-af57-353c98f8a574
## A
They control access based on source IP address and cannot verify if the address is being spoofed.
## B
They use reverse path forwarding lookups.
## C
They are stateless
## D
They do not support logging packets that match firewall rules.
## E
They are stateful.
## F
They defend against TCP syn flood attacks which reduces there effective throughtput.
##  Answer
A,C
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=jP9hzYCbN5I


# Which of the following are UDP based? (Choose 6)
## ID
SOP.d2d2d072-d5a0-440a-834e-78756b6c9184
## A
BGP
## B
RADIUS
## C
SMB
## D
IMAP4
## E
SNMP,
## F
NTP
## G
TFTP
## H
DNS
## I
SMTP
## J
DHCP
##  Answer
B,E,F,G,H,J
## Explanation
RADIUS ports 1812 and 1813
IMAP4 port 143 or 993(SSL)
SNMP port 162
NTP port 123
DNS port 53
TFTP port 69  (trival file transfer protocol)
## Goal Implication
## Source
https://www.youtube.com/watch?v=jP9hzYCbN5I


# Which of the following is not a component of a configuration management system?
## ID
SOP.3a546dca-ae00-4f0a-aa87-3fc474ac64f9
## A
Version Control Mechanism
## B
System Threat Modeling
## C
Automated Deployment Scripts
## D
Change history logging
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.youtube.com/watch?v=aLfDayIwZnQ&list=PLBpnwlO9U5unYmbZp2DJETNOHg8s_yW37&index=23

# You are evaluating the merits of differential and incremental backup strategies.  Which of the following is true?
## ID
SOP.3e2ec968-351f-4fbd-a258-a9e6f949c4a6
## A
Differential begin with a full back and incrementals do not
## B
Incremental backups do not evalate the archive bit when determining if a file should be backed up
## C
Differentials backups only backup files modified since the previous differenetial of full backup
## D
Compared to differentials, a complete restore will take longer if using an incremental strategy
## E
A 'copy' backup cannot be used if using a differential backup strategy
##  Answer
D
## Explanation
Differentials back up files since last full or last incremental backup.

Incremental backups require full back and all incremental backups
## Goal Implication
## Source
https://www.youtube.com/watch?v=Vq1ECMX-iG4

# Which of the following best defines a Recover Point Objective?
## ID
SOP.8b1dc255-f09c-42cb-91f9-3681bf4bdd7c
## A
Maximum amount of time a business process can be unavailable
## B
Maximum amount of time to recover a business process
## C
The amount of time needed to verify a system/data after recovery
## D
Measure of time indicating the maximum amount of data that can be lost
##  Answer
D
## Explanation
MTD - (Maximum Tolerable Downtime) - Maximum amount of time a business process can be unavailable
RTO - Maximum amount of time to recover a business process
WTR - (Work Receovery Time) - The amount of time needed to verify a system/data after recovery
## Goal Implication
## Source
https://www.youtube.com/watch?v=stf9UlkYYn0

# What type of investigation would typically be launched in response to a report of high network latency?
## ID
SOP.2fd0039e-5158-46ba-be95-802cb7cc70f1
## A
Civil
## B
Operational
## C
Regulatory
## D
Criminal
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4594149?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Server logs are an example of _____ evidence.
## ID
SOP.96644989-2185-4805-a905-2245e27ebe5c
## A
Testimonial
## B
Documentary
## C
Expert Opinion
## D
Real
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4594149?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Which evidence source should be collected first when considering the order of volatility?
## ID
SOP.5dad8d56-1dec-48cc-b8e2-eb6d83296b85
## A
Process Information
## B
Logs
## C
Memory Contents
## D
Temporary Files
##  Answer
C
## Explanation
## Goal Implication
## Source

# What type of technology prevents a forensic examiner from accidentally corrupting evidence while creating an image of a disk?
## ID
SOP.af4f8e91-3de9-4cec-aa0c-cbcabea7e624
## A
hashing
## B
evidence log
## C
sealed container
## D
write blocker
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4594149?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Three of these choices are data elements found in NetFlow data. Which is not?
## ID
SOP.42671402-184d-4df4-a367-c268184a5df9
## A
Packet Contents
## B
Destination Address
## C
Source Address
## D
Amount of Data Transfered
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4594149?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# Federal law requires U.S. businesses to report verified security incidents to US-CERT.
## ID
SOP.2993d07c-eb9a-4ffc-af14-8aa0b055d57c
## A
True
## B
False
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4594149?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# During what phase of ediscovery does an organization share information with the other side?
## ID
SOP.8877bf13-3d52-43b6-abd1-aca09eaf87fe
## A
Collection
## B
Analysis
## C
Production
## D
Preservation
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4594149?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# During what phase of continuous security monitoring does the organization define metrics?
## ID
SOP.b0b02852-24ed-4cd7-8942-69c169f48cb7
## A
Analyze/Report
## B
Establish
## C
Implement
## D
Define
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4595102?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What DLP technique tags sensitive content and then watches for those tags in data leaving the organization?
## ID
SOP.f0a0275e-3b63-4ecb-9912-181365697090
## A
Host Base DLP
## B
watermarking
## C
intrusion detection
## D
Pattern recogniation
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4595102?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What type of hypervisor runs directly on top of bare hardware?
## ID
SOP.c6465ae8-e1bf-482d-ad2d-d23aaeadcc4b
## A
Type 4
## B
Type 1
## C
Type 2
## D
Type 3
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4592711?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What additional patching requirements apply only in a virtualized environment?
## ID
SOP.6bffd6bc-ccf6-4d3b-9844-7c6afd052ca6
## A
Patching the hypervisor
## B
Patching Firewalls
## C
Patching Apps
## D
Patching the OS
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4592711?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Which public cloud computing tier places the most security responsibility on the vendor?
## ID
SOP.385523fb-1054-43b9-8a87-c0cfa21d1bfc
## A
IaaS
## B
IDaaS
## C
SaaS
## D
PaaS
##  Answer
C
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4592711?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675


# What security principle most directly applies to limiting information access?
## ID
SOP.364679fc-a38a-458e-88bf-59b2d361b8d7
## A
Two person control
## B
separation of duties
## C
least privilege
## D
need to known
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4594150?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# What security principle requires two individuals to perform a sensitive action?
## ID
SOP.0ca091d6-bcf5-4cfa-9a3b-36d171b6495f
## A
2 person control
## B
separation of duties
## C
least privilege
## D
need to know
##  Answer
A
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4594150?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Which one of the following individuals would not normally be found on the incident response team?
## ID
SOP.e0d10f59-044d-4d65-a36e-120ca32df26a
## A
Legal Counsel
## B
Human Resource Staff
## C
Information security professional
## D
CEO
##  Answer
D
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4595103?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# During an incident response, what is the highest priority of first responders?
## ID
SOP.2f0bac6f-a8d6-4a2a-9014-8a85f7f56f6a
## A
Identifying Root Cause
## B
Containing the damage
## C
Collecting Evidence
## D
Restoring operations
##  Answer
B
## Explanation
## Goal Implication
## Source
https://www.linkedin.com/learning/cissp-cert-prep-7-security-operations-2018/quiz/urn:li:learningApiAssessment:4595103?contextUrn=urn%3Ali%3AlyndaLearningPath%3A59276e00498e600cecac734b&u=25288675

# Which one of the following tools is used primarily to perform network discovery scans?
## ID
SAT.84238ca0-19f9-49fb-adc2-da62d00c76fc
## A
Nmap
## B
Nessus
## C
Metasploit
## D
lsof
##  Answer
A
## Explanation
## Goal Implication
## Source
Chp 15 Review Question 1
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 


# Adam recently ran a network port scan of a web server running in his organization. He ran the scan from an external network to get an attacker’s perspective on the scan. Which one of the following results is the greatest cause for alarm?
## ID
SAT.7ea1883d-05a4-4d7e-a6d7-d37eadd8b069
## A
80/filtered
## B
22/filtered
## C
433/open
## D
1433/open
##  Answer
D
## Explanation
Only open ports represent potentially significant security risks. Ports 80 and 443 are expected to be open on a web server. Port 1433 is a database port and should never be exposed to an external network.
## Goal Implication
## Source
Chp 15 Review Question 2
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 

# Which one of the following factors should not be taken into consideration when planning a security testing schedule for a particular system?
## ID
SAT.114ed533-cc87-491d-aa36-000d5c3f4dab
## A
Sensitivity of the information stored on the system
## B
Difficulty of performing the test
## C
Desire to experiment with new testing tools
## D
Desirability of the system to attackers
##  Answer
C
## Explanation
The sensitivity of information stored on the system, difficulty of performing the test, and likelihood of an attacker targeting the system are all valid considerations when planning a security testing schedule. The desire to experiment with new testing tools should not influence the production testing schedule.
## Goal Implication
## Source
Chp 15 Review Question 3
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 

# Which one of the following is not normally included in a security assessment?
## ID
SAT.3471e7d9-8b8e-48cd-8ab1-aed94e2b003f
## A
Vulnerability Scan
## B
Risk assessment
## C
Mitigation Vulnerabilities
## D
Threat assessment
##  Answer
C
## Explanation
C. Security assessments include many types of tests designed to identify vulnerabilities, and the assessment report normally includes recommendations for mitigation. The assessment does not, however, include actual mitigation of those vulnerabilities.
## Goal Implication
## Source
Chp 15 Review Question 4
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 

# Who is the intended audience for a security assessment report?
## ID
SAT.e7317cc1-68b2-4199-a532-64bbff150c0d
## A
Management
## B
Security Auditor
## C
Security Professional
## D
Customers
##  Answer
A
## Explanation
Security assessment reports should be addressed to the organization’s management. For this reason, they should be written in plain English and avoid technical jargon.
## Goal Implication
## Source
Chp 15 Review Question 5
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 

# Beth would like to run an nmap scan against all of the systems on her organization’s private network. These include systems in the 10.0.0.0 private address space. She would like to scan this entire private address space because she is not certain what subnets are used. What network address should Beth specify as the target of her scan?
## ID
SAT.aa0ddee5-f507-4c98-be38-5fef27e7e05d
## A
10.0.0.0/0
## B
10.0.0.0/8
## C
10.0.0.0/16
## D
10.0.0.0/24
##  Answer
B
## Explanation
The use of an 8-bit subnet mask means that the first octet of the IP address represents the network address. In this case, that means 10.0.0.0/8 will scan any IP address beginning with 10.
## Goal Implication
## Source
Chp 15 Review Question 6
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 


# Alan ran an nmap scan against a server and determined that port 80 is open on the server. What tool would likely provide him the best additional information about the server’s purpose and the identity of the server’s operator?
## ID
SAT.ccbcfaea-b581-4729-b151-e072fa8064ea
## A
SSH
## B
Web Browser
## C
telnet
## D
ping
##  Answer
B
## Explanation
The server is likely running a website on port 80. Using a web browser to access the site may provide important information about the site’s purpose.
## Goal Implication
## Source
Chp 15 Review Question 7
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 

# What port is typically used to accept administrative connections using the SSH utility?
## ID
SAT.f73b8168-e059-40fb-9c90-21dbe7982e1c
## A
20
## B
22
## C
25
## D
80
##  Answer
B
## Explanation
## Goal Implication
## Source
Chp 15 Review Question 8
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 

# Which one of the following tests provides the most accurate and detailed information about the security state of a server?
## ID
SAT.61a46f1d-286d-4dba-8d71-398e9fafcd69
## A
Unauthenticated scan
## B
Port scan
## C
Half-open scan
## D
Authenticated scan
##  Answer
D
## Explanation
Authenticated scans can read configuration information from the target system and reduce the instances of false positive and false negative reports.
## Goal Implication
## Source
Chp 15 Review Question 9
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 

# What type of network discovery scan only follows the first two steps of the TCP handshake?
## ID
SAT.6a97e3f9-f4a9-4b0f-9f4f-5ba6cc71d95b
## A
TCP connection scan
## B
Xmas scan
## C
TCP Syn Scan
## D
TCP Ack San
##  Answer
C
## Explanation
The TCP SYN scan sends a SYN packet and receives a SYN ACK packet in response, but it does not send the final ACK required to complete the three-way handshake.
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 974). Wiley. Kindle Edition. 
## Goal Implication
## Source
Chp 15 Review Question 10
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 



# Matthew would like to test systems on his network for SQL injection vulnerabilities. Which one of the following tools would be best suited to this task?
## ID
SAT.c16f7cdf-a681-48ca-9648-4fbf0cc068a7
## A
Port Scanner
## B
Network Vulnerablity Scanner
## C
Network discovery scanner
## D
Web vulnerablity scanner
##  Answer
D
## Explanation
SQL injection attacks are web vulnerabilities, and Matthew would be best served by a web vulnerability scanner. A network vulnerability scanner might also pick up this vulnerability, but the web vulnerability scanner is specifically designed for the task and more likely to be successful.
## Goal Implication
## Source
Chp 15 Review Question 11
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 

# Badin Industries runs a web application that processes e-commerce orders and handles credit card transactions. As such, it is subject to the Payment Card Industry Data Security Standard (PCI DSS). The company recently performed a web vulnerability scan of the application and it had no unsatisfactory findings. How often must Badin rescan the application?
## ID
SAT.21ab0d2d-b99b-4b5e-9d57-2f8e7e556ab7
## A
Only if the appication changes
## B
At least monthly
## C
At least annually
## D
There is no rescanning requirement
##  Answer
C
## Explanation
PCI DSS requires that Badin rescan the application at least annually and after any change in the application.
## Goal Implication
## Source
Chp 15 Review Question 12
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 


# Grace is performing a penetration test against a client’s network and would like to use a tool to assist in automatically executing common exploits. Which one of the following security tools will best meet her needs?
## ID
SAT.03364d32-b755-42f9-a74c-5db90d774e5e
## A
nnap
## B
metasploit
## C
nessus
## D
snort
##  Answer
B
## Explanation
## Goal Implication
Metasploit is an automated exploit tool that allows attackers to easily execute common attack techniques.
## Source
Chp 15 Review Question 13
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 

# Paul would like to test his application against slightly modified versions of previously used input. What type of test does Paul intend to perform? Code review
## ID
SAT.01b1e58a-4bd5-401b-aa4e-f3c13310d709
## A
Code Review
## B
Application Vulnerability Review
## C
Mutation Fuzzing
## D
Generational Fuzzing
##  Answer
C
## Explanation
Mutation fuzzing uses bit flipping and other techniques to slightly modify previous inputs to a program in an attempt to detect software flaws.
## Goal Implication
## Source
Chp 15 Review Question 14
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 


# Users of a banking application may try to withdraw funds that don’t exist from their account. Developers are aware of this threat and implemented code to protect against it. What type of software testing would most likely catch this type of vulnerability if the developers have not already remediated it?
## ID
SAT.e43305b3-c5f3-4bbd-9a28-4430bd31496b
## A
Misuse case testing
## B
user interface testing
## C
physical interface testing
## D
security inteface testing
##  Answer
A
## Explanation
Misuse case testing identifies known ways that an attacker might exploit a system and tests explicitly to see if those attacks are possible in the proposed code.
## Goal Implication
## Source
Chp 15 Review Question 15
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 


# What type of interface testing would identify flaws in a program’s command-line interface?
## ID
SAT.30bb57df-122c-4c2d-b9da-2050bd254831
## A
Application Programming Interface Testing
## B
User interface testing
## C
Physical Interface Testing
## D
Security Interface Testing
##  Answer
B
## Explanation
User interface testing includes assessments of both graphical user interfaces (GUIs) and command-line interfaces (CLIs) for a software program.
## Goal Implication
## Source
Chp 15 Review Question 16
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 

# During what type of penetration test does the tester always have access to system configuration information?
## ID
SAT.6095e0f2-9330-465b-8784-87654cdfcdb3
## A
Black Box
## B
White Box
## C
Gray Box
## D
Red Box
##  Answer
B
## Explanation
## Goal Implication
## Source
Chp 15 Review Question 17
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 


# Which one of the following is the final step of the Fagan inspection process?
## ID
SAT.ce73bd95-5fd9-4064-97c7-e622a5f5cd46
## A
Inspection
## B
Rework
## C
Follow-Up
## D
None of the above
##  Answer
C
## Explanation
## Goal Implication
## Source
Chp 15 Review Question 19
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 


# What information security management task ensures that the organization’s data protection requirements are met effectively?
## ID
SAT.1751890d-9647-4ee7-9706-576d6f0669f6
## A
Account Management
## B
Backup Verification
## C
Log Review
## D
Key Performance Indictors
##  Answer
B
## Explanation
The backup verification process ensures that backups are running properly and thus meeting the organization’s data protection objectives.
## Goal Implication
## Source
Chp 15 Review Question 20
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 

# What is the most commonly used technique to protect against virus attacks?

## ID
SOP.4f2c5d0f-ebfc-44a0-a264-ffcad16bc463
## A
Signature Detection
## B
Heuristic Detection
## C
Data integrity assurance
## D
Automated Reconstruction
##  Answer
A
## Explanation
Signature detection mechanisms use known descriptions of viruses to identify malicious code resident on a system.
## Goal Implication
## Source
Chp 21 Review Question 1
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 


# You are the security administrator for an e-commerce company and are placing a new web server into production. What network zone should you use?
## ID
SOP.c243ab0b-1533-44e1-97bb-36d7467c0444
## A
Internet
## B
DMZ
## C
Intranet
## D
Sandbox
##  Answer
B
## Explanation
## Goal Implication
## Source
Chp 21 Review Question 2
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 


# Which one of the following types of attacks relies on the difference between the timing of two events?
## ID
SOP.3fcff942-b99a-409e-950b-b72be6942100
## A
Smurf
## B
TOCTTOU
## C
Land
## D
Fraggle
##  Answer
B
## Explanation
## Goal Implication
## Source
Chp 21 Review Question 3
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 

# Which one of the following techniques is most closely associated with APT attacks?
## ID
SOP.c0769ce8-16e4-4ffd-9bdc-03796b72e30d
## A
Zero-day exploit
## B
Social Engineering
## C
Trojan Horse
## D
SQL Injection
##  Answer
A
## Explanation
While an advanced persistent threat (APT) may leverage any of these attacks, they are most closely associated with zero-day attacks.
## Goal Implication
## Source
Chp 21 Review Question 4
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 


# What advanced virus technique modifies the malicious code of a virus on each system it infects?
## ID
SOP.19d1f13f-d5f2-40a5-a9e6-af3e49667c5f
## A
Polymorphism
## B
Stealth
## C
Encryption
## D
Multipartitism
##  Answer
A
## Explanation
In an attempt to avoid detection by signature-based antivirus software packages, polymorphic viruses modify their own code each time they infect a system.
## Goal Implication
## Source
Chp 21 Review Question 5
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 

# Which one of the following tools provides a solution to the problem of users forgetting complex passwords?
## ID
SOP.158ee2fe-1418-4682-ad69-01141fa18d95
## A
LastPass
## B
Crack
## C
Shadow password files
## D
Tripwire
##  Answer
A
## Explanation
LastPass is a tool that allows users to create unique, strong passwords for each service they use without the burden of memorizing them all.
## Goal Implication
## Source
Chp 21 Review Question 6
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 

# What type of application vulnerability most directly allows an attacker to modify the contents of a system’s memory?
## ID
SOP.3aebeb50-1594-44b7-8432-c44833a4d67a
## A
Rootkit
## B
Back door
## C
TOCTOU
## D
Buffer Overflow
##  Answer
D
## Explanation
Buffer overflow attacks allow an attacker to modify the contents of a system’s memory by writing beyond the space allocated for a variable.
## Goal Implication
## Source
Chp 21 Review Question 7
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 


# What technique may be used to limit the effectiveness of rainbow table attacks?
## ID
SOP.11b4470a-bbd9-4143-ae1c-f3386b956cab
## A
Hashing
## B
Salting
## C
Digital Signature
## D
Transport Encryption
##  Answer
B
## Explanation
Salting passwords adds a random value to the password prior to hashing, making it impractical to construct a rainbow table of all possible values.
## Goal Implication
## Source
Chp 21 Review Question 8
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 


# What character should always be treated carefully when encountered as user input on a web form?
## ID
SOP.9fe60b3a-6f02-4b31-983a-3b7a1d639658
## A
Exclamation
## B
Ampersand
## C
Quote
## D
Apostrophy
##  Answer
D
## Explanation
The single quote character (') is used in SQL queries and must be handled carefully on web forms to protect against SQL injection attacks.
## Goal Implication
## Source
Chp 21 Review Question 10
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 


# What database technology, if implemented for web forms, can limit the potential for SQL injection attacks?
## ID
SOP.f16fc34a-034b-4c2b-a55f-0021aa908c04
## A
Triggers
## B
Stored Procedure
## C
Column Encryption
## D
Concurrency Control
##  Answer
B
## Explanation
Developers of web applications should leverage database stored procedures to limit the application’s ability to execute arbitrary code. With stored procedures, the SQL statement resides on the database server and may only be modified by database administrators.
## Goal Implication
## Source
Chp 21 Review Question 11
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 




# What condition is necessary on a web page for it to be used in a cross-site scripting attack?
## ID
SOP.5241b832-13e2-417b-ae1e-f9adef03c0e1
## A
Reflected Input
## B
Database driven content
## C
.Net Technology
## D
CGI Scripts
##  Answer
A
## Explanation
## Goal Implication
## Source
Chp 21 Review Question 13
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition. 


# What type of virus utilizes more than one propagation technique to maximize the number of penetrated systems?
## ID
SOP.73d05727-bc7c-4609-a87c-1b7a3ec9b29e
## A
Stealth Virus
## B
Companion Virus
## C
Polymorhic Virus
## D
Multipartite virus
##  Answer
D
## Explanation
Multipartite viruses use two or more propagation techniques (for example, file infection and boot sector infection) to maximize their reach.
## Goal Implication
## Source
Chp 21 Review Question 14
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 911). Wiley. Kindle Edition. 

# What worm was the first to cause major physical damage to a facility?
## ID
SOP.d680d9d2-403b-495c-8656-4db8b8bdbe75
## A
Stuxnet
## B
Code Red
## C
Melissa
## D
RTM
##  Answer
A
## Explanation
Stuxnet was a highly sophisticated worm designed to destroy nuclear enrichment centrifuges attached to Siemens controllers.
## Goal Implication
## Source
Chp 21 Review Question 16
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 

# Ben’s system was infected by malicious code that modified the operating system to allow the malicious code author to gain access to his files. What type of exploit did this attacker engage in?
## ID
SOP.f7042693-a796-4881-aba4-32b72fad9dd0
## A
Escalation of Privilege
## B
Back door
## C
Root kit
## D
Buffer overflow
##  Answer
B
## Explanation
## Goal Implication
## Source
Chp 21 Review Question 17
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 


# When designing firewall rules to prevent IP spoofing, which of the following principles should you follow?
## ID
SOP.3225644f-3cf8-47a4-b102-e2186eea5b37
## A
Packets with internal source IP addresses don’t enter the network from the outside.
## B
Packets with internal source IP addresses don’t exit the network from the inside.
## C
Packets with public IP addresses don’t pass through the router in either direction.
## D
Packets with external source IP addresses don’t enter the network from the outside.
##  Answer
A
## Explanation
Packets with internal source IP addresses should not be allowed to enter the network from the outside because they are likely spoofed.
## Goal Implication
## Source
Chp 21 Review Question 20
Chapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 693). Wiley. Kindle Edition. 