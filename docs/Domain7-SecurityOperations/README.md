
- Security Incident Response
- The Forensics Investigation Process Part 1
- The Forensic Investigation Process Part 2
- Evidence Types
- Fault Management
- Backups

Collect Evidence
- Locards Principle - WHen there is a crime the criminal will leave something behind and take something with them.
- MOM - motive opportunity and means
- Chain of Custody

Evidence
- Real
- Direct
    - Provided by First Hand 
 - Parol Evidence - anything in written form means there is no other verbal agreement
 - Best Evidence - when a document is produced it is the original document.

 Admissibility of Evidence
 - Relevant - relevant to determining a fact
 - Material - related to the case
 - Competent - obtained legally

Rules of Evidence [Reference](https://www.sans.org/reading-room/whitepapers/incident/paper/652)
- Admissable - Can be used as evidence
- Authentic - Can be tied to the incident
- Accurate/Believable/Convincing- Evidence must be clear and understandable in making the case
- Complete (including exculpable evidence) - cant pick and choose evidence
- Reliable - Method of obtaining evidence must cast doubt ont the evidence


Types of INvestigations
- Criminal
- Civil
- Regulatory
- Internal

Event - observable occurence
Incident - adverse event

[Incident Response Life Cycle](./IncidentResponse.md)


Configuration Management
- Popular Tools - Ansible, Chef, Puppet


Operational Investigations
- restoring normal operations
- root cause analysis

Conducting Investigations

- Types of Investigations
    - Administrative (resolve problem and get back to work)
    vs
    - Crimimal
        - beyond a reasonable doubt
    vs
    - Civil
        - perpondence of the evidence (more likely than not one party is correct)
        - a dispute between 2 parties

Evidence Types
- Real Evidence - Tangible Objects
- Documentary Evidence - written information

- Evidence Rules
    - Authentication Rule
    - documents must be authenicated by testimony
    - Best Evidence Rule - must provide original 
    - Parol evidence rule - assume the entire agreement is documented

- Testimonial Evidence
    - Witness provideds direct evidence
    - expert opinion
    - hearsay is not allowed in evidence

- Digital Forensics
    - Collect and Preserve evidence
    - investigators must never alter evidence
    - Volatility 
        - Network Traffic
        - Memory
        - Files (temp first)
        - logs
        - archive records
    - Be sure to record time

- System and File Forensics
    - Never work with the original media...alway copy it first
    - Use hash to protect evidence

- Network Forensics
    - Use wireshark for tons of data
    - Netflow - records sessions but not content

- Software Code Forensics
    - used to resolve intellectual property
    - use to determine origins of malware
    
- Embedded Device Forensics

- Chain of Custody
    - both parties have rights to ensure that evidence is not tampered with
    - chain of custody or evidence
        - tracks each time someone touches evidence
    - evidence logs
        - actions
            - initial collection
            - transfer
            - storage
            - opening or resealing of evidence
        - who and when the action occurred

- Notification
    - who to contact in the event of an incident
    - have a centralized place to report incidents
    - Information Sharing and Analysis Centers
    - Formal Incident Response at the end of the response

- Electronic Discovery
    - litigation hold - preservation of evidence - must suspend the automatic deletion of logs

Logging and Monitoring
- SIEM
    - All systems send log entries directly to the SIEM
    - Detects patterns that other system might miss
- Continuous Security Monitoring
    - Maintaining ongoing awareness of information security, vulnerabilities and threats
    - NIST  SP 800-137
        - Maps to risk tolerances
        - Adapts to ongoing needs
        - Actively involves management

    - Analysis
        - Heuristic Analysis - Comparison versus other data
        - Trend Analysis - Changes over time
        - Behavorial Analysis - Detects unusual user activity
        - Availability Analysis - 

Data Loss Prevention -
    - Tech solution to monitor for removal of sensitive information
    - Types
        - Host Base DLP - detecting credit card number and other identification information on a host
            - Can block copying information onto USB
        - Network Based DLP - scan network transmissions
        - Cloud Based 
    - Actions
        - Pattern Matching - formats of special id or data classifications
        - Watermarking - checks for watermarks in documentss

Physical Asset Management
- build asset inventory
- hardware lifecycle
    - when order is place - inventory record is created
    - upon receipt of hardware - update inventory record 
    - provide the equipment to IT staff - who set it up and then pass to employee

Media Management
- can also be tracked the same way as hardware

Change and Configuration Management
- change acheives objectives without interrupting services
- goal of minimizing impact of changes to a business
- RFC - request for change
    - description of change
    - expected impact
    - risk
    - individuals involved
    - schedule for change
- Baseline - configuration snapshot
- Versioning -  MajorVersion.MajorUpdate.MinUpdate

Virtualization
- Host machine - hosts many guest machines
- Hypervisor
    - Type 1 - hypervisor runs on hardware
    - Type 2 - hypervisor a program on host os
- VM Escape attack - breaking out of VM

Cloud Computing
    -Benefits
        - Flexibility
        - Scalability
        - Agility
        - Cost Effectiveness
    - Models
        - Private Cloud 
        - Public Cloud
        - Hybrid Cloud
    - Security in a public cloud is based on shared responsibility model
        - Provider is response for encryption and implement your ACLs
        - Our responsibility is to set up ACLs correctly
        - IAAS - 
            Vendor - hardware, data center
        - PAAS 
            Vendor - hardware, data center and OS
        - SAAS
            Vendor - hardware, data center, OS and application


Security Principles
- Need to know - limits information access
- Least Privelege - assign minimum privileges to carry out system permissions
    - Can be cumbersome to implement in practice
    - Privilege Aggregation -
- Separation of Duties - prevent actions of a single rogue individual
    - 2 separate people are required to acheive a sensitive operation
    - developer should not put their own code into production
    - 2 person control - 2 authorizations required to acheive a single action
        - 2 signatures on a check above a certain amount

Password Vaults 
    - no one knows the password - the password value logs into the system
    - command proxy
    - credential management

Incident Response Program
- NIST - Computer Security Incident Handling Guide
- Program should involve
    - Policy
        - lays out powers of the incident response team
        - general in nature
    - Procedures for incident handling
        - notification
        - escalation
        - reporting
        - system isolation
        - foresics
    - Guidelines for communicating externally
    - Structure and Staffing Model
    - Description of relationships with other groups
- Incident Data Sources
    - IDS
    - Firewalls
    - Vulnerability Scanners
    - AntiMalware Packages
    - SIEM
    - External Channels

- Incident Stages
    - First Responder
        - Responsible to contain the damage by isolate affected systems - Quarantine
    - Escalation and Notification Process
        - evaluate incident severity - triaging
            - low,medium,high
    - Incident Mitigation
        - controlling damaging and loss to the organization through containment
    - Recovery and Reconstitution
        - Recover, Rebuild, Restore
        - Correct vulnerabilities
    - Lessons Learned



