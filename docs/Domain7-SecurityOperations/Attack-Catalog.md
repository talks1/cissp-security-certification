
# Heartbleed 
Vulnerability in SSL

# Fraggle
A Fraggle Attack is a denial-of-service (DoS) attack that involves sending a large amount of spoofed UDP traffic to a router's broadcast address within a network.

# DNS Amplification

Send a query to DNS server with a different IP response address.  The DNS server responds to the request which is larger than the query. This can overwhelm the network.

# LAND - Local Area Network Denial
A land attack is a remote denial-of-service (DOS) attack caused by sending a packet to a machine with the source host/port the same as the destination host/port.

# TearDrop

A teardrop attack is a denial-of-service (DoS) attack that involves sending fragmented packets to a target machine. Since the machine receiving such packets cannot reassemble them due to a bug in TCP/IP fragmentation reassembly, the packets overlap one another, crashing the target network device.

# MitM

Man in the middle attach