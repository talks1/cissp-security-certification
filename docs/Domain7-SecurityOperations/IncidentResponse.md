

## Four Phases of Incident Response

- Preparation
- Detection
- Action
    - Response
    - Mitigation
    - Reporting
- Recovery
    - Recovery
    - Remediation
        - Root Cause Analysis
    - Lessons Learned


## Incident Response cycle
Prepare -> Detection -> Response -> Mitigation -> Report -> Recovery -> Remediate -> Lessons Learned

    - Response - effect response steam
    - Remediation - where root cause analysis occurs
